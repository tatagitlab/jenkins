#ifndef ALARMREADER_H
#define ALARMREADER_H

#include <QObject>
#include <QQmlContext>
#include <QDebug>
#include <qstring.h>
#include <QDomDocument>
#include <QException>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include "../Source/AlarmArea/Include/alarmdatamodel.h"

class AlarmReader : public QObject
{
    Q_OBJECT
public:
    AlarmReader(bool errorStatus = false);

    Q_INVOKABLE QList<QString> getMasterList();

    void updateLowPriorityList(int iAlarmId) ;
    void updateMediumPriorityList(int iAlarmId, QString strTimeStamp) ;
    void updateHighPriorityList(int iAlarmId, QString strTimeStamp) ;
    void updateMasterList();
    void deleteFromLowPriorityList(int iAlarmId) ;
    void deleteFromMediumPriorityList(int iAlarmId) ;
    void deleteFromHighPriorityList(int iAlarmId) ;

    AlarmLogDataModel* getList();
signals:
    void alarmMasterListUpdated();

private:
    QStringList m_highAlarmList;
    QStringList m_mediumAlarmList;
    QStringList m_lowAlarmList;
    QStringList m_masterAlarmList;

    QList<QString> m_highPriorityList;
    QList<QString> m_mediumPriorityList;
    QList<QString> m_lowPriorityList;
    QList<QString> m_masterList;

    AlarmLogDataModel m_AlarmLogDataModel;

    QString m_filename;
    QFile m_file;
    bool m_errorStat ;
    QString m_errorStr;

};

#endif // ALARMREADER_H
