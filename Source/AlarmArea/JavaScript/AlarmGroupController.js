/********************************************************************
*    GE Healthcare
*    JFWTC
*    Bangalore, KA-560066
*
*    Copyright 2019, GE Healthcare.
*    CONFIDENTIAL & PROPRIETARY - PORTIONS OF THIS
*    SOURCE CODE CONTAIN VALUABLE TRADE SECRET
*    INFORMATION THAT IS THE PROPERTY
*    OF GE Healthcare.
*
*    $Archive::  https://gitlab-gxp.cloud.health.ge.com/arc-sw-dev/Subsystems/SMB/Jarvis_SMB_GUI
*    $Workfile:: AlarmGroupController.js
*    $Author::  GE Healthcare
*
*    Purpose:  This javascript file is used for the creation and destruction of AlarmTroubleShoot PopUp and
*              has functions that deals with AlarmMessage display on AlarmArea.
********************************************************************/

var audioPauseComp;
var alarmComp;
var alarmTroubleShootComp;
var sysTimeComp ;


var audioPauseCreator;
var alarmCompCreator1;
var alarmCompCreator2;
var alarmCompCreator3;
var alarmCompCreator4;
var sysTimeCreator;
var alarmTroubleShootCreator;

var lMasterList = [] ;
var lTempMasterList = [] ;
var lMasterSubList = [] ;
var slotFourCounter = -1;

var InstData1 = physicalparam.getAlarmInstructionData("AlarmMsg","HA_32")
var InstData2 = ["Calibrate the flow Sensors.", "Inspect the check valves."]
var alarmMsgText1
var alarmMsgText2


//For AudioPause Area
var audioPauseBgColor = component.getComponentXmlDataByTag("Component","AlarmMessageArea","AlarmBgColor")
var audioPauseAreaWidth = component.getComponentXmlDataByTag("Component","AudioPause","AudioPauseAreaWidth")
var audioPauseAreaHeight = component.getComponentXmlDataByTag("Component","AudioPause","AudioPauseAreaHeight")
var audioPauseIconWidth = component.getComponentXmlDataByTag("Component","AudioPause","AudioPauseIconWidth")
var audioPauseIconHeight =  component.getComponentXmlDataByTag("Component","AudioPause","AudioPauseIconHeight")
var audioPausePixelSize =  component.getComponentXmlDataByTag("Component","AudioPause","AudioPausePixelSize")
var audioPauseFontColor  = component.getComponentXmlDataByTag("Component","AudioPause","AudioPauseFontColor")
var audioPauseIconSource = component.getComponentXmlDataByTag("Component","AudioPause","AudioPauseIconSource")

//For AlarmMessage Area
var alarmBgColor = component.getComponentXmlDataByTag("Component","AlarmMessageArea","AlarmBgColor")
var alarmMsgWidth = component.getComponentXmlDataByTag("Component","AlarmMessageArea","AlarmMsgWidth")
var alarmMsgHeight = component.getComponentXmlDataByTag("Component","AlarmMessageArea","AlarmMsgHeight")
var alarmMsgRadius =  component.getComponentXmlDataByTag("Component","AlarmMessageArea","AlarmMsgRadius")
var alarmMsgMargin =  component.getComponentXmlDataByTag("Component","AlarmMessageArea","AlarmMsgMargin")
var highPriPixelSize  = component.getComponentXmlDataByTag("Component","AlarmMessageArea","HighPriPixelSize")
var highPriFontColor = component.getComponentXmlDataByTag("Component","AlarmMessageArea","HighPriFontColor")
var highPriBgColor = component.getComponentXmlDataByTag("Component","AlarmMessageArea","HighPriBgColor")
var medPriPixelSize = component.getComponentXmlDataByTag("Component","AlarmMessageArea","MedPriPixelSize")
var medPriFontColor = component.getComponentXmlDataByTag("Component","AlarmMessageArea","MedPriFontColor")
var medPriBgColor = component.getComponentXmlDataByTag("Component","AlarmMessageArea","MedPriBgColor")
var lowPriPixelSize = component.getComponentXmlDataByTag("Component","AlarmMessageArea","LowPriPixelSize")
var lowPriFontColor = component.getComponentXmlDataByTag("Component","AlarmMessageArea","LowPriFontColor")

//For Alarm TroubleShoot
var alarmTroubleShootPopUpHeight = parseInt(component.getComponentDimension("AlarmTroubleShoot", "height"))
var alarmTroubleShootPopUpWidth = parseInt(component.getComponentDimension("AlarmTroubleShoot", "width"))
var alarmTroubleShootAreaColor = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootAreaColor")
var alarmTroubleShootTitleFontSize = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootTitleFontSize")
var alarmTroubleShootIcnWidth = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootIcnWidth")
var alarmTroubleShootIcnHeight =  component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootIcnHeight")
var alarmTroubleShootFontColor =  component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootFontColor")
var alarmTroubleShootInstTextFontSize = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootInstTextFontSize")
var alarmTroubleShootInstTextFontColor = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootInstTextFontColor")
var alarmTroubleShootBorderWidth = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootBorderWidth")
var alarmTroubleShootBorderColor = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootBorderColor")
var alarmTroubleShootBorderRadius = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootBorderRadius")
var alarmTroubleShootCloseBtnSource = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootCloseBtnSource")
var alarmTroubleShootCloseBtnHeight = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootCloseBtnHeight")
var alarmTroubleShootCloseBtnWidth = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootCloseBtnWidth")
var alarmTroubleShootInfoIconSource = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootInfoIconSource")
var alarmTroubleShootTitleLeftMargin = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootTitleLeftMargin")
var alarmTroubleShootTitleTopMargin = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootTitleTopMargin")
var alarmTroubleShootCloseBtnBgColor = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootCloseBtnBgColor")
var alarmTroubleShootCloseBtnBorderColor = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootCloseBtnBorderColor")
var alarmTroubleShootCloseBtnBorderWidth = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootCloseBtnBorderWidth")
var alarmTroubleShootCloseBtnTopMargin = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootCloseBtnTopMargin")
var alarmTroubleShootCloseBtnRightMargin = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootCloseBtnRightMargin")
var alarmTroubleShootInfoIconTopMargin = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootInfoIconTopMargin")
var alarmTroubleShootInfoIconLeftMargin = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootInfoIconLeftMargin")
var alarmTroubleShootInstTopMargin = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootInstTopMargin")
var alarmTroubleShootInstLeftMargin = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootInstLeftMargin")
var alarmTroubleShootOpacityVal = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootOpacityVal")
var alarmTroubleShootPopupTopMargin = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootPopupTopMargin")
var alarmTroubleShootTimerInterval = component.getComponentXmlDataByTag("Component","AlarmTroubleShoot","AlarmTroubleShootTimerInterval")

//For SystemIcon Area
var sysIconBgColor = component.getComponentXmlDataByTag("Component","AlarmMessageArea","AlarmBgColor")
var sysIconAreaWidth = component.getComponentXmlDataByTag("Component","SystemIcons","SysIconAreaWidth")
var sysIconAreaHeight = component.getComponentXmlDataByTag("Component","SystemIcons","SysIconAreaHeight")
var sysTimePixelSize = component.getComponentXmlDataByTag("Component","SystemIcons","SysTimePixelSize")
var sysTimeFontColor =  component.getComponentXmlDataByTag("Component","SystemIcons","SysTimeFontColor")
var sysTimeTopMargin =  component.getComponentXmlDataByTag("Component","SystemIcons","SysTimeTopMargin")
var batteryIconWidth  = component.getComponentXmlDataByTag("Component","SystemIcons","BatteryIconWidth")
var batteryIconHeight = component.getComponentXmlDataByTag("Component","SystemIcons","BatteryIconHeight")
var batteryIconSource = component.getComponentXmlDataByTag("Component","SystemIcons","BatteryIconSource")

var componentsInAlarmGroup=["audioPauseButton","alarmButtonOne","alarmButtonTwo","alarmButtonThree","alarmButtonFour","SysIconButton"];


function createComponent()
{
    audioPauseComp = Qt.createComponent("qrc:/Source/AlarmArea/Qml/AudioPause.qml");
    alarmComp = Qt.createComponent("qrc:/Source/AlarmArea/Qml/AlarmMessage.qml");
    sysTimeComp = Qt.createComponent("qrc:/Source/AlarmArea/Qml/SystemIcons.qml");

    if(alarmComp.status === Component.Ready && audioPauseComp.status === Component.Ready
            && sysTimeComp.status === Component.Ready )
    {
        finishComponentCreation();
    }
    else
    {
        audioPauseComp.statusChanged.connect(finishComponentCreation);
        alarmComp.statusChanged.connect(finishComponentCreation);
        sysTimeComp.statusChanged.connect(finishComponentCreation);
    }
}


function finishComponentCreation()
{
    audioPauseCreator=audioPauseComp.createObject(alarmGrpId,{"iCompID":0,
                                                      "strAlarmColor" : audioPauseBgColor,
                                                      "iAudioPauseAreaWidth" : audioPauseAreaWidth,
                                                      "iAudioPauseAreaHeight" : audioPauseAreaHeight,
                                                      "iAudioPauseIconWidth" : audioPauseIconWidth,
                                                      "iAudioPauseIconHeight" : audioPauseIconHeight,
                                                      "iAudioPausePixelSize" : audioPausePixelSize,
                                                      "strAudioPauseFontColor" : audioPauseFontColor,
                                                      "strAudioPauseIconSource" : audioPauseIconSource
                                                  });

    alarmCompCreator1=alarmComp.createObject(alarmRow,{"iCompID":1,
                                                 "strAlarmMsgColor" : alarmBgColor,
                                                 "strAlarmAreaColor":alarmBgColor,
                                                 "iAlarmWidth" : alarmMsgWidth,
                                                 "iAlarmHeight" : alarmMsgHeight,
                                                 "iAlarmRadius" : alarmMsgRadius,
                                                 "iAlarmMsgMargin" : alarmMsgMargin,
                                                 "iHighPriPixelSize": highPriPixelSize,
                                                 "strHighPriFontColor" : highPriFontColor,
                                                 "strHighPriBgColor" : highPriBgColor,
                                                 "iMedPriPixelSize" : medPriPixelSize,
                                                 "strMedPriFontColor" : medPriFontColor,
                                                 "strMedPriBgColor" : medPriBgColor,
                                                 "iLowPriPixelSize"  : lowPriPixelSize,
                                                 "strLowPriFontColor"  : lowPriFontColor,
                                                 "strLowPriBgColor":"#00d9ff"
                                             });
    alarmCompCreator2=alarmComp.createObject(alarmRow,{"iCompID":2,
                                                 "strAlarmMsgColor" : alarmBgColor,
                                                 "strAlarmAreaColor":alarmBgColor,
                                                 "iAlarmWidth" : alarmMsgWidth,
                                                 "iAlarmHeight" : alarmMsgHeight,
                                                 "iAlarmRadius" : alarmMsgRadius,
                                                 "iAlarmMsgMargin" : alarmMsgMargin,
                                                 "iHighPriPixelSize": highPriPixelSize,
                                                 "strHighPriFontColor" : highPriFontColor,
                                                 "strHighPriBgColor" : highPriBgColor,
                                                 "iMedPriPixelSize" : medPriPixelSize,
                                                 "strMedPriFontColor" : medPriFontColor,
                                                 "strMedPriBgColor" : medPriBgColor,
                                                 "iLowPriPixelSize"  : lowPriPixelSize,
                                                 "strLowPriFontColor"  : lowPriFontColor,
                                                 "strLowPriBgColor":"#00d9ff"}) ;
    alarmCompCreator3=alarmComp.createObject(alarmRow,{"iCompID":3,
                                                 "strAlarmMsgColor" : alarmBgColor,
                                                 "strAlarmAreaColor":alarmBgColor,
                                                 "iAlarmWidth" : alarmMsgWidth,
                                                 "iAlarmHeight" : alarmMsgHeight,
                                                 "iAlarmRadius" : alarmMsgRadius,
                                                 "iAlarmMsgMargin" : alarmMsgMargin,
                                                 "iHighPriPixelSize": highPriPixelSize,
                                                 "strHighPriFontColor" : highPriFontColor,
                                                 "strHighPriBgColor" : highPriBgColor,
                                                 "iMedPriPixelSize" : medPriPixelSize,
                                                 "strMedPriFontColor" : medPriFontColor,
                                                 "strMedPriBgColor" : medPriBgColor,
                                                 "iLowPriPixelSize"  : lowPriPixelSize,
                                                 "strLowPriFontColor"  : lowPriFontColor,
                                                 "strLowPriBgColor":"#00d9ff"}) ;
    alarmCompCreator4=alarmComp.createObject(alarmRow,{"iCompID":4,
                                                 "strAlarmMsgColor" : alarmBgColor,
                                                 "strAlarmAreaColor":alarmBgColor,
                                                 "iAlarmWidth" : alarmMsgWidth,
                                                 "iAlarmHeight" : alarmMsgHeight,
                                                 "iAlarmRadius" : alarmMsgRadius,
                                                 "iAlarmMsgMargin" : alarmMsgMargin,
                                                 "iHighPriPixelSize": highPriPixelSize,
                                                 "strHighPriFontColor" : highPriFontColor,
                                                 "strHighPriBgColor" : highPriBgColor,
                                                 "iMedPriPixelSize" : medPriPixelSize,
                                                 "strMedPriFontColor" : medPriFontColor,
                                                 "strMedPriBgColor" : medPriBgColor,
                                                 "iLowPriPixelSize"  : lowPriPixelSize,
                                                 "strLowPriFontColor"  : lowPriFontColor,
                                                 "strLowPriBgColor":"#00d9ff"});
    sysTimeCreator=sysTimeComp.createObject(alarmGrpId,{"iCompID":5,
                                                "strAlarmColor" : sysIconBgColor,
                                                "iSysIconAreaWidth" : sysIconAreaWidth,
                                                "iSysIconAreaHeight" : sysIconAreaHeight,
                                                "iSysTimePixelSize" : sysTimePixelSize,
                                                "strSysTimeFontColor" : sysTimeFontColor,
                                                "iSysTimeTopMargin" : sysTimeTopMargin,
                                                "iBatteryIconWidth" : batteryIconWidth,
                                                "iBatteryIconHeight" : batteryIconHeight,
                                                "strBatteryIconSource" : batteryIconSource
                                            });

    //  alarmProcessing();
    alarmCompCreator4.displayNextAlarm.connect(displayAlarmInSlotFour);
    alarmRow.anchors.left = audioPauseCreator.right
}

//*******************************************
// Purpose: Loads AlarmTroubleShoot PopUp component and waits till the component is ready for object creation.
//          Once done calls the object creation function
// Input Parameters: objN
// Description and Use of each parameter:objN - objectName of currently clicked
//                                       AlarmSlot(S1,S2,S3 or S4) in AlarmMessge Area.
// Output Parameters:
// Description and Use of each Parameter:
// Return:
// Description of the return value and type:
//***********************************************/

function createAlarmTroubleShootComponent(objN){
    console.log("createAlarmTroubleShootComponent..")
    alarmTroubleShootComp = Qt.createComponent("qrc:/Source/AlarmArea/Qml/AlarmTroubleShoot.qml");
    console.log("Created ALarmTroubleShoot")
    if(alarmTroubleShootComp.status === Component.Ready )
    {
        finishAlarmTroubleShootComponentCreation(objN);
    }
    else
    {
        alarmTroubleShootComp.statusChanged.connect(finishComponentCreation);

    }
}

//*******************************************
// Purpose: Creates object for the component loaded and defines properties for the created object
// Input Parameters: objN
// Description and Use of each parameter:objN - objectName of currently clicked
//                                       AlarmSlot(S1,S2,S3 or S4) in AlarmMessge Area.
// Output Parameters:
// Description and Use of each Parameter:
// Return:
// Description of the return value and type:
//***********************************************/

function finishAlarmTroubleShootComponentCreation(objN)
{
    console.log("finishAlarmTroubleShootComponentCreation.."+objN)
    var alarmTroubleShootRef =   getCompRef(mainitem,"AlarmTroubleShootObj")
    alarmTroubleShootCreator = alarmTroubleShootComp.createObject(mainitem,{"objectName": "AlarmTroubleShootObj",
                                                                      "iAlarmTroubleShootPopUpHeight" : alarmTroubleShootPopUpHeight,
                                                                      "iAlarmTroubleShootPopUpWidth" : alarmTroubleShootPopUpWidth,
                                                                      "strAlarmTroubleShootAreaColor" : alarmTroubleShootAreaColor,
                                                                      "iAlarmTroubleShootTitleFontSize" : alarmTroubleShootTitleFontSize,
                                                                      "iAlarmTroubleShootIcnWidth" : alarmTroubleShootIcnWidth,
                                                                      "iAlarmTroubleShootIcnHeight" : alarmTroubleShootIcnHeight,
                                                                      "strAlarmTroubleShootFontColor" : alarmTroubleShootFontColor,
                                                                      "iAlarmTroubleShootInstTextFontSize" : alarmTroubleShootInstTextFontSize,
                                                                      "strAlarmTroubleShootInstTextFontColor" : alarmTroubleShootInstTextFontColor,
                                                                      "iAlarmTroubleShootBorderWidth" : alarmTroubleShootBorderWidth,
                                                                      "strAlarmTroubleShootBorderColor" : alarmTroubleShootBorderColor,
                                                                      "iAlarmTroubleShootBorderRadius" : alarmTroubleShootBorderRadius,
                                                                      "strAlarmTroubleShootCloseBtnSource" : alarmTroubleShootCloseBtnSource,
                                                                      "iAlarmTroubleShootCloseBtnHeight" : alarmTroubleShootCloseBtnHeight,
                                                                      "iAlarmTroubleShootCloseBtnWidth" : alarmTroubleShootCloseBtnWidth,
                                                                      "strAlarmTroubleShootInfoIconSource" : alarmTroubleShootInfoIconSource,
                                                                      "iAlarmTroubleShootTitleLeftMargin" : alarmTroubleShootTitleLeftMargin,
                                                                      "iAlarmTroubleShootTitleTopMargin" : alarmTroubleShootTitleTopMargin,
                                                                      "strAlarmTroubleShootCloseBtnBgColor" : alarmTroubleShootCloseBtnBgColor,
                                                                      "strAlarmTroubleShootCloseBtnBorderColor" : alarmTroubleShootCloseBtnBorderColor,
                                                                      "iAlarmTroubleShootCloseBtnBorderWidth" : alarmTroubleShootCloseBtnBorderWidth,
                                                                      "iAlarmTroubleShootCloseBtnTopMargin" : alarmTroubleShootCloseBtnTopMargin,
                                                                      "iAlarmTroubleShootCloseBtnRightMargin" : alarmTroubleShootCloseBtnRightMargin,
                                                                      "iAlarmTroubleShootInfoIconTopMargin" : alarmTroubleShootInfoIconTopMargin,
                                                                      "iAlarmTroubleShootInfoIconLeftMargin" : alarmTroubleShootInfoIconLeftMargin,
                                                                      "iAlarmTroubleShootInstTopMargin" : alarmTroubleShootInstTopMargin,
                                                                      "iAlarmTroubleShootInstLeftMargin" : alarmTroubleShootInstLeftMargin,
                                                                      "iAlarmTroubleShootOpacityVal" : alarmTroubleShootOpacityVal,
                                                                      "iAlarmTroubleShootTimerInterval" : alarmTroubleShootTimerInterval

                                                                  });

    alarmTroubleShootRef =   getCompRef(mainitem,"AlarmTroubleShootObj")
    alarmTroubleShootRef.isAlarmTroubleShootPopUpCreated = true
    var alarmGroupRef =   getCompRef(mainitem,"AlarmGroup")

    var waveformGroupReference = getCompRef(mainitem,"waveformGroup")

    alarmTroubleShootRef.anchors.top = alarmGroupRef.bottom

    alarmTroubleShootRef.anchors.topMargin = alarmTroubleShootPopupTopMargin
    alarmTroubleShootRef.anchors.horizontalCenter = waveformGroupReference.horizontalCenter

}

//*******************************************
// Purpose: Destruction of AlarmTroubleShoot PopUp
//          Destroys the AlarmTroubleShoot PopUp when close button is clicked
// Input Parameters:
// Description and Use of each parameter:
// Output Parameters:
// Description and Use of each Parameter:
// Return:
// Description of the return value and type:
//***********************************************/

function destroyAlarmTroubleShootPopUp() {
    var alarmTroubleShootRef =   getCompRef(mainitem,"AlarmTroubleShootObj")
    if (alarmTroubleShootRef !== null && typeof(alarmTroubleShootRef) !== "undefined") {
        alarmTroubleShootRef.isAlarmTroubleShootPopUpCreated = false
        console.log("destroy alarmTroubleShootPopUp")
        alarmTroubleShootRef.destroy()
        alarmTroubleShootRef.stopTimer()
        console.log("set group ref to null")
        alarmTroubleShootRef = null;
        console.log("alarmTroubleShootRef.objectName"+alarmTroubleShootRef)
    } else {
        console.log("alarmTroubleShoot deletion : alarmTroubleShootRef === null")
    }
}
//*******************************************
// Purpose: This function check for the  AlarmTroubleShoot PopUp
//          if it is null or undefined and based on condition return the status value.
// Input Parameters:
// Description and Use of each parameter:
// Output Parameters:
// Description and Use of each Parameter:
// Return:  alarmTroubleShootPopUpStatus
// Description of the return value and type: returns bool value alarmTroubleShootPopUpStatus true/false
//                                           if AlarmTroubleShoot PopUp exists or not.
//***********************************************/

function getAlarmTroubleShootPopupStatus(){
    var alarmTroubleShootRef =   getCompRef(mainitem,"AlarmTroubleShootObj")
    if (alarmTroubleShootRef !== null && typeof(alarmTroubleShootRef) !== "undefined") {

        var alarmTroubleShootPopUpStatus =  alarmTroubleShootRef.isAlarmTroubleShootPopUpCreated
        return alarmTroubleShootPopUpStatus
    }else{
        console.log("AlarmTroubleShootPopUp is not created")
        return false
    }
}

//*******************************************
// Purpose: This function check for the currently clicked AlarmSlot(1,2,3 or 4) and call assignInst()
//          to update the Instructions and AlarmTroubleShoot Title.
// Input Parameters: objN
// Description and Use of each parameter: objN- object name of currently clicked AlarmSlot
//                                              in AlarmMessage Area
// Output Parameters:
// Description and Use of each Parameter:
// Return:
// Description of the return value and type:
//***********************************************/

function updateInst(objN){
    var alarmTroubleShootRef =   getCompRef(mainitem,"AlarmTroubleShootObj")
    if (alarmTroubleShootRef !== null && typeof(alarmTroubleShootRef) !== "undefined") {
        var alarmMesgComp1 =   getCompRef(alarmRow,"alarmButton1")
        var alarmMesgComp2 =   getCompRef(alarmRow,"alarmButton2")
        if(objN === "alarmButton1") {
            console.log("inside Data1")
            alarmMsgText1 = alarmMesgComp1.strAlarmMsg
            alarmTroubleShootRef.assignInst(InstData1,alarmMsgText1)
        }
        if(objN === "alarmButton2") {
            console.log("inside Data2")
            alarmMsgText2 = alarmMesgComp2.strAlarmMsg
            alarmTroubleShootRef.assignInst(InstData2,alarmMsgText2)
        }
    }else{
        console.log("AlarmTroubleShootPopUp1 is not created")
    }
}

    //***********************************************
    // Purpose:Get Only Alarm Message from an Alarm String
    // Input Parameters:
    // masterListItem: List of Alarm message Strings
    // Return:alarm message string
    // Description:Get Only Alarm Message from the Alarm String
    //***********************************************/
    function getAlarmMessageFromString(masterListItem)
    {
        //console.log("MasterListItem is :" + masterListItem)
        if( null !== masterListItem ){
            var message = masterListItem.split(",")[1] ;
            return message ;
        }
    }

//*******************************************
// Purpose:To process the alarms triggerred.
// Input Parameters:
// masterListItem: List of Alarm message Strings
// Description:Processes the incoming list of alarms & stops slot four timer.
//***********************************************/
function alarmProcessing(masterList)
{
    lMasterSubList = [] ;
    alarmCompCreator4.stopSlotFourTimer();
    console.log("Master list inside the alarm processing is :"+ masterList)
    var alarmList = []
    if(masterList.length > 0 )
    {
        alarmList=appendAlarmsName(masterList)
        lMasterList=[];
        lMasterList = alarmList.slice();
        alarmCompCreator1.setAlarmMessage("") ;
        alarmCompCreator2.setAlarmMessage("") ;
        alarmCompCreator3.setAlarmMessage("") ;
        alarmCompCreator4.setAlarmMessage("") ;
        displayAlarms(lMasterList);
    }
    else{

        alarmCompCreator1.setAlarmMessage("") ;
        alarmCompCreator2.setAlarmMessage("") ;
        alarmCompCreator3.setAlarmMessage("") ;
        alarmCompCreator4.setAlarmMessage("") ;
        alarmCompCreator1.setAlarmAreaColor(alarmCompCreator1.getDefaultBgColor())
        alarmCompCreator2.setAlarmAreaColor(alarmCompCreator2.getDefaultBgColor())
        alarmCompCreator3.setAlarmAreaColor(alarmCompCreator3.getDefaultBgColor())
        alarmCompCreator4.setAlarmAreaColor(alarmCompCreator4.getDefaultBgColor())


    }
}

//***********************************************
// Purpose:Append alarm messages to the alarms
// Input Parameters:
// alarmsList: List of Alarm Strings
// Return:alarm messages list
// Description:Append corresponding messages for the list of alarms.Since incoming alarms only
//             represent message priority and their numbers.
//***********************************************/
function appendAlarmsName(alarmsList){
    var tempAlarmsList = alarmsList ;
    for(var index = 0; index < tempAlarmsList.length ; index++){
        var item = tempAlarmsList[index]
        var separator = "," ;
        item = item + separator + physicalparam.getAlarmData("AlarmMsg",item)
        tempAlarmsList[index] = item
    }
    return tempAlarmsList;
}


//*******************************************
// Purpose:Display alarms on UI.
// Input Parameters:
// alarmsList: List of Alarm message Strings
//Description:Display alarms in allotted UX space.Namely slots 1,2,3&4 respectively.
//            And set stylings for the slots
//***********************************************/
function displayAlarms(alarmsList)
{
    var alarmsLength = alarmsList.length ;

    /*Display alarms from master list*/
    if(parseInt(alarmsLength) === 1)
    {
        var firstMasterListItem = lMasterList[0] ;
        console.log("Alarm message is :" + getAlarmMessageFromString(firstMasterListItem))
        var priority = getPriority(firstMasterListItem)
        setAlarmAreaStyling("slotOne",priority)
        if( alarmCompCreator1.getAlarmMessage() !== getAlarmMessageFromString(firstMasterListItem))
        {
            alarmCompCreator1.setAlarmMessage(getAlarmMessageFromString(firstMasterListItem)) ;
        }

        alarmCompCreator2.setAlarmMessage("")
        alarmCompCreator3.setAlarmMessage("")
        alarmCompCreator4.setAlarmMessage("")
    }
    if(parseInt(alarmsLength) === 2)
    {
        var firstMasterListItem = lMasterList[0] ;
        var priority = getPriority(firstMasterListItem)
        setAlarmAreaStyling("slotOne",priority)
        if( alarmCompCreator1.getAlarmMessage() !== getAlarmMessageFromString(firstMasterListItem))
        {
            alarmCompCreator1.setAlarmMessage(getAlarmMessageFromString(firstMasterListItem)) ;
        }

        var priority = getPriority(lMasterList[1])
        var secondMasterListItem = lMasterList[1] ;
        setAlarmAreaStyling("slotTwo",priority)
        if(alarmCompCreator2.getAlarmMessage() !== getAlarmMessageFromString(secondMasterListItem))
        {
            alarmCompCreator2.setAlarmMessage(getAlarmMessageFromString(secondMasterListItem)) ;
        }

        alarmCompCreator3.setAlarmMessage("")
        alarmCompCreator4.setAlarmMessage("")

    }
    if(parseInt(alarmsLength) === 3)
    {
        var priority = getPriority(lMasterList[0])
        var firstMasterListItem = lMasterList[0] ;
        setAlarmAreaStyling("slotOne",priority)
        alarmCompCreator1.setAlarmMessage(getAlarmMessageFromString(firstMasterListItem)) ;

        var priority = getPriority(lMasterList[1])
        var secondMasterListItem = lMasterList[1] ;
        setAlarmAreaStyling("slotTwo",priority)
        alarmCompCreator2.setAlarmMessage(getAlarmMessageFromString(secondMasterListItem)) ;

        var priority = getPriority(lMasterList[2])
        var thirdMasterListItem = lMasterList[2] ;
        setAlarmAreaStyling("slotThree",priority)
        alarmCompCreator3.setAlarmMessage(getAlarmMessageFromString(thirdMasterListItem)) ;

        alarmCompCreator4.setAlarmMessage("")
    }

    if(parseInt(alarmsLength) >= 4){
        var priority = getPriority(lMasterList[0])
        var firstMasterListItem = lMasterList[0] ;
        setAlarmAreaStyling("slotOne",priority)
        if( alarmCompCreator1.getAlarmMessage() !== getAlarmMessageFromString(firstMasterListItem))
        {
            alarmCompCreator1.setAlarmMessage(getAlarmMessageFromString(firstMasterListItem)) ;
        }

        var priority = getPriority(lMasterList[1])
        var secondMasterListItem = lMasterList[1] ;
        setAlarmAreaStyling("slotTwo",priority)
        if(alarmCompCreator2.getAlarmMessage() !== getAlarmMessageFromString(secondMasterListItem))
        {
            alarmCompCreator2.setAlarmMessage(getAlarmMessageFromString(secondMasterListItem)) ;
        }
        var priority = getPriority(lMasterList[2])
        var thirdMasterListItem = lMasterList[2] ;
        setAlarmAreaStyling("slotThree",priority)
        if(alarmCompCreator3.getAlarmMessage() !== getAlarmMessageFromString(thirdMasterListItem))
        {
            alarmCompCreator3.setAlarmMessage(getAlarmMessageFromString(thirdMasterListItem)) ;
        }

        var priority = getPriority(lMasterList[3])
        var fourthMasterListItem = lMasterList[3] ;
        setAlarmAreaStyling("slotFour",priority)
        if(alarmCompCreator4.getAlarmMessage() !== getAlarmMessageFromString(fourthMasterListItem))
        {
            alarmCompCreator4.setAlarmMessage(getAlarmMessageFromString(fourthMasterListItem));
        }
    }

    if(parseInt(alarmsLength) >4){
        lMasterSubList = [] ;
        lMasterSubList = lMasterList.slice(3)
        slotFourCounter = -1 ;
        alarmCompCreator4.restartSlotFourTimer();
    }

}

//*******************************************
// Purpose:To get the priority of alarm Message
// Input Parameters:
// masterListItem:Alarm message string
// Return:priority of the alarm message
// Description:Returns the priority of the alarm message
//***********************************************/
function getPriority(masterListItem)
{
    if( null !== masterListItem )
    {
        var priority = masterListItem.split(",")[0] ;
        priority = priority.split("_")[0]
        if(priority.includes("HA"))
        {
            priority = "High" ;
        }
        else if(priority.includes("MA"))
        {
            priority = "Medium" ;
        }
        else if(priority.includes("LA"))
        {
            priority = "Low" ;
        }
        return priority ;
    }
}

//*******************************************
// Purpose:Set styling for Alarm Displaying Areas.
// Input Parameters:
// alarmArea: Where the alarm message should be displayed.
// priority : Priority of the alarm message area.
// Description:Set styling properties for the alarm display areas based on the priority of the alarm
//***********************************************/
function setAlarmAreaStyling(alarmArea,priority)
{

    switch (alarmArea)
    {
    case "slotOne":
        if(priority === "High")
        {
            alarmCompCreator1.setAlarmMessageColor(alarmCompCreator1.strHighPriBgColor)
            alarmCompCreator1.setAlarmMessagePixelSize(alarmCompCreator1.iHighPriPixelSize)
            alarmCompCreator1.setAlarmMessageFontColor(alarmCompCreator1.strHighPriFontColor)
            alarmCompCreator1.setAlarmAreaColor(alarmCompCreator1.strHighPriBgColor)
        }
        else  if(priority === "Medium")
        {
            alarmCompCreator1.setAlarmMessageColor(alarmCompCreator1.strMedPriBgColor)
            alarmCompCreator1.setAlarmMessagePixelSize(alarmCompCreator1.iMedPriPixelSize)
            alarmCompCreator1.setAlarmMessageFontColor(alarmCompCreator1.strMedPriFontColor)
            alarmCompCreator1.setAlarmAreaColor(alarmCompCreator1.strMedPriBgColor);
        }
        else if(priority === "Low")
        {
            alarmCompCreator1.setAlarmMessageColor(alarmCompCreator1.strLowPriBgColor)
            alarmCompCreator1.setAlarmMessagePixelSize(alarmCompCreator1.iLowPriPixelSize)
            alarmCompCreator1.setAlarmMessageFontColor(alarmCompCreator1.strLowPriFontColor)
            alarmCompCreator1.setAlarmAreaColor(alarmCompCreator1.strLowPriBgColor)
        }
        break;

    case "slotTwo":
        if(priority === "High")
        {
            alarmCompCreator2.setAlarmMessageColor(alarmCompCreator2.strHighPriBgColor)
            alarmCompCreator2.setAlarmMessagePixelSize(alarmCompCreator2.iHighPriPixelSize)
            alarmCompCreator2.setAlarmMessageFontColor(alarmCompCreator2.strHighPriFontColor)
            alarmCompCreator2.setAlarmAreaColor(alarmCompCreator2.strHighPriBgColor)
        }
        else  if(priority === "Medium")
        {
            alarmCompCreator2.setAlarmMessageColor(alarmCompCreator2.strMedPriBgColor)
            alarmCompCreator2.setAlarmMessagePixelSize(alarmCompCreator2.iMedPriPixelSize)
            alarmCompCreator2.setAlarmMessageFontColor(alarmCompCreator2.strMedPriFontColor)
            alarmCompCreator2.setAlarmAreaColor(alarmCompCreator2.strMedPriBgColor)
        }
        else if(priority === "Low")
        {
            alarmCompCreator2.setAlarmMessageColor(alarmCompCreator2.strLowPriBgColor)
            alarmCompCreator2.setAlarmMessagePixelSize(alarmCompCreator2.iLowPriPixelSize)
            alarmCompCreator2.setAlarmMessageFontColor(alarmCompCreator2.strLowPriFontColor)
            alarmCompCreator2.setAlarmAreaColor(alarmCompCreator2.strLowPriBgColor)
        }
        break;
    case "slotThree":
        if(priority === "High")
        {
            alarmCompCreator3.setAlarmMessageColor(alarmCompCreator3.strHighPriBgColor)
            alarmCompCreator3.setAlarmMessagePixelSize(alarmCompCreator3.iHighPriPixelSize)
            alarmCompCreator3.setAlarmMessageFontColor(alarmCompCreator3.strHighPriFontColor)
            alarmCompCreator3.setAlarmAreaColor(alarmCompCreator3.strHighPriBgColor)
        }
        else  if(priority === "Medium")
        {
            alarmCompCreator3.setAlarmMessageColor(alarmCompCreator3.strMedPriBgColor)
            alarmCompCreator3.setAlarmMessagePixelSize(alarmCompCreator3.iMedPriPixelSize)
            alarmCompCreator3.setAlarmMessageFontColor(alarmCompCreator3.strMedPriFontColor)
            alarmCompCreator3.setAlarmAreaColor(alarmCompCreator3.strMedPriBgColor)
        }
        else if(priority === "Low")
        {
            alarmCompCreator3.setAlarmMessageColor(alarmCompCreator3.strLowPriBgColor)
            alarmCompCreator3.setAlarmMessagePixelSize(alarmCompCreator3.iLowPriPixelSize)
            alarmCompCreator3.setAlarmMessageFontColor(alarmCompCreator3.strLowPriFontColor)
            alarmCompCreator3.setAlarmAreaColor(alarmCompCreator3.strLowPriBgColor)
        }
        break;
    case "slotFour":
        if(priority === "High")
        {
            alarmCompCreator4.setAlarmMessageColor(alarmCompCreator4.strHighPriBgColor)
            alarmCompCreator4.setAlarmMessagePixelSize(alarmCompCreator4.iHighPriPixelSize)
            alarmCompCreator4.setAlarmMessageFontColor(alarmCompCreator4.strHighPriFontColor)
            alarmCompCreator4.setAlarmAreaColor(alarmCompCreator4.strHighPriBgColor)
        }
        else  if(priority === "Medium")
        {
            alarmCompCreator4.setAlarmMessageColor(alarmCompCreator4.strMedPriBgColor)
            alarmCompCreator4.setAlarmMessagePixelSize(alarmCompCreator4.iMedPriPixelSize)
            alarmCompCreator4.setAlarmMessageFontColor(alarmCompCreator4.strMedPriFontColor)
            alarmCompCreator4.setAlarmAreaColor(alarmCompCreator4.strMedPriBgColor)
        }
        else if(priority === "Low")
        {
            alarmCompCreator4.setAlarmMessageColor(alarmCompCreator4.strLowPriBgColor)
            alarmCompCreator4.setAlarmMessagePixelSize(alarmCompCreator4.iLowPriPixelSize)
            alarmCompCreator4.setAlarmMessageFontColor(alarmCompCreator4.strLowPriFontColor)
            alarmCompCreator4.setAlarmAreaColor(alarmCompCreator4.strLowPriBgColor)
        }
        break;
    }
}


//*******************************************
// Purpose:Displays alarm messages In Slot four.
// Description:Displays alarm messages In Slot four present in master sub list in the slot four.
//***********************************************/
function displayAlarmInSlotFour()
{
    if(lMasterList.length < 1){
        return;
    }

    if(slotFourCounter < lMasterSubList.length - 1 ){
        slotFourCounter++;
        var priority = getPriority(lMasterSubList[slotFourCounter])
        setAlarmAreaStyling("slotFour",priority)
        if(alarmCompCreator4.getAlarmMessage() !== getAlarmMessageFromString(lMasterSubList[slotFourCounter]))
        {
            alarmCompCreator4.setAlarmMessage(getAlarmMessageFromString(lMasterSubList[slotFourCounter])) ;
        }
    }
    else {
        slotFourCounter = -1 ;
    }
}

