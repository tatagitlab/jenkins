import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "qrc:/Source/AlarmArea/JavaScript/AlarmGroupController.js" as AlarmGroupCtrl


//import "WaveFormArea"

Rectangle {
    id: alarmButtonId
    anchors.top: parent.top

    property bool bIsVisible: true
    property int iAlarmHeight
    property int iAlarmRadius
    property int iAlarmWidth
    property int iCompID
    property int index: -1
    property int iAlarmMsgMargin
    property int iAlarmMsgPixelSize
    property int iAlarmRotationInterval: 2000
    property int iHighPriPixelSize
    property int iLowPriPixelSize
    property int iMedPriPixelSize
    property string strDefaultBgColor: "#151515"
    property string strAlarmAreaColor
    property string strAlarmMsg: ""
    property string strAlarmMsgColor
    property string strHighPriBgColor
    property string strHighPriFontColor
    property string strLowPriBgColor
    property string strLowPriFontColor
    property string strMedPriBgColor
    property string strMedPriFontColor
    property string strMsgFontColor
    property string strPriority
    property bool isAlarmTroubleShootCreated : AlarmGroupCtrl.getAlarmTroubleShootPopupStatus()

    //To display alarm message in slot four
    signal displayNextAlarm()

    width: iAlarmWidth
    height: iAlarmHeight
    radius: iAlarmRadius
    color: alarmButtonId.strAlarmAreaColor
    objectName: "alarmButton"+iCompID
    visible: bIsVisible

    //*******************************************
    // Purpose:Setter for background color
    // Description:When there is no alarm displayed,Set background color to default color
    //***********************************************/
    function getDefaultBgColor() {
        return alarmButtonId.strDefaultBgColor;
    }

    //*******************************************
    // Purpose:Set background color for Alarm Message area
    // Description:Different priority alarms have different colors.It sets accordingly.
    //***********************************************/
    function setAlarmBackgroundColor(color) {
        alarmButtonId.strAlarmAreaColor = color ;
    }

    //*******************************************
    // Purpose:Restarts the slot four timer
    // Description:In slot four of Alarm message area, Alarms are rotated at an interval of 2 seconds.
    //              This restarts the timer whenever there is updated master sub alarms list.
    //***********************************************/
    function restartSlotFourTimer() {
        console.log("Restarting slot four timer :\n")
        slotFourTimer.restart();
    }

    //*******************************************
    // Purpose:Stops the slot four timer
    // Description:In slot four of Alarm message area, Alarms are rotated at an interval of 2 seconds.
    //              This stops the timer if there are no items in master sub list
    //***********************************************/
    function stopSlotFourTimer() {
        slotFourTimer.stop();
    }

    /*******************************************
    // Purpose:Getter for Alarm area color.
    // Description:Returns the alarm message area color for different priority alarms.
    //***********************************************/
    function getAlarmAreaColor() {
        return alarmButtonId.strAlarmAreaColor;
    }
    /*******************************************
    // Purpose:Setter for Alarm area color.
    // Description:Sets the alarm message area color for different priority alarms.
    //***********************************************/
    function setAlarmAreaColor(color) {
        alarmButtonId.strAlarmAreaColor = color;
    }

    /*******************************************
    // Purpose:Getter for Alarm Message.
    // Description:Returns the alarm message.
    //***********************************************/
    function getAlarmMessage() {
        return  alarmButtonId.strAlarmMsg ;
    }

    /*******************************************
    // Purpose:Setter for Visibility
    // Description:Sets the visibility of alarm message area.
    //***********************************************/
    function setVisibility(visibility) {
        alarmButtonId.bIsVisible = visibility ;
    }

    /*******************************************
    // Purpose:Setter for alarm message
    // Description:Sets the alarm message for alarm message area.
    //***********************************************/
    function setAlarmMessage(alarmMessage) {
        alarmButtonId.strAlarmMsg = alarmMessage ;
    }

    /*******************************************
    // Purpose:Setter for alarm message color
    // Description:Sets the alarm message color for alarm message area.
    //***********************************************/
    function setAlarmMessageColor(color) {
        alarmButtonId.strAlarmMsgColor = color;
    }

    /*******************************************
    // Purpose:Setter for alarm message pixel size
    // Description:Sets the alarm message pixel size for alarm message area.
    //***********************************************/
    function setAlarmMessagePixelSize(pixelSize) {
        alarmButtonId.iAlarmMsgPixelSize = pixelSize
    }

    /*******************************************
    // Purpose:Setter for alarm message font size
    // Description:Sets the alarm message fonty size for alarm message area.
    //***********************************************/
    function setAlarmMessageFontColor(fontColor) {
        alarmButtonId.strMsgFontColor =  fontColor ;
    }

    /*******************************************
    // Purpose:Setter for alarm message area color
    // Description:Sets the alarm message area color for alarm message area.
    //***********************************************/
    function setAlarmMessageAreaColor(messageColor) {
        alarmButtonId.strAlarmMsgColor = messageColor
    }
    Image{
        id:infoIcon
        height:18
        width:18
        anchors.left:alarmButtonId.left
        anchors.top:alarmButtonId.top
        anchors.leftMargin : 2
        anchors.topMargin: 2
        source:{
            if (alarmmsg.text === ""){
                return""
            }else
            {
                return "../../../Config/Assets/icon_info.png"
            }
        }
    }


    MouseArea{
        id:alarmInfoButton //major one for opening
        anchors.fill:alarmButtonId
        onClicked: {
            console.log(isAlarmTroubleShootCreated+"isAlarmTroubleShootCreated or not")
            if (isAlarmTroubleShootCreated === false){
                console.log("inital creation of AlarmTroubleShootPopup")
                AlarmGroupCtrl.createAlarmTroubleShootComponent(alarmButtonId.objectName)
                AlarmGroupCtrl.updateInst(alarmButtonId.objectName)
            }
            else {
                console.log("PopUp already Exists")
                AlarmGroupCtrl.updateInst(alarmButtonId.objectName)
            }
        }
    }

    Text {
        id: alarmmsg
        text: qsTrId(strAlarmMsg)
        fontSizeMode: Text.Fit
        minimumPixelSize: 15
        wrapMode: Text.WordWrap
        anchors.centerIn: parent
        anchors.verticalCenter: parent.verticalCenter
        color: strMsgFontColor
        font.pixelSize: iAlarmMsgPixelSize
    }

    Timer {
        id: slotFourTimer
        interval: iAlarmRotationInterval
        running: false
        repeat: true
        onTriggered: {
            displayNextAlarm()
        }
    }

    onStrAlarmMsgChanged: {
        if(strAlarmMsg === "" || strAlarmMsg === null) {
            alarmButtonId.strAlarmAreaColor = strDefaultBgColor ;
        }
    }

    Component.onCompleted: {
        console.log("Alarm Button is created :\n")
    }
}
