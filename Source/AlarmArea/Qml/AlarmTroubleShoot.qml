/********************************************************************
*    GE Healthcare
*    JFWTC
*    Bangalore, KA-560066
*
*    Copyright 2019, GE Healthcare.
*    CONFIDENTIAL & PROPRIETARY - PORTIONS OF THIS
*    SOURCE CODE CONTAIN VALUABLE TRADE SECRET
*    INFORMATION THAT IS THE PROPERTY
*    OF GE Healthcare.
*
*    $Archive::  https://gitlab-gxp.cloud.health.ge.com/arc-sw-dev/Subsystems/SMB/Jarvis_SMB_GUI
*    $Workfile:: AlarmTroubleShoot.qml
*    $Author::  GE Healthcare
*
*    Purpose: This QML file creates component to be displayed for the AlarmTroubleShoot Popup .
*             This includes Instructions Text,closeBtn,InfoBtn.
*             This gets created onClick of Info Icon on AlarmMessage in AlarmArea.
********************************************************************/

import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "qrc:/Source/AlarmArea/JavaScript/AlarmGroupController.js" as AlarmGroupCtrl



Rectangle{
    property int iAlarmTroubleShootPopUpHeight
    property int iAlarmTroubleShootPopUpWidth
    property string strAlarmTroubleShootAreaColor
    property int iAlarmTroubleShootTitleFontSize
    property int iAlarmTroubleShootIcnWidth
    property int iAlarmTroubleShootIcnHeight
    property string strAlarmTroubleShootFontColor
    property int iAlarmTroubleShootInstTextFontSize
    property string strAlarmTroubleShootInstTextFontColor
    property int iAlarmTroubleShootBorderWidth
    property string strAlarmTroubleShootBorderColor
    property bool isAlarmTroubleShootPopUpCreated : false
    property int iAlarmTroubleShootBorderRadius
    property string strAlarmTroubleShootCloseBtnSource  //"../../../Config/Assets/icon_close.png"
    property int iAlarmTroubleShootCloseBtnHeight
    property int iAlarmTroubleShootCloseBtnWidth
    property string strAlarmTroubleShootInfoIconSource //"../../../Config/Assets/icon_info.png"
    property int iAlarmTroubleShootTitleLeftMargin
    property int iAlarmTroubleShootTitleTopMargin
    property string strAlarmTroubleShootCloseBtnBgColor
    property string strAlarmTroubleShootCloseBtnBorderColor
    property int iAlarmTroubleShootCloseBtnBorderWidth
    property int iAlarmTroubleShootCloseBtnTopMargin
    property int iAlarmTroubleShootCloseBtnRightMargin
    property int iAlarmTroubleShootInfoIconTopMargin
    property int iAlarmTroubleShootInfoIconLeftMargin
    property int iAlarmTroubleShootInstTopMargin
    property int iAlarmTroubleShootInstLeftMargin
    property int iAlarmTroubleShootOpacityVal
    property int iAlarmTroubleShootTimerInterval
    property string alarmTroubleShootPopUpTitle
    property bool bAlarmTroubleShootTimerRunning: false

    id:infoo
    color: strAlarmTroubleShootAreaColor
    border.color: strAlarmTroubleShootBorderColor
    border.width: iAlarmTroubleShootBorderWidth
    radius: iAlarmTroubleShootBorderRadius
    opacity: iAlarmTroubleShootOpacityVal * 0.01
    z:10
    height: iAlarmTroubleShootPopUpHeight
    width: iAlarmTroubleShootPopUpWidth
    Text{
        id:infootxt
        color : strAlarmTroubleShootInstTextFontColor
        wrapMode: Text.Wrap
        anchors.top: infoIconRect.bottom
        anchors.topMargin: iAlarmTroubleShootInstTopMargin
        anchors.left: infoo.left
        anchors.leftMargin: iAlarmTroubleShootInstLeftMargin
        font.pixelSize: iAlarmTroubleShootInstTextFontSize
        width: parent.width - iAlarmTroubleShootInstLeftMargin
    }

    Text{
        id: titleTxt
        color : strAlarmTroubleShootFontColor
        anchors.top: infoo.top
        anchors.topMargin: iAlarmTroubleShootTitleTopMargin
        anchors.left: infoIconRect.right
        anchors.leftMargin: iAlarmTroubleShootTitleLeftMargin
        font.pixelSize: iAlarmTroubleShootTitleFontSize
    }

    //*******************************************
    // Purpose: This function updates the InstructionSet and AlarmTitle of the corresponding AlarmMessage
    //          displaying on AlarmArea to the AlarmTroubleShoot Popup.
    // Input Parameters:instSet , alarmMsgText
    // Description and Use of each parameter:instSet - Instructions to be updated.
    //                                       alarmMsgText - Title to be updated
    // Output Parameters:
    // Description and Use of each Parameter:
    // Return:
    // Description of the return value and type:
    //***********************************************/

    function assignInst(instSet,alarmMsgText) {
        console.log("assignInst function")
        infootxt.text = '\u2022'+" "+instSet.join("\n" + '\u2022'+" ");
        alarmTroubleShootPopUpTitle = alarmMsgText
        titleTxt.text = alarmTroubleShootPopUpTitle
    }
    Rectangle{
        id: infoIconRect
        height:iAlarmTroubleShootIcnHeight
        width:iAlarmTroubleShootIcnWidth
        radius: iAlarmTroubleShootIcnWidth * 0.5
        color: "transparent"
        anchors.top: infoo.top
        anchors.left: infoo.left
        anchors.leftMargin: iAlarmTroubleShootInfoIconLeftMargin
        anchors.topMargin: iAlarmTroubleShootInfoIconTopMargin
        Image{
            id:infoIconInsideRect
            anchors.fill: parent
            source: strAlarmTroubleShootInfoIconSource //"../../../Config/Assets/icon_info.png"
        }
    }

    Rectangle {
        id: closeIcon
        anchors.right: infoo.right
        anchors.rightMargin: iAlarmTroubleShootCloseBtnRightMargin
        anchors.top: infoo.top
        anchors.topMargin: iAlarmTroubleShootCloseBtnTopMargin
        border.width: iAlarmTroubleShootCloseBtnBorderWidth//iCloseBtnBorderWidth
        height: iAlarmTroubleShootCloseBtnHeight
        width: iAlarmTroubleShootCloseBtnWidth
        radius: iAlarmTroubleShootBorderRadius
        color: strAlarmTroubleShootCloseBtnBgColor
        objectName: "CloseBtnObj"
        border.color:strAlarmTroubleShootCloseBtnBorderColor
        Image {
            source: strAlarmTroubleShootCloseBtnSource
            anchors.centerIn: closeIcon
        }

        MouseArea {
            id: closeMouseArea
            anchors.fill: closeIcon
            onClicked: AlarmGroupCtrl.destroyAlarmTroubleShootPopUp()  // close button
        }
    }

    Timer {
        id : timeOut
        interval : iAlarmTroubleShootTimerInterval
        repeat: false
        objectName: "timeOutObj"
        onTriggered: {
            console.log("AlarmTroubleShoot time out initiated")
            // mediaPlayer.play()
            timeOut.running = false
            timeOut.stop()
            AlarmGroupCtrl.destroyAlarmTroubleShootPopUp();
        }
    }
    function stopTimer() {
        timeOut.running = false ;
        timeOut.stop() ;
    }
    Component.onCompleted: {
        timeOut.start()
    }


}
