import QtQuick 2.9
import QtQuick.Controls 2.2

Rectangle {
    id: audioPause

    property int iAudioPauseAreaHeight
    property int iAudioPauseAreaWidth
    property int iAudioPauseIconHeight
    property int iAudioPauseIconWidth
    property int iAudioPausePixelSize
    property int iMin: 1
    property int iSec: 60
    property int iAudioPauseInterval: 1000
    property string strAlarmColor
    property string strAudioPauseFontColor
    property string strAudioPauseIconSource
    property string strDefaultVal: "02:00"

    width: iAudioPauseAreaWidth
    height: iAudioPauseAreaHeight
    color: strAlarmColor
    anchors.left: parent.left

    //*******************************************
    // Purpose:Perform timer decrement operation
    // Description:When the user touches audio pause Icon , Display 2 iMin decrement timer
    //***********************************************/

    function decrementTimer() {
        if(audioPause.iSec === 0 && audioPause.iMin !== 0) {
            audioPause.iSec = 60
            audioPause.iMin = 0
        }
        if(audioPause.iSec === 0 && audioPause.iMin === 1) {
            audioPause.iSec = 60
            audioPause.iMin = 0
        }

        if(audioPause.iSec > 0) {
            audioPause.iSec = audioPause.iSec - 1
        }

        if(audioPause.iSec < 10) {
            displayTime.text = "0" + audioPause.iMin + ":" + "0" + audioPause.iSec
        } else {
            displayTime.text = "0" + audioPause.iMin + ":" + audioPause.iSec
        }

        if(audioPause.iSec === 0 && audioPause.iMin === 0) {
            timer.stop();
            audioPause.iSec = 60
            audioPause.iMin = 1
            displayTime.text = audioPause.strDefaultVal
            audio.source = strAudioPauseIconSource
        }
    }

    Image {
        id: audio
        width: iAudioPauseIconWidth
        height: iAudioPauseIconHeight
        source: strAudioPauseIconSource
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
    }

    MouseArea {
        anchors.fill: audio
        onClicked: {
            audio.source =  "qrc:/Config/Assets/icon_audiopause.png"
            if(timer.running === true) {
                timer.running = false ;
                timer.stop();
                audioPause.iSec = 60
                audioPause.iMin = 1
                displayTime.text = audioPause.strDefaultVal
                audio.source = strAudioPauseIconSource
            } else {
                timer.running = true ;
                timer.restart();
            }
        }
    }
    Text {
        id: displayTime
        text: audioPause.strDefaultVal
        font.pixelSize: iAudioPausePixelSize
        color: strAudioPauseFontColor
        anchors.horizontalCenter: audio.horizontalCenter
        anchors.bottom: parent.bottom

    }
    Timer {
        id: timer
        interval: iAudioPauseInterval
        running: false
        repeat: true
        onTriggered: {
            audioPause.decrementTimer();
        }
    }
}






