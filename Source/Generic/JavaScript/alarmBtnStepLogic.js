/*Increment step Logic for Ppeak Low*/
function clkHandlePpeakLow(m_ppeakLow) {
    var strPpeakHigh = settingReader.read("Ppeak_High")
    var highLimit = 20
    console.log("m_ppeakLow " + m_ppeakLow)
    if(strPpeakHigh <= highLimit){
        highLimit = strPpeakHigh - 1
    }
    if((m_ppeakLow >= 1) && (m_ppeakLow < highLimit)) {
        m_ppeakLow += 1 ;
    }
    return m_ppeakLow;
}

/*Decrement step Logic for Ppeak Low*/
function antClkHandlePpeakLow(m_ppeakLow){
    m_ppeakLow -= 1 ;
    return m_ppeakLow;
}

/*Increment step Logic for Ppeak High*/
function clkHandlePpeakHigh(m_ppeakHigh) {
    var strPpeakLow = settingReader.read("Ppeak_Low")
    if(strPpeakLow < 10){
        strPpeakLow = 10
    }
    if((m_ppeakHigh >= strPpeakLow) && (m_ppeakHigh < 99)) {
        m_ppeakHigh += 1 ;
    }
    return m_ppeakHigh;
}
/*Decrement step Logic for Ppeak High*/
function antClkHandlePpeakHigh(m_ppeakHigh){
    m_ppeakHigh -= 1 ;
    return m_ppeakHigh;
}

/*Increment step Logic for MV Low*/
function clkHandleMVLow(m_mvLow) {
    var strMVHigh = parseFloat(settingReader.read("MV_High"))
    var highLimit = 15.0
    console.log("m_mvLow " + m_mvLow)
    if(strMVHigh <= highLimit){
        highLimit = strMVHigh - 0.1
    }
    console.log("highLimit in mv low" + highLimit)
    if((m_mvLow >= 0.1) && (m_mvLow < highLimit)) {
        m_mvLow =  (parseFloat (m_mvLow + 0.1)).toFixed(1) ;
    }
    return m_mvLow;
}
/*Decrement step Logic for MV Low*/
function antClkHandleMVLow(m_mvLow){
    m_mvLow = (parseFloat (m_mvLow - 0.1)).toFixed(1) ;
    return m_mvLow;
}

/*Increment step Logic for MV High*/
function clkHandleMVHigh(m_mvHigh) {
    var strMvLow = parseFloat(settingReader.read("MV_Low"))
    if(strMvLow < 3.0){
        strMvLow = 3.0
    }
    if((m_mvHigh >= strMvLow) && (m_mvHigh < 40.0)) {
        m_mvHigh = (parseFloat(m_mvHigh + 1.0)).toFixed(1) ;
    }
    return m_mvHigh;
}

/*Decrement step Logic for MV High*/
function antClkHandleMVHigh(m_mvHigh){
    m_mvHigh = (parseFloat(m_mvHigh - 1.0)).toFixed(1) ;
    return m_mvHigh;
}

/*Increment step Logic for TVexp Low*/
function clkHandleTVolumeExpLow(m_tVolumeLow)
{
    var strTVexpHigh = settingReader.read("TVexp_High")
    var highLimit1 = 10
    var highLimit2 = 800
    console.log("m_tVolumeLow " + m_tVolumeLow)
    if(strTVexpHigh <= highLimit1){
        highLimit1 = strTVexpHigh - 5
    }
    if(strTVexpHigh <= highLimit2){
        highLimit2 = strTVexpHigh - 10
    }
    if(m_tVolumeLow < highLimit1) {
        m_tVolumeLow = m_tVolumeLow+5;
    }
    else if(m_tVolumeLow < highLimit2) {
        m_tVolumeLow = m_tVolumeLow+10;
    }
    return m_tVolumeLow;
}

/*Decrement step Logic for TVexp Low*/
function antClkHandleTVolumeExpLow(m_tVolumeLow)
{
    if((10 < m_tVolumeLow) && (m_tVolumeLow <= 800)) {
        m_tVolumeLow = m_tVolumeLow - 10 ;
    }
    else if((5 < m_tVolumeLow) && (m_tVolumeLow <= 10)) {
        m_tVolumeLow = m_tVolumeLow - 5 ;
    }
    return m_tVolumeLow;
}

/*Increment step Logic for TVexp High*/
function clkHandleTVolumeExpHigh(m_tVolumeHigh)
{
    var strTVexpLow = settingReader.read("TVexp_Low")
    if(strTVexpLow < 100){
        strTVexpLow = 100
    }
    if((m_tVolumeHigh>=strTVexpLow) && (m_tVolumeHigh < 100)) {
        m_tVolumeHigh = m_tVolumeHigh+5;
    }
    else if(m_tVolumeHigh < 1800) {
        m_tVolumeHigh = m_tVolumeHigh+10;
    }
    return m_tVolumeHigh;
}

/*Decrement step Logic for TVexp High*/
function antClkHandleTVolumeExpHigh(m_tVolumeHigh)
{
    if((100 < m_tVolumeHigh) && (m_tVolumeHigh <= 1800)) {
        m_tVolumeHigh = m_tVolumeHigh - 10 ;
    }
    else if((5 < m_tVolumeHigh) && (m_tVolumeHigh <= 10)) {
        m_tVolumeHigh = m_tVolumeHigh - 5 ;
    }
    return m_tVolumeHigh;
}
/*Increment step Logic for O2 Low*/
function clkHandleO2Low(m_o2Low) {
    var strO2High = settingReader.read("O2_High")
    var highLimit = 70
    if(strO2High <= highLimit){
        highLimit = strO2High - 1
    }
    if((m_o2Low>=20) && (m_o2Low < highLimit)) {
        m_o2Low += 1 ;
    }
    return m_o2Low;

}
/*Decrement step Logic for O2 Low*/
function antClkHandleO2Low(m_o2Low){
    m_o2Low -= 1 ;
    return m_o2Low;
}

/*Increment step Logic for O2 High*/
function clkHandleO2High(m_o2High) {
    var strO2Low = settingReader.read("O2_Low")
    if(strO2Low < 40){
        strO2Low = 40
    }
    if((m_o2High >= strO2Low) && (m_o2High < 100)) {
        m_o2High += 1 ;
    }
    return m_o2High;

}
/*Decrement step Logic for O2 High*/
function antClkHandleO2High(m_o2High){
    m_o2High -= 1 ;
    return m_o2High;
}

/*Increment step Logic for Apnea Delay */
function clkHandleApneaDelay(m_apneaDelay) {
    if((m_apneaDelay>=10) && (m_apneaDelay < 30)) {
        m_apneaDelay += 1 ;
    }
    return m_apneaDelay;

}
/*Decrement step Logic for Apnea Delay */
function antClkHandleApneaDelay(m_apneaDelay){
    m_apneaDelay -= 1 ;
    return m_apneaDelay;
}

/*Increment step Logic for Alarm Volume */
function clkHandleAlarmVolume(m_alarmVol) {
    if((m_alarmVol>=1) && (m_alarmVol < 5)) {
        m_alarmVol += 1 ;
    }
    return m_alarmVol;

}
/*Decrement step Logic for Alarm Volume */
function antClkHandleAlarmVolume(m_alarmVol){
    m_alarmVol -= 1 ;
    return m_alarmVol;
}
