var alarmLogMenuComponent;
var alarmLogMenuObj = null;
var alarmScrollViewComponent;
var alarmScrollViewObj = null;

var alarmLogMenuWidth = parseInt(layout.getGroupLayoutDataByTag("AlarmLogMenu","Dimension","width"));
var alarmLogMenuHeight = parseInt(layout.getGroupLayoutDataByTag("AlarmLogMenu","Dimension","height"));
var alarmLogMenuRadius = parseInt(component.getComponentXmlDataByTag("Group","AlarmLogMenu", "BorderRadius"));
var alarmLogMenuLabel = component.getComponentResourceString("Group","AlarmScrollView", "MenuTitle","default")
var alarmLogMenuBgColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "Color")
var alarmLogMenuBorderColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "BorderColor")
var alarmLogMenuBorderWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "BorderWidth")
var alarmLogMenuBorderRadius = component.getComponentXmlDataByTag("Group","AlarmScrollView", "BorderRadius")

var alarmLogTableTimeLabel = component.getComponentResourceString("Group","AlarmScrollView", "TableLabel","Time")
var alarmLogTableAlarmLabel = component.getComponentResourceString("Group","AlarmScrollView", "TableLabel","Alarm")
var alarmLogTablePriorityLabel = component.getComponentResourceString("Group","AlarmScrollView", "TableLabel","Priority")

var alarmLogLabelFontColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "AlarmLogLabelFontColor")
var AlarmLogLabelFontSize = component.getComponentXmlDataByTag("Group","AlarmScrollView", "AlarmLogLabelFontSize")
var alarmLogTableTimeLabelFontColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableLabelFontColor")
var alarmLogTableTimeLabelFontSize = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableLabelFontSize")
var alarmLogTableTextFontColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableTextFontColor")
var alarmLogTableTextFontSize = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableTextFontSize")
var alarmLogBackBtnwidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "BackBtnwidth")
var alarmLogBackBtnHeight = component.getComponentXmlDataByTag("Group","AlarmScrollView", "BackBtnHeight")
var alarmLogBackBtnBgColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "BackBtnBgColor")
var alarmLogBackBtnBorderColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "BackBtnBorderColor")
var alarmLogProgressBarBtnWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarBtnWidth")
var alarmLogProgressBarBtnHeight = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarBtnHeight")
var alarmLogProgressBarBtnRadius = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarBtnRadius")
var alarmLogProgressBarBtnBgColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarBtnBgColor")
var alarmLogProgressBarBtnBorderColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarBtnBorderColor")
var alarmLogProgressBarBtnBorderWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarBtnBorderWidth")
var alarmLogProgressBarWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarWidth")
var alarmLogProgressBarBgColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarBgColor")
var alarmLogProgressBarBorderColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarBorderColor")
var alarmLogProgressBarBorderWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarBorderWidth")
var alarmLogProgressBarAreaWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarAreaWidth")
var alarmLogProgressBarAreaBgColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "ProgressBarAreaBgColor")
var alarmLogTchBgColor = component.getComponentXmlDataByTag("Group","AlarmScrollView","AlarmLogTchBgColor")

var alarmLogTableBorderColor = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableBorderColor")
var alarmLogTableBorderWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableBorderWidth")
var alarmLogTableLeftColumnWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableLeftColumnWidth")
var alarmLogTableMiddleColumnWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableMiddleColumnWidth")
var alarmLogTableRightColumnWidth = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableRightColumnWidth")

var alarmLogMenuBackBtn = component.getComponentXmlDataByTag("Group","AlarmScrollView", "BackIcon")
var alarmLogMenuTableUpTouched = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableUpTouchedIcon")
var alarmLogMenuTableUpEnabled = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableUpEnabledIcon")
var alarmLogMenuTableUpDisabled = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableUpDisabledIcon")
var alarmLogMenuTableDownTouched = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableDownTouchedIcon")
var alarmLogMenuTableDownEnabled = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableDownEnabledIcon")
var alarmLogMenuTableDownDisabled = component.getComponentXmlDataByTag("Group","AlarmScrollView", "TableDownDisabledIcon")

function createAlarmLogMenu(bTouch) {
    console.log("Create Alarm Log Menu")
    alarmLogMenuComponent = Qt.createComponent("qrc:/Source/Generic/Qml/AlarmLogMenu.qml")
    if(alarmLogMenuComponent.status === Component.Ready) {
        finishAlarmLogMenuCreation(bTouch);
    }else if(alarmLogMenuComponent.status === Component.Error) {
        console.log("Error loading component:", alarmLogMenuComponent.errorString());
    }else {
        alarmLogMenuComponent.statusChanged.connect(finishAlarmLogMenuCreation());
    }
}

function finishAlarmLogMenuCreation(bTouch) {
    console.log("Finish creation....")
    if(alarmLogMenuComponent.status === Component.Ready) {
        alarmLogMenuObj = alarmLogMenuComponent.createObject(mainitem,{"bEnteredByTouch":bTouch,
                                                                 "iAlarmLogMenuWidth":alarmLogMenuWidth,
                                                                 "iAlarmLogMenuHeight":alarmLogMenuHeight,
                                                                 "iAlarmLogMenuBorderWidth":alarmLogMenuBorderWidth ,
                                                                 "iAlarmLogMenuRadius":alarmLogMenuRadius,
                                                                 "strAlarmLogMenuBgColor":alarmLogMenuBgColor,
                                                                 "strAlarmLogMenuBorderColor":alarmLogMenuBorderColor,
                                                                 "strAlarmLogMenuLabelFontColor":alarmLogLabelFontColor,
                                                                 "ialarmLogMenuLabelFontSize": AlarmLogLabelFontSize ,
                                                                 "strAlarmTableBorderColor":alarmLogTableBorderColor,
                                                                 "iAlarmTableBorderWidth":alarmLogTableBorderWidth,
                                                                 "strAlarmLogMenuLabel":alarmLogMenuLabel,
                                                                 "strAlarmLogMenuLabelColor":alarmLogLabelFontColor,
                                                                 "strAlarmLogTimeLabel":alarmLogTableTimeLabel,
                                                                 "iAlarmLogTimeLabelFontSize":alarmLogTableTimeLabelFontSize,
                                                                 "strAlarmLogTimeLabelColor":alarmLogTableTimeLabelFontColor ,
                                                                 "strAlarmLogAlarmLabel": alarmLogTableAlarmLabel,
                                                                 "iAlarmLogAlarmLabelFontSize": alarmLogTableTimeLabelFontSize,
                                                                 "strAlarmLogAlarmLabelColor":alarmLogTableTimeLabelFontColor ,
                                                                 "strAlarmLogPriorityLabel":alarmLogTablePriorityLabel,
                                                                 "iAlarmLogPriorityLabelFontSize": alarmLogTableTimeLabelFontSize,
                                                                 "strAlarmLogPriorityLabelColor": alarmLogTableTimeLabelFontColor ,
                                                                 "iAlarmLogBackBtnWidth":alarmLogBackBtnwidth,
                                                                 "iAlarmLogBackBtnHeight":alarmLogBackBtnHeight,
                                                                 "strAlarmLogBackBtnBorderColor":alarmLogBackBtnBorderColor,
                                                                 "strAlarmLogBackBtnBgColor":alarmLogBackBtnBgColor,
                                                                 "strBackBtnSource":alarmLogMenuBackBtn,
                                                                 "strAlarmLogTchBgColor":alarmLogTchBgColor})

        var alarmGrpReference=getGroupRef("AlarmGroup")
        alarmLogMenuObj.anchors.top = alarmGrpReference.bottom;
        alarmLogMenuObj.anchors.topMargin = 5
    }
}

function destroyAlarmLogMenu() {
    var refalarmLogMenuObj = getGroupRef("AlarmLogMenuObj")
    if (refalarmLogMenuObj !== null && typeof(refalarmLogMenuObj) !== "undefined") {
        console.log("destroy alarm log menu")
        refalarmLogMenuObj.destroy()
        console.log("set group ref to null")
        refalarmLogMenuObj = null;
    } else {
        console.log("menu deletion : refalarmLogMenuObj === null")
    }
}

function navigateFocusTo(currentObjectName) {
    console.log("navigateFocusTo")
    var currentComp = getCompRef(idAlarmLogMenu, currentObjectName)
    currentComp.focus = true
}

function switchToBackBtn() {
    var alarmLogMenuObj = getGroupRef("AlarmLogMenuObj")
    var backBtnObj = getCompRef(alarmLogMenuObj, "BackBtnObj")
    backBtnObj.focus = true
}

function createAlarmScrollView(bTouch) {
    console.log("Create Alarm scroll view")
    alarmScrollViewComponent = Qt.createComponent("qrc:/Source/Generic/Qml/AlarmScrollView.qml")
    if(alarmScrollViewComponent.status === Component.Ready) {
        finishAlarmScrollViewCreation(bTouch);
    }else if(alarmScrollViewComponent.status === Component.Error) {
        console.log("Error loading component:", alarmScrollViewComponent.errorString());
    }else {
        alarmScrollViewComponent.statusChanged.connect(finishAlarmScrollViewCreation());
    }
}

function finishAlarmScrollViewCreation(bTouch) {
    console.log("Finish creation....",bTouch)
    if(alarmScrollViewComponent.status === Component.Ready) {
        alarmScrollViewObj = alarmScrollViewComponent.createObject(idAlarmLogMenu,{"x":11,
                                                                       "y":83,
                                                                       "strAlarmTableLineColor":alarmLogTableBorderColor,
                                                                       "iAlarmTableLineWidth":1,
                                                                       "strAlarmTableTextFontColor":alarmLogTableTextFontColor,
                                                                       "iAlarmTableTextFontSize":alarmLogTableTextFontSize,
                                                                       "iAlarmTableLeftRowWidth":alarmLogTableLeftColumnWidth,
                                                                       "iAlarmTableMiddleRowWidth":alarmLogTableMiddleColumnWidth,
                                                                       "iAlarmTableRightRowWidth":alarmLogTableRightColumnWidth,
                                                                       "iAlarmTableRowHeight":35,
                                                                       "iAlarmProgressBarBtnWidth":alarmLogProgressBarBtnWidth,
                                                                       "iAlarmProgressBarBtnHeight":alarmLogProgressBarBtnHeight,
                                                                       "strAlarmProgressBarBtnBorderColor":alarmLogProgressBarBtnBorderColor,
                                                                       "strAlarmProgressBarBtnBgColor":alarmLogProgressBarBtnBgColor,
                                                                       "iAlarmProgressBarBtnBorderWidth":alarmLogProgressBarBtnBorderWidth,
                                                                       "iAlarmProgressBarBtnRadius":alarmLogProgressBarBtnRadius,
                                                                       "strAlarmProgressBarAreaBgColor":alarmLogProgressBarAreaBgColor,
                                                                       "iAlarmProgressBarAreaWidth":alarmLogProgressBarAreaWidth,
                                                                       "iAlarmProgressBarHandleWidth":alarmLogProgressBarWidth,
                                                                       "strAlarmProgressBarHandleColor":alarmLogProgressBarBgColor,
                                                                       "strAlarmProgressBarHandleBorderColor":alarmLogProgressBarBorderColor,
                                                                       "iAlarmProgressBarHandleBorderWidth":alarmLogProgressBarBorderWidth,
                                                                       "strTableUpEnableSource":alarmLogMenuTableUpEnabled,
                                                                       "strTableUpTocuhedSource":alarmLogMenuTableUpTouched,
                                                                       "strTableDownEnableSource":alarmLogMenuTableDownEnabled,
                                                                       "strTableDownTocuhedSource":alarmLogMenuTableDownTouched,
                                                                       "strTableUpDisableSource":alarmLogMenuTableUpDisabled,
                                                                       "strTableDownDisableSource":alarmLogMenuTableDownDisabled})
        if (bTouch) {
            console.log("Entered through touch ")
            alarmScrollViewObj.removeHighlight();
            alarmScrollViewObj.setScrollView();
        }else {
            console.log("Entered through key")
            alarmScrollViewObj.removeHighlight();
            alarmScrollViewObj.setTableHighlight();
        }
    }
}

function shortcutInsideLogMenu() {
    console.log("Handling Short cut inside log menu")
    var alarmLogMenuObj = getGroupRef("AlarmLogMenuObj")
    var backBtnObj = getCompRef(alarmLogMenuObj, "BackBtnObj")
    if(backBtnObj.focus === true) {
        destroyAlarmLogMenu();
        //Setting the focus back to alarm log button in Alarm setup menu
        var alarmMenuObj = getCompRef(mainitem, "AlarmSetupMenuObj")
        var logBtnObj = getCompRef(alarmMenuObj, "AlarmLogObj")
        logBtnObj.focus = true
        alarmMenuObj.bTimerRunning = true
        alarmMenuObj.restartTimer()
        console.log("timer from touched comp");
    }
    // If highlight is on some log item, then on click of "k"/"13" will set the focus to back button
   else if(alarmScrollViewObj !==null) {
        if(AlarmDataModel.rowCount() !== 0 && (alarmScrollViewObj.getHighlightStatus() !== null)) {
            navigateFocusTo("BackBtnObj")
            alarmScrollViewObj.removeHighlight();
        }
    }else if(alarmScrollViewObj === null) {
        switchToBackBtn()
    }
}
