var ventComponent;
var modeComponent;
var settingsComponent;
var groupComponent
var moreMenuComponent;
var moreMenuSecondaryComponent;
var moreMenuSecondaryObj;
var moreMenuObj;
var ventObj;
var modeObj;
var settingsObj;
var groupObj;
//var iconComponent;
//var iconObj;
var alarmComponent;
var numericsComponent;
var waveComponent;
var alarmObj;
var numericsObj;
var waveObj;

var ventMenuComponent;
var ventMenuObj;
var ventMenuBtnComponent;
var ventMenuBtnObj;

var popupComponent;
var popupObj;
var alarmSetupMenuComponent;
var alarmSetupMenuObj = null;


//QuickKeys property
var quickKeysWidth = parseInt(component.getComponentXmlDataByTag("QuickKeyButton", "Width"))
var quickKeysHeight = parseInt(component.getComponentXmlDataByTag("QuickKeyButton", "Height"))
var quickKeysBorderColor = component.getComponentXmlDataByTag("QuickKeyButton", "BorderColor")
var quickKeysBorderWidth = parseInt(component.getComponentXmlDataByTag("QuickKeyButton", "BorderWidth"))
var quickKeysBorderRadius = parseInt(component.getComponentXmlDataByTag("QuickKeyButton", "BorderRadius"))
var quickKeysColor = component.getComponentXmlDataByTag("QuickKeyButton", "Color")
var quickKeysControlFontFamily = component.getComponentXmlDataByTag("QuickKeyButton", "VentControlFontFamily")
var quickKeysControlPixelSize = parseInt(component.getComponentXmlDataByTag("QuickKeyButton", "VentControlPixelSize"))
var quickKeysControlValueFontFamily = component.getComponentXmlDataByTag("QuickKeyButton", "VentControlValueFontFamily")
var quickKeysControlValuePixelSize = parseInt(component.getComponentXmlDataByTag("QuickKeyButton", "VentControlValuePixelSize"))
var quickKeysUnitFontFamily = component.getComponentXmlDataByTag("QuickKeyButton", "UnitFontFamily")
var quickKeysUnitPixelSize = parseInt(component.getComponentXmlDataByTag("QuickKeyButton", "UnitPixelSize"))
var quickkeyActiveStateColor = component.getComponentXmlDataByTag("QuickKeyButton", "Color")
var quickkeyDisabledStateColor = component.getComponentXmlDataByTag("QuickKeyButton", "DisabledStateColor")
var quickkeyEndOfScaleStateColor = component.getComponentXmlDataByTag("QuickKeyButton", "EndOfScaleStateColor")
var quickkeyEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("QuickKeyButton", "EndOfScaleStateBorderColor")
var quickkeyScrollStateBorderColor = component.getComponentXmlDataByTag("QuickKeyButton", "ScrollStateBorderColor")
var quickkeySelectedStateColor = component.getComponentXmlDataByTag("QuickKeyButton", "SelectedStateColor")
var quickkeySelectedStateBorderColor = component.getComponentXmlDataByTag("QuickKeyButton", "SelectedStateBorderColor")
var quickkeyTouchedStateColor = component.getComponentXmlDataByTag("QuickKeyButton", "TouchedStateColor")
var quickkeyStateHeightChange = parseInt(component.getComponentXmlDataByTag("QuickKeyButton", "StateHeightChange"))
var quickkeySpinnerUpEnabled = component.getComponentXmlDataByTag("QuickKeyButton", "QkSpinnerUpEnabled")
var quickkeySpinnerUpDisabled = component.getComponentXmlDataByTag("QuickKeyButton", "QkSpinnerUpDisabled")
var quickkeySpinnerDownDisabled = component.getComponentXmlDataByTag("QuickKeyButton", "QkSpinnerDownDisabled")
var quickkeySpinnerDownEnabled = component.getComponentXmlDataByTag("QuickKeyButton", "QkSpinnerDownEnabled")
//For quick key spinner
var quickeySpinnerArrowBtnHeight = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerArrowBtnHeight")
var quickeySpinnerArrowBtnWidth = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerArrowBtnWidth")
var quickkeySpinnerIconHeight = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerIconHeight")
var quickkeySpinnerIconWidth =  component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerIconWidth")
var quickkeySpinnerAreaHeight =  component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerAreaHeight")
var quickkeySpinnerAreaWidth  = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerAreaWidth")
var quickkeySpinnerSelStateColor = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerSelStateColor")
var quickkeySpinnerTchStateColor = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerTchStateColor")
var quickkeySpinnerScrlStateBorderColor = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerScrlStateBorderColor")
var quickkeySpinnerDisStateColor = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerDisStateColor")
var quickkeySpinnerSelStateBorderColor = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerSelStateBorderColor")
var quickkeySpinnerEoSStateColor = component.getComponentXmlDataByTag("QuickKeyButton","QkSpinnerEoSStateColor")

//SettingConfirmPopUp
var settingPopUpWidth = parseInt(component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "Width"))
var settingPopUpHeight = parseInt(component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "Height"))
var settingPopUpColor = component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "Color")
var popUpBtnHeight = parseInt(component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "PopUpBtnHeight"))
var popUpBtnWidth = parseInt(component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "PopUpBtnWidth"))
var popUpBtnColor = component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "PopUpBtnColor")
var popUpBtnRadius = parseInt(component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "PopUpBtnRadius"))
var popUpBtnMargin = parseInt(component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "PopUpBtnMargin"))
var settingConfirmLabel = component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "SettingConfirmLabel")
var settingBtnFontColor = component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "SettingBtnFontColor")
var settingBtnValuePixelSize = parseInt(component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "SettingBtnValuePixelSize"))
var settingCancelLabel = component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "SettingCancelLabel")
var popUpTriHeight = parseInt(component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "PopUpTriHeight"))
var popUpTriWidth = parseInt(component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "PopUpTriWidth"))
var popUpTriIconSource = component.getComponentXmlDataByTag("QuickKeySettingConfirmPopUp", "PopUpTriIconSource")
//MoreSettings Menu property
var strGroupnames = "VentGroup"
var strMoreSMode =settingReader.read("MODE")
var moreSettingsWidth = parseInt(group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", strMoreSMode, "MoreSettings", "MoreMenuGroup", "Width"))
var moreSettingsHeight = parseInt(group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", strMoreSMode, "MoreSettings", "MoreMenuGroup", "Height"))
var moreSettingsBorderWidth = parseInt(group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", strMoreSMode, "MoreSettings", "MoreMenuGroup", "BorderWidth"))
var moreSettingsBorderRadius =  parseInt(group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", strMoreSMode, "MoreSettings", "MoreMenuGroup", "BorderRadius"))
var moreSettingsLabelFontFamily = group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", strMoreSMode, "MoreSettings", "MoreMenuGroup", "MoreSettingsLabelFontFamily")
var moreSettingsLabelPixelsize = parseInt(group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", strMoreSMode, "MoreSettings", "MoreMenuGroup", "MoreSettingsLabelPixelsize"))
var moreSettingsBorderColor = group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", strMoreSMode, "MoreSettings", "MoreMenuGroup", "BorderColor")
var moreSettingsColor = group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", strMoreSMode, "MoreSettings", "MoreMenuGroup", "Color")

//MoreSettingParam property
var moreSettingParamWidth = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "Width"))
var moreSettingParamHeight = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "Height"))
var moreSettingParamLabelWidth = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "LabelWidth"))
var moreSettingParamLabelHeight = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "LabelHeight"))
var moreSettingParamLabelValueWidth = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "LabelValueWidth"))
var moreSettingParamLabelValueHeight = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "LabelValueHeight"))
var moreSettingParamBorderWidth = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "BorderWidth"))
var moreSettingParamBorderRadius = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "BorderRadius"))
var moreStringParamLabelValueColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "LabelValueColor")
var moreSettingParamColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "Color")
var moreSettingParamBorderColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "BorderColor")
var moreSettingParamActiveStateColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "LabelValueColor")
var moreSettingParamDisabledStateColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "DisabledStateColor")
var moreSettingParamEndOfScaleStateColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "EndOfScaleStateColor")
var moreSettingParamEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "EndOfScaleStateBorderColor")
var moreSettingParamScrollStateBorderColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "ScrollStateBorderColor")
var moreSettingParamSelectedStateColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "SelectedStateColor")
var moreSettingParamSelectedStateBorderColor =  component.getComponentXmlDataByTag("SecondaryParamSetting", "SelectedStateBorderColor")
var moreSettingParamTouchedStateColor =  component.getComponentXmlDataByTag("SecondaryParamSetting", "TouchedStateColor")
var moreSettingParamSpinnerUpEnabled =  component.getComponentXmlDataByTag("SecondaryParamSetting", "SpinnerUpEnabled")
var moreSettingParamSpinnerUpDisabled = component.getComponentXmlDataByTag("SecondaryParamSetting", "SpinnerUpDisabled")
var moreSettingParamSpinnerDownEnabled = component.getComponentXmlDataByTag("SecondaryParamSetting", "SpinnerDownEnabled")
var moreSettingParamSpinnerDownDisabled = component.getComponentXmlDataByTag("SecondaryParamSetting", "SpinnerDownDisabled")

var selectedStateWidth = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "StateWidthChange"))
var selectedStateHeight = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "SecParSpinArrowBtnHeight"))
var selectedStateArrowWidth = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "SecParSpinArrowBtnWidth"))
var selectedStateArrowHeight = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "SecParSpinArrowBtnHeight"))
var selectedStateArrowColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "SecParSpinBgColor")
var arrowWidth = parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "SecParSpinIconWidth"))
var arrowHeight =parseInt(component.getComponentXmlDataByTag("SecondaryParamSetting", "SecParSpinIconHeight"))
var touchedStateColor = component.getComponentXmlDataByTag("SecondaryParamSetting", "TouchedStateColor")
//VentModeButton property
var ventModeWidth =  parseInt(component.getComponentXmlDataByTag("VentMode", "Width"))
var ventModeHeight =  parseInt(component.getComponentXmlDataByTag("VentMode", "Height"))
var ventModeBorderWidth =  parseInt(component.getComponentXmlDataByTag("VentMode","BorderWidth"))
var ventStatusTextFontFamily = parseInt(component.getComponentXmlDataByTag("VentMode", "StatusTextFontFamily"))
var ventStatusTextFontSize  = parseInt(component.getComponentXmlDataByTag("VentMode", "StatusTextPixelSize"))
var ventStatusiconWidth = parseInt(component.getComponentXmlDataByTag("VentMode", "StatusIconWidth"))
var ventStatusiconHeight = parseInt(component.getComponentXmlDataByTag("VentMode", "StatusIconHeight"))
var ventModeBtnWidth = parseInt(component.getComponentXmlDataByTag("VentMode", "ModeWidth"))
var ventModeBtnHeight = parseInt(component.getComponentXmlDataByTag("VentMode", "ModeHeight"))
var ventModeBtnBorderWidth = parseInt(component.getComponentXmlDataByTag("VentMode", "ModeBorderWidth"))
var ventModeBtnRadius = parseInt(component.getComponentXmlDataByTag("VentMode", "ModeBorderRadius"))
var ventlabelFontFamily = component.getComponentXmlDataByTag("VentMode", "ModeFontFamily")
var ventlabelFontPixelSize = parseInt(component.getComponentXmlDataByTag("VentMode", "ModePixelSize"))
var ventText1FontFamily = component.getComponentXmlDataByTag("VentMode", "ModeValueFontFamily")
var ventText1FontPixelSize = parseInt(component.getComponentXmlDataByTag("VentMode", "ModeValuePixelSize"))
var ventModeBorderColor = component.getComponentXmlDataByTag("VentMode","BorderColor")
var ventModeColor = component.getComponentXmlDataByTag("VentMode", "StatusBackColor")
var ventModeBtnBorderColor = component.getComponentXmlDataByTag("VentMode", "ModeBorderColor")
var ventModeBtnColor = component.getComponentXmlDataByTag("VentMode", "Color")
var ventActiveStateColor = component.getComponentXmlDataByTag("VentMode", "Color")
var ventDisabledStateColor = component.getComponentXmlDataByTag("VentMode", "DisabledStateColor")
var ventEndOfScaleStateColor = component.getComponentXmlDataByTag("VentMode", "EndOfScaleStateColor")
var ventEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("VentMode", "EndOfScaleStateBorderColor")
var ventScrollStateBorderColor = component.getComponentXmlDataByTag("VentMode", "ScrollStateBorderColor")
var ventSelectedStateColor = component.getComponentXmlDataByTag("VentMode", "SelectedStateColor")
var ventSelectedStateBorderColor = component.getComponentXmlDataByTag("VentMode", "SelectedStateBorderColor")
var ventTouchedStateColor = component.getComponentXmlDataByTag("VentMode", "TouchedStateColor")
var ventStateHeightChange = parseInt(component.getComponentXmlDataByTag("VentMode", "StateHeightChange"))
//VentMoreSetting property
var ventMoreSettingWidth = parseInt(component.getComponentXmlDataByTag("MoreSetting", "Width"))
var ventMoreSettingHeight = parseInt(component.getComponentXmlDataByTag("MoreSetting", "Height"))
var ventMoreSettingBorderWidth = parseInt(component.getComponentXmlDataByTag("MoreSetting", "BorderWidth"))
var ventMoreSettingRadius = parseInt(component.getComponentXmlDataByTag("MoreSetting", "BorderRadius"))
var ventMoreSettingText1FontFamily = component.getComponentXmlDataByTag("MoreSetting", "LabelFontFamily")
var ventMoreSettingText1FontPixelSize = parseInt(component.getComponentXmlDataByTag("MoreSetting", "LabelPixelSize"))
var ventMoreSettingBorderColor = component.getComponentXmlDataByTag("MoreSetting", "BorderColor")
var ventMoreSettingColor = component.getComponentXmlDataByTag("MoreSetting", "Color")
var moreSettingActiveStateColor = component.getComponentXmlDataByTag("MoreSetting", "Color")
var moreSettingDisabledStateColor = component.getComponentXmlDataByTag("MoreSetting", "DisabledStateColor")
var moreSettingEndOfScaleStateColor = component.getComponentXmlDataByTag("MoreSetting", "EndOfScaleStateColor")
var moreSettingEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("MoreSetting", "EndOfScaleStateBorderColor")
var moreSettingScrollStateBorderColor = component.getComponentXmlDataByTag("MoreSetting", "ScrollStateBorderColor")
var moreSettingSelectedStateColor= component.getComponentXmlDataByTag("MoreSetting", "SelectedStateColor")
var moreSettingSelectedStateBorderColor = component.getComponentXmlDataByTag("MoreSetting", "SelectedStateBorderColor")
var moreSettingTouchedStateColor = component.getComponentXmlDataByTag("MoreSetting", "TouchedStateColor")
var moreSettingStateHeightChange = parseInt(component.getComponentXmlDataByTag("MoreSetting", "StateHeightChange"))

/*vent mode menu*/
var groupnames = "VentGroup"
var ventModeMenuWidth = parseInt(group.getComponentGroupDataByTag(groupnames,"VentMode","VentModeMenuGroup","Width"));
var ventModeMenuHeight = parseInt(group.getComponentGroupDataByTag(groupnames,"VentMode","VentModeMenuGroup","Height"));
var ventModeMenuColor = group.getComponentGroupDataByTag(groupnames,"VentMode","VentModeMenuGroup","Color");
var ventModeMenuBorderWidth = parseInt(group.getComponentGroupDataByTag(groupnames,"VentMode","VentModeMenuGroup","BorderWidth"));
var ventModeMenuBorderColor = group.getComponentGroupDataByTag(groupnames,"VentMode","VentModeMenuGroup","BorderColor");
var ventModeMenuRadius = parseInt(group.getComponentGroupDataByTag(groupnames,"VentMode","VentModeMenuGroup","BorderRadius"));
var ventModeMenuLabelFontFamily = group.getComponentGroupDataByTag(groupnames,"VentMode","VentModeMenuGroup","ModeMenuLabelFontFamily");
var ventModeMenuLabelPixelsize = parseInt(group.getComponentGroupDataByTag(groupnames,"VentMode","VentModeMenuGroup","ModeMenuLabelPixelsize"));

/*vent mode menu button*/
var modeMenuBtnWidth = parseInt(component.getComponentXmlDataByTag("ModeMenuButton", "Width"))
var modeMenuBtnHeight = parseInt(component.getComponentXmlDataByTag("ModeMenuButton", "Height"))
var modeMenuBtnColor = component.getComponentXmlDataByTag("ModeMenuButton", "Color")
var modeMenuBtnLabelHeight = parseInt(component.getComponentXmlDataByTag("ModeMenuButton", "LabelValueHeight"))
var modeMenuBtnLabelBorderColor = component.getComponentXmlDataByTag("ModeMenuButton", "BorderColor")
var modeMenuBtnLabelBorderWidth = parseInt(component.getComponentXmlDataByTag("ModeMenuButton", "BorderWidth"))
var modeMenuBtnLabelRadius = parseInt(component.getComponentXmlDataByTag("ModeMenuButton", "BorderRadius"))
var modeMenuBtnLabelColor = component.getComponentXmlDataByTag("ModeMenuButton", "LabelValueColor")
var modeMenuBtnLabelFontFamily = component.getComponentXmlDataByTag("ModeMenuButton", "LabelFontFamily")
var modeMenuBtnLabelPixelSize = parseInt(component.getComponentXmlDataByTag("ModeMenuButton", "LabelPixelSize"))
var modeExpansionLabelHeight = parseInt(component.getComponentXmlDataByTag("ModeMenuButton", "ModeExpansionLabelHeight"))
var modeExpansionLabelColor = component.getComponentXmlDataByTag("ModeMenuButton", "ModeExpansionLabelColor")
var modeExpansionFontFamily = component.getComponentXmlDataByTag("ModeMenuButton", "ModeExpansionFontFamily")
var modeExpansionPixelSize = parseInt(component.getComponentXmlDataByTag("ModeMenuButton", "ModeExpansionPixelSize"))
var ventMenuBtnActiveStateColor = component.getComponentXmlDataByTag("ModeMenuButton", "LabelValueColor");
var ventMenuBtnDisabledStateColor = component.getComponentXmlDataByTag("ModeMenuButton", "DisabledStateColor");
var ventMenuBtnEndOfScaleStateColor = component.getComponentXmlDataByTag("ModeMenuButton", "EndOfScaleStateColor");
var ventMenuBtnEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("ModeMenuButton", "EndOfScaleStateBorderColor");
var ventMenuBtnScrollStateBorderColor = component.getComponentXmlDataByTag("ModeMenuButton", "ScrollStateBorderColor");
var ventMenuBtnSelectedStateColor = component.getComponentXmlDataByTag("ModeMenuButton", "SelectedStateColor");
var ventMenuBtnSelectedStateBorderColor = component.getComponentXmlDataByTag("ModeMenuButton", "SelectedStateBorderColor");
var ventMenuBtnTouchedStateColor = component.getComponentXmlDataByTag("ModeMenuButton", "TouchedStateColor")

/*vent mode Confirmation popup*/
var popupWidth = parseInt(component.getComponentXmlDataByTag("ModeConfirmPopup", "Width"))
var popupHeight = parseInt(component.getComponentXmlDataByTag("ModeConfirmPopup", "Height"))
var popupRadius = parseInt(component.getComponentXmlDataByTag("ModeConfirmPopup", "BorderRadius"))
var popupBorderColor =component.getComponentXmlDataByTag("ModeConfirmPopup", "BorderColor")
var popupColor = component.getComponentXmlDataByTag("ModeConfirmPopup", "Color")
var popupLabelFontFamily = component.getComponentXmlDataByTag("ModeConfirmPopup", "LabelFontFamily")
var popupTipTriangleSource = component.getComponentXmlDataByTag("ModeConfirmPopup", "PopUpTriIconSource")
var popupCancelLabel = component.getComponentXmlDataByTag("ModeConfirmPopup", "PopupCancelLabel")
var popupCancelBorderColor = component.getComponentXmlDataByTag("ModeConfirmPopup", "CancelBtnColor")
var popupLabelPixelSize = parseInt(component.getComponentXmlDataByTag("ModeConfirmPopup", "LabelPixelSize"))
var popupFontColor = component.getComponentXmlDataByTag("ModeConfirmPopup", "labelFontColor")

/*Numerics*/
var numColor = component.getComponentXmlDataByTag("Numerics", "Color")
var numBorderColor = component.getComponentXmlDataByTag("Numerics", "BorderColor")
var numBorderWidth = parseInt(component.getComponentXmlDataByTag("Numerics", "BorderWidth"))

/*Alarm*/
var alarmColor = component.getComponentXmlDataByTag("Alarm", "Color")
var alarmBorderColor = component.getComponentXmlDataByTag("Alarm", "BorderColor")
var alarmBorderWidth = parseInt(component.getComponentXmlDataByTag("Alarm", "BorderWidth"))


/*Alarm setup Menu */
var alarmMenuLabel = component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmSetupMenuLabel")
var alarmMenuWidth = parseInt(component.getComponentXmlDataByTag("AlarmSetUpMenu", "Width"))
var alarmMenuHeight = parseInt(component.getComponentXmlDataByTag("AlarmSetUpMenu", "Height"))
var alarmMenuRadius = parseInt(component.getComponentXmlDataByTag("AlarmSetUpMenu", "BorderRadius"))
var alarmMenuBorderWidth = parseInt(component.getComponentXmlDataByTag("AlarmSetUpMenu", "BorderWidth"))
var alarmLogWidth = parseInt(component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmLogWidth"))
var alarmLogHeight = parseInt(component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmLogHeight"))
var alarmLogRadius = parseInt(component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmLogBorderRadius"))
var alarmLogBorderWidth = parseInt(component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmLogBorderWidth"))
var alarmMenuBgColor = component.getComponentXmlDataByTag("AlarmSetUpMenu", "Color")
var alarmBorderColor = component.getComponentXmlDataByTag("AlarmSetUpMenu", "BorderColor")
var alarmLogBgColor = component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmLogColor")
var alarmLogBorderColor = component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmLogBorderColor")
var limitLabel1 = component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmSetupLowLimitLabel")
var limitLabel2 = component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmSetupHighLimitLabel")
var logLabel = component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmSetupLogLabel")
var fontFamilyAlarmSet = component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmLabelFontFamily")
var fontColor = component.getComponentXmlDataByTag("AlarmSetUpMenu", "LimitLabelFontColor")
var pixelSizeAlarmSet = component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmSetupLabelFontSize")
var pixelSizeLimitLabel = component.getComponentXmlDataByTag("AlarmSetUpMenu", "LimitLabelFontSize")
var pixelSizeLogLabel = component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmLogFontSize")
var alarmPpeakLabel = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamPpeak")
var alarmPpeakUnit = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamPpeakUnit")
var alarmMVLabel = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamMV")
var alarmMVUnit = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamMVUnit")
var alarmTVexpLabel = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamTVexp")
var alarmTVexpUnit = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamTVexpUnit")
var alarmO2Label = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamO2")
var alarmO2Unit = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamO2Unit")
var alarmApneaLabel = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamApneaDelay")
var alarmApneaUnit = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamApneaDelayUnit")
var alarmVolumeLabel = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamAlarm")
var alarmVolumeUnit = component.getComponentXmlDataByTag("AlarmSetUpMenu","AlarmSetupParamAlarmUnit")
var alarmParamScrollStateBorderColor = component.getComponentXmlDataByTag("AlarmSetupButton", "AlarmParamScrollStateBorderColor")
var alarmParamScrollStateBorderWidth = parseInt(component.getComponentXmlDataByTag("AlarmSetupButton", "AlarmParamScrollStateBorderWidth"))
/*icon*/
var iconColor = component.getComponentXmlDataByTag("Icon", "Color")
var iconBorderColor = component.getComponentXmlDataByTag("Icon", "BorderColor")
var iconBorderWidth = parseInt(component.getComponentXmlDataByTag("Icon", "BorderWidth"))

/*Waveform*/
var waveColor = component.getComponentXmlDataByTag("Waveform", "Color")
var waveBorderColor = component.getComponentXmlDataByTag("Waveform", "BorderColor")
var waveBorderWidth = parseInt(component.getComponentXmlDataByTag("Waveform", "BorderWidth"))

var objs = []

function createObjects() {
    ventComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentBtn.qml")
    modeComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentModeBtn.qml")
    settingsComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentMoreSettingBtn.qml")
   // iconComponent = Qt.createComponent("qrc:/Source/ButtonBarArea/Qml/IconBtn.qml")
    //alarmComponent = Qt.createComponent("qrc:/Source/AlarmArea/Qml/AlarmSettings.qml")
    numericsComponent = Qt.createComponent("qrc:/Source/NumericsArea/Qml/NumericsArea.qml")
    waveComponent = Qt.createComponent("qrc:/Source/WaveFormArea/Qml/WaveGroup.qml")
    if (ventComponent.status === Component.Ready && modeComponent.status === Component.Ready
            && settingsComponent.status === Component.Ready 
            && numericsComponent.status === Component.Ready
            && waveComponent.status === Component.Ready){
        finishObjectCreation();
    } else if (ventComponent.status === Component.Error || modeComponent.status === Component.Error
               || settingsComponent.status === Component.Error  ||
                numericsComponent.Error === Component.Error
               || waveComponent.Error === Component.Error) {
        console.log("Error loading component:", ventComponent.errorString());
        console.log("Error loading component:", modeComponent.errorString());
        console.log("Error loading component:", settingsComponent.errorString());
        //console.log("Error loading component:", iconComponent.errorString());
       // console.log("Error loading component:", alarmComponent.errorString());
        console.log("Error loading component:", numericsComponent.errorString());
        console.log("Error loading component:", waveComponent.errorString());
    }else{
        ventComponent.statusChanged.connect(finishObjectCreation);
        modeComponent.statusChanged.connect(finishObjectCreation);
        settingsComponent.statusChanged.connect(finishObjectCreation);
        //iconComponent.statusChanged.connect(finishObjectCreation);
        //alarmComponent.statusChanged.connect(finishObjectCreation);
        numericsComponent.statusChanged.connect(finishObjectCreation);
        waveComponent.statusChanged.connect(finishObjectCreation);
    }
}

function finishObjectCreation(){
    if (ventComponent.status === Component.Ready && modeComponent.status === Component.Ready
            && settingsComponent.status === Component.Ready 
             && numericsComponent.status === Component.Ready
            && waveComponent.status === Component.Ready) {
        var complength = group.getMainComponentLength(strGroupname);

        for(var maincompindex = 0; maincompindex<complength; maincompindex++){
            var comptype = group.getMainComponentType(maincompindex, strGroupname);

            if(comptype === "VentMode"){
                var modeLab = settingReader.read("MODE")
                backupData.validateParams("MODE",modeLab);
                var subgroupComplength = group.getSubGroupLength(strGroupname, comptype, "VentSetting", modeLab)
                //var compMode;
                modeObj = modeComponent.createObject(groupRect.parentid,{"iCompID":0,"strModeLabel":modeLab ,"focus": maincompindex==0 ?true:false,
                                                                                                                                         "iVentModeWidth":ventModeWidth,"iVentModeHeight":ventModeHeight,
                                                                                                                                         "iVentModeBorderWidth":ventModeBorderWidth,"strVentModeBorderColor":ventModeBorderColor,
                                                                                                                                         "strVentModeColor":ventModeColor,
                                                                                                                                         "strVentModeActiveStateColor":ventActiveStateColor,
                                                                                                                                         "strVentModeDisabledStateColor":ventDisabledStateColor,
                                                                                                                                         "strVentModeEndOfScaleStateColor" : ventEndOfScaleStateColor,
                                                                                                                                         "strVentModeEndOfScaleStateBorderColor":ventEndOfScaleStateBorderColor,
                                                                                                                                         "strVentModeScrollStateBorderColor":ventScrollStateBorderColor,
                                                                                                                                         "strVentModeSelectedStateColor":ventSelectedStateColor,
                                                                                                                                         "strVentModeSelectedStateBorderColor":ventSelectedStateBorderColor,
                                                                                                                                         "strVentModeTouchedStateColor":ventTouchedStateColor,
                                                                                                                                         "iVentModeStateHeightChange":ventStateHeightChange,
                                                                                                                                         "strStatusTextFontFamily":ventStatusTextFontFamily,"iStatusTextFontSize":ventStatusTextFontSize,
                                                                                                                                         "iStatusiconWidth":ventStatusiconWidth,"iStatusiconHeight":ventStatusiconHeight,
                                                                                                                                         "iventModeBtnWidth":ventModeBtnWidth,"iventModeBtnHeight":ventModeBtnHeight,
                                                                                                                                         "strventModeBtnBorderColor":ventModeBtnBorderColor,"iventModeBtnBorderWidth":ventModeBtnBorderWidth,
                                                                                                                                         "iventModeBtnRadius":ventModeBtnRadius,"strventModeBtnColor":ventModeBtnColor,
                                                                                                                                         "strlabelFontFamily": ventlabelFontFamily,"ilabelFontPixelSize":ventlabelFontPixelSize,
                                                                                                                                         "strText1FontFamily": ventText1FontFamily,"iText1FontPixelSize":ventText1FontPixelSize
                                                     });
                modeObj.rotatedClockwise.connect(onRotatedClockwise)
                modeObj.rotatedAntiClockwise.connect(onRotatedAntiClockwise)
                modeObj.ventModeTouched.connect(onVentModeTouched)
                modeObj.ventModeSelected.connect(onVentModeSelected)
                modeObj.differentVentModeSelected.connect(onSelectionOfDifferentVentMode)
                modeObj.scrollActiveStateChangeTimerTriggerred.connect(onChangeScrollActiveStateTimerTriggerred)

                //keyboard keys
                modeObj.shortCutKeyPressed.connect(onShortCutKeySelected)
                //end
                if (modeObj === null) {
                    // Error Handling
                    console.log("Error creating object");
                }
                createQKsAndMoreSettingsComponent(subgroupComplength,modeLab,comptype)
            }else if(comptype === "Waveform"){
                waveObj = waveComponent.createObject(groupRect.parentid,{"iCompID":maincompindex,"strWaveColor":waveColor,"strWaveBorderColor":waveBorderColor,"iWaveBorderWidth":waveBorderWidth})
                if (waveObj === null) {
                    // Error Handling
                    console.log("Error creating object");
                }
                waveObj.touchedWaveGroup.connect(otherGroupTouched)
                //waveObj.touchedWaveGroup.connect(appwindow.groupsTouched)

            }else if(comptype === "Numerics"){
                numericsObj = numericsComponent.createObject(groupRect.parentid,{"iCompID":maincompindex,"strNumColor":numColor,"strNumBorderColor":numBorderColor,"iNumBorderWidth":numBorderWidth})
                if (numericsObj === null) {
                    // Error Handling
                    console.log("Error creating object");
                }
                numericsObj.touchedNumericGroup.connect(otherGroupTouched)

            }
        }

    } else if (ventComponent.status === Component.Error || modeComponent.status === Component.Error || settingsComponent.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", ventComponent.errorString());
    }
}

function finishObjectCreationOnModeChange(currentMode){
    if (ventComponent.status === Component.Ready && settingsComponent.status === Component.Ready) {
        var complength = group.getMainComponentLength(strGroupname);
        for(var maincompindex = 0; maincompindex<complength; maincompindex++){
            var comptype = group.getMainComponentType(maincompindex, strGroupname);
            if(comptype === "VentMode"){
                var modeLab = currentMode + "_mode"
                console.log("modelab is :" +modeLab )
                var subgroupComplength = group.getSubGroupLength(strGroupname, comptype, "VentSetting", modeLab)
                createQKsAndMoreSettingsComponent(subgroupComplength,modeLab,comptype)
            }
        }
    }
}

function createQKsAndMoreSettingsComponent(subgroupComplength,modeLab,comptype) {
    for(var subgroupcompindex=0; subgroupcompindex < subgroupComplength ; subgroupcompindex++){
        var subgroup = group.getComponentType(subgroupcompindex,strGroupname, comptype, "VentSetting", modeLab);
        if(subgroup === "QuickKey"){
            var modeParamLabel = group.getComponentSubGroupDataByTag(subgroupcompindex,strGroupname, comptype, "VentSetting", modeLab, "VentParam");
            var paramValue = settingReader.read(modeParamLabel);
            backupData.validateParams(modeParamLabel,paramValue);
            ventObj = ventComponent.createObject(groupRect.parentid , {"iCompID": subgroupcompindex+1,"strName":modeParamLabel,  "strValue": settingReader.read(modeParamLabel),"strUnit":group.getComponentSubGroupDataByTag(subgroupcompindex,strGroupname, comptype, "VentSetting", modeLab, "Unitlabel"),
                                                     "iVentWidth":quickKeysWidth,
                                                     "iVentHeight":quickKeysHeight,
                                                     "strVentBorderColor":quickKeysBorderColor,
                                                     "iVentBorderWidth":quickKeysBorderWidth,"iVentRadius":quickKeysBorderRadius,"strVentcolor":quickKeysColor,
                                                     "strFontFamilylabel":quickKeysControlFontFamily,"iPixelSizelabel":quickKeysControlPixelSize,
                                                     "strFontFamilyText":quickKeysControlValueFontFamily,"iPixelSizeText":quickKeysControlValuePixelSize,
                                                     "strFontFamilylabel1":quickKeysUnitFontFamily,"iPixelSizelabel1":quickKeysUnitPixelSize,
                                                     "strVentActiveStateColor":quickkeyActiveStateColor,
                                                     "strVentDisabledStateColor":quickkeyDisabledStateColor,
                                                     "strVentEndOfScaleStateColor" : quickkeyEndOfScaleStateColor,
                                                     "strVentEndOfScaleStateBorderColor" : quickkeyEndOfScaleStateBorderColor,
                                                     "strVentScrollStateBorderColor" : quickkeyScrollStateBorderColor,
                                                     "strVentSelectedStateColor" : quickkeySelectedStateColor,
                                                     "strVentSelectedStateBorderColor" : quickkeySelectedStateBorderColor,
                                                     "strVentTouchedStateColor" : quickkeyTouchedStateColor,
                                                     "iVentStateHeightChange" : quickkeyStateHeightChange,
                                                     "strQuickkeySpinnerUpEnabled" : quickkeySpinnerUpEnabled,
                                                     "strQuickkeySpinnerUpDisabled" : quickkeySpinnerUpDisabled,
                                                     "strQuickkeySpinnerDownDisabled" : quickkeySpinnerDownDisabled,
                                                     "strQuickkeySpinnerDownEnabled" : quickkeySpinnerDownEnabled,
                                                     "iSettingPopUpWidth" : settingPopUpWidth,
                                                     "iSettingPopUpHeight" : settingPopUpHeight,
                                                     "strSettingPopUpColor" : settingPopUpColor,
                                                     "iPopUpBtnHeight" : popUpBtnHeight,
                                                     "iPopUpBtnWidth" : popUpBtnWidth,
                                                     "strPopUpBtnColor" : popUpBtnColor,
                                                     "iPopUpBtnRadius" : popUpBtnRadius,
                                                     "iPopUpBtnMargin" : popUpBtnMargin,
                                                     "strSettingConfirmLabel" : settingConfirmLabel,
                                                     "strSettingBtnFontColor" : settingBtnFontColor,
                                                     "iSettingBtnValuePixelSize" : settingBtnValuePixelSize,
                                                     "strSettingCancelLabel" : settingCancelLabel,
                                                     "iPopUpTriHeight" : popUpTriHeight,
                                                     "iPopUpTriWidth" : popUpTriWidth,
                                                     "strPopUpTriIconSource" : popUpTriIconSource,
                                                     "iQuickeySpinnerArrowBtnHeight": quickeySpinnerArrowBtnHeight,
                                                     "iQuickeySpinnerArrowBtnWidth" :quickeySpinnerArrowBtnWidth ,
                                                     "iQuickkeySpinnerIconHeight": quickkeySpinnerIconHeight,
                                                     "iQuickkeySpinnerIconWidth": quickkeySpinnerIconWidth,
                                                     "iQuickkeySpinnerAreaHeight": quickkeySpinnerAreaHeight,
                                                     "iQuickkeySpinnerAreaWidth" : quickkeySpinnerAreaWidth ,
                                                     "strQuickkeySpinnerSelStateColor": quickkeySpinnerSelStateColor,
                                                     "strQuickkeySpinnerTchStateColor": quickkeySpinnerTchStateColor ,
                                                     "strQuickkeySpinnerScrlStateBorderColor" : quickkeySpinnerScrlStateBorderColor,
                                                     "strQuickkeySpinnerDisStateColor": quickkeySpinnerDisStateColor ,
                                                     "strQuickkeySpinnerSelStateBorderColor": quickkeySpinnerSelStateBorderColor,
                                                     "strQuickkeySpinnerEoSStateColor": quickkeySpinnerEoSStateColor
                                                 });
            ventObj.rotatedClockwise.connect(onRotatedClockwise)
            ventObj.rotatedAntiClockwise.connect(onRotatedAntiClockwise)
            ventObj.ventEnterPressed.connect(onEnterPressed)
            ventObj.totalIntTimerTriggered.connect(onTimerTriggered)
            ventObj.compTouched.connect(onCompTouched)
            ventObj.compSelected.connect(onCompSelected)
            ventObj.tenSecondsTimerIsTriggerred.connect(onInitialWaitingTimerTriggered)
            ventObj.noValueChangeInTenSeconds.connect(onNoValueChangeInTenSeconds)
            ventObj.scrollActiveStateChangeTimerTriggerred.connect(onChangeScrollActiveStateTimerTriggerred)

            //keyboard keys
            ventObj.shortCutKeyPressed.connect(onShortCutKeySelected)
            ventObj.shortCutKeyReleased.connect(onShortCutKeyReleased)
            //end
            if (ventObj === null) {
                // Error Handling
                console.log("Error creating object");
            }
        } else if(subgroup === "MoreSettings"){

            settingsObj =  settingsComponent.createObject(groupRect.parentid,{"iCompID":subgroupcompindex+1,"strMoreSetting":group.getComponentSubGroupDataByTag(subgroupcompindex,strGroupname, comptype, "VentSetting", modeLab, "Label"),
                                                              "iMoreSettingWidth" : ventMoreSettingWidth,
                                                              "iMoreSettingHeight" : ventMoreSettingHeight,
                                                              "iMoreSettingBorderWidth" : ventMoreSettingBorderWidth,
                                                              "iMoreSettingRadius" : ventMoreSettingRadius,
                                                              "strMoreSettingBorderColor" : ventMoreSettingBorderColor,
                                                              "strMoreSettingColor" : ventMoreSettingColor,
                                                              "strMoreSettingText1FontFamily":ventMoreSettingText1FontFamily,
                                                              "iMoreSettingText1FontPixelSize":ventMoreSettingText1FontPixelSize,
                                                              "strMoreSettingActiveStateColor" : moreSettingActiveStateColor,
                                                              "strMoreSettingDisabledStateColor": moreSettingDisabledStateColor,
                                                              "strMoreSettingEndOfScaleStateColor": moreSettingEndOfScaleStateColor,
                                                              "strMoreSettingEndOfScaleStateBorderColor": moreSettingEndOfScaleStateBorderColor,
                                                              "strMoreSettingScrollStateBorderColor": moreSettingScrollStateBorderColor,
                                                              "strMoreSettingTouchedStateColor": moreSettingTouchedStateColor,
                                                              "iMoreSettingStateHeightChange": moreSettingStateHeightChange
                                                          });
            settingsObj.rotatedClockwise.connect(onRotatedClockwise)
            settingsObj.rotatedAntiClockwise.connect(onRotatedAntiClockwise)
            settingsObj.settingsCompTouched.connect(onSettingsCompTouched)
            settingsObj.settingsCompSelected.connect(onSettingsCompSelected)
            //keyboard keys
            settingsObj.shortCutKeyPressed.connect(onShortCutKeySelected)
            //end
            settingsObj.scrollActiveStateChangeTimerTriggerred.connect(onChangeScrollActiveStateTimerTriggerred)
            settingsObj.destroyAlarmMenu.connect(onDestroyAlarmMenu)
            if (settingsObj === null) {
                // Error Handling
                console.log("Error creating object");
            }
        }
    }
}

function createGroups(){
    groupComponent = Qt.createComponent("qrc:/Source/Generic/Qml/Group.qml");
    if(groupComponent.status === Component.Ready){
        finishGroupCreation()
    }else if (groupComponent.status === Component.Error 
              || numericsComponent.Error === Component.Error
              || waveComponent.Error === Component.Error) {
        console.log("Error loading component:", groupComponent.errorString());
    }else{
        groupComponent.statusChanged.connect(finishGroupCreation);
    }
}

function finishGroupCreation(){
    if(groupComponent.status === Component.Ready){
        var groupLength = layout.getGroupLength();
        var groupdata
        var groupType;
        for(var i = 0; i< groupLength; i++) {
            groupType = layout.getGroupName(i);
            groupObj = groupComponent.createObject(mainitem, {"strGroupname" : groupType,"x" : layout.getGroupLayoutDataByTag(groupType,"Position","x"),"y" : layout.getGroupLayoutDataByTag(groupType,"Position","y"),"width" :group.getMainGroupDataByTag(groupType, "Width"),"height" : group.getMainGroupDataByTag(groupType, "Height"),"color":group.getMainGroupDataByTag(groupType, "Color"),"firstfocus":"false"});
            objs.push(groupObj)
        }
    }
}

/*Creating more settings menu*/
function createMoreSettingMenu(strName){
    moreMenuComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/MoreSettingsMenu.qml")
    if (moreMenuComponent.status === Component.Ready){
        finishMenuCreation(strName);
    } else if (moreMenuComponent.status === Component.Error) {
        console.log("Error loading component:", moreMenuComponent.errorString());
    }else{
        moreMenuComponent.statusChanged.connect(finishMenuCreation(strName));
    }
}

function finishMenuCreation(strname){
    if(moreMenuComponent.status === Component.Ready){
        var modeLab = settingReader.read("MODE")
        moreMenuObj = moreMenuComponent.createObject(moreSettingId,{"strMoreSLabel":group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", modeLab, "MoreSettings", "MoreMenuGroup", "MoreSettingLabel"),
                                                         "x":-1104,"y":-585,
                                                         "strMoreSMode":group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", modeLab, "MoreSettings", "MoreMenuGroup", "MoreSettingModeLabel"),
                                                         "iMoreMenuWidth":moreSettingsWidth,
                                                         "iMoreMenuHeight":moreSettingsHeight,
                                                         "strMoreMenuBorderColor":moreSettingsBorderColor,
                                                         "iMoreMenuBorderWidth":moreSettingsBorderWidth,
                                                         "iMoreMenuRadius":moreSettingsBorderRadius,
                                                         "strMoreMenuColor":moreSettingsColor,
                                                         "strFontFamilyMoreSetLabel":moreSettingsLabelFontFamily,
                                                         "iPixelSizeMoreSet":moreSettingsLabelPixelsize
                                                     });
        moreMenuObj.moreSettingsMenuClosed.connect(onMoreSettingsMenuClosed);
    }
}

function createMenuComponents(){
    moreMenuSecondaryComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/MoreSettingsParam.qml")
    if (moreMenuSecondaryComponent.status === Component.Ready){
        finishMenuComponentCreation();
    } else if (moreMenuSecondaryComponent.status === Component.Error) {
        console.log("Error loading component:", moreMenuSecondaryComponent.errorString());
    }else{
        moreMenuSecondaryComponent.statusChanged.connect(finishMenuComponentCreation);
    }
}

function finishMenuComponentCreation(){
    if(moreMenuSecondaryComponent.status === Component.Ready){
        var modeLab = settingReader.read("MODE")
        var complength = group.getSecondarySubGroupLength(strGroupnames/*strGroupname*/, "VentMode", "VentSetting", modeLab, "MoreSettings", "MoreMenuGroup");
        for(var morecompindex=0; morecompindex<complength ; morecompindex++){
            //console.log("more setting component"+modeLab+complength+group.getSecondaryComponentSubGroupDataByTag(morecompindex,strGroupname, "VentMode", "VentSetting", modeLab, "MoreSettings", "MoreMenuGroup","SecondaryParamLabel"))
            var secLabel = group.getSecondaryComponentSubGroupDataByTag(morecompindex,strGroupnames, "VentMode", "VentSetting", modeLab, "MoreSettings", "MoreMenuGroup","SecondaryParamLabel");
            var secParamValue = settingReader.read(secLabel);
            backupData.validateParams(secLabel,secParamValue);
            moreMenuSecondaryObj=moreMenuSecondaryComponent.createObject(moreMenuId.parentid,{"iCompID": morecompindex,
                                                                             "strValue": settingReader.read(secLabel),
                                                                             "strSecondaryParamLabel":group.getSecondaryComponentSubGroupDataByTag(morecompindex, strGroupnames, "VentMode", "VentSetting", modeLab, "MoreSettings", "MoreMenuGroup", "SecondaryParamLabel"),
                                                                             "strSecondaryParamUnit":group.getSecondaryComponentSubGroupDataByTag(morecompindex, strGroupnames, "VentMode", "VentSetting", modeLab, "MoreSettings", "MoreMenuGroup", "SecondaryParamUnit"),
                                                                             "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                             "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                             "strMoreSettingParamColor":moreSettingParamColor,
                                                                             "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                             "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                             "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                             "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                             "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                             "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                             "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                             "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                             "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                             "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                             "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                             "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                             "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                             "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                             "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                             "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                             "iSelectedStateWidth" : selectedStateWidth,
                                                                             "iSelectedStateHeight" :selectedStateHeight,
                                                                             "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                             "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                             "strTouchedStateColor" : touchedStateColor,
                                                                             "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                             "iArrowWidth" : arrowWidth ,
                                                                             "iArrowHeight" : arrowHeight
                                                                         });
            moreMenuSecondaryObj.rotatedClockwise.connect(onRotatedClockwise)
            moreMenuSecondaryObj.rotatedAntiClockwise.connect(onRotatedAntiClockwise)

            moreMenuSecondaryObj.ventSecParamEnterPressed.connect(onVentSecParamEnterPressed)
            moreMenuSecondaryObj.compTouched.connect(onCompTouched)
            moreMenuSecondaryObj.compSelected.connect(onCompSelected)
            moreMenuSecondaryObj.restartMoreSetMenuCloseTimer.connect(onRestartMoreSetMenuCloseTimer)
        }
    }
}

function createVentModeMenu(){
    ventMenuComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentMenu.qml")
    if (ventMenuComponent.status === Component.Ready){
        finishVentMenuCreation();
    } else if (ventMenuComponent.status === Component.Error) {
        console.log("Error loading component:", ventMenuComponent.errorString());
    }else{
        ventMenuComponent.statusChanged.connect(finishVentMenuCreation());
    }
}

function finishVentMenuCreation(){
    if(ventMenuComponent.status === Component.Ready){
        ventMenuObj = ventMenuComponent.createObject(ventModeId,{"x":0,"y":-585,"strMenuTitle":group.getComponentGroupDataByTag(strGroupnames,"VentMode","VentModeMenuGroup","ModeMenuLabel"),
                                                         "iVentModeMenuWidth":ventModeMenuWidth,
                                                         "iVentModeMenuHeight":ventModeMenuHeight,
                                                         "strVentModeMenuColor":ventModeMenuColor,
                                                         "iVentModeMenuBorderWidth":ventModeMenuBorderWidth,
                                                         "strVentModeMenuBorderColor":ventModeMenuBorderColor,
                                                         "iVentModeMenuRadius":ventModeMenuRadius,
                                                         "ventModeMenuLabelFontFamily":ventModeMenuLabelFontFamily,
                                                         "ventModeMenuLabelPixelsize":ventModeMenuLabelPixelsize
                                                     });
        ventMenuObj.ventMenuClosed.connect(onVentMenuClosed)
        ventMenuObj.differentModeSelected.connect(onModeSelectionInMenu)
    }
}

function createVentMenuComponents(){
    ventMenuBtnComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentMode.qml")
    if (ventMenuBtnComponent.status === Component.Ready){
        finishVentMenuComponentCreation();
    } else if (ventMenuBtnComponent.status === Component.Error) {
        console.log("Error loading component:", ventMenuBtnComponent.errorString());
    }else{
        ventMenuBtnComponent.statusChanged.connect(finishVentMenuComponentCreation);
    }
}

function finishVentMenuComponentCreation(){
    if(ventMenuBtnComponent.status === Component.Ready){
        var modes = ["VCV_mode","PCV_mode","SIMV VCV_mode","SIMV PCV_mode","PSVPro_mode","Bypass_mode"]
        var modesName = ["VCV_Exp","PCV_Exp","SIMV VCV_Exp","SIMV PCV_Exp","PSVPro_Exp","Bypass_Exp"]
        for(var mode =0 ;mode<modes.length;mode++){
            if(mode <3){
                ventMenuBtnObj = ventMenuBtnComponent.createObject(ventMenu.menuModesID,{"iCompID":mode,"strModeName":modes[mode],"strExpModeName":modesName[mode],"strModeCompState":"Active",
                                                                       "iModeMenuBtnWidth":modeMenuBtnWidth,
                                                                       "iModeMenuBtnHeight":modeMenuBtnHeight,
                                                                       "strModeMenuBtnColor":modeMenuBtnColor,
                                                                       "iModeMenuBtnLabelHeight":modeMenuBtnLabelHeight,
                                                                       "strModeMenuBtnLabelBorderColor":modeMenuBtnLabelBorderColor,
                                                                       "iModeMenuBtnLabelBorderWidth":modeMenuBtnLabelBorderWidth,
                                                                       "iModeMenuBtnLabelRadius":modeMenuBtnLabelRadius,
                                                                       "strModeMenuBtnLabelColor":modeMenuBtnLabelColor,
                                                                       "strModeMenuBtnLabelFontFamily":modeMenuBtnLabelFontFamily,
                                                                       "iModeMenuBtnLabelPixelSize":modeMenuBtnLabelPixelSize,
                                                                       "iModeExpansionLabelHeight":modeExpansionLabelHeight,
                                                                       "strModeExpansionLabelColor":modeExpansionLabelColor,
                                                                       "strModeExpansionFontFamily":modeExpansionFontFamily,
                                                                       "iModeExpansionPixelSize":modeExpansionPixelSize,
                                                                       "strVentMenuBtnActiveStateColor":ventMenuBtnActiveStateColor,
                                                                       "strVentMenuBtnDisabledStateColor":ventMenuBtnDisabledStateColor,
                                                                       "strVentMenuBtnEndOfScaleStateColor":ventMenuBtnEndOfScaleStateColor,
                                                                       "strVentMenuBtnEndOfScaleStateBorderColor":ventMenuBtnEndOfScaleStateBorderColor,
                                                                       "strVentMenuBtnScrollStateBorderColor":ventMenuBtnScrollStateBorderColor,
                                                                       "strVentMenuBtnSelectedStateColor":ventMenuBtnSelectedStateColor,
                                                                       "strVentMenuBtnSelectedStateBorderColor":ventMenuBtnSelectedStateBorderColor,
                                                                       "strVentMenuBtnTouchedStateColor":ventMenuBtnTouchedStateColor
                                                                   });

            }else if(mode == 3 || mode == 4){  //needs to delete this else if, once all vent modes are supported
                ventMenuBtnObj = ventMenuBtnComponent.createObject(ventMenu.menuModesID,{"iCompID":mode,"strModeName":modes[mode],"strExpModeName":modesName[mode],"strModeCompState":"Disabled",
                                                                       "iModeMenuBtnWidth":modeMenuBtnWidth,
                                                                       "iModeMenuBtnHeight":modeMenuBtnHeight,
                                                                       "strModeMenuBtnColor":modeMenuBtnColor,
                                                                       "iModeMenuBtnLabelHeight":modeMenuBtnLabelHeight,
                                                                       "strModeMenuBtnLabelBorderColor":modeMenuBtnLabelBorderColor,
                                                                       "iModeMenuBtnLabelBorderWidth":modeMenuBtnLabelBorderWidth,
                                                                       "iModeMenuBtnLabelRadius":modeMenuBtnLabelRadius,
                                                                       "strModeMenuBtnLabelColor":modeMenuBtnLabelColor,
                                                                       "strModeMenuBtnLabelFontFamily":modeMenuBtnLabelFontFamily,
                                                                       "iModeMenuBtnLabelPixelSize":modeMenuBtnLabelPixelSize,
                                                                       "iModeExpansionLabelHeight":modeExpansionLabelHeight,
                                                                       "strModeExpansionLabelColor":modeExpansionLabelColor,
                                                                       "strModeExpansionFontFamily":modeExpansionFontFamily,
                                                                       "iModeExpansionPixelSize":modeExpansionPixelSize,
                                                                       "strVentMenuBtnActiveStateColor":ventMenuBtnActiveStateColor,
                                                                       "strVentMenuBtnDisabledStateColor":ventMenuBtnDisabledStateColor,
                                                                       "strVentMenuBtnEndOfScaleStateColor":ventMenuBtnEndOfScaleStateColor,
                                                                       "strVentMenuBtnEndOfScaleStateBorderColor":ventMenuBtnEndOfScaleStateBorderColor,
                                                                       "strVentMenuBtnScrollStateBorderColor":ventMenuBtnScrollStateBorderColor,
                                                                       "strVentMenuBtnSelectedStateColor":ventMenuBtnSelectedStateColor,
                                                                       "strVentMenuBtnSelectedStateBorderColor":ventMenuBtnSelectedStateBorderColor,
                                                                       "strVentMenuBtnTouchedStateColor":ventMenuBtnTouchedStateColor
                                                                   });

            }else{
                ventMenuBtnObj = ventMenuBtnComponent.createObject(ventMenu.menuBypassID,{"iCompID":mode,"strModeName":modes[mode],"strExpModeName":modesName[mode],"strModeCompState":"Disabled",
                                                                       "iModeMenuBtnWidth":modeMenuBtnWidth,
                                                                       "iModeMenuBtnHeight":modeMenuBtnHeight,
                                                                       "strModeMenuBtnColor":modeMenuBtnColor,
                                                                       "iModeMenuBtnLabelHeight":modeMenuBtnLabelHeight,
                                                                       "strModeMenuBtnLabelBorderColor":modeMenuBtnLabelBorderColor,
                                                                       "iModeMenuBtnLabelBorderWidth":modeMenuBtnLabelBorderWidth,
                                                                       "iModeMenuBtnLabelRadius":modeMenuBtnLabelRadius,
                                                                       "strModeMenuBtnLabelColor":modeMenuBtnLabelColor,
                                                                       "strModeMenuBtnLabelFontFamily":modeMenuBtnLabelFontFamily,
                                                                       "iModeMenuBtnLabelPixelSize":modeMenuBtnLabelPixelSize,
                                                                       "iModeExpansionLabelHeight":modeExpansionLabelHeight,
                                                                       "strModeExpansionLabelColor":modeExpansionLabelColor,
                                                                       "strModeExpansionFontFamily":modeExpansionFontFamily,
                                                                       "iModeExpansionPixelSize":modeExpansionPixelSize,
                                                                       "strVentMenuBtnActiveStateColor":ventMenuBtnActiveStateColor,
                                                                       "strVentMenuBtnDisabledStateColor":ventMenuBtnDisabledStateColor,
                                                                       "strVentMenuBtnEndOfScaleStateColor":ventMenuBtnEndOfScaleStateColor,
                                                                       "strVentMenuBtnEndOfScaleStateBorderColor":ventMenuBtnEndOfScaleStateBorderColor,
                                                                       "strVentMenuBtnScrollStateBorderColor":ventMenuBtnScrollStateBorderColor,
                                                                       "strVentMenuBtnSelectedStateColor":ventMenuBtnSelectedStateColor,
                                                                       "strVentMenuBtnSelectedStateBorderColor":ventMenuBtnSelectedStateBorderColor,
                                                                       "strVentMenuBtnTouchedStateColor":ventMenuBtnTouchedStateColor
                                                                   });
            }
            ventMenuBtnObj.rotatedClockwise.connect(onModeRotatedClockwise)
            ventMenuBtnObj.rotatedAntiClockwise.connect(onModeRotatedAntiClockwise)
            ventMenuBtnObj.ventModeSelected.connect(onDifferentVentModeSelected)
            ventMenuBtnObj.ventModeCompTouched.connect(onVentModeCompTouched)
            ventMenuBtnObj.ventModeCompSelected.connect(onVentModeCompSelected)
        }
    }
}

/*create mode confirmation popup*/

function createModeConfirmationPopup(mode){
    popupComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentModeConfirmPopup.qml");
    if(popupComponent.status === Component.Ready){
        finishModeConfirmationPopupCreation(mode)
    }else if (popupComponent.status === Component.Error){
        console.log("Error loading component:", popupComponent.errorString());
    }else{
        popupComponent.statusChanged.connect(finishModeConfirmationPopupCreation(mode));
    }
}

function finishModeConfirmationPopupCreation(mode){
    var subgroupComponentIndex = 5;
    var modeLab = mode + "_mode"
    if(popupComponent.status === Component.Ready){
        popupObj = popupComponent.createObject(mainitem,{"x":380,"y":547,
                                                   "strConfirmationText":group.getComponentSubGroupDataByTag(subgroupComponentIndex,strGroupnames, "VentMode", "VentSetting", modeLab, "ConfirmPopupText"),
                                                   "iPopupWidth":popupWidth,
                                                   "iPopupHeight":popupHeight,
                                                   "iPopupRadius":popupRadius,
                                                   "strPopupColor":popupColor,
                                                   "strPopupLabelFontFamily":popupLabelFontFamily,
                                                   "strToolTipTriangleSource":popupTipTriangleSource,
                                                   "strPopupCancel":popupCancelLabel,
        					   "strCancelBtnColor":popupCancelBorderColor,
						   "iPopupLabelPixelSize":popupLabelPixelSize,
						   "strPopupFontColor": popupFontColor});
        popupObj.cancelModeConfirmation.connect(onCancellingModeConfirmation)
    }
}

/*create Alarm Setup Menu*/

function createAlarmSetupMenu(){
    console.log("Create Alarm SetUp Menu !!!!!!!!!")
    alarmSetupMenuComponent = Qt.createComponent("qrc:/Source/Generic/Qml/AlarmSetupMenu.qml")
    if (alarmSetupMenuComponent.status === Component.Ready){
        finishAlarmSetupMenuCreation();
    } else if (alarmSetupMenuComponent.status === Component.Error) {
        console.log("Error loading component:", alarmSetupMenuComponent.errorString());
    }else{
        alarmSetupMenuComponent.statusChanged.connect(finishAlarmSetupMenuCreation());
    }
}

function finishAlarmSetupMenuCreation(){
    console.log("Finish creation....")
    if(alarmSetupMenuComponent.status === Component.Ready){
        console.log("1.........")
       if (alarmSetupMenuObj === null){
        alarmSetupMenuObj = alarmSetupMenuComponent.createObject(mainitem,{"strAlarmMenuLabel":component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmSetupMenuLabel"),
                                                                     "x":0,"y":65,
                                                                     "iAlarmMenuWidth" : alarmMenuWidth,
                                                                     "iAlarmMenuHeight" : alarmMenuHeight,
                                                                     "iAlarmMenuRadius" : alarmMenuRadius,
                                                                     "iAlarmMenuBorderWidth" : alarmMenuBorderWidth,
                                                                     "iAlarmLogWidth" : alarmLogWidth,
                                                                     "iAlarmLogHeight" : alarmLogHeight,
                                                                     "iAlarmLogRadius" : alarmLogRadius,
                                                                     "iAlarmLogBorderWidth" : alarmLogBorderWidth,
                                                                     "strAlarmMenuBgColor" : alarmMenuBgColor,
                                                                     "strAlarmBorderColor" : alarmBorderColor,
                                                                     "strAlarmLogBgColor" : alarmLogBgColor,
                                                                     "strAlarmLogBorderColor" : alarmLogBorderColor,
                                                                     "strLimitLabel1" : limitLabel1,
                                                                     "strLimitLabel2" : limitLabel2,
                                                                     "strLogLabel" : logLabel,
                                                                     "strFontFamilyAlarmSet" : fontFamilyAlarmSet,
                                                                     "strFontColor" : fontColor,
                                                                     "iPixelSizeAlarmSet" : pixelSizeAlarmSet,
                                                                     "iPixelSizeLimitLabel" : pixelSizeLimitLabel,
                                                                     "iPixelSizeLogLabel" : pixelSizeLogLabel,
                                                                     "strAlarmParamPpeak": alarmPpeakLabel,
                                                                     "strAlarmParamPpeakUnit": alarmPpeakUnit,
                                                                     "strAlarmParamMV": alarmMVLabel,
                                                                     "strAlarmParamMVUnit": alarmMVUnit,
                                                                     "strAlarmParamTVexp":alarmTVexpLabel,
                                                                     "strAlarmParamTVexpUnit": alarmTVexpUnit,
                                                                     "strAlarmParamO2":alarmO2Label,
                                                                     "strAlarmParamO2Unit": alarmO2Unit,
                                                                     "strAlarmParamApneaDelay":alarmApneaLabel,
                                                                     "strAlarmParamApneaDelayUnit":alarmApneaUnit,
                                                                     "strAlarmParamAlarm":alarmVolumeLabel,
                                                                     "strAlarmParamAlarmUnit":alarmVolumeUnit,
                                                                     "strAlarmParamScrollStateBorderColor":alarmParamScrollStateBorderColor,
                                                                     "strAlarmParamScrollStateBorderWidth":alarmParamScrollStateBorderWidth
                                                                 });
        }

    }
}

function destroyAlarmSetupMenu()
{
    if (alarmSetupMenuObj !== null){
        console.log("destroy alarm set up menu")
        alarmSetupMenuObj.destroy()
        alarmSetupMenuObj = null;
    }
    else
    {
        console.log("alarmSetupMenuObj === null")
    }

}
