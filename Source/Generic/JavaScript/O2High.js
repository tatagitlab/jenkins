var O2HighRangeList;
var O2HighMin;
var O2HighMax;
var O2HighStepSize;
O2HighRangeList = physicalparam.getRangeData("O2_High", "%")
O2HighMin = O2HighRangeList[0]
O2HighMax = O2HighRangeList[1]
O2HighStepSize = O2HighRangeList[2]


/*Increment step Logic for O2 High*/
function clkHandleO2High(m_o2High) {
    var o2LowValue = parseInt( settingReader.read("O2_Low"))
    if(o2LowValue < parseInt(O2HighMin)){
        o2LowValue = parseInt(O2HighMin)
    }
    if((m_o2High >= o2LowValue) && (m_o2High < O2HighMax)) {
        m_o2High += parseInt(O2HighStepSize) ;
    }
    return m_o2High;

}
/*Decrement step Logic for O2 High*/
function antClkHandleO2High(m_o2High){
    m_o2High -= parseInt(O2HighStepSize) ;
    return m_o2High;
}

function onO2HighIncrementValue(objectName) {
    console.log("onO2HighIncrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(alarmSetupMenuId, objectName)
    if(objectName === "O2HighObj"){
        if(parseInt(currentAlarmSetupComp.strValue) < parseInt(O2HighMax)){
            currentAlarmSetupComp.strValue =parseInt(clkHandleO2High(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onO2HighDecrementValue(objectName) {
    console.log("onO2HighDecrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(alarmSetupMenuId, objectName)
    if(objectName === "O2HighObj"){
        var o2LowValue = parseInt(settingReader.read("O2_Low"))
        if (o2LowValue <= parseInt(O2HighMin)) {
            o2LowValue = parseInt(O2HighMin)
        } else {
            o2LowValue = parseInt(o2LowValue) + parseInt(O2HighStepSize)
        }
        if(parseInt(currentAlarmSetupComp.strValue) > parseInt(o2LowValue)){
            currentAlarmSetupComp.strValue = parseInt(antClkHandleO2High(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}
function onO2HighEndOfScale(objectName){
    console.log("checking end of scale O2High")
    var currentVentComp = getCompRef(alarmSetupMenuId, objectName)
    if((objectName === "O2HighObj" ))
    {var o2LowValue = parseInt(settingReader.read("O2_Low"))
        if (o2LowValue <= parseInt(O2HighMin)) {
            o2LowValue = parseInt(O2HighMin)
        } else {
            o2LowValue = parseInt(o2LowValue) + parseInt(O2HighStepSize)
        }
        if(currentVentComp.strValue === o2LowValue.toString()) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon

        } else if(currentVentComp.strValue === O2HighMax) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(currentVentComp.strValue !== o2LowValue.toString() && currentVentComp.strValue !== O2HighMin) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetO2HighUpArrowBgColor(objectName){
    console.log("O2High bg color"+objectName)
    var currentVentComp = getCompRef(alarmSetupMenuId, objectName)
    console.log("setting up arrow bg O2"+currentVentComp)
    if((objectName === "O2HighObj" )){
        if(parseInt(currentVentComp.strValue) === parseInt(O2HighMax)){
            console.log("changing Color  of Up arrow for Select state...........")
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("changing Color  of Up arrow for Touch state..........")
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

function onSetO2HighDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(alarmSetupMenuId, objectName)
    console.log("setting down arrow bg O2 High ...."+currentVentComp.strValue)
    if((objectName === "O2HighObj" )){
        var o2LowValue = parseInt(settingReader.read("O2_Low"))
        if (o2LowValue <= pareseInt(O2HighMin)) {
            o2LowValue = pareseInt(O2HighMin)
        } else {
            o2LowValue = parseInt(o2LowValue) + parseInt(O2HighStepSize)
        }
        if(parseInt(currentVentComp.strValue) === parseInt(o2LowValue)){
            console.log(" changing select color for down arrow...........")

            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log(" changing touch color for down arrow...........")

            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
