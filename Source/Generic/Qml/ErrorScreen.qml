
import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3


Window {
    id: errorWindow
    visible: true
    width: 1280
    height: 800
    title: ""
    flags: Qt.FramelessWindowHint
Image {
    id: shutdownbanner
    //property string strShutdownBanner: component.getComponentXmlDataByTag("Component","ShutDownBanner", "ShutDownBannerSource")
    //property int iBannerWidth: parseInt(component.getComponentDimension("ShutDownBanner","width"))
    //property int iBannerHeight: parseInt(component.getComponentDimension("ShutDownBanner", "height"))
    property string uniqueObjectID: "ShutDownBanner"

    width: 1280//iBannerWidth
    height: 800//iBannerHeight
    source: "qrc:/../Config/Assets/failurescreen.png" //strShutdownBanner
    visible: true
    z: 11

    Connections {
        target: objCSBValue
        onShowBanner: {
            console.log("SYSERR Banner shown");
            shutdownbanner.forceActiveFocus()
            shutdownbanner.focus = true
            shutdownbanner.visible = true
        }
    }
}
}
