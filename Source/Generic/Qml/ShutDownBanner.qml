import QtQuick 2.0
import QtQuick.Controls 2.2

Image {
    id: shutdownbanner
    property string strShutdownBanner: component.getComponentXmlDataByTag("Component","ShutDownBanner", "ShutDownBannerSource")
    property int iBannerWidth: parseInt(component.getComponentDimension("ShutDownBanner","width"))
    property int iBannerHeight: parseInt(component.getComponentDimension("ShutDownBanner", "height"))
    property string uniqueObjectID: "ShutDownBanner"

    width: iBannerWidth
    height: iBannerHeight
    source: strShutdownBanner
    visible: false
    z: 11

    Connections {
        target: objCSBValue
        onShowBanner: {
            console.log("Banner shown");
            shutdownbanner.forceActiveFocus()
            shutdownbanner.focus = true
            shutdownbanner.visible = true
        }
    }

    Connections {
        target: ventMismatch
        onShowVentMismatchBanner: {
            console.log("Vent Mismatch Banner shown");
            shutdownbanner.forceActiveFocus()
            shutdownbanner.focus = true
            shutdownbanner.visible = true
        }
    }
}
