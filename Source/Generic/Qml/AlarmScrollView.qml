import QtQuick 2.9
import QtQuick.Controls 2.2

import "qrc:/../Source/Generic/JavaScript/AlarmLogMenuController.js" as AlarmLogCtrl

Rectangle {

    objectName: "ScrollView"
    property bool bIsKeyOnePressed: false
    property bool bIsKeyThreePressed: false
    property bool bIsKeyTwoPressed: false
    property bool bIsKeyFivePressed: false
    property bool bIsKeySixPressed: false
    property bool isKeyPressedInSequence: false
    property string strAlarmTableLineColor: "#B5B5B6"
    property int iAlarmTableLineWidth: 1

    property string strAlarmTableTextFontColor: "#787878"
    property int iAlarmTableTextFontSize: 16

    property int iAlarmTableLeftRowWidth: 85
    property int iAlarmTableMiddleRowWidth: 265
    property int iAlarmTableRightRowWidth: 120
    property int iAlarmTableRowHeight: 35

    property int iAlarmProgressBarBtnWidth: 37
    property int iAlarmProgressBarBtnHeight: 37
    property string strAlarmProgressBarBtnBorderColor: "#444444"
    property string strAlarmProgressBarBtnBgColor: "#2e2e2e"
    property int iAlarmProgressBarBtnBorderWidth: 1
    property int iAlarmProgressBarBtnRadius: 3

    property  string strAlarmProgressBarAreaBgColor: "#242424"
    property int iAlarmProgressBarAreaWidth: 42

    property int iAlarmProgressBarHandleWidth: 37
    property string strAlarmProgressBarHandleColor: "#2e2e2e"
    property string strAlarmProgressBarHandleBorderColor: "#444444"
    property int iAlarmProgressBarHandleBorderWidth: 1
    property string strTableUpEnableSource
    property string strTableUpTocuhedSource
    property string strTableDownEnableSource
    property string strTableDownTocuhedSource
    property string strAlarmScrollTchBgColor : "#4565A6"
    property string strTableUpDisableSource
    property string strTableDownDisableSource
    property int iAlarmLogContentRowCount: 14


    property string strFontFamilyAlarmLog: geFont.geFontStyle()

    function setHighlight()
    {
        console.log("set setHighlight")
        idListView.highlight = highlightBar;
    }
    function removeHighlight() {
        console.log("remove highlight")
        idListView.highlight = null;
    }

    function getHighlightStatus() {
        return idListView.highlight ;
    }

    function setTableHighlight() {
        alarmLogTable.forceActiveFocus();
        alarmLogTable.z = 2;
    }

    function setScrollView() {
        idScrollview.forceActiveFocus();
    }

    //This component is used to define highlight over list
    Component {
        id: highlightBar

        Rectangle {
            id:idScrollHighlight
            implicitWidth: 468
            height: 35
            color: "transparent"
            border.width: 2
            border.color: "#ecbd00"
            z : 3
            y: idListView.currentItem.y;
        }

    }

    Rectangle{
        id:alarmLogTable
        implicitHeight: 507
        implicitWidth: 512
        border.width: 2
        border.color: activeFocus ? "#ecbd00": strAlarmTableBorderColor
        color: "transparent"

        Keys.onReturnPressed: {
            removeHighlight();
            AlarmLogCtrl.switchToBackBtn();
        }

        Keys.onLeftPressed: {
            if ( AlarmDataModel.rowCount() !== 0){
                idScrollview.forceActiveFocus();
                setHighlight();
                alarmLogTable.z = 0;
            } else {
                setTableHighlight();
            }

        }

        Keys.onRightPressed: {
            if ( AlarmDataModel.rowCount() !== 0){
                idScrollview.forceActiveFocus();
                setHighlight();
                alarmLogTable.z = 0;
            } else {
                setTableHighlight();
            }

        }

    }

    //This rectangle is used to create down arrow scroll bar button
    Rectangle{
        id : idProgressBarDownBtn
        implicitWidth : iAlarmProgressBarBtnWidth
        implicitHeight: iAlarmProgressBarBtnHeight
        color: (scrollDownArr.pressed && AlarmDataModel.rowCount() > iAlarmLogContentRowCount) ? strAlarmScrollTchBgColor : strAlarmProgressBarBtnBgColor
        border.color: strAlarmProgressBarBtnBorderColor
        border.width: iAlarmProgressBarBtnBorderWidth
        z: 10
        radius: iAlarmProgressBarBtnRadius
        x:508 - iAlarmProgressBarBtnWidth
        y:503 - iAlarmProgressBarBtnHeight
        Image {
            id: idProgressBarDownImg
            anchors.centerIn: parent
            source: strTableDownEnableSource
        }
        MouseArea{
            id: scrollDownArr
            anchors.fill: parent
            onPressed: {
                console.log("onPressed")
                if (AlarmDataModel.rowCount() > iAlarmLogContentRowCount){
                    idProgressBarDownImg.source =  strTableDownTocuhedSource ;
                }
                else
                {
                    idProgressBarDownImg.source =  strTableDownDisableSource;
                }
            }
            onReleased: {
                console.log("onReleased")
                if (AlarmDataModel.rowCount() > iAlarmLogContentRowCount) {
                    idProgressBarDownImg.source =  strTableDownEnableSource ;
                    vbar.increase()
                }
                else {
                    idProgressBarDownImg.source =  strTableDownDisableSource;
                }
            }
        }
    }

    //This rectangle is used to create up arrow scroll bar button
    Rectangle{
        id : idProgressBarUpBtn
        x : 508 - iAlarmProgressBarBtnWidth
        implicitWidth : iAlarmProgressBarBtnWidth
        implicitHeight: iAlarmProgressBarBtnHeight
        color: (scrollUpArr.pressed && AlarmDataModel.rowCount() > iAlarmLogContentRowCount)? strAlarmScrollTchBgColor : strAlarmProgressBarBtnBgColor
        border.color: strAlarmProgressBarBtnBorderColor
        border.width: iAlarmProgressBarBtnBorderWidth
        radius: iAlarmProgressBarBtnRadius
        z:10
        y : 2
        Image {
            id: idProgressBarUpImg
            anchors.centerIn: parent
            source: strTableUpEnableSource
        }
        MouseArea{
            id: scrollUpArr
            anchors.fill: parent
            onPressed: {
                console.log("onPressed")
                if (AlarmDataModel.rowCount() > iAlarmLogContentRowCount) {
                    idProgressBarUpImg.source =  strTableUpTocuhedSource ;
                }
                else {
                    idProgressBarUpImg.source =  strTableUpDisableSource;

                }
            }

            onReleased: {
                console.log("onReleased")
                idProgressBarUpImg.source = (AlarmDataModel.rowCount() > iAlarmLogContentRowCount) ? strTableUpEnableSource : strTableUpDisableSource;
                vbar.decrease()

            }
        }
    }


    //This view is used to create Flickable view
    ScrollView {
        id : idScrollview
        width: 512
        height: 505
        clip : true
        objectName: "AlarmScrollViewObj"
        focus: true
        ScrollBar.vertical: vbar

        Keys.onRightPressed: {
            if ( AlarmDataModel.rowCount() !== 0 && (getHighlightStatus() !== null)){
                idListView.incrementCurrentIndex()
            }
            else
            {
                setHighlight();
            }
        }
        Keys.onLeftPressed: {
            if ( AlarmDataModel.rowCount() !== 0 && (getHighlightStatus() !== null)){
                idListView.decrementCurrentIndex()
            }
            else
            {
                setHighlight();
            }
        }
        Keys.onReturnPressed: {
            console.log("AlarmDataModel.rowCount()",AlarmDataModel.rowCount())
            if ( AlarmDataModel.rowCount() !== 0 && (getHighlightStatus() !== null)){

                removeHighlight();
            }
            else
            {
                setHighlight();
            }
            AlarmLogCtrl.navigateFocusTo("BackBtnObj")
        }

        // handling shortcut for the scrollview("k","13"- enter press, "215","216"- clockwise and anticlockwise rotation)
        Keys.onPressed:  {
            event.accepted = true
            console.log("ALARM LOGS ARE THERE!!!!,ENTER PRESS!!!!")
            if (event.key === Qt.Key_K) {
                if (AlarmDataModel.rowCount() !== 0 && (getHighlightStatus() !== null)){
                    AlarmLogCtrl.navigateFocusTo("BackBtnObj")
                    removeHighlight();
                }
            } else if (event.key === Qt.Key_1) {
                if(bIsKeyTwoPressed) {
                    isKeyPressedInSequence = true
                }
                bIsKeyOnePressed = true
            } else if (event.key === Qt.Key_3) {
                bIsKeyThreePressed = true
                if (bIsKeyOnePressed && bIsKeyThreePressed) {
                    if (AlarmDataModel.rowCount() !== 0 && (getHighlightStatus() !== null)){
                        AlarmLogCtrl.navigateFocusTo("BackBtnObj")
                        removeHighlight();
                    }
                }
            } else if (event.key === Qt.Key_2) {
                bIsKeyTwoPressed = true
            } else if (event.key === Qt.Key_5) {
                bIsKeyFivePressed = true
                if (bIsKeyTwoPressed && bIsKeyOnePressed && bIsKeyFivePressed && isKeyPressedInSequence) {
                    console.log("215 pressed in alarm log!!")
                    idListView.incrementCurrentIndex()
                    isKeyPressedInSequence = false
                }
            } else if (event.key === Qt.Key_6) {
                bIsKeySixPressed = true
                if (bIsKeyTwoPressed && bIsKeyOnePressed && bIsKeySixPressed && isKeyPressedInSequence) {
                    console.log("216 pressed in alarm log!!")
                    idListView.decrementCurrentIndex()
                    isKeyPressedInSequence = false
                }
            }
        }

        Keys.onReleased: {
            if (event.key === Qt.Key_1) {
                bIsKeyOnePressed = false
            } else if (event.key === Qt.Key_3) {
                bIsKeyThreePressed = false
            }  else if (event.key === Qt.Key_1) {
                bIsKeyOnePressed = false
            }  else if (event.key === Qt.Key_2) {
                bIsKeyTwoPressed = false
            }  else if (event.key === Qt.Key_5) {
                bIsKeyFivePressed = false
            }  else if (event.key === Qt.Key_6) {
                bIsKeySixPressed = false
            }
        }

        Keys.onUpPressed: {
            console.log("do nothing")
        }
        Keys.onDownPressed: {
            console.log("do nothing")
        }

        //This view is created to populate data from alarm data model using delegate
        ListView {
            id : idListView
            model: AlarmDataModel
            keyNavigationEnabled: true
            highlightFollowsCurrentItem : false;
            spacing: -1//Spacing is set to -1 so that elements of grid can share boundary
            highlight: highlightBar
            delegate:
                Row{
                spacing: -1 //Spacing is set to -1 so that elements of grid can share boundary
                Rectangle {
                    id : idTime
                    width: iAlarmTableLeftRowWidth
                    height: iAlarmTableRowHeight
                    border.color: strAlarmTableLineColor
                    border.width: iAlarmTableLineWidth
                    color: "transparent"
                    Text{
                        anchors.fill: parent
                        text: timestamp
                        font.pixelSize: iAlarmTableTextFontSize
                        font.family: strFontFamilyAlarmLog
                        color: strAlarmTableTextFontColor
                        leftPadding: 9
                        verticalAlignment: Text.AlignVCenter
                    }
                }
                Rectangle {
                    id : idAlarm
                    width: iAlarmTableMiddleRowWidth
                    height: iAlarmTableRowHeight
                    border.color: strAlarmTableLineColor
                    border.width: iAlarmTableLineWidth
                    color: "transparent"
                    Text{
                        anchors.fill: parent
                        text: qsTrId(physicalparam.getAlarmData("AlarmMsg",alarmName))
                        font.pixelSize: iAlarmTableTextFontSize
                        font.family: strFontFamilyAlarmLog
                        color: strAlarmTableTextFontColor
                        leftPadding: 9
                        verticalAlignment: Text.AlignVCenter
                    }
                }
                Rectangle {
                    id : idPriority
                    width: iAlarmTableRightRowWidth
                    height: iAlarmTableRowHeight
                    border.color: strAlarmTableLineColor
                    border.width: iAlarmTableLineWidth
                    color: "transparent"
                    Text{
                        anchors.fill: parent
                        text: qsTrId(priority)
                        font.pixelSize: iAlarmTableTextFontSize
                        font.family: strFontFamilyAlarmLog
                        color: strAlarmTableTextFontColor
                        leftPadding: 9
                        verticalAlignment: Text.AlignVCenter
                    }
                    Keys.onRightPressed:  {
                        console.log("on right pressed")
                        idListView.currentIndex = index
                    }
                }
            }
        }
        //This is created to get the scroll bar Component.
        ScrollBar {
            id: vbar
            active: true
            orientation: Qt.Vertical
            parent: idScrollview
            size:idScrollview.height/(AlarmDataModel.rowCount() *iAlarmTableRowHeight)
            policy: ScrollBar.AlwaysOn
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.rightMargin: 2
            anchors.bottom: parent.bottom
            anchors.topMargin: iAlarmProgressBarBtnHeight+1
            anchors.bottomMargin:  iAlarmProgressBarBtnHeight +1
            //This property  defines scroll bar background
            background: Rectangle {
                width: iAlarmProgressBarAreaWidth-1
                height: parent.height
                color: strAlarmProgressBarAreaBgColor
            }
            //This property  defines scroll bar handler
            contentItem: Rectangle{
                id: idHandlebutton
                implicitWidth: iAlarmProgressBarHandleWidth
                implicitHeight: parent.height - (2*iAlarmProgressBarBtnHeight)
                color: strAlarmProgressBarHandleColor
                border.color: strAlarmProgressBarHandleBorderColor
                border.width: 1
                radius: iAlarmProgressBarBtnRadius
            }
        }

        Connections{
            target : AlarmDataModel
            onModelReset:{
                console.log("onModelReset")
                idProgressBarDownImg.source = ( AlarmDataModel.rowCount() > iAlarmLogContentRowCount) ? strTableDownEnableSource : strTableDownDisableSource;
                idProgressBarUpImg.source = ( AlarmDataModel.rowCount() > iAlarmLogContentRowCount) ? strTableUpEnableSource : strTableUpDisableSource;
            }
        }

        Component.onCompleted: {
            // idScrollview.forceActiveFocus();
            contentItem.interactive = false
            //if ( AlarmDataModel.rowCount() === 0){
              //  console.log("navigate focus to back button since list is empty")
               // AlarmLogCtrl.navigateFocusTo("BackBtnObj")

            //}
            idProgressBarDownImg.source = ( AlarmDataModel.rowCount() > iAlarmLogContentRowCount) ? strTableDownEnableSource : strTableDownDisableSource;
            idProgressBarUpImg.source = ( AlarmDataModel.rowCount() > iAlarmLogContentRowCount) ? strTableUpEnableSource : strTableUpDisableSource;
        }
    }
}
