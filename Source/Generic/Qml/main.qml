import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "qrc:/../Source/Generic/JavaScript/MasterController.js" as Controller
import "qrc:/../Source/VentSettingArea/JavaScript/VentGroupController.js" as GrpCtrl
import "qrc:/../Source/VentSettingArea/JavaScript/VentModeController.js" as VentModeController
import "qrc:/Source/VentSettingArea/JavaScript/MoreSettingsController.js" as MoreSettingsCtrl
import "qrc:/../Source/Generic/JavaScript/AlarmSetupController.js" as AlarmSetupController
import "qrc:/../Source/Generic/JavaScript/AlarmLogMenuController.js" as AlarmLogController
import "../../Checkout/JavaScript/CheckoutController.js" as CheckoutComp
import "../../Standby/JavaScript/StandbyController.js" as StandbyComp
import "../../Spirometry/JavaScript/SpiroController.js" as SpiroController

Window {
    id: mainWindow
    visible: true
    width: 1280
    height: 800
    title: ""
    flags: Qt.FramelessWindowHint
    property bool bIsVPopupVisible: false
    property bool bIsAlarmSMOpen: false
    property bool bIsKeyOnePressedInAlarm: false
    property bool bIsKeyThreePressedInAlarm: false
    property bool bIsKeyTwoPressedInAlarm: false
    property bool bIsKeyFivePressedInAlarm: false
    property bool bIsKeySixPressedInAlarm: false
    property bool bIsOnBootup: false
    property int iFocusCount: 0
    property var alarmKeySequenceList: []
    property var previousActiveItem

    //*******************************************
    // Purpose: To get group object reference for accessing group
    // properties and functions
    // Input Parameters: Groupname for which reference is needed
    //***********************************************/
    function getGroupRef(groupName) {
        console.log("getting group reference"+mainitem.children.length)
        var length=mainitem.children.length
        for(var grpindex=0;grpindex<length;grpindex++) {
            if(mainitem.children[grpindex].objectName===groupName) {
                return mainitem.children[grpindex];
            }
        }
    }

    //*******************************************
    // Purpose: To get component reference for accessing component's
    // properties and functions
    // Input Parameters: Component parent's group name and
    // object name of the component
    //***********************************************/
    function getCompRef(groupName,objectName) {
        console.log("getting obj of parent"+groupName+objectName)
        var length = groupName.children.length
        for(var compindex=0;compindex<length;compindex++) {
            if(objectName===groupName.children[compindex].objectName) {
                return groupName.children[compindex];
            }
        }
    }

    //*******************************************
    // Purpose: To get the reference to a component based on the index
    // Input Parameters: Component parent's group name and
    // Component's index
    //***********************************************/
    function getCompRefByIndex(groupName,compindex) {
        return groupName.children[compindex];
    }

    //*******************************************
    // Purpose: To pass the short cut key press to main whenever in a
    // group other than the main item
    // Input Parameters: current key pressed
    //***********************************************/
    function fromGroups(currentKey) {
        Controller.shortcutPressMain(currentKey)
    }

    //*******************************************
    // Purpose: To set whether the alarm setup menu is opened or not
    // Input Parameters: Boolean value which tells whether the menu is opened
    //***********************************************/
    function setIsAlarmSMOpen (val) {
        bIsAlarmSMOpen = val
    }

    Item {
        id: mainitem
        width: parent.width
        height: parent.height

        signal shortcut_keyPressed(string currentKey, string objectName)
        signal shortcut_keyReleased(string currentKey, string objectName)

        ShutDownBanner {
            id: shutdownbanner
        }

        Keys.onPressed: {
            Controller.shortcutPressMain(event.key)
        }

        Keys.onReleased: {
            Controller.shortcutReleaseMain(event.key)
        }

        Keys.onRightPressed: {
            console.log("Right pressed in main")
            Controller.ventGroupContainer.firstfocus = "right"
        }
        Keys.onLeftPressed: {
            console.log("Left pressed in main")
            Controller.ventGroupContainer.firstfocus = "left"
        }

        Timer {
            id : readCSB
            running: true
            interval: 80
            repeat: true
            onTriggered: objCSBValue.readFromCSB(backupData,alarm);
        }

        //To support keep-alive packet
        Timer {
            id : writeCSB
            running: true
            interval: 1000
            repeat: true
            onTriggered: objCSBValue.writeOnCSB(250,1)
        }
    }

    //Information about PI and Sprint releases and date
    Rectangle {
        id: versionPopUp
        height: 150
        width: 300
        color: "#b3c2d8"
        x: 0
        y: 60
        visible: false
        Label {
            id: idMajorversion
            text: "Major Version : PI_2"
            anchors.top: parent.top
            anchors.topMargin: 30
            anchors.left: parent.left
            anchors.leftMargin: 20
            font.bold: true
            color: "Black"
            font.pixelSize: 18
        }
        Label {
            id: idMinorverion
            text: "Sprint: Sprint8 P09"
            anchors.top: idMajorversion.top
            anchors.topMargin: 30
            anchors.left: parent.left
            anchors.leftMargin: 20
            font.bold: true
            color: "Black"
            font.pixelSize: 18
        }
        Label {
            id: idDate
            text : "Date : 26-April-2019"
            anchors.top: idMinorverion.top
            anchors.topMargin: 30
            anchors.left: parent.left
            anchors.leftMargin: 20
            font.bold: true
            color: "Black"
            font.pixelSize: 18
        }
        Timer {
            id: infoID
            interval: 5000;
            onTriggered: {
                console.log("Version Timeout triggered")
                versionPopUp.visible = false
            }
        }
    }

    Connections {
        target: objCSBValue
        //        onSetStandByMode: {
        //            console.log("*******STANDBY MODE******")
        //            GrpCtrl.switchVentToBag("Bag")
        //        }
        onVentilationOn: {
            console.log("*******BTV_SWITCH = 1******")
            GrpCtrl.switchVentToBag("Vent")

        }
        onVentilationOff: {
            console.log("*******BTV_SWITCH = 0******")
            GrpCtrl.switchVentToBag("Bag")
            console.log("Clearing alarm log data")
            AlarmDataModel.clearAlarmLogData()
            AlarmLogController.switchToBackBtn()
        }
    }

    onActiveFocusItemChanged: {
        console.log("********ACTIVE FOCUS IS SHIFTING ACCROSS APPLICATION******",activeFocusItem)
        iFocusCount = iFocusCount + 1
        Controller.handleWindowsFocusShift(activeFocusItem,iFocusCount)
    }

    Component.onCompleted: {
        console.log ("triggered")
        Controller.createGroup();
        mainitem.forceActiveFocus();
        // connecting signals to check anyother group is touched
        Controller.buttonBarGroupContainer.iconGrpClicked.connect(GrpCtrl.onOtherGrpTouched)
        Controller.alarmGroupContainer.alarmGrpClicked.connect(GrpCtrl.onOtherGrpTouched)
        Controller.numericGroupContainer.numericGrpClicked.connect(GrpCtrl.onOtherGrpTouched)
        Controller.waveformGroupContainer.waveGrpClicked.connect(GrpCtrl.onOtherGrpTouched)

        Controller.buttonBarGroupContainer.iconGrpClicked.connect(AlarmSetupController.destroyAlarmSetupMenu)
        Controller.alarmGroupContainer.alarmGrpClicked.connect(AlarmSetupController.destroyAlarmSetupMenu)
        Controller.numericGroupContainer.numericGrpClicked.connect(AlarmSetupController.destroyAlarmSetupMenu)
        Controller.waveformGroupContainer.waveGrpClicked.connect(AlarmSetupController.destroyAlarmSetupMenu)

        Controller.ventGroupContainer.finalDestroy.connect(AlarmSetupController.destroyedAlarmSetupMenu)

        Controller.buttonBarGroupContainer.setCheckoutAsObjectName.connect(GrpCtrl.whichGroupIsClicked)
        Controller.alarmGroupContainer.setAlarmAsObjectName.connect(GrpCtrl.whichGroupIsClicked)
        Controller.numericGroupContainer.setNumericAsObjectName.connect(GrpCtrl.whichGroupIsClicked)
        Controller.waveformGroupContainer.setWaveformAsObjectName.connect(GrpCtrl.whichGroupIsClicked)

        //spiro signal
        Controller.buttonBarGroupContainer.spiroBtnClicked.connect(SpiroController.createSpiroComponent)
        Controller.buttonBarGroupContainer.spiroBtnClicked.connect(SpiroController.resizeWaveformArea)
//        SpiroController.spiroCloseClicked.connect(buttonBarGroupContainer.spiroClosebtnState);

        //Signal to Signal
        mainitem.shortcut_keyPressed.connect(Controller.ventGroupContainer.shortCutKeyPressed)
        mainitem.shortcut_keyReleased.connect(Controller.ventGroupContainer.shortCutKeyReleased)

        //Check if checkout has been done in last 24 hours.
        //If not directly go to checkout screen.

//        var checkoutStatus = checkoutReader.isCheckoutDoneBeforeTwentyFourHours();
//        console.log("checkout status in main.qml :" + checkoutStatus)
//        if(checkoutStatus === false) {
//            CheckoutComp.createChkoutComponent() ;
//            CheckoutComp.preCheckTitle();
//        }
    }
}
