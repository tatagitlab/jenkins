
#ifndef rs232_INCLUDED
#define rs232_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>



#if defined(__linux__) || defined(__FreeBSD__)

#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <sys/file.h>
#include <errno.h>
int OpenComport();
#else

#include <windows.h>
int OpenComport(char comPortSettings[]);
#endif


int ReadFromCSBComport(unsigned char *, int);
int WriteToCSBComport( unsigned char *, int);
void CloseComport();

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif


