#include <QDebug>
#include <QThread>      //For adding 3ms delay between the consecutive writeOnCSB() calls at boot-up
#include "../Source/CSB/Include/csbcommunication.h"
#include "../Source/CSB/Include/csbreadwrite.h"
#include "../Source/Generic/Include/ventsettings.h"

/**
 * @brief CSBCommunication::CSBCommunication - Constructor, which initializes flags and writes data to CSB(startup)
 * @param parent
 */
CSBCommunication::CSBCommunication(QObject *parent) : QObject(parent)
{
    m_ventilationMode = -1;
    m_d0 = 0xAA;
    m_d1 = 0;
    m_d2 = 0;
    m_d3 = 0;
    m_d4 = 0x55;


    m_bkpModeActiveAlarm = false;
    m_bkpModeSIMVPCV = true ;
    m_bkpModePSVPro = false ;
    m_standByMode = true;
    m_ventModeOn = false;
    m_bagModeOn = false;


    initializeEnum();
    ////qDebug()<<("CSB Comm")<<endl;

    openPort();
    writeOnCSB(250,1);
    QThread::msleep(3);
    writeOnCSB(246, 0);
    QThread::msleep(3);
    writeOnCSB(80,15);
    QThread::msleep(3);
    writeOnCSB(246, 0);
    QThread::msleep(3);
    writeOnCSB(80,15);
    QThread::msleep(3);
    writeOnCSB(246, 0);
    QThread::msleep(3);
    writeOnCSB(80,15);
    QThread::msleep(3);
    writeOnCSB(255,0);
    QThread::msleep(3);
    writeOnCSB(254,0);
    QThread::msleep(3);
    writeOnCSB(252,0);
    QThread::msleep(3);
    writeOnCSB(81,1);
    QThread::msleep(3);
    writeOnCSB(81,2);
    QThread::msleep(3);
    writeOnCSB(98,0);

}

/**
 * @brief CSBCommunication::~CSBCommunication - Destructor
 */
CSBCommunication::~CSBCommunication()
{
    CloseComport();
}

/**
 * @brief CSBCommunication::openPort - Opens the COM port(Windows - COM5, Linux - ttymxc2)
 */
void CSBCommunication::openPort()
{
    int  ret = -1;
#if defined(__linux__) || defined(__FreeBSD__)
    //qDebug()<< "---------Open com port Return  =  " ;
    ret = OpenComport();
    //qDebug()<< "+++++Open com port Return  =  " <<ret;
#else
    char comPortSettings[128] = "baud=9600 data=8 parity=n stop=1 dtr=on rts=on";
    ret =  OpenComport(comPortSettings);
    //qDebug()<< "+++++Open com port Return  =  " <<ret;
#endif
    if(ret == -1)
    {
        //emit showBanner();
    }
}

/**
 * @brief CSBCommunication::writeToCSB - Writes data to CSB
 * @param id - Quick key id(Primary and secondary)
 * @param data - value of the key
 * @param ventParmType - Primary/Secondary
 */
void CSBCommunication::writeToCSB(QString id, QString data, QString ventParmType)
{

    PrimaryQK pQK;
    SecondaryParams secParam;

    //qDebug()<<"writeToCSB"<<id<<data<<ventParmType<<endl;
    if(ventParmType == "Primary"){
        pQK = primaryQKList.at(id.toUtf8().constData());
        switch (pQK) {

        case PrimaryQK::TV_qkey:
        {
            int tv = data.toInt();

            //setting HighTV value
            int highTV = tv / 256;

            //setting lowTV value
            int lowTV = tv - (highTV * 256);

            ventilationMismatch->setCntTVLow(0);
            ventilationMismatch->setCntTVHigh(0); ;

            ventilationMismatch->setVmismatchTVLow(lowTV);
            writeOnCSB(VentModeParamTypes::LOW_tv,lowTV);

            ventilationMismatch->setVmismatchTVHigh(highTV);
            writeOnCSB(VentModeParamTypes::HIGH_tv,highTV);

        }
            break;

        case PrimaryQK::RR_qkey:
        {
            int rr = data.toInt();
            ventilationMismatch->setCntRR(0);
            ventilationMismatch->setVmismatchRR(rr);
            //qDebug() << "write RR" << rr;
            writeOnCSB(VentModeParamTypes::RR,rr);
        }
            break;

        case PrimaryQK::IE_qkey:
        {
            int ratio;
            ventilationMismatch->setCntIE(0);

            if( data == "2:1"){
                ratio = 5;
            }else{
                QRegExp rx("(\\:)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
                QStringList ie = data.split(rx);
                Q_ASSERT(ie.count()>1);
                float d2 = ie[1].toFloat();
                ratio = static_cast<int>(d2 * 10);

            }
            ventilationMismatch->setVmismatchIE(ratio);
            writeOnCSB(VentModeParamTypes::IE,ratio);
        }
            break;

        case PrimaryQK::PEEP_qkey:
        {
            int pp = data.toInt();
            ventilationMismatch->setCntPeep(0);
            ventilationMismatch->setVmismatchPeep(pp);
            //qDebug() << "write PEEP" << data;
            writeOnCSB(VentModeParamTypes::PEEP,pp);
        }
            break;

        case PrimaryQK::PSUPPORT_qkey:
        {
            int psupport = data.toInt();
            //qDebug() << "write PSUPPORT" <<psupport;
            writeOnCSB(VentModeParamTypes::PSupport,psupport);
        }
            break;

        case PrimaryQK::PINSP_qkey:
        {
            int iPinsp = data.toInt();
            ventilationMismatch->setCntPinsp(0);
            ventilationMismatch->setVmismatchPinsp(iPinsp);
            //qDebug() << "write Pinsp" << iPinsp;
            writeOnCSB(VentModeParamTypes::PInsp,iPinsp);
        }
            break;

        case PrimaryQK::RRMECH_qkey:
        {

            int iRRMech = data.toInt();
            
            writeOnCSB(VentModeParamTypes::RRMech,iRRMech);
        }
            break;
        }
    }
    else if(ventParmType == SECONDARY ){
        secParam = secondaryParamList.at(id.toUtf8().constData());

        switch (secParam) {
        case SecondaryParams::FLOW_TRIGGER_skey:
        {
            int iFlowTrigger = int(data.toFloat()*10);
            //qDebug() << "write FlowTrigger" << data;
            writeOnCSB(VentModeParamTypes::FlowTrigger,iFlowTrigger);
        }
            break;

        case SecondaryParams::TINSP_skey:
        {
            int iTinsp = int(data.toFloat()*10);
            //qDebug() << "write Tinsp" << data;
            writeOnCSB(VentModeParamTypes::TInsp,iTinsp);
        }
            break;
        case SecondaryParams::PMAX_skey:
        {
            int pMax = data.toInt();
            ventilationMismatch->setCntPmax(0);
            ventilationMismatch->setVmismatchPmax(pMax);
            //qDebug() << "write Pmax" << data;
            writeOnCSB(VentModeParamTypes::PMax,pMax);
        }
            break;
        case SecondaryParams::BACKUPTIME_skey:
        {
            int bt = data.toInt();
            ventilationMismatch->setCntBackupTime(0);
            ventilationMismatch->setVmismatchBackupTime(bt);
            //qDebug() << "write backup time" << data;
            writeOnCSB(VentModeParamTypes::BackUpTime,bt);
        }
            break;
        case SecondaryParams::EXITBACKUP_skey:
        {
            int bt = data.toInt();
            //qDebug() << "write Exit backup " << data;
            writeOnCSB(VentModeParamTypes::ExitBackUp,bt);
        }
            break;
        case SecondaryParams::TRIG_WINDOW_skey:
        {
            int trigWindow = data.toInt();
            //qDebug() << "write TriggerWindow" << data;
            writeOnCSB(VentModeParamTypes::TriggerWindow,trigWindow);
        }
            break;

        case SecondaryParams::END_OF_BREATH_skey:
        {
            int endOfBreath = data.toInt();
            //qDebug() << "write End of breath" << data;
            writeOnCSB(VentModeParamTypes::EndOfBreath,endOfBreath);
        }
            break;

        case SecondaryParams::TPAUSE_skey:
        {

            int tPause = data.toInt();
            //qDebug() << "write Tpause" << data;
            writeOnCSB(VentModeParamTypes::TPause,tPause);

        }
            break;
        }
    }
    else if (ventParmType == ALARM){

        AlarmSettingParams alarmSettings;

        alarmSettings = alarmSettingsParamList.at(id.toUtf8().constData());
        //qDebug()<<"Writing......"<<ventParmType;

        switch (alarmSettings) {
        case AlarmSettingParams::PpeakAlarmLow:
        {
            int ppeakLow = data.toInt();
            //qDebug()<<"write ppeak Low Limit"<< ppeakLow;
            writeOnCSB(AlarmSettingParams::PpeakAlarmLow,ppeakLow);
        }
            break;

        case AlarmSettingParams::PpeakAlarmHigh:
        {
            int ppeakHigh = data.toInt();
            //qDebug()<<"write ppeak High Limit"<< ppeakHigh;
            writeOnCSB(AlarmSettingParams::PpeakAlarmHigh,ppeakHigh);
        }
            break;
        case AlarmSettingParams::MVAlarmLow:
        {
            int mvLow = int(data.toFloat()*10);
            //qDebug()<<"write Minute Voume low Limit"<< mvLow;
            writeOnCSB(AlarmSettingParams::MVAlarmLow,mvLow);
        }
            break;

        case AlarmSettingParams::MVAlarmHigh:
        {
            int mvHigh = int(data.toFloat()*10);
            //qDebug()<<"write Minute Voume High Limit"<< mvHigh;
            writeOnCSB(AlarmSettingParams::MVAlarmHigh,mvHigh);
        }
            break;

        case AlarmSettingParams::TVAlarmLow:
        {
            int tvLow = data.toInt();
            //qDebug()<<"write Tidal Volume Low Limit"<< tvLow;
            writeOnCSB(AlarmSettingParams::TVAlarmLow,tvLow);
        }
            break;


        case AlarmSettingParams::TVAlarmHigh:
        {
            int tvHigh = data.toInt();
            //qDebug()<<"write Tidal Volume High Limit"<< tvHigh;
            writeOnCSB(AlarmSettingParams::TVAlarmHigh,tvHigh);
        }
            break;

        case AlarmSettingParams::O2AlarmLow:
        {
            int o2Low = data.toInt();
            //qDebug()<<"write O2 Low Limit"<< o2Low;
            writeOnCSB(AlarmSettingParams::O2AlarmLow,o2Low);
        }
            break;

        case AlarmSettingParams::O2AlarmHigh:
        {
            int o2High = data.toInt();
            //qDebug()<<"write O2 High Limit"<< o2High;
            writeOnCSB(AlarmSettingParams::O2AlarmHigh,o2High);
        }
            break;

        case AlarmSettingParams::AlarmVolume:
        {
            int alarmVolume = data.toInt();
            //qDebug()<<"write Alarm Volume"<< alarmVolume;
            writeOnCSB(AlarmSettingParams::AlarmVolume,alarmVolume);
        }
            break;
        case AlarmSettingParams::ApneaDelay:
        {
            int apneaDelay = data.toInt();
            //qDebug()<<"write Apnea Delay"<< apneaDelay;
            writeOnCSB(AlarmSettingParams::ApneaDelay,apneaDelay);
        }
            break;

        }

    }
}

/**
 * @brief CSBCommunication::writeVentModeToCSB - Writing mode to CSB
 * @param mode - Mode type(BtnMode_VCV, BtnMode_PCV, BtnMode_SIMVVCV, BtnMode_SIMVPCV, BtnMode_PSVPro)
 */
void CSBCommunication::writeVentModeToCSB(QString mode)
{
    //qDebug()<< "Mode +++++++ =  " << mode<< endl;
    if (mode == MODE_VCV )
    {
        ventilationMismatch->setVmismatchMode(ventilationModes::VCV_mode);
        ventilationMismatch->setCntMode(0);
        writeOnCSB(0, ventilationModes::VCV_mode); // VCV mode
    }
    else if (mode == MODE_PCV )
    {
        ventilationMismatch->setCntMode(0);
        ventilationMismatch->setVmismatchMode(ventilationModes::PCV_mode);
        writeOnCSB(0, ventilationModes::PCV_mode); // PCV mode
    }
    else if (mode == MODE_SIMV_VCV )
    {
        ventilationMismatch->setCntMode(0);
        ventilationMismatch->setVmismatchMode(ventilationModes::SIMV_VCV_mode);
        writeOnCSB(0, ventilationModes::SIMV_VCV_mode); // SIMV VCV mode
    }
    else if(mode == MODE_SIMV_PCV)
    {
        ventilationMismatch->setCntMode(0);
        ventilationMismatch->setVmismatchMode(ventilationModes::SIMV_PCV_mode);
        writeOnCSB(0, ventilationModes::SIMV_PCV_mode); // SIMV PCV mode
    }
    else if(mode == MODE_PSV_PRO)
    {
        ventilationMismatch->setCntMode(0);
        ventilationMismatch->setVmismatchMode(ventilationModes::PSV_pro_mode);
        writeOnCSB(0, ventilationModes::PSV_pro_mode); // PSV Pro mode
    }
}


/**
 * @brief CSBCommunication::initializeEnum - initializing the maps for Primary and Secondary keys
 */
void CSBCommunication::initializeEnum()
{
    //Primary quick key
    primaryQKList["TV_qKey"] = PrimaryQK::TV_qkey;
    primaryQKList["RR_qKey"] = PrimaryQK::RR_qkey;
    primaryQKList["IE_qKey"] = PrimaryQK::IE_qkey;
    primaryQKList["PEEP_qKey"] = PrimaryQK::PEEP_qkey;
    primaryQKList["Psupport_qKey"] = PrimaryQK::PSUPPORT_qkey;
    primaryQKList["Pinsp_qKey"] = PrimaryQK::PINSP_qkey;
    primaryQKList["RRMech_qKey"] = PrimaryQK::RRMECH_qkey;

    //Secondary params
    secondaryParamList["FlowTrigger_sKey"] = SecondaryParams::FLOW_TRIGGER_skey;
    secondaryParamList["Tinsp_sKey"] = SecondaryParams::TINSP_skey;
    secondaryParamList["Pmax_sKey"] = SecondaryParams::PMAX_skey;
    secondaryParamList["Trig_Window_sKey"] = SecondaryParams::TRIG_WINDOW_skey;
    secondaryParamList["EndofBreath_sKey"] = SecondaryParams::END_OF_BREATH_skey;
    secondaryParamList["Tpause_sKey"] = SecondaryParams::TPAUSE_skey;
    secondaryParamList["BackupTime_sKey"] = SecondaryParams::BACKUPTIME_skey;
    secondaryParamList["ExitBackup_sKey"] = SecondaryParams::EXITBACKUP_skey;


    //Alarm Settings Param List
    alarmSettingsParamList["Ppeak_Low"] = AlarmSettingParams::PpeakAlarmLow;
    alarmSettingsParamList["Ppeak_High"] = AlarmSettingParams::PpeakAlarmHigh;
    alarmSettingsParamList["MV_Low"] = AlarmSettingParams::MVAlarmLow;
    alarmSettingsParamList["MV_High"] = AlarmSettingParams::MVAlarmHigh;
    alarmSettingsParamList["TVexp_Low"] = AlarmSettingParams::TVAlarmLow;
    alarmSettingsParamList["TVexp_High"] = AlarmSettingParams::TVAlarmHigh;
    alarmSettingsParamList["O2_Low"] = AlarmSettingParams::TVAlarmHigh;
    alarmSettingsParamList["O2_High"] = AlarmSettingParams::TVAlarmHigh;
    alarmSettingsParamList["Alarm_Volume"] = AlarmSettingParams::AlarmVolume;
    alarmSettingsParamList["Apnea_Delay"] = AlarmSettingParams::ApneaDelay;

}

/**
 * @brief CSBCommunication::setVentMismatch - Initializing VentSettingMismatch object
 * @param ventMismatch
 */
void CSBCommunication::setVentMismatch(VentSettingMismatch *ventMismatch)
{
    ventilationMismatch = ventMismatch ;

}

/**
 * @brief CSBCommunication::readFromCSB - Reads data from CSB, for every 80ms this method is called(from main.qml)
 * @param dualStorageData - Dual storage object
 * @param alarmReader - Alarm object
 */
void CSBCommunication::readFromCSB(ParamDualStorage *dualStorageData, AlarmReader *alarmReader)
{
    unsigned char *buf;
    buf = static_cast<unsigned char*>(malloc(23));

    int numOfBytesRead = ReadFromCSBComport(buf, 23);
    int firstByte=0;
    int lastByte=0;
    int iSecondByte=0;
    int type=-1;
    int data = -1;
    int iAlarmId = -1 ;
    int iByte = -1;
    int iAlarmPosition = -1 ;
    int iBitPos = -1 ;

    if(numOfBytesRead == 23)
    {
        bool bVentMismatch = false;
        firstByte = static_cast<int>(buf[0]);
        iSecondByte = static_cast<int>(buf[1]);
        lastByte = static_cast<int>(buf[22]);

        type = static_cast<int>(buf[6]);
        data = static_cast<int>(buf[7]);


        //Vent mismatch - validating the critical parameters
        if(firstByte == CSB_FIRST_BYTE && lastByte == CSB_LAST_BYTE && iSecondByte == CSB_WAVEFORM_PKT){
            int csbCommand = 0;
            int csbData = 0;
            bool isBannerShown = false;

            bVentMismatch = ventilationMismatch->isVentilationSettingsMismatch(type, data, dualStorageData, csbCommand, csbData, isBannerShown);

            if(isBannerShown == true){
                writeOnCSB(250, 0);
                writeOnCSB(csbCommand, csbData) ;
            }
            else if(bVentMismatch == true){
                if((type == VentModeParamTypes::LOW_tv) ||(type == VentModeParamTypes::HIGH_tv)){
                    writeOnCSB(VentModeParamTypes::LOW_tv,ventilationMismatch->getVmismatchTVLow() );
                    writeOnCSB(VentModeParamTypes::HIGH_tv,ventilationMismatch->getVmismatchTVHigh() );
                }else{
                    writeOnCSB(csbCommand, csbData) ;
                }
            }
        }

        //Checking for preset mode and Vent/Bag (Command type = 0 (mode) and data = 3 (standby mode))
        if(firstByte == CSB_FIRST_BYTE && lastByte == CSB_LAST_BYTE && iSecondByte == CSB_WAVEFORM_PKT ) {
            if (type == MODE && data == ventilationModes::STANDBY_mode && m_standByMode) {
                //qDebug() << "*****Standby mode*****"<< endl;
                emit setStandByMode();
                m_standByMode = false;
            }
            else if (type == MODE && data == ventilationModes::BAG_mode && !m_bagModeOn) {
                //qDebug() << "*****Vent OFF/Bag Mode*****"<< endl;
                emit ventilationOff();
                m_bagModeOn = true;
                m_ventModeOn = false;
            }
            else if (type == MODE && data != ventilationModes::STANDBY_mode  && data != ventilationModes::BAG_mode && !m_ventModeOn) {
                //qDebug() << "*****Vent ON/Vent Mode*****"<< endl;
                emit ventilationOn();
                m_ventModeOn = true;
                m_bagModeOn = false;
            }
        }

        if(firstByte == CSB_FIRST_BYTE && lastByte == CSB_LAST_BYTE && iSecondByte == CSB_LOG_PKT){
            QString strTimestamp = QTime::currentTime().toString("HH:mm");

            //Alarms_START

            //Read Alarm data from CSB[D13, D14, D15, D16, D17, D18, D19, D20]

            //High priorities
            for( iByte=HALARM_START_BYTE, iAlarmPosition=32; iByte<=HALARM_END_BYTE; iByte++)
            {
                for(iBitPos =0; iBitPos<8; iBitPos++)
                {
                    iAlarmId = iAlarmPosition + iBitPos ;
                    if(((buf[iByte] >> iBitPos) & 1)){

                        //39 - SYSERR  TO_ADD
                        if(iAlarmId == CSB_ALARM_SYSERR){
                            writeOnCSB(250,0);
                          //  emit showBanner();
                        }

                        alarmReader->updateHighPriorityList(iAlarmId, strTimestamp);

                    }else{
                        alarmReader->deleteFromHighPriorityList(iAlarmId);
                    }
                }
                iAlarmPosition = iAlarmPosition+8;
            }

            //Medium priorities
            for( iByte=MALARM_START_BYTE, iAlarmPosition=0; iByte<=MALARM_END_BYTE; iByte++)
            {
                for( iBitPos =0; iBitPos<8; iBitPos++)
                {
                    iAlarmId = iAlarmPosition + iBitPos ;
                    if(((buf[iByte] >> iBitPos) & 1)){
                        alarmReader->updateMediumPriorityList(iAlarmId, strTimestamp);
                    }else{
                        alarmReader->deleteFromMediumPriorityList(iAlarmId);
                    }
                }
                iAlarmPosition = iAlarmPosition+8;
            }

            //Low priorities

            for( iByte=LALARM_START_BYTE, iAlarmPosition=24; iByte<=LALARM_END_BYTE; )
            {
                for(iBitPos =0; iBitPos<8; iBitPos++)
                {
                    iAlarmId = iAlarmPosition + iBitPos ;
                    if(((buf[iByte] >> iBitPos) & 1)){
                        alarmReader->updateLowPriorityList(iAlarmId);
                        //Backup mode changes
                        if(iAlarmId == CSB_ALARM_BACKUPMODE){
                            //If CSB sends Backup mode alarm bit as set, check mode from CSB(if it is SIMVPCV)
                            //send signal to view to change mode from PSVPro to SIMVPCV
                            //qDebug()<<"Backup mode alarm is set: "<<m_bkpModeActiveAlarm ;
                            m_bkpModeActiveAlarm = true ;
                            m_bkpModePSVPro = true;


                        }
                    }else{
                        alarmReader->deleteFromLowPriorityList(iAlarmId);
                        //Backup mode changes
                        if(iAlarmId == CSB_ALARM_BACKUPMODE){
                            //If CSB sends Backup mode alarm bit as reset, check mode from CSB(if it is PSVPro)
                            //send signal to view to change mode from SIMVPCV to PSVPro
                            //qDebug()<<"Backup mode alarm is reset: "<<m_bkpModeActiveAlarm ;
                            m_bkpModeActiveAlarm = false ;
                            m_bkpModeSIMVPCV = true;


                        }
                    }
                }
                iByte = iByte+4 ;
                iAlarmPosition = iAlarmPosition+32;

            }

        }

        if(firstByte == CSB_FIRST_BYTE && lastByte == CSB_LAST_BYTE
                && type == ventilationModes::MECHANICAL_mode && iSecondByte == CSB_WAVEFORM_PKT){

            m_ventilationMode = data;

            if(data == ventilationModes::SIMV_PCV_mode){
                //qDebug()<<"Vent SIM PCV mode ===== " << m_ventilationMode<<endl ;

                //send signal to view to change mode from PSVPro to SIMVPCV
                //Check the Backup mode alarm bit and make sure to emit signal only once.
                if(m_bkpModeActiveAlarm && m_bkpModeSIMVPCV){
                    m_bkpModeSIMVPCV = false ;
                    //qDebug()<<"Changing the mode to SIMVPCV";
                    emit csbBkpModeSIMVPCV();
                }

            }else if(data == ventilationModes::PSV_pro_mode){

                //qDebug()<<"Vent PSV pro mode ===== " << m_ventilationMode<<endl ;

                //send signal to view to change mode from SIMVPCV to PSVPro
                //Check the Backup mode alarm bit(should be reset) and make sure to emit signal only once.
                if(!m_bkpModeActiveAlarm && m_bkpModePSVPro){
                    m_bkpModePSVPro = false ;
                    //qDebug()<<"Changing the mode to PSVPro";
                    emit csbBkpModePSVPro();
                }
            }


        }

        //This code need to be re-used when DU needs to show the user confirmed value only once the CSB sends that data
#if 0
        //VCV Mode

        if(firstByte == CSB_FIRST_BYTE && lastByte == CSB_LAST_BYTE
                && iSecondByte == CSB_WAVEFORM_PKT){
            switch (ventilationMode) {
            case ventilationModes::VCV_mode:
            {

                switch (type) {
                case VentModeParamTypes::LOW_tv:
                {
                    tvValuelow = data;
                }
                    break;
                case VentModeParamTypes::HIGH_tv:
                {
                    tvValuehigh = data;
                    tvValue = tvValuelow + (tvValuehigh * 256);
                    //qDebug()<<"Value of TV at Buffer[7] in readvalCSB VCV" << tvValue<<endl ;
                    emit tvValuesUI(QString::number(tvValue));
                }
                    break;

                case VentModeParamTypes::RR:
                {
                    rrValue = data;
                    //qDebug()<<"Value of RR at Buffer[7] in readvalCSB VCV" <<rrValue<<endl ;
                    emit rrValuesUI(rrValue);
                }
                    break;

                case VentModeParamTypes::PEEP:
                {
                    ppValue = data;
                    //qDebug()<<"Value of PEEP at Buffer[7] in readvalCSB VCV" <<ppValue<<endl ;
                    emit ppValuesUI(ppValue);
                }
                    break;
                case VentModeParamTypes::IE:
                {
                    ieValue = data;
                    QString temp1 = "1";
                    //qDebug()<<"Value of I:E at Buffer[7] in readvalCSB VCV" <<ieValue<<endl ;
                    if(ieValue > 5){
                        float temp = ieValue / 10.0f;
                        i_e = temp1 + ":" + QString::number(temp);
                        //qDebug()<<"Value of I:E converted" << i_e ;
                    }else{
                        temp1 = "1";
                        QString temp2 = "2";
                        i_e = temp2 + ":" + temp1;
                        //qDebug()<<"Value of I:E converted" << i_e ;
                    }
                    emit ieValuesUI(i_e);
                }
                    break;
                }
            }
                break;

            case ventilationModes::PCV_mode:
            {
                switch (type) {
                case VentModeParamTypes::PInsp:
                {
                    pinspValue = data;
                    //qDebug()<<"Value of PINSP at Buffer[7] in readvalCSB PCV" <<pinspValue<<endl ;
                    emit pinspValueUI(pinspValue);
                }
                    break;

                case VentModeParamTypes::RR:
                {
                    rrValue = data;
                    //qDebug()<<"Value of RR at Buffer[7] in readvalCSB PCV" <<rrValue<<endl ;
                    emit rrValuesUI(rrValue);
                }
                    break;

                case VentModeParamTypes::PEEP:
                {
                    ppValue = data;
                    //qDebug()<<"Value of PEEP at Buffer[7] in readvalCSB PCV" <<ppValue<<endl ;
                    emit ppValuesUI(ppValue);
                }
                    break;

                case VentModeParamTypes::IE:
                {

                    ieValue = data;
                    QString temp1 = "1";
                    //qDebug()<<"Value of I:E at Buffer[7] in readvalCSB PCV" <<ieValue<<endl ;
                    if(ieValue > 5){
                        float temp = ieValue / 10.0f;
                        i_e = temp1 + ":" + QString::number(temp);
                        //qDebug()<<"Value of I:E converted" << i_e ;
                    }else{
                        temp1 = "1";
                        QString temp2 = "2";
                        i_e = temp2 + ":" + temp1;
                        //qDebug()<<"Value of I:E converted" << i_e ;
                    }
                    emit ieValuesUI(i_e);
                }
                    break;

                case VentModeParamTypes::PMax:
                {
                    pmaxValue = data;
                    //qDebug()<<"Value of Pmax at Buffer[7] in readvalCSB PCV" <<pmaxValue<<endl ;
                    emit pmaxValueUI(pmaxValue);
                }
                    break;
                }
            }
                break;

            case ventilationModes::SIMV_VCV_mode:
            {
                switch (type) {
                case VentModeParamTypes::LOW_tv:
                {
                    tvValuelow = data;
                }

                    break;

                case VentModeParamTypes::HIGH_tv:
                {
                    tvValuehigh = data;
                    tvValue = tvValuelow + (tvValuehigh * 256);
                    //qDebug()<<"Value of TV at Buffer[7] in readvalCSB SIMV VCV" << tvValue<<endl ;
                    emit tvValuesUI(QString::number(tvValue));
                }
                    break;

                case VentModeParamTypes::RR:
                {
                    rrValue = data;
                    //qDebug()<<"Value of RR at Buffer[7] in readvalCSB SIMV VCV" <<rrValue<<endl ;
                    emit rrValuesUI(rrValue);
                }
                    break;

                case VentModeParamTypes::PEEP:
                {
                    ppValue = data;
                    //qDebug()<<"Value of PEEP at Buffer[7] in readvalCSBSIMV VCV" <<ppValue<<endl ;
                    emit ppValuesUI(ppValue);

                }
                    break;

                case VentModeParamTypes::PSupport:
                {
                    psValue = data;
                    //qDebug()<<"Value of PSUPPORT at Buffer[7] in readvalCSB SIMV VCV" <<psValue<<endl ;
                    emit psValueUI(psValue);
                }
                    break;

                case VentModeParamTypes::FlowTrigger:
                {
                    flowTriggerValue = data/10;
                    //qDebug()<<"Value of FLOW TRIGGER at Buffer[7] in readvalCSB SIMV VCV" <<flowTriggerValue<<endl ;
                    emit flowTriggerValueUI(flowTriggerValue);
                }
                    break;

                case VentModeParamTypes::TInsp:
                {
                    tInspValue = data/10;
                    //qDebug()<<"Value of TINSP at Buffer[7] in readvalCSB SIMV VCV" <<tInspValue<<endl ;
                    emit tInspValueUI(tInspValue);
                }
                    break;

                case VentModeParamTypes::PMax:
                {
                    pmaxValue = data;
                    //qDebug()<<"Value of PMAX at Buffer[7] in readvalCSB SIMV VCV" <<pmaxValue<<endl ;
                    emit pmaxValueUI(pmaxValue);
                }
                    break;

                case VentModeParamTypes::TriggerWindow:
                {
                    trigWindowValue = data;
                    //qDebug()<<"Value of TRIGGER WINDOW at Buffer[7] in readvalCSB SIMV VCV" <<trigWindowValue<<endl ;
                    emit trigWindowValueUI(trigWindowValue);
                }
                    break;

                case VentModeParamTypes::EndOfBreath:
                {
                    eobValue = data;
                    //qDebug()<<"Value of END OF BREATH at Buffer[7] in readvalCSB SIMV VCV" <<eobValue<<endl ;
                    emit eobValueUI(eobValue);
                }
                    break;
                case VentModeParamTypes::TPause:
                {
                    tpauseValue = data;
                    //qDebug()<<"Value of TPAUSE at Buffer[7] in readvalCSB SIMV VCV" <<tpauseValue<<endl ;
                    emit tPauseValueUI(tpauseValue);

                }
                    break;
                }
            }
                break;

            case ventilationModes::PSV_pro_mode:
            {
                switch (type) {
                case VentModeParamTypes::PEEP:
                {
                    ppValue = data;
                    emit ppValuesUI(ppValue);
                }
                    break;

                case VentModeParamTypes::PSupport:
                {
                    psValue = data;
                    ////qDebug()<<"Value of PSUPPORT at Buffer[7] in readvalCSB PSV PRO" <<psValue<<endl ;
                    emit psValueUI(psValue);
                }
                    break;

                case VentModeParamTypes::PInsp:
                {
                    pinspValue = data;
                    ////qDebug()<<"Value of PINSP at Buffer[7] in readvalCSB PSV PRO" <<pinspValue<<endl ;
                    emit pinspValueUI(pinspValue);
                }
                    break;

                case VentModeParamTypes::RRMech:
                {
                    rrMechValue = data;
                    ////qDebug()<<"Value of rrMEch at Buffer[7] in readvalCSB PSV PRO" <<rrMechValue<<endl ;
                    emit rrMechValueUI(rrMechValue);
                }
                    break;

                case VentModeParamTypes::FlowTrigger:
                {
                    flowTriggerValue = data/10;
                    ////qDebug()<<"Value of FLOW TRIGGER at Buffer[7] in readvalCSB PSV PRO" <<flowTriggerValue<<endl ;
                    emit flowTriggerValueUI(flowTriggerValue);
                }
                    break;

                case VentModeParamTypes::TriggerWindow:
                {
                    trigWindowValue = data;
                    emit trigWindowValueUI(trigWindowValue);
                }
                    break;
                case VentModeParamTypes::EndOfBreath:
                {
                    eobValue = data;
                    ////qDebug()<<"Value of END OF BREATH at Buffer[7] in readvalCSB PSV PRO" <<eobValue<<endl ;
                    emit eobValueUI(eobValue);
                }
                    break;

                case VentModeParamTypes::PMax:
                {
                    pmaxValue = data;
                    ////qDebug()<<"Value of PMAX at Buffer[7] in readvalCSB  PSV PRO" <<pmaxValue<<endl ;
                    emit pmaxValueUI(pmaxValue);

                }
                    break;

                case VentModeParamTypes::TInsp:
                {
                    tInspValue = data/10;
                    ////qDebug()<<"Value of TINSP at Buffer[7] in readvalCSB PSV PRO" <<tInspValue<<endl ;
                    emit tInspValueUI(tInspValue);
                }
                    break;

                case VentModeParamTypes::BackUpTime:
                {
                    backupTimeValue = data;
                    ////qDebug()<<"Value of backupTime at Buffer[7] in readvalCSB PSVPRO" <<backupTimeValue<<endl ;
                    emit backupTimeValueUI(backupTimeValue);
                }
                    break;

                }

            }
                break;
            }
        }

#endif
        //Alarm Setup Data (Need to check only the first,last and waveform byte)
        switch (type) {
        case AlarmSettingParams::PpeakAlarmLow:
        {
            ppeakAlarmLow = data;
            //qDebug()<<"Value of PPEAK Low Limit at Buffer[7] in readvalCSB" <<ppeakAlarmLow<<endl ;
        }
            break;
        case AlarmSettingParams::PpeakAlarmHigh:
        {
            ppeakAlarmHigh = data;
            //qDebug()<<"Value of PPEAK High Limit at Buffer[7] in readvalCSB" <<ppeakAlarmHigh<<endl ;
        }
            break;
        case AlarmSettingParams::MVAlarmLow:
        {
            mvAlarmLow = data/10;
            //qDebug()<<"Value of MV Low Limit at Buffer[7] in readvalCSB" <<mvAlarmLow<<endl ;
        }
            break;

        case AlarmSettingParams::MVAlarmHigh:
        {
            mvAlarmHigh = data/10;
            //qDebug()<<"Value of MV High Limit at Buffer[7] in readvalCSB" <<mvAlarmHigh<<endl ;
        }
            break;

        case AlarmSettingParams::TVAlarmLow:
        {
            tvAlarmLow = data;
            //qDebug()<<"Value of TV Low Limit at Buffer[7] in readvalCSB" <<tvAlarmLow<<endl ;
        }
            break;

        case AlarmSettingParams::TVAlarmHigh:
        {
            tvAlarmHigh = data;
            //qDebug()<<"Value of TV High Limit at Buffer[7] in readvalCSB" <<tvAlarmHigh<<endl ;
        }
            break;

        case AlarmSettingParams::O2AlarmLow:
        {
            o2AlarmLow = data;
            //qDebug()<<"Value of 02 Low Limit at Buffer[7] in readvalCSB" <<o2AlarmLow<<endl ;
        }
            break;

        case AlarmSettingParams::O2AlarmHigh:
        {
            o2AlarmHigh = data;
            //qDebug()<<"Value of O2 High Limit at Buffer[7] in readvalCSB" <<o2AlarmHigh<<endl ;
        }
            break;

        case AlarmSettingParams::AlarmVolume:
        {
            alarmVolume = data;
            //qDebug()<<"Value of alarm Volume at Buffer[7] in readvalCSB" <<alarmVolume<<endl ;
        }
            break;

        case AlarmSettingParams::ApneaDelay:
        {
            apneaDelay = data;
            //qDebug()<<"Value of apnea Delay at Buffer[7] in readvalCSB" <<apneaDelay<<endl ;
        }
            break;
        }

    }
}

/**
 * @brief CSBCommunication::writeOnCSB - Writing data to CSB, if port is not opened, shows the banner
 * @param D1 - command type(Ex: RR, TV, ...)
 * @param D2 - command data
 */
void CSBCommunication::writeOnCSB(int D1, int D2)//QString value
{

    int bytesWritten = -1;
    int d1 = D1;
    int d2 = D2;
    int checksum = (((d1 + d2) & 0xff) ^ 0xf0); //checksum
    unsigned char writeData[5];
    writeData[0] = static_cast<unsigned char>(m_d0);
    writeData[1] = static_cast<unsigned char>(d1);
    writeData[2] = static_cast<unsigned char>(d2);
    writeData[3] = static_cast<unsigned char>(checksum);
    writeData[4] = static_cast<unsigned char>(m_d4);

    unsigned char *data =static_cast<unsigned char*>(writeData);
    bytesWritten = WriteToCSBComport(data,5);
    if(bytesWritten == -1)
    {
        //emit showBanner();
    }
}
