

#include "../Source/CSB/Include/csbreadwrite.h"

#if defined(__linux__) || defined(__FreeBSD__)

int comPort;

int error;

struct termios newComPortSettings,
        comPortSettings;

/**
 * @brief OpenComport - setting the flags and opening the port ttymxc2
 * @return
 */
int OpenComport()
{
    int baudr = B19200;
    int status;

    int cbits=CS8;
    int cpar=0;
    int ipar=IGNPAR;
    int bstop=0;

    comPort = open("/dev/ttymxc2", O_RDWR | O_NOCTTY | O_NDELAY);
    if(comPort == -1)
    {
        perror("unable to open comport ");
        return(-1);
    }

    /* lock access so that another process can't also use the port */
    if(flock(comPort, LOCK_EX | LOCK_NB) != 0)
    {
        close(comPort);
        perror("Another process has locked the comport.");
        return(-1);
    }

    memset(&newComPortSettings, 0, sizeof(newComPortSettings));  /* clear the new struct */

    newComPortSettings.c_cflag = cbits | cpar | bstop | CLOCAL | CREAD;
    newComPortSettings.c_iflag = ipar;
    newComPortSettings.c_oflag = 0;
    newComPortSettings.c_lflag = 0;
    newComPortSettings.c_cc[VMIN] = 0;      /* block untill n bytes are received */
    newComPortSettings.c_cc[VTIME] = 0;     /* block untill a timer expires (n * 100 mSec.) */

    cfsetispeed(&newComPortSettings, baudr);
    cfsetospeed(&newComPortSettings, baudr);

    error = tcsetattr(comPort, TCSANOW, &newComPortSettings);
    if(error==-1)
    {
        close(comPort);
        flock(comPort, LOCK_UN);  /* free the port so that others can use it. */
        perror("unable to adjust portsettings ");
        return(-1);
    }

    if(ioctl(comPort, TIOCMGET, &status) == -1)
    {
        flock(comPort, LOCK_UN);  /* free the port so that others can use it. */
        perror("unable to get portstatus");
        return(-1);
    }

    status |= TIOCM_DTR;    /* turn on DTR */
    status |= TIOCM_RTS;    /* turn on RTS */

    if(ioctl(comPort, TIOCMSET, &status) == -1)
    {

        flock(comPort, LOCK_UN);  /* free the port so that others can use it. */
        perror("unable to set portstatus");
        return(-1);
    }

    return(0);
}

/**
 * @brief ReadFromCSBComport - Read CSB data
 * @param buf - Buffer to fill
 * @param size - size of buffer (23 bytes)
 * @return
 */
int ReadFromCSBComport( unsigned char *buf, int size)
{
    int n;

    n = read(comPort, buf, size);

    if(n < 0)
    {
        if(errno == EAGAIN)  return 0;
    }

    return(n);
}

/**
 * @brief WriteToCSBComport - Write data to CSB
 * @param buf - Buffer to write
 * @param size - Size of the buffer(5 bytes)
 * @return
 */
int WriteToCSBComport( unsigned char *buf, int size)
{
    int n = write(comPort, buf, size);
    if(n < 0)
    {
        if(errno == EAGAIN)
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }

    return(n);
}

/**
 * @brief CloseComport - Close the port
 */
void CloseComport()
{
    int status;

    if(ioctl(comPort, TIOCMGET, &status) == -1)
    {
        perror("unable to get portstatus");
    }

    status &= ~TIOCM_DTR;    /* turn off DTR */
    status &= ~TIOCM_RTS;    /* turn off RTS */

    if(ioctl(comPort, TIOCMSET, &status) == -1)
    {
        perror("unable to set portstatus");
    }

    close(comPort);

    flock(comPort, LOCK_UN);  /* free the port so that others can use it. */
}


#else

#define RS232_PORTNR  16

HANDLE comPort;

/**
 * @brief OpenComport - setting the flags and opening the port COM5(Windows)
 * @param comPortSettings
 * @return
 */
int OpenComport(char comPortSettings[])
{


    comPort = CreateFileA("\\\\.\\COM5",
                          GENERIC_READ|GENERIC_WRITE,
                          0,
                          NULL,
                          OPEN_EXISTING,
                          0,
                          NULL);

    if(comPort == INVALID_HANDLE_VALUE)
    {
        printf("unable to open comport\n");
        return(-1);
    }

    DCB port_settings;
    memset(&port_settings, 0, sizeof(port_settings));
    port_settings.DCBlength = sizeof(port_settings);

    //if(!BuildCommDCBA(mode_str, &port_settings))
    if(!BuildCommDCBA(comPortSettings, &port_settings))
    {
        printf("unable to set comport dcb settings\n");
        CloseHandle(comPort);
        return(-1);
    }

    if(!SetCommState(comPort, &port_settings))
    {
        printf("unable to set comport cfg settings\n");
        CloseHandle(comPort);
        return(-1);
    }

    COMMTIMEOUTS comportTimeouts;

    comportTimeouts.ReadIntervalTimeout         = MAXDWORD;
    comportTimeouts.ReadTotalTimeoutMultiplier  = 0;
    comportTimeouts.ReadTotalTimeoutConstant    = 0;
    comportTimeouts.WriteTotalTimeoutMultiplier = 0;
    comportTimeouts.WriteTotalTimeoutConstant   = 0;

    if(!SetCommTimeouts(comPort, &comportTimeouts))
    {
        printf("unable to set comport time-out settings\n");
        CloseHandle(comPort);
        return(-1);
    }

    return(0);
}

/**
 * @brief ReadFromCSBComport - Read data from CSB
 * @param buf - Buffer to fill
 * @param size - size of the buffer(23 bytes)
 * @return
 */

int ReadFromCSBComport(unsigned char *buf, int size)
{
    int numOfBytesRead = -1;

    ReadFile(comPort, buf, size, (LPDWORD)((void *)&numOfBytesRead), NULL);

    return(numOfBytesRead);
}

/**
 * @brief WriteToCSBComport - Writing data to CSB
 * @param buf - Data to send it to CSB
 * @param size - Size of the buf(5bytes)
 * @return
 */
int WriteToCSBComport( unsigned char *buf, int size)
{
    int n;

    if(WriteFile(comPort, buf, size, (LPDWORD)((void *)&n), NULL))
    {
        return(n);
    }

    return(-1);
}

/**
 * @brief CloseComport - Closing the comPort
 */
void CloseComport()
{
    CloseHandle(comPort);
}

#endif
