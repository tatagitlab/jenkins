var dropdownComponent;
var strObjDropdown;
var spiroDropdownObj;
var spiroBottomDropdownObj;
var dropdownValues ;


var spiroDropdownZValue = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdownZValue"))
var spiroDropdownImage = component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdownImage")
var spiroDropdownHeight = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdownHeight"))
var spiroDropdownMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdownMargin"))
var iSpiroDropdownTextMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdownTextMargin"))
var spiroChosenItemTextLeftMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroChosenItemTextLeftMargin"))
var spiroChosenItemTextTopMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroChosenItemTextTopMargin"))
var spiroTextColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroTextColor")
var upperdropdownValue1 = component.getComponentResourceString("Component","Spirometry","Label","Spiro_Upper_Dropdown")
var upperdropdownValue2 = component.getComponentResourceString("Component","Spirometry","Label","Spiro_Upper_Dropdown")
var bottomdropdownValue1 = component.getComponentResourceString("Component","Spirometry","Label","Spiro_Bottom_Dropdown")
var bottomdropdownValue2 = component.getComponentResourceString("Component","Spirometry","Label","Spiro_Bottom_Dropdown")

var arrtopDropdownList = [upperdropdownValue1,upperdropdownValue2]
var arrbottomDropdownList = [bottomdropdownValue1,bottomdropdownValue2]

function createDropdownComponent() {
    dropdownComponent = Qt.createComponent("qrc:/Source/Spirometry/Qml/SpiroDropDown.qml");
    if (dropdownComponent.status === Component.Ready)
        finishCreation();
    else
        dropdownComponent.statusChanged.connect(finishCreation);
}
function finishCreation() {
    if (dropdownComponent.status === Component.Ready) {
        spiroDropdownObj = dropdownComponent.createObject(spiromainpage,{"objectName":"topDropdown",
                                                         "iSpiroDropdownZValue":spiroDropdownZValue,
                                                         "strSpiroDropdownImage":spiroDropdownImage,
                                                         "iSpiroDropdownHeight":spiroDropdownHeight,
                                                         "iSpiroDropdownMargin":spiroDropdownMargin,
                                                         "iSpiroDropdownTextMargin":iSpiroDropdownTextMargin,
                                                         "iSpiroChosenItemTextLeftMargin":spiroChosenItemTextLeftMargin,
                                                         "iSpiroChosenItemTextTopMargin":spiroChosenItemTextTopMargin,
                                                         "strSpiroTextColor":spiroTextColor
                                                     });
        spiroBottomDropdownObj = dropdownComponent.createObject(spiromainpage,{"objectName":"bottomDropdown",
                                                         "iSpiroDropdownZValue":spiroDropdownZValue,
                                                         "strSpiroDropdownImage":spiroDropdownImage,
                                                         "iSpiroDropdownHeight":spiroDropdownHeight,
                                                         "iSpiroDropdownMargin":spiroDropdownMargin,
                                                         "iSpiroDropdownTextMargin":iSpiroDropdownTextMargin,
                                                         "iSpiroChosenItemTextLeftMargin":spiroChosenItemTextLeftMargin,
                                                         "iSpiroChosenItemTextTopMargin":spiroChosenItemTextTopMargin,
                                                         "strSpiroTextColor":spiroTextColor
                                                     });
        spiroDropdownObj.getValue(arrtopDropdownList);
        spiroBottomDropdownObj.getValue(arrbottomDropdownList);
        spiroDropdownObj.spiroComboBoxClicked.connect(otherDropdownClicked);
        spiroBottomDropdownObj.spiroComboBoxClicked.connect(spiroOtherDropdownClicked);
        spiroDropdownObj.spiroPageMouseClicked.connect(settingSpiroPageFocus);
        spiroBottomDropdownObj.spiroPageMouseClicked.connect(settingSpiroPageFocus);

        //        dropdownObj.getValue(dropdownValues);
        if (spiroDropdownObj === null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (dropdownComponent.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}
