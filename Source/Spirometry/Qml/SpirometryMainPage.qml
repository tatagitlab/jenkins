import QtQuick 2.9
import "../JavaScript/SpiroController.js" as SproCtrl
import "../JavaScript/SpiroDropdownController.js" as SpirodropdownCtrl
Rectangle{
    id: spiromainpage
    objectName: "SpirometryGrp"

    property int iSpiroWidth
    property int iSpiroHeight
    property int iSpiroZvalue
    property string strSpiroColor
    property string strSpiroBorderColor

    property string strSpiroTitleText
    property string strSpiroFontFamily: geFont.geFontStyle()
    property int iSpiroTitlePixelSize
    property string strSpiroTitleColor
    property int iSpiroTitleLeftMargin
    property int iSpiroTitleTopMargin

    property int iSpiroCloseBtnWidth
    property int iSpiroCloseBtnHeight
    property int iSpiroCloseBtnBorderWidth
    property int iSpiroCloseRightMargin
    property int iSpiroCloseTopMargin
    property string strSpiroTchColor
    property string strSpiroCloseBtnBgColor
    property string strSpiroStateBorderColor
    property string strSpiroCloseBtnBorderColor
    property string strSpiroCloseBtnSource
    property string strSpiroCloseBtnTchSource

    property string strSpiroValueLabel
    property int iSpiroValueLabelPixelSize
    property string strSpiroValueLabelColor
    property int iSpiroValueLabelBottomMargin
    property int iSpiroValueLabelLeftMargin

    property string strSpiroValue
    property int iSpiroValuePixelSize
    property string strSpiroValueColor
    property int iSpiroValueBottomMargin
    property int iSpiroValueLeftMargin

    property string strSpiroValueRightLabel
    property int iSpiroValueRightLabelPixelSize
    property string strSpiroValueRightLabelColor
    property int iSpiroValueRightLabelBottomMargin
    property int iSpiroValueRightLabelLeftMargin

    property string strSpiroDropdownLabel
    property int iSpiroDropdowntLabelPixelSize
    property string strSpiroDropdownLabelColor
    property int iSpiroDropdownLabelBottomMargin
    property int iSpiroDropdownLabelLeftMargin

    property int iSpiroSaveBtnWidth
    property int iSpiroSaveBtnHeight
    property string strSaveBtnBgColor: "#444444"
    property int iSaveBtnBorderWidth: 1
    property string strSaveBtnBorderColor: "#444444"
    property int iSpiroSavebtnBottomMargin
    property int iSpiroSavebtnRightMargin
    property string strSpiroSavebtnImage

    property int iTopDropdownLeftMargin
    property int iTopDropdownTopMargin
    property int iTopDropdownHeight
    property int iTopDropdownWidth
    property string strTopDropdownColor
    property string strTopDropdownBorderColor
    property int iTopDropdownFontSize
    property int iTopDropdownImageWidth
    property int iTopDropdownImageHeight
    property int iTopDropdownImageTopMargin
    property int iTopDropdownImageRightMargin

    property int iBottomDropdownBottomMargin
    property int iBottomDropdownLeftMargin
    property int iBottomDropdownHeight
    property int iBottomDropdownWidth
    property string strBottomDropdownColor
    property string strBottomDropdownBorderColor
    property int iBottomdropdownFontSize
    property int iBottomDropdownImageWidth
    property int iBottomDropdownImageHeight
    property int iBottomDropdownImageTopMargin
    property int iBottomDropdownImageRightMargin

    property var topDropdownList
    property var bottomDropdownList

    property var dropdownRef
    property var bottomDropdownRef




    property int iSpiroDropdownLabelPixelSize: 16


    signal spiroCloseClicked();

    function otherDropdownClicked()
    {

//        var spiroTopdropdownRef = getCompRef(spiromainpage,"topDropdown");
        var spiroBottomdropdownRef = getCompRef(spiromainpage,"bottomDropdown");

//        if(spiroBottomdropdownRef.state === "dropDown")
//        {
    //        console.log("spiroBottomdropdownRef.state"+spiroBottomdropdownRef.state);
//            console.log("2nd dropdown");
//            spiroTopdropdownRef.state = "";
//        }
//         if(spiroTopdropdownRef.state === "dropDown")
//        {
//            console.log("1st dropdown");
            spiroBottomdropdownRef.state = "noDropdown";
//        }
    }

    function spiroOtherDropdownClicked()
    {
     var spiroTopdropdownRef = getCompRef(spiromainpage,"topDropdown");
      spiroTopdropdownRef.state = "noDropdown";
    }

    function settingSpiroPageFocus()
    {
        spiromainpage.focus = true;
        spiromainpage.forceActiveFocus();
    }

    width: iSpiroWidth
    height: iSpiroHeight
    z:iSpiroZvalue
    color: strSpiroColor
    border.color: strSpiroBorderColor

    MouseArea{
        anchors.fill: parent
        onClicked: {
            var spiroTopdropdownRef = getCompRef(spiromainpage,"topDropdown");
            var spiroBottomdropdownRef = getCompRef(spiromainpage,"bottomDropdown");
            spiroTopdropdownRef.state = "noDropdown";
            spiroBottomdropdownRef.state = "noDropdown";
            spiromainpage.focus = true;
            spiromainpage.forceActiveFocus();
        }
    }

    Text {
        id: spirotitletext
        text: qsTr(strSpiroTitleText)
        font.family: strSpiroFontFamily
        font.pixelSize: iSpiroTitlePixelSize
        color: strSpiroTitleColor
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: iSpiroTitleTopMargin
        anchors.leftMargin: iSpiroTitleLeftMargin
    }
    Rectangle {
        id: closeBtn
        border.width: iSpiroCloseBtnBorderWidth
        height: iSpiroCloseBtnHeight
        width: iSpiroCloseBtnWidth
        radius: 3
        color: closeMouseArea.pressed? strSpiroTchColor:strSpiroCloseBtnBgColor
        objectName: "CloseBtnObj"
        border.color: activeFocus ? strSpiroStateBorderColor :strSpiroCloseBtnBorderColor
        anchors.right: parent.right
        anchors.rightMargin: iSpiroCloseRightMargin
        anchors.top: parent.top
        anchors.topMargin: iSpiroCloseTopMargin
        Image {
            source: closeMouseArea.pressed? strSpiroCloseBtnTchSource : strSpiroCloseBtnSource
            anchors.centerIn: parent
        }

        MouseArea {
            id: closeMouseArea
            anchors.fill: closeBtn
            onClicked: {
                spiroCloseClicked();
            }
        }
        Keys.onReturnPressed: {
            spiroCloseClicked();
        }
    }
    Text {
        id: label
        text: qsTr(strSpiroValueLabel)
        font.family: strSpiroFontFamily
        font.pixelSize: iSpiroValueLabelPixelSize
        color: strSpiroValueLabelColor
        anchors.left: parent.left
        anchors.leftMargin: iSpiroValueLabelLeftMargin
        anchors.bottom: spirodropdownLabel.top
        anchors.bottomMargin: iSpiroValueLabelBottomMargin
    }
    Text {
        id: values
        text: qsTr(strSpiroValue)
        font.family: strSpiroFontFamily
        font.pixelSize: iSpiroValuePixelSize
        color: strSpiroValueColor
        anchors.left: label.right
        anchors.leftMargin: iSpiroValueLeftMargin
        anchors.bottom: spirodropdownLabel.top
        anchors.bottomMargin: iSpiroValueBottomMargin
    }
    Text {
        id: rightlabel
        text: qsTr(strSpiroValueRightLabel)
        font.family: strSpiroFontFamily
        font.pixelSize: iSpiroValueRightLabelPixelSize
        color: strSpiroValueRightLabelColor
        anchors.left: values.right
        anchors.leftMargin: iSpiroValueRightLabelLeftMargin
        anchors.bottom: spirodropdownLabel.top
        anchors.bottomMargin: iSpiroDropdownLabelBottomMargin
    }
    Text {
        id: spirodropdownLabel
        text: qsTr(strSpiroDropdownLabel)
        font.family: strSpiroFontFamily
        font.pixelSize: iSpiroDropdowntLabelPixelSize
        color: strSpiroDropdownLabelColor
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: iSpiroDropdownLabelBottomMargin
        anchors.leftMargin: iSpiroDropdownLabelLeftMargin
    }
    Rectangle {
        id: saveBtn
        height: iSpiroSaveBtnHeight
        width: iSpiroSaveBtnWidth
        color: strSaveBtnBgColor
        border.width: iSaveBtnBorderWidth
        border.color: activeFocus ? strSpiroStateBorderColor :strSaveBtnBorderColor
        anchors.right: parent.right
        anchors.rightMargin: iSpiroSavebtnRightMargin
        anchors.bottom: parent.bottom
        anchors.bottomMargin: iSpiroSavebtnBottomMargin
        Image {
            source: strSpiroSavebtnImage
            anchors.centerIn: parent
        }
    }

    Component.onCompleted: {
        SpirodropdownCtrl.createDropdownComponent();
        dropdownRef = getCompRef(spiromainpage,"topDropdown");

        dropdownRef.iSpiroDropdownBoxWidth = iTopDropdownWidth
        dropdownRef.iSpiroDropdownBoxHeight = iTopDropdownHeight
        dropdownRef.strSpiroDropdownBoxColor = strTopDropdownColor
        dropdownRef.iSpiroChosenItemFontSize = iTopDropdownFontSize
        dropdownRef.strSpiroDropdownBorderColor = strTopDropdownBorderColor
        dropdownRef.anchors.left = spirotitletext.right
        dropdownRef.anchors.leftMargin = iTopDropdownLeftMargin
        dropdownRef.anchors.top = spiromainpage.top
        dropdownRef.anchors.topMargin = iTopDropdownTopMargin
        dropdownRef.iSpiroDropdownImageWidth = iTopDropdownImageWidth
        dropdownRef.iSpiroDropdownImageHeight = iTopDropdownImageHeight
        dropdownRef.iSpiroDropdownImageTopMargin = iTopDropdownImageTopMargin
        dropdownRef.iSpiroDropdownImageRightMargin = iBottomDropdownImageRightMargin


//        SpirodropdownCtrl.createDropdownComponent(bottomDropdownList,"bottomDropdown");
        bottomDropdownRef = getCompRef(spiromainpage,"bottomDropdown");

        bottomDropdownRef.iSpiroDropdownBoxWidth = iBottomDropdownWidth
        bottomDropdownRef.iSpiroDropdownBoxHeight = iBottomDropdownHeight
        bottomDropdownRef.strSpiroDropdownBoxColor = strBottomDropdownColor
        bottomDropdownRef.iSpiroChosenItemFontSize = iBottomdropdownFontSize
        bottomDropdownRef.strSpiroDropdownBorderColor = strBottomDropdownBorderColor
        bottomDropdownRef.iSpiroDropdownImageWidth = iBottomDropdownImageWidth
        bottomDropdownRef.iSpiroDropdownImageHeight = iBottomDropdownImageHeight
        bottomDropdownRef.iSpiroDropdownImageTopMargin = iBottomDropdownImageTopMargin
        bottomDropdownRef.iSpiroDropdownImageRightMargin = iBottomDropdownImageRightMargin
        bottomDropdownRef.anchors.left = spirodropdownLabel.right
        bottomDropdownRef.anchors.leftMargin = iBottomDropdownLeftMargin
        bottomDropdownRef.anchors.bottom = spiromainpage.bottom
        bottomDropdownRef.anchors.bottomMargin = iBottomDropdownBottomMargin

        spiromainpage.focus = true;
        spiromainpage.forceActiveFocus();

    }



    Keys.onLeftPressed: {
        if(spiromainpage.focus === true)
        {
            saveBtn.focus = true;
            saveBtn.forceActiveFocus();
        }

        else if(closeBtn.focus === true)
        {
            dropdownRef.children[0].focus = true;
            dropdownRef.children[0].forceActiveFocus();
        }
        else if(dropdownRef.children[0].focus === true)
        {
            bottomDropdownRef.children[0].focus = true;
            bottomDropdownRef.children[0].forceActiveFocus();
        }
        else if(bottomDropdownRef.children[0].focus === true)
        {
            saveBtn.focus = true;
            saveBtn.forceActiveFocus();
        }
        else if(saveBtn.focus === true){
            closeBtn.focus = true;
            closeBtn.forceActiveFocus();
        }
    }

    Keys.onRightPressed: {
        if(spiromainpage.focus === true)
        {
            closeBtn.focus = true;
            closeBtn.forceActiveFocus();
        }
        else if(closeBtn.focus === true)
        {
            saveBtn.focus = true;
            saveBtn.forceActiveFocus();
        }
        else if(saveBtn.focus === true){
            bottomDropdownRef.children[0].focus = true;
            bottomDropdownRef.children[0].forceActiveFocus();
        }
        else if(bottomDropdownRef.children[0].focus === true)
        {
            dropdownRef.children[0].focus = true;
            dropdownRef.children[0].forceActiveFocus();
        }
        else if(dropdownRef.children[0].focus === true)
        {
            closeBtn.focus = true;
            closeBtn.forceActiveFocus();
        }

    }
}
