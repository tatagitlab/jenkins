import QtQuick 2.0


Rectangle {
    id:dropdownBox
    property var arry;

    property variant items:arry
    property alias selectedItem: chosenItemText.text;
    property alias selectedIndex: listView.currentIndex;

    property int iSpiroDropdownBoxWidth
    property int iSpiroDropdownBoxHeight
    property int iSpiroDropdownZValue
    property string strSpiroDropdownBoxColor
    property string strSpiroDropdownBorderColor
    property int iChosenItemTextmargin
    property string strSpiroChosenItemFontFamily: geFont.geFontStyle()
    property int iSpiroChosenItemFontSize
    property int iSpiroDropdownImageHeight
    property int iSpiroDropdownImageWidth
    property string strSpiroDropdownImage
    property int iSpiroDropdownImageTopMargin
    property int iSpiroDropdownImageRightMargin

    property int iSpiroDropdownHeight
    property string strSpiroDropdownColor
    property int iSpiroDropdownMargin
    property int iSpiroDropdownTextMargin
    property int iSpiroChosenItemTextLeftMargin
    property int iSpiroChosenItemTextTopMargin
    property string strSpiroTextColor


    signal comboClicked;
    signal spiroComboBoxClicked;
    signal spiroPageMouseClicked();

    width: iSpiroDropdownBoxWidth;
    height: iSpiroDropdownBoxHeight;
    z: iSpiroDropdownZValue
    smooth:true;

    function getValue(arr)
    {
        arry = arr;
    }

    Rectangle {
        id:chosenItem
        width:dropdownBox.width;
        height:dropdownBox.height;
        color: strSpiroDropdownBoxColor
        border.color: chosenItem.focus?"yellow":strSpiroDropdownBorderColor
        smooth:true;
        Text {
            id:chosenItemText
            font.family: strSpiroChosenItemFontFamily
            font.pixelSize: iSpiroChosenItemFontSize;
            color: strSpiroTextColor
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: iSpiroChosenItemTextLeftMargin
            anchors.topMargin: iSpiroChosenItemTextTopMargin
            text:dropdownBox.items[0];

            smooth:true
        }
        Image {
            id: dropdownSymbol
            source: "qrc:/Config/Assets/icon_spinner_down_disabled.png"
            width: iSpiroDropdownImageWidth
            height: iSpiroDropdownImageHeight
            anchors.right: chosenItem.right
            anchors.rightMargin: iSpiroDropdownImageRightMargin
            anchors.top: chosenItem.top
            anchors.topMargin: iSpiroDropdownImageTopMargin
        }

        MouseArea {
            anchors.fill: parent;
            onClicked: {

                dropdownBox.state = dropdownBox.state==="dropDown"?"":"dropDown"
                spiroComboBoxClicked();
                listView.focus = true;
                listView.forceActiveFocus();
            }
        }
        Keys.onReturnPressed: {
            dropdownBox.state = dropdownBox.state==="dropDown"?"":"dropDown"
            listView.focus = true;
            listView.forceActiveFocus();

        }
    }

    Rectangle {
        id:dropDown
        width:dropdownBox.width;
        height:iSpiroDropdownHeight;
        color: chosenItem.color
        clip:true;
        anchors.top: chosenItem.bottom;
        anchors.margins: iSpiroDropdownMargin;

        ListView {
            id:listView
            height:iSpiroDropdownBoxHeight*dropdownBox.items.length;
            model: dropdownBox.items
            currentIndex: 0
            interactive: false
            delegate: Item{
                width:dropdownBox.width;
                height: dropdownBox.height;


                Text {
                    text: modelData
                    font.family: strSpiroChosenItemFontFamily
                    font.pixelSize: iSpiroChosenItemFontSize
                    anchors.top: parent.top;
                    anchors.left: parent.left;
                    anchors.margins: iSpiroDropdownTextMargin;
                    color: strSpiroTextColor

                }
                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
                        dropdownBox.state = ""
                        var prevSelection = chosenItemText.text
                        chosenItemText.text = modelData
                        if(chosenItemText.text !== prevSelection){
                            dropdownBox.comboClicked();
                        }
                        spiroPageMouseClicked();
                    }
                }
                Keys.onPressed: {
                    event.accepted = true;
                    if(event.key===Qt.Key_Left){
                        if(listView.currentIndex>0){
                            listView.currentIndex=listView.currentIndex-1
                        }
                        else if(listView.currentIndex===0){
                            listView.currentIndex= dropdownBox.items.length-1
                        }
                    }
                    if(event.key===Qt.Key_Right){
                        if(dropdownBox.items.length-1>listView.currentIndex){
                            listView.currentIndex=listView.currentIndex+1
                        }
                        else if(dropdownBox.items.length-1===listView.currentIndex){
                            listView.currentIndex= 0
                        }
                    }
                    if(event.key===Qt.Key_Return){
                        dropdownBox.state = ""
                        var prevSelection = chosenItemText.text
                        chosenItemText.text = modelData
                        if(chosenItemText.text !== prevSelection){
                            dropdownBox.comboClicked();
                        }
                        listView.currentIndex = 0
                        chosenItem.focus = true;
                        chosenItem.forceActiveFocus();
                    }
                }
            }
            highlight: highlight
        }
        Component {
            id: highlight
            Rectangle {
                width:dropdownBox.width
                color: "lightgray";
                radius: 4
            }
        }
    }


    states: [
        State {
            name: "dropDown";
            PropertyChanges { target: dropDown; height:iSpiroDropdownBoxHeight*dropdownBox.items.length }
        },
        State {
            name: "noDropdown";
            PropertyChanges { target: dropDown; height:0 }
        }
    ]

    transitions: Transition {
        NumberAnimation { target: dropDown; properties: "height"; easing.type: Easing.OutExpo; duration: 1000 }
    }
}

