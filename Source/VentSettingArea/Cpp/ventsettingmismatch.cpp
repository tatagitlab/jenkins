#include "../Source/VentSettingArea/Include/ventsettingmismatch.h"

#include "../Source/CSB/Include/csbcommunication.h"
#include "../Source/Generic/Include/ventsettings.h"
#include <QDebug>

/**
 * @brief VentSettingMismatch::VentSettingMismatch - Constructor initializes variables
 */
VentSettingMismatch::VentSettingMismatch()
{
    m_vMismatchIE = 0 ;
    m_vMismatchTV = 0 ;
    m_vMismatchTVLow = 0 ;
    m_vMismatchTVHigh = 0 ;
    m_vMismatchRR = 0 ;
    m_vMismatchPeep = 0 ;
    m_vMismatchPinsp = 0 ;
    m_vMismatchMode = 1 ;
    m_vMismatchPmax = 0 ;

    m_vMismatchBackupTime = 0;

    m_cntMode = 0 ;
    m_cntRR = 0 ;
    m_cntTVLow = 0 ;
    m_cntTVHigh = 0 ;
    m_cntIE = 0 ;
    m_cntPeep = 0 ;
    m_cntPmax = 0 ;
    m_cntPinsp = 0 ;
    m_cntBackupTime = 0;

}

/**
 * @brief VentSettingMismatch::defaultSettings - initializing critical params data
 * @param settings - settings file object
 */
void VentSettingMismatch::defaultSettings(SettingFileUpdate *settings)
{
    if(settings->getIe() == "2:1"){
        m_vMismatchIE = 5 ;
    }else{
        QRegExp rx("(\\:)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
        QStringList ie = settings->getIe().split(rx);
        Q_ASSERT(ie.count()>1);
        float d2 = ie[1].toFloat();
        m_vMismatchIE = static_cast<int>(d2 * 10);

    }

    m_vMismatchTV = settings->getTv();

    m_vMismatchTVHigh = m_vMismatchTV/256;
    m_vMismatchTVLow = m_vMismatchTV - (m_vMismatchTVHigh * 256) ;

    m_vMismatchRR = settings->getRr();

    if(settings->getPeep() == "Off"){
        m_vMismatchPeep = 0 ;
    }else{
        m_vMismatchPeep = settings->getPeep().toInt();
    }

    m_vMismatchPinsp = settings->getPinsp();

    QString ventMode = settings->getMode();
    if (ventMode == MODE_VCV )
    {
        m_vMismatchMode = ventilationModes::VCV_mode;
    }
    else if (ventMode == MODE_PCV )
    {
        m_vMismatchMode = ventilationModes::PCV_mode ;
    }
    else if (ventMode == MODE_SIMV_VCV )
    {
        m_vMismatchMode = ventilationModes::SIMV_VCV_mode ;
    }
    else if (ventMode == MODE_SIMV_PCV )
    {
        m_vMismatchMode = ventilationModes::SIMV_PCV_mode ;
    }
    else if (ventMode == MODE_PSV_PRO )
    {
        m_vMismatchMode = ventilationModes::PSV_pro_mode ;
    }

    m_vMismatchPmax = settings->getPmax();
    m_vMismatchBackupTime = settings->getBackupTime();


}

/**
 * @brief VentSettingMismatch::isVentilationSettingsMismatch - checks for the ventilattion mismatch for critical params.
 *                                                              triggers banner in case of mismatch(count is more than 6 times)/dualstorage mismatch
 * @param type - type of the param(Mode, quick key param(RR, TV,..)
 * @param data - value of the type
 * @param dualStorageData - dual storage object to check for mismatch
 * @param csbCommand - output param(critical param type)
 * @param csbData - output param(value of csbCommand)
 * @param isBannerShown - output param(banner is trigered or not)
 * @return true - incase of mismatch and banner trigger
 *         false - incase of no mismatch
 */

bool VentSettingMismatch::isVentilationSettingsMismatch(int type, int data, ParamDualStorage *dualStorageData,
                                        int &csbCommand, int &csbData, bool &isBannerShown)
{
    bool bRetVal = false ;
    if (m_cntMode > VENT_MISMATCH_CNT || m_cntRR > VENT_MISMATCH_CNT || m_cntIE > VENT_MISMATCH_CNT || m_cntPeep > VENT_MISMATCH_CNT
            || m_cntPmax > VENT_MISMATCH_CNT ||  m_cntPinsp > VENT_MISMATCH_CNT  || m_cntBackupTime > VENT_MISMATCH_CNT
            || m_cntTVLow > VENT_MISMATCH_CNT || m_cntTVHigh > VENT_MISMATCH_CNT || dualStorageData->getIsDualStorageMismatch())
    {

        isBannerShown = true;
        qDebug()<<"Vent mismatch happnded, showing the shutdown form: "<<dualStorageData->getIsDualStorageMismatch();

        //Show shutdown form
        //emit showVentMismatchBanner();

        if(dualStorageData->getIsDualStorageMismatch() == true){
            csbCommand = CSB_BANNER_COMMAND;
            csbData = CSB_DUALSTRG_DATA;
        }else{
            csbCommand = CSB_BANNER_COMMAND;
            csbData = CSB_VENTMISMATCH_DATA;
        }
        bRetVal = true;
    }
    else{
        switch(type){

        case ventilationModes::MECHANICAL_mode:
            //Mode mismatch
            if(m_vMismatchMode != data){
                m_cntMode ++;
                qDebug()<<"Mode mismatch: "<<m_cntMode;
                csbCommand = 0;
                csbData = m_vMismatchMode;
                bRetVal = true;
            }else{
                m_cntMode = 0;
            }
            break;

        case VentModeParamTypes::RR:
            //RR mismatch
            if(m_vMismatchRR != data){
                m_cntRR ++;
                qDebug()<<"RR mismatch: "<<m_cntRR;
                csbCommand = VentModeParamTypes::RR;
                csbData = m_vMismatchRR;
                bRetVal = true;
            }
            else{
                m_cntRR = 0;
            }
            break;

        case VentModeParamTypes::IE:
            //IE mismatch
            if(m_vMismatchIE != data){
                m_cntIE ++;
                qDebug()<<"IE mismatch: "<<m_cntIE;
                csbCommand = VentModeParamTypes::IE;
                csbData = m_vMismatchIE;
                bRetVal = true;
            }else{
                m_cntIE = 0;
            }
            break;

        case VentModeParamTypes::LOW_tv:
            //TV Low mismatch
            if(m_vMismatchTVLow != data){
                m_cntTVLow ++;
                qDebug()<<"TV Low mismatch: "<<m_cntTVLow;
                csbCommand = VentModeParamTypes::LOW_tv;
                csbData = m_vMismatchTVLow;
                bRetVal = true;
            }
            else{
                m_cntTVLow = 0;
            }
            break;

        case VentModeParamTypes::HIGH_tv:
            //TV High mismatch
            if(m_vMismatchTVHigh != data){
                m_cntTVHigh ++;
                qDebug()<<"TV High mismatch: "<<m_cntTVHigh;
                csbCommand = VentModeParamTypes::HIGH_tv;
                csbData = m_vMismatchTVHigh;
                bRetVal = true;

            }
            else{
                m_cntTVHigh = 0;
            }
            break;

        case VentModeParamTypes::PEEP:
            //Peep mismatch
            if(m_vMismatchPeep != data){
                m_cntPeep ++ ;
                qDebug()<<"Peep mismatch: "<<m_cntPeep;
                csbCommand = VentModeParamTypes::PEEP;
                csbData = m_vMismatchPeep;
                bRetVal = true;

            }
            else{
                m_cntPeep = 0;
            }
            break;

        case VentModeParamTypes::PMax:
            //Pmax mismatch
            if(m_vMismatchPmax != data){
                m_cntPmax++;
                qDebug()<<"Pmax mismatch: "<<m_cntPmax;
                csbCommand = VentModeParamTypes::PMax;
                csbData = m_vMismatchPmax;
                bRetVal = true;
            }
            else{
                m_cntPmax = 0;
            }

            break;

        case VentModeParamTypes::PInsp:
            //Pinsp mismatch
            if(m_vMismatchPinsp != data){
                m_cntPinsp ++;
                qDebug()<<"Pinsp mismatch: "<<m_cntPinsp;
                csbCommand = VentModeParamTypes::PInsp;
                csbData = m_vMismatchPinsp;
                bRetVal = true;
            }
            else{
                m_cntPinsp = 0;
            }
            break;

        case VentModeParamTypes::BackUpTime:
            //Backuptime mismatch
            if(m_vMismatchBackupTime != data){
                m_cntBackupTime ++;
                qDebug()<<"Backup time mismatch: "<<m_cntBackupTime;
                csbCommand = VentModeParamTypes::BackUpTime;
                csbData = m_vMismatchBackupTime;
                bRetVal = true;
            }
            else{
                m_cntBackupTime = 0;
            }

            break;

        default:
            break ;
        }
    }

    return bRetVal ;

}

/**
 * @brief VentSettingMismatch::setVmismatchTVLow - setter for TVLow mismatch
 * @param vMismatchTVLow - to set for TVLow mismatch
 */
void VentSettingMismatch::setVmismatchTVLow(const int &vMismatchTVLow)
{
    m_vMismatchTVLow = vMismatchTVLow;
}

/**
 * @brief VentSettingMismatch::setVmismatchTVHigh - setter for TVHigh mismatch
 * @param vMismatchTVHigh - to set for TVHigh mismatch
 */
void VentSettingMismatch::setVmismatchTVHigh(const int &vMismatchTVHigh)
{
    m_vMismatchTVHigh = vMismatchTVHigh;
}

/**
 * @brief VentSettingMismatch::setCntTVLow - setter for TVLow mismatch count
 * @param cntTVLow - value to set for TVLow mismatch count
 */
void VentSettingMismatch::setCntTVLow(const int &cntTVLow)
{
    m_cntTVLow = cntTVLow;
}

/**
 * @brief VentSettingMismatch::setCntTVHigh - setter for TVHigh mismatch count
 * @param cntTVHigh - value to set for TVHigh mismatch count
 */
void VentSettingMismatch::setCntTVHigh(const int &cntTVHigh)
{
    m_cntTVHigh = cntTVHigh;
}

/**
 * @brief VentSettingMismatch::setVmismatchRR - setter for RR mismatch
 * @param vMismatchRR - to set for RR mismatch
 */
void VentSettingMismatch::setVmismatchRR(const int &vMismatchRR)
{
    m_vMismatchRR = vMismatchRR;
}

/**
 * @brief VentSettingMismatch::setCntRR - setter for RR mismatch count
 * @param cntRR - value to set for RR mismatch count
 */
void VentSettingMismatch::setCntRR(const int &cntRR)
{
    m_cntRR = cntRR;
}

/**
 * @brief VentSettingMismatch::setVmismatchIE - setter for IE mismatch
 * @param vMismatchIE - to set for IE mismatch
 */
void VentSettingMismatch::setVmismatchIE(const int &vMismatchIE)
{
    m_vMismatchIE = vMismatchIE;
}

/**
 * @brief VentSettingMismatch::setCntIE - setter for IE mismatch count
 * @param cntIE - value to set for IE mismatch count
 */
void VentSettingMismatch::setCntIE(const int &cntIE)
{
    m_cntIE = cntIE;
}

/**
 * @brief VentSettingMismatch::setVmismatchPeep - setter for Peep mismatch
 * @param vMismatchPeep - to set for Peep mismatch
 */
void VentSettingMismatch::setVmismatchPeep(const int &vMismatchPeep)
{
    m_vMismatchPeep = vMismatchPeep;
}

/**
 * @brief VentSettingMismatch::setCntPeep - setter for Peep mismatch count
 * @param cntPeep - value to set for Peep mismatch count
 */
void VentSettingMismatch::setCntPeep(const int &cntPeep)
{
    m_cntPeep = cntPeep;
}

/**
 * @brief VentSettingMismatch::setVmismatchPinsp - setter for Pinsp mismatch
 * @param vMismatchPinsp - to set for Pinsp mismatch
 */
void VentSettingMismatch::setVmismatchPinsp(const int &vMismatchPinsp)
{
    m_vMismatchPinsp = vMismatchPinsp;
}

/**
 * @brief VentSettingMismatch::setCntPinsp - setter for Pinsp mismatch count
 * @param cntPinsp - value to set for Pinsp mismatch count
 */
void VentSettingMismatch::setCntPinsp(const int &cntPinsp)
{
    m_cntPinsp = cntPinsp;
}

/**
 * @brief VentSettingMismatch::setVmismatchPmax - setter for Pmax mismatch
 * @param vMismatchPmax - to set for Pmax mismatch
 */
void VentSettingMismatch::setVmismatchPmax(const int &vMismatchPmax)
{
    m_vMismatchPmax = vMismatchPmax;
}

/**
 * @brief VentSettingMismatch::setCntPmax - setter for Pmax mismatch count
 * @param cntPmax - value to set for Pmax mismatch count
 */
void VentSettingMismatch::setCntPmax(const int &cntPmax)
{
    m_cntPmax = cntPmax;
}

/**
 * @brief VentSettingMismatch::setVmismatchBackupTime - setter for Backup time mismatch
 * @param vMismatchBackupTime - to set for backup time mismatch
 */
void VentSettingMismatch::setVmismatchBackupTime(const int &vMismatchBackupTime)
{
    m_vMismatchBackupTime = vMismatchBackupTime;
}

/**
 * @brief VentSettingMismatch::setCntBackupTime - setter for Backup time mismatch count
 * @param cntBackupTime - value to set for Backup time mismatch count
 */
void VentSettingMismatch::setCntBackupTime(const int &cntBackupTime)
{
    m_cntBackupTime = cntBackupTime;
}

/**
 * @brief VentSettingMismatch::setVmismatchMode - setter for Mode mismatch
 * @param vMismatchMode - to set for mode mismatch
 */
void VentSettingMismatch::setVmismatchMode(const int &vMismatchMode)
{
    m_vMismatchMode = vMismatchMode;
}

/**
 * @brief VentSettingMismatch::setCntMode - setter for Mode mismatch count
 * @param cntMode - value to set for Mode mismatch count
 */
void VentSettingMismatch::setCntMode(const int &cntMode)
{
    m_cntMode = cntMode;
}

/**
 * @brief VentSettingMismatch::getVmismatchTVLow - getter for TVLow value
 * @return - returns TVLow value
 */
int VentSettingMismatch::getVmismatchTVLow() const
{
    return m_vMismatchTVLow;
}

/**
 * @brief VentSettingMismatch::getVmismatchTVHigh - getter for TVHigh value
 * @return - returns TVHigh value
 */
int VentSettingMismatch::getVmismatchTVHigh() const
{
    return m_vMismatchTVHigh;
}
