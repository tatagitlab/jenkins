#include "../Source/VentSettingArea/Include/settingfileupdate.h"
#include<QDir>
#include <QDebug>

/**
 * @brief SettingFileUpdate::SettingFileUpdate - constructor to initialise all the member variables with default values
 * @param errorStatus
 */
SettingFileUpdate::SettingFileUpdate (bool errorStatus):m_errorStat(errorStatus)
{   
    m_tv = "500";
    m_rr = "12";
    m_rrMech = "12";
    m_ie = "1:2";
    m_pp = "Off";
    m_pinsp = "10";
    m_psupport = "2";
    m_tp = "Off";
    m_pm = "40";
    m_fTrig = "0.2";
    m_tinsp = "1.7";
    m_tWindow = "80";
    m_ebreath = "30";
    m_exitBackup = "2";
    m_backupTime = "30" ;
    m_ppeakl = "5";
    m_ppeakh = "10";
    m_mvl = "1.0";
    m_mvh = "10.0";
    m_tvexpl = "100";
    m_tvexph = "600";
    m_o2l = "21";
    m_o2h = "80";
    m_apnead = "30";
    m_alarmv = "3";
    m_language = "English";
    m_mode = "BtnMode_VCV";

    initializeEnum();
}

/**
 * @brief SettingFileUpdate::initializeEnum - initialises the enums
 */

void SettingFileUpdate::initializeEnum()
{
    //Primary quick key
    KeyList["TV_qKey"] = VentKey::TV_qKey;
    KeyList["RR_qKey"] = VentKey::RR_qKey;
    KeyList["IE_qKey"] = VentKey::IE_qKey;
    KeyList["PEEP_qKey"] = VentKey::PEEP_qKey;
    KeyList["Psupport_qKey"] = VentKey::PSUPPORT_qKey;
    KeyList["Pinsp_qKey"] = VentKey::PINSP_qKey;
    KeyList["RRMech_qKey"] = VentKey::RRMECH_qKey;

    KeyList["Language"] = VentKey::Language;
    KeyList["MODE"] = VentKey::MODE;

    //Secondary params
    KeyList["FlowTrigger_sKey"] = VentKey::FLOW_TRIGGER_sKey;
    KeyList["Tinsp_sKey"] = VentKey::TINSP_sKey;
    KeyList["Pmax_sKey"] = VentKey::PMAX_sKey;
    KeyList["Trig_Window_sKey"] = VentKey::TRIG_WINDOW_sKey;
    KeyList["EndofBreath_sKey"] = VentKey::END_OF_BREATH_sKey;
    KeyList["Tpause_sKey"] = VentKey::TPAUSE_sKey;
    KeyList["BackupTime_sKey"] = VentKey::BACKUPTIME_sKey;
    KeyList["ExitBackup_sKey"] = VentKey::EXITBACKUP_sKey;


    //Alarm Settings Param List
    KeyList["Ppeak_Low"] = VentKey::Ppeak_Low;
    KeyList["Ppeak_High"] = VentKey::Ppeak_High;
    KeyList["MV_Low"] = VentKey::MV_Low;
    KeyList["MV_High"] = VentKey::MV_High;
    KeyList["TVexp_Low"] = VentKey::TVexp_Low;
    KeyList["TVexp_High"] = VentKey::TVexp_High;
    KeyList["O2_Low"] = VentKey::O2_Low;
    KeyList["O2_High"] = VentKey::O2_High;
    KeyList["Alarm_Volume"] = VentKey::Alarm_Volume;
    KeyList["Apnea_Delay"] = VentKey::Apnea_Delay;

}

/**
 * @brief SettingFileUpdate::init - Method for schema validation check and file corruption check
 * @param filePath
 */

void SettingFileUpdate::init(QString filePath)
{
    m_filename = filePath;
    //QFile file(m_filename);
    file.setFileName(m_filename);

    try
    {

        if (!file.exists())
        {
            qDebug() << file.fileName()<<"setting file does not exist";
            m_errorStr = "setting file does not exist";
            createNewSettingFile();
            initValues();
            //throw m_errorStr;
        }
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {

            initValues();
        } else {
            qDebug() << "Unable to Open File";
            m_errorStr = "Unable to Open File";
            createNewSettingFile();
            initValues();
            //throw m_errorStr;
        }

    }
    catch (QString errorStr) {
        qDebug() << errorStr;
        m_errorStat = true;
    }
}

/**
 * @brief SettingFileUpdate::read - Method to read the values from the member variables
 * @param param setting file parameter to be read
 * @return returns the param value
 */

QString SettingFileUpdate::read (QString param)
{
    try{

        VentKey vKey;
        QString value;
        if(KeyList.count(param.toUtf8().constData()) > 0){
            vKey = KeyList.at(param.toUtf8().constData());
            switch (vKey) {
            case VentKey::TV_qKey:
                value = m_tv;
                break;
            case VentKey::RR_qKey:
                value = m_rr;
                break;
            case VentKey::IE_qKey:
                value = m_ie;
                break;
            case VentKey::PEEP_qKey:
                value = m_pp;
                break;
            case VentKey::PSUPPORT_qKey:
                value = m_psupport;
                break;
            case VentKey::PINSP_qKey:
                value = m_pinsp;
                break;
            case VentKey::RRMECH_qKey:
                value = m_rrMech;
                break;
            case VentKey::FLOW_TRIGGER_sKey:
                value = m_fTrig;
                break;
            case VentKey::TINSP_sKey:
                value = m_tinsp;
                break;
            case VentKey::PMAX_sKey:
                value = m_pm;
                break;
            case VentKey::TRIG_WINDOW_sKey:
                value = m_tWindow;
                break;
            case VentKey::END_OF_BREATH_sKey:
                value = m_ebreath;
                break;
            case VentKey::TPAUSE_sKey:
                value = m_tp;
                break;
            case VentKey::BACKUPTIME_sKey:
                value = m_backupTime;
                break;
            case VentKey::EXITBACKUP_sKey:
                value = m_exitBackup;
                break;
            case VentKey::Ppeak_High:
                value = m_ppeakh;
                break;
            case VentKey::Ppeak_Low:
                value = m_ppeakl;
                break;
            case VentKey::MV_Low:
                value = m_mvl;
                break;
            case VentKey::MV_High:
                value = m_mvh;
                break;
            case VentKey::TVexp_Low:
                value = m_tvexpl;
                break;
            case VentKey::TVexp_High:
                value = m_tvexph;
                break;
            case VentKey::O2_Low:
                value = m_o2l;
                break;
            case VentKey::O2_High:
                value = m_o2h;
                break;
            case VentKey::Apnea_Delay:
                value = m_apnead;
                break;
            case VentKey::Alarm_Volume:
                value = m_alarmv;
                break;
            case VentKey::Language:
                value = m_language;
                break;
            case VentKey::MODE:
                value = m_mode;
                break;
            default:
                m_errorStr = "wrong param specified";
                throw m_errorStr;
            }}
        else {
            return "";
        }
        return value;
    }
    catch (QString errorStr) {
        qDebug() << errorStr;
        m_errorStat = true;
        return "";
    }

}

/**
 * @brief SettingFileUpdate::writeDataInFile - writes data to se3tting file
 * @param id - setting file param
 * @param data - value to be written
 */

void SettingFileUpdate::writeDataInFile ( QString id, QString data)
{
    try{
        QFile file(m_filename);
        QString    line ;
        QString oldvalue;
        QString newvalue;

        if (!file.exists())
        {
            qDebug() << file.fileName()<<"setting file does not exist";
            m_errorStr = "setting file does not exist";
            createNewSettingFile();
            initValues();
            //throw m_errorStr;
        }



        if ( file.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            QTextStream stream( &file );
            line = file.readAll();
            //qDebug()<<"file contents:"<<line;
            if(KeyList.count(id.toUtf8().constData()) > 0){
                VentKey vKey;
                vKey = KeyList.at(id.toUtf8().constData());
                if(line.contains(id)){
                    qDebug() << "file contains param";
                    switch (vKey) {
                    case VentKey::TV_qKey:
                        oldvalue = id+","+m_tv;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_tv = data;
                        break;
                    case VentKey::RR_qKey:
                        oldvalue = id+","+m_rr;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_rr = data;
                        break;
                    case VentKey::IE_qKey:
                        oldvalue = id+","+m_ie;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_ie = data;
                        break;
                    case VentKey::PEEP_qKey:
                        oldvalue = id+","+m_pp;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_pp = data;
                        break;
                    case VentKey::PSUPPORT_qKey:
                        oldvalue = id+","+m_psupport;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_psupport = data;
                        break;
                    case VentKey::PINSP_qKey:
                        oldvalue = id+","+m_pinsp;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_pinsp = data;
                        break;
                    case VentKey::RRMECH_qKey:
                        oldvalue = id+","+m_rrMech;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_rrMech = data;
                        break;
                    case VentKey::FLOW_TRIGGER_sKey:
                        oldvalue = id+","+m_fTrig;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_fTrig = data;
                        break;
                    case VentKey::TINSP_sKey:
                        oldvalue = id+","+m_tinsp;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_tinsp = data;
                        break;
                    case VentKey::PMAX_sKey:
                        oldvalue = id+","+m_pm;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_pm = data;
                        break;
                    case VentKey::TRIG_WINDOW_sKey:
                        oldvalue = id+","+m_tWindow;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_tWindow = data;
                        break;
                    case VentKey::END_OF_BREATH_sKey:
                        oldvalue = id+","+m_ebreath;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_ebreath = data;
                        break;
                    case VentKey::TPAUSE_sKey:
                        oldvalue = id+","+m_tp;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_tp = data;
                        break;
                    case VentKey::BACKUPTIME_sKey:
                        oldvalue = id+","+m_backupTime;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_backupTime = data;
                        break;
                    case VentKey::EXITBACKUP_sKey:
                        oldvalue = id+","+m_exitBackup;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_exitBackup = data;
                        break;
                    case VentKey::Ppeak_High:
                        oldvalue = id+","+m_ppeakh;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_ppeakh = data;
                        break;
                    case VentKey::Ppeak_Low:
                        oldvalue = id+","+m_ppeakl;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_ppeakl = data;
                        break;
                    case VentKey::MV_Low:
                        oldvalue = id+","+m_mvl;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_mvl = data;
                        break;
                    case VentKey::MV_High:
                        oldvalue = id+","+m_mvh;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_mvh = data;
                        break;
                    case VentKey::TVexp_Low:
                        oldvalue = id+","+m_tvexpl;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_tvexpl = data;
                        break;
                    case VentKey::TVexp_High:
                        oldvalue = id+","+m_tvexph;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_tvexph = data;
                        break;
                    case VentKey::O2_Low:
                        oldvalue = id+","+m_o2l;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_o2l = data;
                        break;
                    case VentKey::O2_High:
                        oldvalue = id+","+m_o2h;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_o2h = data;
                        break;
                    case VentKey::Apnea_Delay:
                        oldvalue = id+","+m_apnead;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_apnead = data;
                        break;
                    case VentKey::Alarm_Volume:
                        oldvalue = id+","+m_alarmv;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_alarmv = data;
                        break;
                    case VentKey::Language:
                        oldvalue = id+","+m_language;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_language = data;
                        break;
                    case VentKey::MODE:
                        oldvalue = id+","+m_mode;
                        newvalue = id+","+data;
                        line.replace(oldvalue, newvalue);
                        m_mode = data;
                        break;
                    default:
                        m_errorStr = "wrong param specified";
                        throw m_errorStr;
                    }
                    file.close();
                    file.open(QIODevice::ReadWrite | QIODevice::Truncate);
                    QTextStream stream(&file);
                    stream << line;
                    file.close();
                    return;

                } else {
                    qDebug() << "file does not contain param, writing it new";
                    file.open(QIODevice::ReadWrite | QIODevice::Truncate);
                    QTextStream stream(&file);
                    stream << endl << id+","+data;
                    file.close();
                    switch (vKey) {
                    case VentKey::TV_qKey:
                        m_tv = data;
                        break;
                    case VentKey::RR_qKey:
                        m_rr = data;
                        break;
                    case VentKey::IE_qKey:
                        m_ie = data;
                        break;
                    case VentKey::PEEP_qKey:
                        m_pp = data;
                        break;
                    case VentKey::PSUPPORT_qKey:
                        m_psupport = data;
                        break;
                    case VentKey::PINSP_qKey:
                        m_pinsp = data;
                        break;
                    case VentKey::RRMECH_qKey:
                        m_rrMech = data;
                        break;
                    case VentKey::FLOW_TRIGGER_sKey:
                        m_fTrig = data;
                        break;
                    case VentKey::TINSP_sKey:
                        m_tinsp = data;
                        break;
                    case VentKey::PMAX_sKey:
                        m_pm = data;
                        break;
                    case VentKey::TRIG_WINDOW_sKey:
                        m_tWindow = data;
                        break;
                    case VentKey::END_OF_BREATH_sKey:
                        m_ebreath = data;
                        break;
                    case VentKey::TPAUSE_sKey:
                        m_tp = data;
                        break;
                    case VentKey::BACKUPTIME_sKey:
                        m_backupTime = data;
                        break;
                    case VentKey::EXITBACKUP_sKey:
                        m_exitBackup = data;
                        break;
                    case VentKey::Ppeak_High:
                        m_ppeakh = data;
                        break;
                    case VentKey::Ppeak_Low:
                        m_ppeakl = data;
                        break;
                    case VentKey::MV_Low:
                        m_mvl = data;
                        break;
                    case VentKey::MV_High:
                        m_mvh = data;
                        break;
                    case VentKey::TVexp_Low:
                        m_tvexpl = data;
                        break;
                    case VentKey::TVexp_High:
                        m_tvexph = data;
                        break;
                    case VentKey::O2_Low:
                        m_o2l = data;
                        break;
                    case VentKey::O2_High:
                        m_o2h = data;
                        break;
                    case VentKey::Apnea_Delay:
                        m_apnead = data;
                        break;
                    case VentKey::Alarm_Volume:
                        m_alarmv = data;
                        break;
                    case VentKey::Language:
                        m_language = data;
                        break;
                    case VentKey::MODE:
                        m_mode = data;
                        break;
                    default:
                        break;
                    }
                }}
            else {
                return;

            }
        }
        else
        {
            qDebug() << "setting file open failed"<< file.errorString();
            m_errorStr = "setting file open failed";
            throw m_errorStr;
        }

    }
    catch (QString errorStr) {
        qDebug() << errorStr;
        m_errorStat = true;
    }
}

/**
 * @brief SettingFileUpdate::initValues - initialise the member variables with the value in the setting file
 */

void SettingFileUpdate::initValues(){

    QString key;
    QString value;
    //file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    VentKey vKey;

    while(!in.atEnd())
    {
        QString line = in.readLine();
        if (!line.isEmpty()) {
            QStringList fields = line.split( "," );
            key = fields[0];
            value = fields[1];
            vKey = KeyList.at(key.toUtf8().constData());
            if(!value.isEmpty() && !key.isEmpty()){
                switch (vKey) {
                case VentKey::TV_qKey:
                    if((value.toInt() >= 20 && value.toInt() <= 1500)){
                    m_tv = value;
                    }
                    break;
                case VentKey::RR_qKey:
                    if((value.toInt() >= 4 && value.toInt() <= 99)){
                    m_rr = value;
                    }
                    break;
                case VentKey::IE_qKey:
                    m_ie = value;
                    break;
                case VentKey::PEEP_qKey:
                    if((value.toInt() >= 4 && value.toInt() <= 25)){
                    m_pp = value;
                    }
                    break;
                case VentKey::PSUPPORT_qKey:
                    if((value.toInt() >= 2 && value.toInt() <= 40)){
                    m_psupport = value;
                    }
                    break;
                case VentKey::PINSP_qKey:
                    if((value.toInt() >= 5 && value.toInt() <= 50)){
                    m_pinsp = value;
                    }
                    break;
                case VentKey::RRMECH_qKey:
                    if((value.toInt() >= 2 && value.toInt() <= 60)){
                    m_rrMech = value;
                    }
                    break;
                case VentKey::FLOW_TRIGGER_sKey:
                    if((value.toFloat() >= 0.2 && value.toFloat() <= 10.0)){
                    m_fTrig = value;
                    }
                    break;
                case VentKey::TINSP_sKey:
                    if((value.toFloat() >= 0.2 && value.toFloat() <= 5.0)){
                    m_tinsp = value;
                    }
                    break;
                case VentKey::PMAX_sKey:
                    if((value.toInt() >= 10 && value.toInt() <= 99)){
                    m_pm = value;
                    }
                    break;
                case VentKey::TRIG_WINDOW_sKey:
                    if((value.toInt() >= 5 && value.toInt() <= 80)){
                    m_tWindow = value;
                    }
                    break;
                case VentKey::END_OF_BREATH_sKey:
                    if((value.toInt() >= 5 && value.toInt() <= 75)){
                    m_ebreath = value;
                    }
                    break;
                case VentKey::TPAUSE_sKey:
                    if(((value.toInt() >= 5 && value.toInt() <= 60) || value == "Off")){
                    m_tp = value;
                    }
                    break;
                case VentKey::BACKUPTIME_sKey:
                    if((value.toInt() >= 10 && value.toInt() <= 30)){
                    m_backupTime = value;
                    }
                    break;
                case VentKey::EXITBACKUP_sKey:
                    if((value.toInt() >= 1 && value.toInt() <= 5)){
                    m_exitBackup = value;
                    }
                    break;
                case VentKey::Ppeak_High:
                    if((value.toInt() >= 10 && value.toInt() <= 99)){
                    m_ppeakh = value;
                    }
                    break;
                case VentKey::Ppeak_Low:
                    if((value.toInt() >= 1 && value.toInt() <= 20)){
                    m_ppeakl = value;
                    }
                    break;
                case VentKey::MV_Low:
                    if((value.toFloat() >= 0.1 && value.toFloat() <= 15.0)){
                    m_mvl = value;
                    }
                    break;
                case VentKey::MV_High:
                    if((value.toFloat() >= 3.0 && value.toFloat() <= 40.0)){
                    m_mvh = value;
                    }
                    break;
                case VentKey::TVexp_Low:
                    if((value.toInt() >= 5 && value.toInt() <= 800)){
                    m_tvexpl = value;
                    }
                    break;
                case VentKey::TVexp_High:
                    if((value.toInt() >= 100 && value.toInt() <= 1800)){
                    m_tvexph = value;
                    }
                    break;
                case VentKey::O2_Low:
                    if((value.toInt() >= 20 && value.toInt() <= 70)){
                    m_o2l = value;
                    }
                    break;
                case VentKey::O2_High:
                    if((value.toInt() >= 40 && value.toInt() <= 100)){
                    m_o2h = value;
                    }
                    break;
                case VentKey::Apnea_Delay:
                    if((value.toInt() >= 10 && value.toInt() <= 30)){
                    m_apnead = value;
                    }
                    break;
                case VentKey::Alarm_Volume:
                    if((value.toInt() >= 1 && value.toInt() <= 5)){
                    m_alarmv = value;
                    }
                    break;
                case VentKey::Language:
                    if((value == "English" || value == "Italian")){
                    m_language = value;
                    }
                    break;
                case VentKey::MODE:
                    if((value == "BtnMode_VCV" || value == "BtnMode_PCV" || value == "BtnMode_SIMVVCV" || value == "BtnMode_SIMVPCV" || value == "BtnMode_PSVPro")){
                    m_mode = value;
                    }
                    break;
                default:
                    break;
                }
            }
        }
    }

    file.close();
}

/**
 * @brief SettingFileUpdate::createNewSettingFile - Method to create new setting file and load it with default values incase the file does not exist
 */
void SettingFileUpdate::createNewSettingFile(){
    qDebug() << "creating file";

    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        stream << "TV_qKey,500" << endl;
        stream << "RR_qKey,12" << endl;
        stream << "IE_qKey,1:2" << endl;
        stream << "PEEP_qKey,Off" << endl;
        stream << "Psupport_qKey,2" << endl;
        stream << "Pinsp_qKey,10" << endl;
        stream << "RRMech_qKey,12" << endl;
        stream << "Pmax_sKey,40" << endl;
        stream << "Tpause_sKey,Off" << endl;
        stream << "FlowTrigger_sKey,2.0" << endl;
        stream << "Tinsp_sKey,1.7" << endl;
        stream << "Trig_Window_sKey,80" << endl;
        stream << "EndofBreath_sKey,30" << endl;
        stream << "ExitBackup_sKey,2" << endl;
        stream << "BackupTime_sKey,30" << endl;
        stream << "Ppeak_Low,5" << endl;
        stream << "Ppeak_High,40" << endl;
        stream << "MV_Low,1.0" << endl;
        stream << "MV_High,10.0" << endl;
        stream << "TVexp_Low,100" << endl;
        stream << "TVexp_High,600" << endl;
        stream << "O2_Low,21" << endl;
        stream << "O2_High,80" << endl;
        stream << "Apnea_Delay,30" << endl;
        stream << "Alarm_Volume,3" << endl;
        stream << "MODE,BtnMode_VCV" << endl;
        stream << "Language,English" << endl;
    }

    file.close();
    initValues();

    qDebug() <<"finish creating and writing to setting file";

}



/**
 * @brief SettingFileUpdate::getErrorString Method to get errorstring incase of an error
 * @return return errorstring incase of an error
 */

QString SettingFileUpdate::getErrorString()
{
    return m_errorStr;
}

/**
 * @brief SettingFileUpdate::getIe getter for IE
 * @return returns IE value
 */

QString SettingFileUpdate::getIe()
{
    return m_ie;
}

/**
 * @brief SettingFileUpdate::getTv getter for TV
 * @return returns TV value
 */

int SettingFileUpdate::getTv()
{
    return m_tv.toInt();
}

/**
 * @brief SettingFileUpdate::getRr getter for RR
 * @return returns RR value
 */

int SettingFileUpdate::getRr()
{
    return m_rr.toInt();
}

/**
 * @brief SettingFileUpdate::getRrMech getter for RR Mech
 * @return returns RR MEch value
 */
int SettingFileUpdate::getRrMech() const
{
    return m_rrMech.toInt();
}

/**
 * @brief SettingFileUpdate::getPeep getter for PEEP
 * @return returns PEEP value
 */
QString SettingFileUpdate::getPeep()
{
    return m_pp;
}

/**
 * @brief SettingFileUpdate::getPinsp getter for Pinsp
 * @return returns Pinsp value
 */

int SettingFileUpdate::getPinsp()
{
    return m_pinsp.toInt();
}

/**
 * @brief SettingFileUpdate::getPsupport getter for Psupport
 * @return returns Psupport value
 */

int SettingFileUpdate::getPsupport()
{
    return m_psupport.toInt();
}

/**
 * @brief SettingFileUpdate::getMode getter for Mode
 * @return returns Mode value
 */
QString SettingFileUpdate::getMode()
{
    return m_mode;
}

/**
 * @brief SettingFileUpdate::getPpeaklow getter for Ppeak low
 * @return returns Ppeak low value
 */
int SettingFileUpdate::getPpeaklow()
{
    return m_ppeakl.toInt();
}

/**
 * @brief SettingFileUpdate::getPmax getter for Pmax
 * @return returns Pmax value
 */

int SettingFileUpdate::getPmax()
{
    return m_pm.toInt();
}

/**
 * @brief SettingFileUpdate::getTinsp getter for Tinsp
 * @return returns Tinsp value
 */

float SettingFileUpdate::getTinsp()
{
    return m_tinsp.toFloat();
}

/**
 * @brief SettingFileUpdate::getTpause getter for Tpause
 * @return returns Tpause value
 */
int SettingFileUpdate::getTpause()
{
    return m_tp.toInt();
}

/**
 * @brief SettingFileUpdate::getBackupTime getter for Backup time
 * @return returns Backup time value
 */

int SettingFileUpdate::getBackupTime() const
{
    return m_backupTime.toInt();
}

/**
 * @brief SettingFileUpdate::setBackupTime setter for backup time
 * @param backupTimevalue to set for backup time
 */

void SettingFileUpdate::setBackupTime(const QString &backupTime)
{
    m_backupTime = backupTime;
}

/**
 * @brief SettingFileUpdate::getExitBackup getter for exit backup
 * @return returns value for exit backup
 */
QString SettingFileUpdate::getExitBackup() const
{
    return m_exitBackup;
}

/**
 * @brief SettingFileUpdate::setExitBackup setter for exit backup
 * @param exitBackup value to set for exit backup
 */
void SettingFileUpdate::setExitBackup(const QString &exitBackup)
{
    m_exitBackup = exitBackup;
}

/**
 * @brief SettingFileUpdate::setRrMech setter for RR mech
 * @param rrMech value to set for rr mech
 */
void SettingFileUpdate::setRrMech(const QString &rrMech)
{
    m_rrMech = rrMech;
}




