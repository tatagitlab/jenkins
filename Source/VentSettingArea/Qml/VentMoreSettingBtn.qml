import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "qrc:/Source/VentSettingArea/JavaScript/VentGroupController.js" as GrpCtrl

Rectangle {
    id: moreSettingId
    objectName: "MoreSettingBtn"

    property bool bKeyOnePressed: false
    property bool bKeyFifthPressed: false
    property bool bKeySecondPressed: false
    property bool bKeyThreePressed: false
    property bool bKeySixthPressed: false
    property bool bIsSettingsCompTouched: false
    property bool bIsMousePressed: moreSettingMouseArea.containsPress
    property int iCompID
    //Parent property
    property int iMoreSettingWidth: 140
    property int iMoreSettingHeight: 127
    property int iOtherStateBorderWidth: 0
    property int iScrollBorderWidth: 2
    property int iOneMinInactiveInterval: 60000

    //Child property
    property int iCustomizedBorder: 1
    property int  iMoreSettingText1FontPixelSize: 16
    //Parent property
    property string strMoreSettingColor: "#2a2a2a"
    //Child Property
    property string strFontColor: "#d4d4d4"
    property string strDisabledStateFontColor: "#424242"
    property string strMoreSettingText1FontFamily: geFont.geFontStyle()
    property string strPresentMode: settingReader.read("MODE")

    property string strMoreSetting
    property string strType:"More"
    property string strMoreIconDiabled
    property string strMoreIconEnabled
    property string strMoreIconPreset
    property string strMoreIconTouched

    // state styling properties
    property string strMoreSettingActiveStateColor: "#2a2a2a"
    property string strCustomizedBorderColor: "#424242"
    property string strMoreSettingDisabledStateColor: "#2a2a2a"
    property string strMoreSettingEndOfScaleStateColor: "#f6f925"
    property string strMoreSettingEndOfScaleStateBorderColor: "#a89c8b"
    property string strMoreSettingScrollStateBorderColor: "#edbf00"
    property string strMoreSettingTouchedStateColor: "#b2b2a2"
    property string strMorePresetTextColor : "#7f7f7f"
    property string strVentPreviousState
    property string uniqueObjectID: "MoreSettingsButton"

    property int iMoreSettingStateHeightChange: 140
    property var menuObject: getGroupRef("MoreSettingsMenuObj");

    //keyboard shortcut
    signal shortCutKeyPressed(int iCompID,string currentKey, string strType);
    signal destroyOpenMenu();
    //end

    //For switching Bag/Vent
    property string strBtnState
    property string strVentState

    width: iMoreSettingWidth
    height: iMoreSettingHeight
    color: strMoreSettingColor
    border.width: 0
    radius : 3
    state:strBtnState

    Rectangle {
        id: borderLeft
        width: iCustomizedBorder
        height: parent.height
        visible: true
        anchors.right: parent.right
        color: strCustomizedBorderColor
    }
    Rectangle {
        id: rect1
        width: 10
        height: parent.height
        visible: true
        anchors.left: parent.left
        color: strMoreSettingColor
    }
    Rectangle {
        id: leftBorder1
        width: 2
        height: rect1.height
        visible: false
        anchors.left: parent.left
        color: strMoreSettingScrollStateBorderColor
    }

    Rectangle {
        id: topBorder1
        width: rect1.width
        height: 2
        visible: false
        anchors.top: rect1.top
        color: strMoreSettingScrollStateBorderColor
    }
    Rectangle {
        id: rect2
        width: parent.width
        height: 10
        visible: true
        anchors.bottom : parent.bottom
        color: strMoreSettingColor
    }

    Rectangle {
        id: bottomBorder2
        width: rect2.width
        height: 2
        visible: false
        anchors.bottom : rect2.bottom
        color: strMoreSettingScrollStateBorderColor
    }

    Rectangle {
        id: leftBorder2
        width: 2
        height: rect2.height
        visible: false
        anchors.left : rect2.left
        anchors.bottom: rect2.bottom
        color: strMoreSettingScrollStateBorderColor
    }

    Rectangle {
        id: rightBorder2
        width: 2
        height: parent.height
        visible: false
        anchors.right : rect2.right
        anchors.bottom: rect2.bottom
        color: strMoreSettingScrollStateBorderColor
    }


    Layout.preferredWidth: iMoreSettingWidth
    Layout.preferredHeight: iMoreSettingHeight
    anchors.bottom: parent.bottom
    anchors.right: parent.right

    function resetScrollActiveTimer() {
        scrollActiveTimer.running = true ;
        scrollActiveTimer.restart() ;
    }

    function stopScrollActiveTimer() {
        scrollActiveTimer.running = false ;
        scrollActiveTimer.stop() ;
    }

    Timer {
        id: scrollActiveTimer
        running: false
        repeat: false
        interval: iOneMinInactiveInterval ; // 1 min of inactivity
        onTriggered: {
            console.log("scrollActiveTimer triggerred : \n")
            GrpCtrl.onScrollActiveTimeOut(moreSettingId.objectName)
        }
    }

    onStateChanged: {
        if(moreSettingId.state === "Scroll" || moreSettingId.state === "PresetScroll") {
            moreSettingId.resetScrollActiveTimer()
        } else if(moreSettingId.state === "Selected" || moreSettingId.state === "End of Scale" || moreSettingId.state === "Active" || moreSettingId.state === "Disabled"
                  || moreSettingId.state === "Preset" || moreSettingId.state === "PresetDisabled") {
            moreSettingId.stopScrollActiveTimer() ;
        }
    }

    Label {
        id: text1
        text: qsTrId(strMoreSetting)
        color: strFontColor
        font.family : strMoreSettingText1FontFamily
        font.pixelSize: iMoreSettingText1FontPixelSize
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Image {
        id: moreIcon
        source: strMoreIconEnabled
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom

    }

    MouseArea {
        id: moreSettingMouseArea
        anchors.fill: parent
        onReleased: {
            console.log("state on release more setting"+moreSettingId.state+moreSettingMouseArea.containsMouse);
            if(moreSettingMouseArea.containsMouse){
                moreSettingId.stopScrollActiveTimer()
                var ventGrpObj = getGroupRef("VentGroup")
                if((moreSettingId.state === "Touched" || moreSettingId.state === "PresetTouched") && !ventGrpObj.bIsSettingsMenuCreated) {
                    GrpCtrl.onSettingsCompSelected(moreSettingId.objectName)
                    destroyOpenMenu();
                }
            }
        }
    }

    Keys.onRightPressed: {
        GrpCtrl.onNavigateRightInVentGroup(objectName)
    }

    Keys.onLeftPressed: {
        moreSettingId.stopScrollActiveTimer()
        GrpCtrl.onNavigateLeftInVentGroup(objectName)
    }

    Keys.onReturnPressed: {
        moreSettingId.stopScrollActiveTimer()
        moreSettingId.strVentPreviousState = moreSettingId.state
        GrpCtrl.createMoreSettingMenu()
        //to make sure that mode btn remains in preset mode /active mode when more settings menu is opened
        if (moreSettingId.strVentState === "Vent") {
            moreSettingId.state = "Active"
        } else if (moreSettingId.strVentState === "Bag"){
            moreSettingId.state = "Preset"
        }
        destroyOpenMenu();
    }

    //key board Shortcuts
    Keys.onPressed: {
        event.accepted = true;
        GrpCtrl.shortcutKeysPress(event.key, objectName)
    }

    Keys.onReleased: {
        event.accepted = true;
        GrpCtrl.shortcutKeysRelease(event.key, objectName)
    }

    onFocusChanged:{
        if(moreSettingId.focus === false){
            moreSettingId.stopScrollActiveTimer() ;
            console.log("stop scroll active timer of more setting since focus is false on"+iCompID)

            if (moreSettingId.strVentState === "Vent") {
                moreSettingId.state = "Active"
            } else if (moreSettingId.strVentState === "Bag"){
                moreSettingId.state = "Preset"
            }
        }
    }

    onBIsMousePressedChanged:{
        console.log("mouse press changed..."+moreSettingMouseArea.containsPress)

        if(moreSettingMouseArea.containsPress){
            console.log("state of button on pressed"+moreSettingId.state)
            var ventGrpObj = getGroupRef("VentGroup")
            if (moreSettingId.state !== "Disabled" && !ventGrpObj.bIsSettingsMenuCreated && moreSettingId.state !== "PresetDisabled"){
                moreSettingId.strVentPreviousState = moreSettingId.state
                GrpCtrl.onSettingsCompTouched(moreSettingId.objectName)
            }
        }else if(!moreSettingMouseArea.containsPress){
            if(moreSettingId.state === "Touched"){
                moreSettingId.state = moreSettingId.strVentPreviousState
            }
        }
    }

    states: [State{
            name: "Scroll"
            when: moreSettingId.activeFocus && moreSettingId.strVentState == "Vent"
            PropertyChanges {
                target: moreSettingId;
                border.color: strMoreSettingScrollStateBorderColor;
                border.width: iScrollBorderWidth
            }

            PropertyChanges {
                target: leftBorder1
                visible: true
            }
            PropertyChanges {
                target: topBorder1
                visible: true
            }
            PropertyChanges {
                target: bottomBorder2
                visible: true
            }
            PropertyChanges {
                target: leftBorder2
                visible: true
            }
            PropertyChanges {
                target: rightBorder2
                visible : true
            }
        },
        State{
            name: "PresetScroll"
            extend: "Scroll"
            when: moreSettingId.activeFocus && moreSettingId.strVentState == "Bag"
            PropertyChanges {
                target: moreIcon
                source: strMoreIconPreset
            }
            PropertyChanges {
                target: text1
                color: strMorePresetTextColor
            }
        },
        State{
            name: "Disabled"
            PropertyChanges {
                target: moreSettingId;
                color: strMoreSettingDisabledStateColor
                border.width: iOtherStateBorderWidth
            }
            PropertyChanges {
                target: rect1
                color: strMoreSettingDisabledStateColor
            }
            PropertyChanges {
                target: rect2
                color: strMoreSettingDisabledStateColor
            }
            PropertyChanges {
                target: text1
                color:strDisabledStateFontColor
            }
            PropertyChanges {
                target: moreIcon
                source: strMoreIconDiabled
            }},
        State{
            name: "Active"
            PropertyChanges {
                target: moreSettingId;
                color: strMoreSettingActiveStateColor
                border.width: iOtherStateBorderWidth
            }},
        State{
            name: "Touched"
            PropertyChanges {
                target: moreSettingId;
                color: strMoreSettingTouchedStateColor;
                border.width: iOtherStateBorderWidth
            }
            PropertyChanges {
                target: rect1
                color: strMoreSettingTouchedStateColor
            }
            PropertyChanges {
                target: rect2
                color: strMoreSettingTouchedStateColor
            }
            PropertyChanges {
                target: moreIcon
                source: strMoreIconTouched
            }},
        State{
            name: "PresetTouched"
            extend: "Touched"
            PropertyChanges {
                target: moreIcon
                source : strMoreIconPreset
            }},
        State{
            name: "Preset"
            PropertyChanges {
                target: text1
                color : strMorePresetTextColor
            }
            PropertyChanges {
                target: moreIcon
                source: strMoreIconPreset
            }},
        State{
            name: "PresetDisabled"
            extend: "Disabled"
        }
    ]
}
