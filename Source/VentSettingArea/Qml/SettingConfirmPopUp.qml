import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.0

Rectangle {
    property int popUpWidth
    property int popUpHeight
    property int popUpBtnHeight
    property int popUpBtnWidth
    property int popUpBtnRadius
    property int popUpBtnMargin
    property int popUpTriHeight
    property int popUpTriWidth
    property int settingBtnValuePixelSize

    property string popUpColor
    property string popUpBtnColor
    property string settingConfirmLabel
    property string settingBtnFontColor
    property string settingCancelLabel
    property string popUpTriIconSource
    property string uniqueObjectID: "SettingConfirmPopup"

    z: 1

    signal cancelClicked()
    signal confirmClicked()

    height: popUpHeight
    width: popUpWidth
    color: popUpColor
    anchors.bottom: parent.top
    anchors.bottomMargin: popuptriangle.height

    RowLayout {
        id: rowLayout
        anchors.fill: parent
        spacing: 10
        Rectangle {
            height: popUpBtnHeight
            width: popUpBtnWidth
            color: popUpBtnColor
            radius: popUpBtnRadius
            Layout.bottomMargin: popUpBtnMargin
            Layout.topMargin: popUpBtnMargin
            Layout.leftMargin: popUpBtnMargin

            Label {
                id: label1
                text: qsTrId(settingConfirmLabel)
                color: settingBtnFontColor
                anchors.centerIn: parent
                font.pixelSize:  settingBtnValuePixelSize
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    confirmClicked()
                }
            }
        }

        Rectangle {
            height: popUpBtnHeight
            width: popUpBtnWidth
            color: popUpBtnColor
            radius: popUpBtnRadius
            Layout.bottomMargin: popUpBtnMargin
            Layout.topMargin: popUpBtnMargin
            Layout.rightMargin: popUpBtnMargin
            Label {
                id: label2
                text: qsTrId(settingCancelLabel)
                color: settingBtnFontColor
                anchors.centerIn: parent
                font.pixelSize:  settingBtnValuePixelSize
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    cancelClicked()
                }
            }
        }
    }

    Image {
        id: popuptriangle
        height: popUpTriHeight
        width: popUpTriWidth
        source: popUpTriIconSource
        anchors.top: parent.bottom
        anchors.left: parent.left
        anchors.leftMargin: 30
    }
}


