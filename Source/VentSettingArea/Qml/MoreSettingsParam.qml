import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "qrc:/Source/VentSettingArea/JavaScript/MoreSettingsController.js" as MenuCntrl
import "qrc:/Source/VentSettingArea/JavaScript/PmaxController.js" as PmaxCtrl
import "qrc:/Source/VentSettingArea/JavaScript/TinspController.js" as TinspCtrl
import "qrc:/Source/VentSettingArea/JavaScript/TpauseController.js" as TpauseCtrl
import "qrc:/Source/VentSettingArea/JavaScript/TrigWindowController.js" as TrigWindowCtrl
import "qrc:/Source/VentSettingArea/JavaScript/EndofBthController.js" as EndOfBthCtrl
import "qrc:/Source/VentSettingArea/JavaScript/FlowTriggerController.js" as FlwTrigCtrl
import "qrc:/Source/VentSettingArea/JavaScript/BackupTimeController.js" as BackupTimeController

Rectangle {
    id: moreSettingParamId
    objectName: "secParam" + iCompID

    property bool bIsSpinnerTouchedUp
    property bool bIsSpinnerTouchedDown
    property int iCompID
    property int iAccelerationInterval: 200
    property int iArrowWidth: 50
    property int iArrowHeight: 50
    property int iMoreSettingParamWidth: 300
    property int iMoreSettingParamHeight: 100
    property int iSelectedStateWidth
    property int iSelectedStateHeight
    property int iSelectedStateArrowWidth
    property int iSelectedStateArrowHeight
    property int iUnitlabelWidth: 100
    property int iUnitlabelHeight: 50
    property int iValueBoxWidth: 110
    property int iValueBoxHeight: 50
    property int iValueBoxBorderWidth: 4
    property int iValueBoxRadius: 4
    property string strVentScrollStateBorderColor
    property string strMoreSettingParamSpinnerUpEnabled
    property string strMoreSettingParamSpinnerUpDisabled
    property string strMoreSettingParamSpinnerDownDisabled
    property string strMoreSettingParamSpinnerDownEnabled
    property string strSecParamUpArrowSource: strMoreSettingParamSpinnerUpEnabled
    property string strSecParamDownArrowSource: strMoreSettingParamSpinnerDownEnabled

    //Styling Property
    property string strMoreSettingParamActiveStateColor: "#2e2e2e"
    property string strMoreSettingParamDisabledStateColor: "lightgray"
    property string strMoreSettingParamEndOfScaleStateColor: "#dddddd"
    property string strMoreSettingParamEndOfScaleStateBorderColor: "#a89c8b"
    property string strMoreSettingParamScrollStateBorderColor: "#edbf00"
    property string strMoreSettingParamSelectedStateColor: "#FFE771"
    property string strMoreSettingParamSelectedStateBorderColor: "#edbf00"
    property string strMoreSettingParamTouchedStateColor: "#b2b2a2"
    property string strMoreSettingParamColor: "#242424"
    property string strSecondaryParamLabel
    property string strSecondaryParamLabelPixelSize: "16"
    property string strSecondaryParamUnit
    property string strSpinnerIconTchStateColor: "#4565a6"
    property string strSpinnerIconSelStateColor: "#616161"

    property string strSecParamUpArrowColor: strSpinnerIconSelStateColor
    property string strSecParamDownArrowColor: strSpinnerIconSelStateColor
    property string strSelectedStateArrowColor
    property string strType: "SecondaryParamSetting"
    property string strValue
    property string strValueBoxBorderColor: "#444444"
    property string strValueBoxColor: "#787878"
    property string uniqueObjectID
    property var prevValue

    signal checkEndOfScaleForSpinnerWidget(string objectName)
    signal decrementValue(string objectName, string strName)
    signal incrementValue(string objectName, string strName)
    signal setDownArrowBgColor(string objectName)
    signal setUpArrowBgColor(string objectName)
    signal selection()

    width: iMoreSettingParamWidth
    height: iMoreSettingParamHeight
    color: strMoreSettingParamColor

    //*******************************************
    // Purpose: For setting unique ID for the more settings param based on the mode
    //***********************************************/
    function setUniqueID(uniqueName) {
        moreSettingParamId.uniqueObjectID = uniqueName
        console.log("********uniqueObjectID SECPARAM************",moreSettingParamId.uniqueObjectID)
    }

    //*******************************************
    // Purpose: To handle property changes when accelaration timer is stopped(200 ms)
    // Description: Handles change to spinner widget properties, when the accelaration of the
    // spinner up/down arrow is stopped
    //***********************************************/
    function stopAccelarationTimers() {
        bIsSpinnerTouchedUp = false ;
        bIsSpinnerTouchedDown = false ;
        moreSettingParamId.strSecParamDownArrowColor = strSpinnerIconSelStateColor;
        moreSettingParamId.strSecParamUpArrowColor = strSpinnerIconSelStateColor;
    }

    Label {
        id: paramText
        font.pixelSize: strSecondaryParamLabelPixelSize
        text: qsTrId(strSecondaryParamLabel)
        color: strValueBoxColor
        anchors.right: valueBox.left
        anchors.rightMargin: 60
    }

    Label {
        id: paramLabel
        font.pixelSize: strSecondaryParamLabelPixelSize
        text: qsTrId(strSecondaryParamUnit)
        color: strValueBoxColor
        anchors.right: valueBox.left
        anchors.rightMargin: 60
        anchors.top: paramText.bottom
        anchors.topMargin: 10
    }
    
    Rectangle {
        id: valueBox
        width: iValueBoxWidth
        height: iValueBoxHeight
        anchors.left: moreSettingParamId.left
        anchors.leftMargin: 270
        border.color: strValueBoxBorderColor
        border.width: iValueBoxBorderWidth
        radius: iValueBoxRadius
        focus: moreSettingParamId.focus
        color: strMoreSettingParamActiveStateColor

        TextInput {
            id: paraValue
            text: strValue
            anchors.centerIn: parent
            color: strValueBoxColor
            font.pixelSize: strSecondaryParamLabelPixelSize
        }

        Rectangle {
            id: spinnerRectDown
            width: iSelectedStateArrowWidth
            height: iSelectedStateArrowHeight
            color: moreSettingParamId.strSecParamDownArrowColor
            visible: false
            anchors.right: valueBox.left
            property string uniqueObjectID: "DownArrow_"+moreSettingParamId.uniqueObjectID
            Rectangle{
                id: radiusDownArrSec
                width: 5
                height: spinnerRectDown.height
                anchors.right: spinnerRectDown.right
                color: spinnerRectDown.color
                visible: spinnerRectDown.visible
                z: -1
            }
            Image {
                id: sec_par_down_image
                width: iArrowWidth
                height: iArrowHeight
                anchors.centerIn: parent
                source: strSecParamDownArrowSource
            }
            MouseArea {
                anchors.fill: parent
                pressAndHoldInterval: iAccelerationInterval
                hoverEnabled: true
                onClicked: {
                    console.log("decrement value for"+moreSettingParamId.objectName)
                    decrementValue(moreSettingParamId.objectName,moreSettingParamId.strName)
                    MenuCntrl.restartMenuTimeoutTimer()

                }
                onPressAndHold: {
                    MenuCntrl.restartMenuTimeoutTimer()
                    secParDecrementTimer.running = true ;
                    secParDecrementTimer.restart() ;
                }
                onPressed: {
                    MenuCntrl.restartMenuTimeoutTimer()
                    bIsSpinnerTouchedDown= true ;
                    setDownArrowBgColor(moreSettingParamId.objectName)
                }
                onReleased: {
                    bIsSpinnerTouchedDown= false  ;
                    moreSettingParamId.strSecParamDownArrowColor = strSpinnerIconSelStateColor;
                    secParDecrementTimer.running = false ;
                    secParDecrementTimer.stop() ;
                    MenuCntrl.restartMenuTimeoutTimer()
                }
                onExited: {
                    bIsSpinnerTouchedDown= false  ;
                    moreSettingParamId.strSecParamDownArrowColor = strSpinnerIconSelStateColor;
                    secParDecrementTimer.running = false ;
                    secParDecrementTimer.stop() ;
                }
            }
        }

        Rectangle {
            id:spinnerRectUp
            width:iSelectedStateArrowWidth
            height:iSelectedStateArrowHeight
            color:moreSettingParamId.strSecParamUpArrowColor
            visible: false
            anchors.left: valueBox.right
            property string uniqueObjectID: "UpArrow_"+moreSettingParamId.uniqueObjectID
            Rectangle{
                id: radiusUpArrSec
                width: 5
                height: spinnerRectUp.height
                anchors.left: spinnerRectUp.left
                color: spinnerRectUp.color
                visible: spinnerRectUp.visible
                z: -1
            }
            Image {
                id: sec_par_up_image
                width: iArrowWidth
                height: iArrowHeight
                anchors.centerIn: parent
                source: strSecParamUpArrowSource
            }
            MouseArea {
                anchors.fill: parent
                pressAndHoldInterval: iAccelerationInterval
                hoverEnabled: true ;
                onClicked: {
                    console.log("increment value for"+moreSettingParamId.objectName)
                    incrementValue(moreSettingParamId.objectName,moreSettingParamId.strName)
                    MenuCntrl.restartMenuTimeoutTimer()
                }
                onPressAndHold: {
                    secParIncrementTimer.running = true ;
                    secParIncrementTimer.restart() ;
                    MenuCntrl.restartMenuTimeoutTimer()
                }
                onPressed: {
                    bIsSpinnerTouchedUp = true ;
                    MenuCntrl.restartMenuTimeoutTimer()
                    setUpArrowBgColor(moreSettingParamId.objectName)
                }
                onReleased: {
                    moreSettingParamId.strSecParamUpArrowColor = strSpinnerIconSelStateColor;
                    secParIncrementTimer.running = false ;
                    bIsSpinnerTouchedUp = false ;
                    secParIncrementTimer.stop() ;
                    MenuCntrl.restartMenuTimeoutTimer()
                }
                onExited: {
                    moreSettingParamId.strSecParamUpArrowColor = strSpinnerIconSelStateColor;
                    secParIncrementTimer.running = false ;
                    bIsSpinnerTouchedUp = false ;
                    secParIncrementTimer.stop() ;
                }
            }
        }

        Keys.onRightPressed: {
            stopAccelarationTimers()
            if(moreSettingParamId.state === "Selected" || moreSettingParamId.state === "End of Scale"){
                console.log("calling increment signal"+moreSettingParamId.objectName+moreSettingParamId.strName)
                incrementValue(moreSettingParamId.objectName,moreSettingParamId.strName);
                MenuCntrl.restartMenuTimeoutTimer()
            } else{
                MenuCntrl.moveClockwise(moreSettingParamId.objectName)
            }
        }
        Keys.onLeftPressed: {
            stopAccelarationTimers()
            if(moreSettingParamId.state === "Selected" || moreSettingParamId.state === "End of Scale"){
                console.log("calling decrement signal"+moreSettingParamId.objectName+moreSettingParamId.strName)
                decrementValue(moreSettingParamId.objectName,moreSettingParamId.strName);
                MenuCntrl.restartMenuTimeoutTimer()
            } else{
                MenuCntrl.moveAntiClockwise(moreSettingParamId.objectName)
            }
        }

        Keys.onReturnPressed: {
            MenuCntrl.restartMenuTimeoutTimer()
            MenuCntrl.onVentSecParamEnterPressed(moreSettingParamId.objectName);
        }

        MouseArea {
            id:secParamMouseArea
            anchors.fill: parent
            onPressed: {
                console.log("state of button on pressed "+moreSettingParamId.state)
                MenuCntrl.onSecParamTouched(moreSettingParamId.objectName)

            }
            onReleased: {
                console.log("state of button on clicked "+moreSettingParamId.state)
                MenuCntrl.onSecParamSelected(moreSettingParamId.objectName)
                MenuCntrl.restartMenuTimeoutTimer()
            }
        }
    }

    Rectangle {
        id: scrollRectangle
        color: "transparent"
        visible: false
        border.width: 2
        radius: 5
        border.color: "#edbf00"
        anchors.fill: valueBox
        anchors.margins: 5
        anchors.centerIn: valueBox
    }

    states: [State{
            name: "Scroll"
            when: valueBox.activeFocus
            PropertyChanges {
                target: valueBox; border.color: strMoreSettingParamScrollStateBorderColor ;  border.width: 2 ; radius:3
            }
        },
        State{
            name: "Selected"
            PropertyChanges {
                target: valueBox; color: "#FFE771" ;border.width: 0 ; radius:0
            }
            PropertyChanges {
                target: paraValue; color: strMoreSettingParamColor ;
            }
            PropertyChanges {
                target: moreSettingParamId ; width:iSelectedStateWidth
            }
            PropertyChanges {
                target: spinnerRectUp
                visible:true
                radius:3
                border.width: 0
            }
            PropertyChanges {
                target: spinnerRectDown
                visible:true
                radius:3
                border.width: 0
            }
            StateChangeScript {
                name: "example"
                script: {selection();checkEndOfScaleForSpinnerWidget(moreSettingParamId.objectName) }
            }
        },
        State{
            name: "Disabled"
            PropertyChanges {
                target: valueBox; color: strMoreSettingParamDisabledStateColor; radius:3
            }},
        State{
            name: "Active"
            PropertyChanges {
                target: valueBox; color: strMoreSettingParamActiveStateColor; radius:3
            }},
        State{
            name: "Touched"
            PropertyChanges {
                target: valueBox; color: strMoreSettingParamTouchedStateColor; radius:3
            }},
        State{
            name: "Preset"
            PropertyChanges {
            }},
        State{
            name: "End of Scale"
            PropertyChanges {
                target: valueBox; color: strMoreSettingParamEndOfScaleStateColor; border.width: 0 ; radius:0
            }
            PropertyChanges {
                target: moreSettingParamId ; width:iSelectedStateWidth
            }
            PropertyChanges {
                target: paraValue; color: strMoreSettingParamColor ;
            }
            PropertyChanges {
                target: spinnerRectUp
                visible:true
                border.width: 0
                radius:3
            }
            PropertyChanges {
                target: spinnerRectDown
                visible:true
                border.width: 0
                radius:3
            }}
    ]

    onStateChanged: {
        if(state === "Selected"){
            checkEndOfScaleForSpinnerWidget(moreSettingParamId.objectName)
        }
    }

    onStrValueChanged: {
        checkEndOfScaleForSpinnerWidget(moreSettingParamId.objectName)
    }

    onBIsSpinnerTouchedUpChanged:{
        if (bIsSpinnerTouchedUp === false){
            secParIncrementTimer.stop() ;
        }
    }

    onBIsSpinnerTouchedDownChanged: {
        if(bIsSpinnerTouchedDown === false){
            secParDecrementTimer.stop() ;
        }
    }

    Timer {
        id: secParIncrementTimer
        running: false
        repeat: true
        interval: iAccelerationInterval
        onTriggered: {
            console.log("increment timer triggered menu spinner")
            MenuCntrl.restartMenuTimeoutTimer()
            setUpArrowBgColor(moreSettingParamId.objectName)
            incrementValue(moreSettingParamId.objectName,moreSettingParamId.strName)
        }
    }

    Timer {
        id: secParDecrementTimer
        running: false
        repeat: true
        interval: iAccelerationInterval
        onTriggered: {
            console.log("increment timer triggered menu spinner")
            MenuCntrl.restartMenuTimeoutTimer()
            setDownArrowBgColor(moreSettingParamId.objectName)
            decrementValue(moreSettingParamId.objectName,moreSettingParamId.strName)
        }
    }
}
