import QtQuick 2.0
import QtQuick.Controls 2.2
import "../JavaScript/VentModeController.js" as ModeCtrl

Rectangle {
    id: ventMenuComp

    property bool bIsModeExpansionVisible: true
    property int iCompID
    property int iModeTypeWidth: 160
    property int iModeExpansionWidth: 160
    property string strExpModeName
    property string strModeCompState
    property string strModeName

    //parent property
    property int iModeMenuBtnHeight: 50
    property int iModeMenuBtnWidth: 160
    property string strModeMenuBtnColor: "#2e2e2e"
    property string strVentMenuBtnbgcolor: "#242424"

    //child property
    property int iModeExpansionLabelHeight: 50
    property int iModeExpansionPixelSize: 12
    property int iModeMenuBtnLabelBorderWidth: 3
    property int iModeMenuBtnLabelHeight: 50
    property int iModeMenuBtnLabelPixelSize: 14
    property int iModeMenuBtnLabelRadius: 3
    property string strModeExpansionFontFamily
    property string strModeExpansionLabelColor: "#787878"
    property string strModeMenuBtnLabelBorderColor: "#444444"
    property string strModeMenuBtnLabelColor: "#787878"
    property string strModeMenuBtnLabelFontFamily

    // state styling properties
    property string strVentMenuBtnActiveStateColor: "#2e2e2e"
    property string strVentMenuBtnDisabledStateColor: "#dddddd"
    property string strVentMenuBtnEndOfScaleStateColor: "#f6f925"
    property string strVentMenuBtnEndOfScaleStateBorderColor: "#a89c8b"
    property string strVentMenuBtnScrollStateBorderColor
    property string strVentMenuBtnSelectedStateColor: "#4565a7"
    property string strVentMenuBtnSelectedStateBorderColor: "#4565a7"
    property string strVentMenuBtnTouchedStateColor: "#4565a7"
    property int  iventMenuBtnScrollStateBorderWidth: 2
    property string uniqueObjectID

    width: iModeMenuBtnWidth
    height: iModeMenuBtnHeight
    color: strVentMenuBtnbgcolor
    state: strModeCompState

    function setUniqueID(uniqueName) {
        ventMenuComp.uniqueObjectID = uniqueName
        console.log("********uniqueObjectID VENTMENU************",ventMenuComp.uniqueObjectID)
    }

    Rectangle {
        id:menuType
        width: iModeTypeWidth
        height: iModeMenuBtnLabelHeight
        anchors.left: parent.left
        border.color: strModeMenuBtnLabelBorderColor
        border.width: iModeMenuBtnLabelBorderWidth
        color: strModeMenuBtnColor
        radius: iModeMenuBtnLabelRadius
        Label {
            id: modeType
            text: qsTrId(strModeName)
            anchors.centerIn: parent
            font.family: strModeMenuBtnLabelFontFamily
            font.pixelSize: iModeMenuBtnLabelPixelSize
            color: strModeMenuBtnLabelColor
        }
    }

    Rectangle {
        id: modeExpansion
        width: iModeExpansionWidth/2
        height: iModeExpansionLabelHeight
        color: strVentMenuBtnbgcolor
        anchors.left: menuType.right
        anchors.leftMargin: 10
        visible: bIsModeExpansionVisible
        Label {
            id: expName
            text: qsTrId(strExpModeName)
            anchors.top: parent.top
            anchors.topMargin: 20
            color: strModeExpansionLabelColor
            anchors.left: parent.left
            font.family: strModeExpansionFontFamily
            font.pixelSize: iModeExpansionPixelSize
        }
    }

    Keys.onRightPressed: {
        ModeCtrl.onNavigateRightMode(objectName)
    }

    Keys.onLeftPressed: {
        ModeCtrl.onNavigateLeftMode(objectName)
    }

    Keys.onReturnPressed: {
        ModeCtrl.onSelectMode(objectName,strModeName)
    }
    MouseArea {
        anchors.fill:menuType
        onPressed: {
            ModeCtrl.onModeCompTouched(strModeName,ventMenuComp.objectName)
        }
        onReleased: {
            ModeCtrl.onModeCompSelected(strModeName,ventMenuComp.objectName)
        }
    }

    states: [
        State{
            name: "Scroll"
            when: ventMenuComp.activeFocus
            PropertyChanges {
                target: menuType; border.color: strVentMenuBtnScrollStateBorderColor
                border.width: iventMenuBtnScrollStateBorderWidth
            }
        },
        State{
            name: "Selected"
            PropertyChanges {
                target: menuType; color: strVentMenuBtnSelectedStateColor;border.color: strVentMenuBtnSelectedStateColor
            }
            PropertyChanges {
                target: modeType; color: strVentMenuBtnDisabledStateColor
            }

        },
        State{
            name: "Disabled"
            PropertyChanges {
                target: menuType; color: strVentMenuBtnDisabledStateColor
            }},
        State{
            name: "Active"
            PropertyChanges {
                target: menuType; color: strVentMenuBtnActiveStateColor
            }},
        State{
            name: "Touched"
            PropertyChanges {
                target: menuType; color: strVentMenuBtnTouchedStateColor ;border.color: strVentMenuBtnSelectedStateColor
            }
            PropertyChanges {
                target: modeType; color: strVentMenuBtnDisabledStateColor
            }},
        State{
            name: "Preset"
            PropertyChanges {
                //target: valueBox; moreSetting:"[ "+moreSetting+" ]"
            }},
        State{
            name: "End of Scale"
            PropertyChanges {
                target: menuType; color: strVentMenuBtnEndOfScaleStateColor; border.color: strVentMenuBtnEndOfScaleStateBorderColor
            }}
    ]
}

