#ifndef PARAMDUALSTORAGE_H
#define PARAMDUALSTORAGE_H

#include <QObject>
#include <qstring.h>

#define VCV_MODE "BtnMode_VCV"
#define PCV_MODE "BtnMode_PCV"
#define SIMVVCV_MODE "BtnMode_SIMVVCV"
#define SIMVPCV_MODE "BtnMode_SIMVPCV"
#define PSVPRO_MODE "BtnMode_PSVPro"

#define TV_QKEY "TV_qKey"
#define RR_QKEY "RR_qKey"
#define IE_QKEY "IE_qKey"
#define PEEP_QKEY "PEEP_qKey"
#define PINSP_QKEY "Pinsp_qKey"
#define PMAX_SKEY "Pmax_sKey"
#define VENT_MODE "MODE"
#define BACKUPTIME_SKEY "BackupTime_sKey"

class ParamDualStorage: public QObject
{
    Q_OBJECT

public:
    ParamDualStorage(bool dualStorageMismatch = false);
    Q_INVOKABLE void init(QString mode, int rr, QString ie, int tv, QString peep, int pinsp, int pmax, int backupTime);
    Q_INVOKABLE void storeCriticalParams(QString id, QString value);
    Q_INVOKABLE void validateParams(QString id, QString value);
    Q_INVOKABLE void dualStorageMisMatch(int primary, int secondary);
    Q_INVOKABLE int getBkMechMode();
    Q_INVOKABLE int getBkTvValue();
    Q_INVOKABLE int getBkRrValue();
    Q_INVOKABLE int getBkIeValue();
    Q_INVOKABLE int getBkPeepValue();
    Q_INVOKABLE int getBkPinspValue();
    Q_INVOKABLE int getBkPmaxValue();
    Q_INVOKABLE bool getIsDualStorageMismatch();
    Q_INVOKABLE int getBk_BackupTime() const;
    int setMode(QString mode);
    void initializeEnum();

    enum MachineMode
    {
        Hand = 0,
        VCV,
        PCV,
        Standby,
        Service,
        Selftest,
        ACGO,
        IDLE,
        SIMVVC,
        SIMVPC,
        PSVPRO,
        Checkout
    };

    enum CriticalParams
    {
        MODE = 1,
        TV_qkey,
        RR_qkey,
        IE_qkey,
        PEEP_qkey,
        Pinsp_qkey,
        Pmax_sKey,
        BackupTime_sKey
    };

    std::map<std::string, CriticalParams> criticalParamsList;

private:
    const static int m_guardband_1 = 0x11223344;
    int m_bk_MMode;
    int m_bk_RR;
    int m_bk_IE;
    int m_bk_TV;
    int m_bk_Peep;
    int m_bk_pinsp;
    int m_bk_pmax;
    int m_bk_RRMech;
    int m_bk_BackupTime;
    const static int m_guardband_2 = 0x55667788;
    bool m_bDualStorageMismatch ;
};
#endif // PARAMDUALSTORAGE_H
