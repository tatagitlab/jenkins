#ifndef SETTINGFILEUPDATE_H
#define SETTINGFILEUPDATE_H

#include <QObject>
#include <QQmlContext>
#include <QDebug>
#include <qstring.h>
#include <QDomDocument>
#include <QException>
#include <QFile>
#include <QTextStream>
#include <QDebug>


class SettingFileUpdate: public QObject
{
    Q_OBJECT
public:
    SettingFileUpdate(bool errorStatus  = false );
    void initializeEnum();
    Q_INVOKABLE void init(QString filePath);
    Q_INVOKABLE void createNewSettingFile();
    Q_INVOKABLE void initValues();
    Q_INVOKABLE QString read(QString param);
    Q_INVOKABLE void writeDataInFile(QString id,QString data);
    Q_INVOKABLE QString getErrorString();
    Q_INVOKABLE QString getIe();
    Q_INVOKABLE int getTv();
    Q_INVOKABLE int getRr();
    Q_INVOKABLE int getRrMech() const;
    Q_INVOKABLE void setRrMech(const QString &rrMech);
    Q_INVOKABLE QString getPeep();
    Q_INVOKABLE int getPinsp();
    Q_INVOKABLE QString getMode();
    Q_INVOKABLE int getPpeaklow();
    Q_INVOKABLE int getPmax();
	Q_INVOKABLE float getTinsp();
	 Q_INVOKABLE int getTpause();
	Q_INVOKABLE int getPsupport();
    Q_INVOKABLE int getBackupTime() const;
    Q_INVOKABLE void setBackupTime(const QString &backupTime);
    Q_INVOKABLE QString getExitBackup() const;
    Q_INVOKABLE void setExitBackup(const QString &exitBackup);

    enum  VentKey{
        TV_qKey = 1,
        RR_qKey = 2,
        IE_qKey = 3,
        PEEP_qKey = 4,
        PSUPPORT_qKey = 5,
        PINSP_qKey = 6,
        RRMECH_qKey = 7,
        FLOW_TRIGGER_sKey = 8,
        TINSP_sKey = 9,
        PMAX_sKey = 10,
        TRIG_WINDOW_sKey = 11,
        END_OF_BREATH_sKey = 12,
        TPAUSE_sKey = 13,
        BACKUPTIME_sKey = 14,
        EXITBACKUP_sKey = 15,
        Ppeak_High = 16,
        Ppeak_Low = 17,
        MV_Low = 18,
        MV_High = 19,
        TVexp_Low = 20,
        TVexp_High = 21,
        O2_Low = 22,
        O2_High = 23,
        Apnea_Delay = 24,
        Alarm_Volume = 25,
        Language = 26,
        MODE = 27
    };

    std::map<std::string, VentKey> KeyList;

private:
    QString m_ie ;
    QString m_tv ;
    QString m_rr ;
    QString m_rrMech;
    QString m_pp ;
    QString m_tp ;
    QString m_pm ;
    QString m_pinsp;
    QString m_psupport;
    QString m_fTrig;
	QString m_tinsp;
	QString m_tWindow;
	QString m_ebreath;
    QString m_ppeakl;
    QString m_ppeakh;
    QString m_backupTime;
    QString m_exitBackup;
    QString m_mvl;
    QString m_mvh;
    QString m_tvexpl;
    QString m_tvexph;
    QString m_o2l;
    QString m_o2h;
    QString m_apnead;
    QString m_alarmv;
    QString m_filename;
    QString m_language;
    QString m_mode;
    bool m_errorStat ;
    QString m_errorStr;
    QFile file;
};

#endif // SETTINGFILEUPDATE_H
