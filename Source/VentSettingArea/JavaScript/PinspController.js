/* fetch the range list and the step size for Pinsp */

var pinspRangeList;
var pinspMin;
var pinspMax;
var pinspStepSize;
pinspRangeList = physicalparam.getRangeData("Pinsp", "cmH2O");
console.log("list...Pinsp"+pinspRangeList)
pinspMin = pinspRangeList[0]
pinspMax = pinspRangeList[1]
pinspStepSize = pinspRangeList[2]

/*Increment step Logic for PINSP*/

function clkHandlePinsp(m_pinsp) {

    if((m_pinsp >= parseInt(pinspMin)) && (m_pinsp < parseInt(pinspMax))) {
        m_pinsp = parseInt(m_pinsp) + parseInt(pinspStepSize);
    }
    return m_pinsp
}

/*Decrement step Logic for PINSP*/
function antClkHandlePinsp(m_pinsp) {

    if((m_pinsp <= parseInt(pinspMax)) && (m_pinsp > parseInt(pinspMin))) {
        m_pinsp = parseInt(m_pinsp) - parseInt(pinspStepSize);
    }
    return m_pinsp
}

//*******************************************
// Purpose: handler for state change during value increment of I:E
// Input Parameters: object name of the quickey - I:E
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onPinspIncrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("Pinsp: clicked on widget for increment:"+currentVentComp+strName+selectedMode)
    if((objectName === "quickKey1" && (selectedMode === "PCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey2" && selectedMode === "PSVPro")){
        console.log("inside increment Pinsp")
        if(currentVentComp.strValue < parseInt(pinspMax)){
            currentVentComp.strValue = clkHandlePinsp(currentVentComp.strValue)
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
	    currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of I:E
// Input Parameters: object name of the quickey - I:E
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onPinspDecrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("Pinsp: clicked on widget for decrement:"+currentVentComp+strName+selectedMode)
    if((objectName === "quickKey1" && (selectedMode === "PCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey2" && selectedMode === "PSVPro")){
        console.log("inside decrement Pinsp")
        if(currentVentComp.strValue > parseInt(pinspMin)){
            currentVentComp.strValue = antClkHandlePinsp(currentVentComp.strValue)
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
	    currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - I:E
//***********************************************/

function onPinspEndOfScale(objectName) {

    console.log("checking end of scale Pinsp")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if((objectName === "quickKey1" && (selectedMode === "PCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey2" && selectedMode === "PSVPro")){
        if(currentVentComp.strValue === pinspMin) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled
        } else if(currentVentComp.strValue === pinspMax) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        } else if(currentVentComp.strValue !== pinspMin && currentVentComp.strValue !== pinspMax) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - I:E
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetPinspUpArrowBgColor(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting up arrow bg Pinsp"+currentVentComp.strValue)
    if((objectName === "quickKey1" && (selectedMode === "PCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey2" && selectedMode === "PSVPro")){
        if(currentVentComp.strValue === pinspMax) {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - I:E
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetPinspDownArrowBgColor(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting down arrow bg Pinsp"+currentVentComp.strValue)
    if((objectName === "quickKey1" && (selectedMode === "PCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey2" && selectedMode === "PSVPro")){
        if(currentVentComp.strValue === pinspMin) {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler gets called when the mode changes to connect I:E controller to the corresponding quick key widget based on mode
// Input Parameters: current mode
// Description: takes the current mode as input and based on the input connects the I:E controller to the corresponding widget
//***********************************************/


function onModeChangedPinsp(currentMode) {

    var objref;
    var pinspValue;
    console.log("current mode in Pinsp ctrl",currentMode)
    if(currentMode === "PCV" || currentMode === "SIMVPCV") {
        pinspValue = settingReader.read("Pinsp_qKey")
        objref=getCompRef(ventParentId,"quickKey1");
        objref.setLabelofWidget("Pinsp_qKey");
        objref.setUnitofWidget("cmH2O_unit")
        backupData.validateParams("Pinsp_qKey",pinspValue)
        objref.setQKValue(pinspValue)
        objref.setUniqueID("qkPinsp_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        objref.bIsValueChanged = false
    } else if(currentMode === "PSVPro") {
        pinspValue = settingReader.read("Pinsp_qKey")
        objref=getCompRef(ventParentId,"quickKey2");
        objref.setLabelofWidget("Pinsp_qKey");
        objref.setUnitofWidget("cmH2O_unit")
        backupData.validateParams("Pinsp_qKey",pinspValue)
        objref.setQKValue(pinspValue)
        objref.setUniqueID("qkPinsp_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        objref.bIsValueChanged = false
    }    
}
