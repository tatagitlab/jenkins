/* fetch the range list and the step size for Flow trigger */

var flowTriggerRangeList;
var flowTrigger_min_step1;
var flowTrigger_max_step1;
var flowTrigger_step_1;
var flowTrigger_min_step10;
var flowTrigger_max_step10;
var flowTrigger_step_10;
flowTriggerRangeList = physicalparam.getRangeData("FlowTrigger", "l/min");
flowTrigger_min_step1 = flowTriggerRangeList[0]
flowTrigger_max_step1 = flowTriggerRangeList[1]
flowTrigger_step_1 = flowTriggerRangeList[2]
flowTrigger_min_step10 = flowTriggerRangeList[3]
flowTrigger_max_step10 = flowTriggerRangeList[4]
flowTrigger_step_10 = flowTriggerRangeList[5]

function clkHandleFlowTrigger(iFlowTrigger) {

    var range = flowTriggerRangeList
    if((iFlowTrigger >= parseFloat(flowTrigger_min_step1)) && (iFlowTrigger < (flowTrigger_max_step1))) {
        iFlowTrigger += parseFloat(flowTrigger_step_1);
        iFlowTrigger = iFlowTrigger.toFixed(1)
    } else if((iFlowTrigger >= (flowTrigger_max_step1)) && (iFlowTrigger < parseFloat(flowTrigger_max_step10))) {
        iFlowTrigger += parseFloat(flowTrigger_step_10);
        iFlowTrigger = iFlowTrigger.toFixed(1)
    } else {
        // do nothing
    }
    return iFlowTrigger;
}

/*Decrement step logic for Flow trigger*/
function antClkHandleFlowTrigger(iFlowTrigger) {

    var range = flowTriggerRangeList
    console.log("iFlowTrigger",iFlowTrigger)
    if((iFlowTrigger > parseFloat(flowTrigger_min_step1)) && (iFlowTrigger <= (flowTrigger_max_step1))) {
        iFlowTrigger -= parseFloat(flowTrigger_step_1);
        iFlowTrigger = iFlowTrigger.toFixed(1)
    } else if((iFlowTrigger > (flowTrigger_max_step1)) && (iFlowTrigger <= parseFloat(flowTrigger_max_step10))) {
        iFlowTrigger -= parseFloat(flowTrigger_step_10);
        iFlowTrigger = iFlowTrigger.toFixed(1)
    } else {
        // do nothing
    }
    return iFlowTrigger;
}

//*******************************************
// Purpose: handler for state change during value increment of Flow Trigger
// Input Parameters: object name of the quickey - Flow Trigger
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onFlowTrigIncrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("Flow Trigger: clicked on widget for increment:"+objectName+currentSecParam+strName+selectedMode)
    if(objectName === "secParam0" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" || selectedMode === "PSVPro")){
        console.log("inside increment Flow Trigger")
        if(currentSecParam.strValue < parseFloat(flowTrigger_max_step10)){
            currentSecParam.strValue= clkHandleFlowTrigger(parseFloat(currentSecParam.strValue))
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }

    }
}

//*******************************************
// Purpose: handler for state change during value decrement of Flow Trigger
// Input Parameters: object name of the quickey - Flow Trigger
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onFlowTrigDecrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("Flow Trigger: clicked on widget for decrement:"+objectName+currentSecParam+strName+selectedMode)
    if(objectName === "secParam0" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" || selectedMode === "PSVPro")){
        console.log("inside decrement Flow Trigger")
        if(currentSecParam.strValue > flowTrigger_min_step1){
            currentSecParam.strValue= antClkHandleFlowTrigger(parseFloat(currentSecParam.strValue))
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Flow Trigger
//***********************************************/
function onFlowTrigEndOfScale(objectName) {

    console.log("checking end of scale Flow Trigger")
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if(objectName === "secParam0" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" || selectedMode === "PSVPro")){
        if(currentVentComp.strValue === flowTrigger_min_step1) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownDisabled
        } else if(parseFloat(currentVentComp.strValue) === parseFloat(flowTrigger_max_step10)) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource=currentVentComp.strMoreSettingParamSpinnerDownEnabled
        } else if(currentVentComp.strValue !== flowTrigger_min_step1 && parseFloat(currentVentComp.strValue) !== parseFloat(flowTrigger_max_step10)) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Flow Trigger
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/
function onSetFlowTrigUpArrowBgColor(objectName) {
    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting up arrow bg FlowTrig"+currentVentComp.strValue)
    if(objectName === "secParam0" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" || selectedMode === "PSVPro")){
        console.log("FlowTrig...........")
        if(parseFloat(currentVentComp.strValue) === parseFloat(flowTrigger_max_step10)) {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Flow Trigger
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/
function onSetFlowTrigDownArrowBgColor(objectName) {
    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting down arrow bg FlowTrig"+currentVentComp.strValue)
    if(objectName === "secParam0" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" || selectedMode === "PSVPro")){
        if(currentVentComp.strValue === flowTrigger_min_step1) {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
