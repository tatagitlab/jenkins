Qt.include("qrc:/Source/VentSettingArea/JavaScript/EndofBthController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/FlowTriggerController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/PmaxController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/TinspController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/TpauseController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/TrigWindowController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/ExitBackupController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/BackupTimeController.js")

var moreMenuSecondaryComponent;
var secParamCreationPMAX;
var secParamCreationTPAUSE;
var secParamCreationFlowTrigger;
var secParamCreationTinSP;
var secParamCreationTrig;
var secParamCreationEnd;
var secParamCreationBack;
var secParamCreationBackupTime;
var secParamCreationExitBackup;


//MoreSettingParam property
var moreSettingParamWidth = parseInt(component.getComponentDimension("SecondaryParamSetting", "width"))
var moreSettingParamHeight = parseInt(component.getComponentDimension("SecondaryParamSetting", "height"))
var moreSettingParamLabelWidth = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "LabelWidth"))
var moreSettingParamLabelHeight = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "LabelHeight"))
var moreSettingParamLabelValueWidth = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "LabelValueWidth"))
var moreSettingParamLabelValueHeight = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "LabelValueHeight"))
var moreSettingParamBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "BorderWidth"))
var moreSettingParamBorderRadius = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "BorderRadius"))
var moreStringParamLabelValueColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "LabelValueColor")
var moreSettingParamColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "Color")
var moreStringParamLabelPixelSize = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "LabelPixelSize")
var moreSettingParamBorderColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "BorderColor")
var moreSettingParamActiveStateColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "ValueEditBgColor")
var moreSettingParamDisabledStateColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "DisabledStateColor")
var moreSettingParamEndOfScaleStateColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "EndOfScaleStateColor")
var moreSettingParamEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "EndOfScaleStateBorderColor")
var moreSettingParamScrollStateBorderColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "ScrollStateBorderColor")
var moreSettingParamSelectedStateColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SelectedStateColor")
var moreSettingParamSelectedStateBorderColor =  component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SelectedStateBorderColor")
var moreSettingParamTouchedStateColor =  component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "TouchedStateColor")
var moreSettingParamSpinnerUpEnabled =  component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SpinnerUpEnabled")
var moreSettingParamSpinnerUpDisabled = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SpinnerUpDisabled")
var moreSettingParamSpinnerDownEnabled = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SpinnerDownEnabled")
var moreSettingParamSpinnerDownDisabled = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SpinnerDownDisabled")

var selectedStateWidth = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "StateWidthChange"))
var selectedStateHeight = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SecParSpinArrowBtnHeight"))
var selectedStateArrowWidth = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SecParSpinArrowBtnWidth"))
var selectedStateArrowHeight = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SecParSpinArrowBtnHeight"))
var selectedStateArrowColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SecParSpinBgColor")
var arrowWidth = parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SecParSpinIconWidth"))
var arrowHeight =parseInt(component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "SecParSpinIconHeight"))
var touchedStateColor = component.getComponentXmlDataByTag("Component","SecondaryParamSetting", "TouchedStateColor")
//Resource Strings
var pmaxLabel = component.getComponentResourceString("Component","SecondaryParamSetting","Label","Pmax")
var pmaxUnit = component.getComponentResourceString("Component","SecondaryParamSetting","Unit","Pmax")
var tpauseLabel = component.getComponentResourceString("Component","SecondaryParamSetting","Label","Tpause")
var tpauseUnit = component.getComponentResourceString("Component","SecondaryParamSetting","Unit","Tpause")
var flowTrigLabel = component.getComponentResourceString("Component","SecondaryParamSetting","Label","FlowTrigger")
var flowTrigUnit = component.getComponentResourceString("Component","SecondaryParamSetting","Unit","FlowTrigger")
var tinspLabel = component.getComponentResourceString("Component","SecondaryParamSetting","Label","Tinsp")
var tinspUnit = component.getComponentResourceString("Component","SecondaryParamSetting","Unit","Tinsp")
var trigWinLabel = component.getComponentResourceString("Component","SecondaryParamSetting","Label","TrigWindow")
var trigWinUnit = component.getComponentResourceString("Component","SecondaryParamSetting","Unit","TrigWindow")
var eobLabel = component.getComponentResourceString("Component","SecondaryParamSetting","Label","EndOfBreath")
var eobUnit = component.getComponentResourceString("Component","SecondaryParamSetting","Unit","EndOfBreath")
var backupTimeLabel = component.getComponentResourceString("Component","SecondaryParamSetting","Label","Backup Time")
var backupTimeUnit = component.getComponentResourceString("Component","SecondaryParamSetting","Unit","Backup Time")
var exitBackupLabel = component.getComponentResourceString("Component","SecondaryParamSetting","Label","Exit Backup")
var exitBackupUnit = component.getComponentResourceString("Component","SecondaryParamSetting","Unit","Exit Backup")
function createMenuComponents() {

    console.log("inside more setting param creation...")
    moreMenuSecondaryComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/MoreSettingsParam.qml")
    if (moreMenuSecondaryComponent.status === Component.Ready){
        finishMenuComponentCreation();
    } else{
        moreMenuSecondaryComponent.statusChanged.connect(finishMenuComponentCreation);
    }
}

function finishMenuComponentCreation() {

    console.log("finishMenuComponentCreation")
    console.log("mode is ->>>>",ventGrpId.selectedMode)
    var tinspValue = settingReader.read("Tinsp_sKey")
    var tpauseValue = settingReader.read("Tpause_sKey")
    var pmaxValue = settingReader.read("Pmax_sKey")
    var psupport = settingReader.read("Psupport_qKey")
    var ppeakValue = settingReader.read("Ppeak_Low")
    var pmaxvalue = settingReader.read("Pmax_sKey")
    backupData.validateParams(pmaxLabel,pmaxValue)

    var backupValue = settingReader.read("BackupTime_sKey");
    backupData.validateParams(backupTimeLabel,backupValue);
    ObjCCValue.updateConstraintParam("Tpause_sKey",tpauseValue)
    ObjCCValue.updateConstraintParam("Ppeak_Low",ppeakValue)
    ObjCCValue.updateConstraintParam("Psupport_qKey",psupport)
    ObjCCValue.updateConstraintParam("Tinsp_sKey",tinspValue)
    ObjCCValue.updateConstraintParam("Pmax_sKey",pmaxvalue)

    if (ventGrpId.selectedMode === "VCV"){
        secParamCreationPMAX=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 0,
                                                                         "strValue": settingReader.read("Pmax_sKey"),
                                                                         "strSecondaryParamLabel":pmaxLabel,
                                                                         "strSecondaryParamUnit":pmaxUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     });
        secParamCreationTPAUSE=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 1,
                                                                           "strValue": settingReader.read("Tpause_sKey"),
                                                                           "strSecondaryParamLabel":tpauseLabel,
                                                                           "strSecondaryParamUnit":tpauseUnit,
                                                                           "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                           "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                           "strMoreSettingParamColor":moreSettingParamColor,
                                                                           "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                           "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                           "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                           "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                           "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                           "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                           "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                           "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                           "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                           "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                           "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                           "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                           "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                           "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                           "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                           "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                           "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                           "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                           "iSelectedStateWidth" : selectedStateWidth,
                                                                           "iSelectedStateHeight" :selectedStateHeight,
                                                                           "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                           "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                           "strTouchedStateColor" : touchedStateColor,
                                                                           "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                           "iArrowWidth" : arrowWidth ,
                                                                           "iArrowHeight" : arrowHeight
                                                                       });
        secParamCreationPMAX.decrementValue.connect(onPmaxDecrementValue);
        secParamCreationPMAX.incrementValue.connect(onPmaxIncrementValue);
        secParamCreationPMAX.checkEndOfScaleForSpinnerWidget.connect(onPmaxEndOfScale);
        secParamCreationPMAX.setDownArrowBgColor.connect(onSetPmaxDownArrowBgColor);
        secParamCreationPMAX.setUpArrowBgColor.connect(onSetPmaxUpArrowBgColor);
        secParamCreationPMAX.selection.connect(onPmaxSelection);

        secParamCreationTPAUSE.decrementValue.connect(onTpauseDecrementValue);
        secParamCreationTPAUSE.incrementValue.connect(onTpauseIncrementValue);
        secParamCreationTPAUSE.checkEndOfScaleForSpinnerWidget.connect(onTpauseEndOfScale);
        secParamCreationTPAUSE.setDownArrowBgColor.connect(onSetTpauseDownArrowBgColor);
        secParamCreationTPAUSE.setUpArrowBgColor.connect(onSetTpauseUpArrowBgColor);
        secParamCreationTPAUSE.selection.connect(onTpauseSelection);

        secParamCreationPMAX.setUniqueID("Pmax_"+ventGrpId.selectedMode)
        secParamCreationTPAUSE.setUniqueID("Tpause_"+ventGrpId.selectedMode)

    } else if (ventGrpId.selectedMode === "PCV"){
        secParamCreationPMAX=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 0,
                                                                         "strValue": settingReader.read("Pmax_sKey"),
                                                                         "strSecondaryParamLabel":pmaxLabel,
                                                                         "strSecondaryParamUnit":pmaxUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     })
        secParamCreationPMAX.decrementValue.connect(onPmaxDecrementValue);
        secParamCreationPMAX.incrementValue.connect(onPmaxIncrementValue);
        secParamCreationPMAX.checkEndOfScaleForSpinnerWidget.connect(onPmaxEndOfScale);
        secParamCreationPMAX.setDownArrowBgColor.connect(onSetPmaxDownArrowBgColor);
        secParamCreationPMAX.setUpArrowBgColor.connect(onSetPmaxUpArrowBgColor);
        secParamCreationPMAX.selection.connect(onPmaxSelection);

        secParamCreationPMAX.setUniqueID("Pmax_"+ventGrpId.selectedMode)

    } else if (ventGrpId.selectedMode === "SIMVVCV"){
        secParamCreationFlowTrigger=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 0,
                                                                                "strValue": settingReader.read("FlowTrigger_sKey"),
                                                                                "strSecondaryParamLabel":flowTrigLabel,
                                                                                "strSecondaryParamUnit":flowTrigUnit,
                                                                                "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                                "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                                "strMoreSettingParamColor":moreSettingParamColor,
                                                                                "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                                "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                                "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                                "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                                "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                                "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                                "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                                "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                                "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                                "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                                "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                                "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                                "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                                "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                                "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                                "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                                "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                                "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                                "iSelectedStateWidth" : selectedStateWidth,
                                                                                "iSelectedStateHeight" :selectedStateHeight,
                                                                                "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                                "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                                "strTouchedStateColor" : touchedStateColor,
                                                                                "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                                "iArrowWidth" : arrowWidth ,
                                                                                "iArrowHeight" : arrowHeight
                                                                            });
        secParamCreationTinSP=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 1,
                                                                          "strValue": settingReader.read("Tinsp_sKey"),
                                                                          "strSecondaryParamLabel":tinspLabel,
                                                                          "strSecondaryParamUnit":tinspUnit,
                                                                          "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                          "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                          "strMoreSettingParamColor":moreSettingParamColor,
                                                                          "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                          "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                          "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                          "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                          "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                          "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                          "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                          "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                          "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                          "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                          "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                          "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                          "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                          "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                          "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                          "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                          "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                          "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                          "iSelectedStateWidth" : selectedStateWidth,
                                                                          "iSelectedStateHeight" :selectedStateHeight,
                                                                          "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                          "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                          "strTouchedStateColor" : touchedStateColor,
                                                                          "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                          "iArrowWidth" : arrowWidth ,
                                                                          "iArrowHeight" : arrowHeight
                                                                      });
        secParamCreationTPAUSE=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 2,
                                                                           "strValue": settingReader.read("Tpause_sKey"),
                                                                           "strSecondaryParamLabel":tpauseLabel,
                                                                           "strSecondaryParamUnit":tpauseUnit,
                                                                           "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                           "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                           "strMoreSettingParamColor":moreSettingParamColor,
                                                                           "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                           "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                           "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                           "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                           "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                           "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                           "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                           "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                           "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                           "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                           "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                           "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                           "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                           "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                           "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                           "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                           "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                           "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                           "iSelectedStateWidth" : selectedStateWidth,
                                                                           "iSelectedStateHeight" :selectedStateHeight,
                                                                           "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                           "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                           "strTouchedStateColor" : touchedStateColor,
                                                                           "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                           "iArrowWidth" : arrowWidth ,
                                                                           "iArrowHeight" : arrowHeight
                                                                       });
        secParamCreationPMAX=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 3,
                                                                         "strValue": settingReader.read("Pmax_sKey"),
                                                                         "strSecondaryParamLabel":pmaxLabel,
                                                                         "strSecondaryParamUnit":pmaxUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     });
        secParamCreationTrig=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 4,
                                                                         "strValue": settingReader.read("Trig_Window_sKey"),
                                                                         "strSecondaryParamLabel":trigWinLabel,
                                                                         "strSecondaryParamUnit":trigWinUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     });
        secParamCreationEnd=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 5,
                                                                        "strValue": settingReader.read("EndofBreath_sKey"),
                                                                        "strSecondaryParamLabel":eobLabel,
                                                                        "strSecondaryParamUnit":eobUnit,
                                                                        "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                        "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                        "strMoreSettingParamColor":moreSettingParamColor,
                                                                        "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                        "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                        "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                        "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                        "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                        "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                        "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                        "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                        "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                        "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                        "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                        "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                        "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                        "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                        "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                        "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                        "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                        "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                        "iSelectedStateWidth" : selectedStateWidth,
                                                                        "iSelectedStateHeight" :selectedStateHeight,
                                                                        "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                        "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                        "strTouchedStateColor" : touchedStateColor,
                                                                        "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                        "iArrowWidth" : arrowWidth ,
                                                                        "iArrowHeight" : arrowHeight
                                                                    })
        secParamCreationFlowTrigger.decrementValue.connect(onFlowTrigDecrementValue);
        secParamCreationFlowTrigger.incrementValue.connect(onFlowTrigIncrementValue);
        secParamCreationFlowTrigger.checkEndOfScaleForSpinnerWidget.connect(onFlowTrigEndOfScale);
        secParamCreationFlowTrigger.setDownArrowBgColor.connect(onSetFlowTrigDownArrowBgColor);
        secParamCreationFlowTrigger.setUpArrowBgColor.connect(onSetFlowTrigUpArrowBgColor);

        

        secParamCreationTinSP.decrementValue.connect(onTinspDecrementValue);
        secParamCreationTinSP.incrementValue.connect(onTinspIncrementValue);
        secParamCreationTinSP.checkEndOfScaleForSpinnerWidget.connect(onTinspEndOfScale);
        secParamCreationTinSP.setDownArrowBgColor.connect(onSetTinspDownArrowBgColor);
        secParamCreationTinSP.setUpArrowBgColor.connect(onSetTinspUpArrowBgColor);
        secParamCreationTinSP.selection.connect(onTinspSelection);

        secParamCreationTPAUSE.decrementValue.connect(onTpauseDecrementValue);
        secParamCreationTPAUSE.incrementValue.connect(onTpauseIncrementValue);
        secParamCreationTPAUSE.checkEndOfScaleForSpinnerWidget.connect(onTpauseEndOfScale);
        secParamCreationTPAUSE.setDownArrowBgColor.connect(onSetTpauseDownArrowBgColor);
        secParamCreationTPAUSE.setUpArrowBgColor.connect(onSetTpauseUpArrowBgColor);
        secParamCreationTPAUSE.selection.connect(onTpauseSelection);

        secParamCreationPMAX.decrementValue.connect(onPmaxDecrementValue);
        secParamCreationPMAX.incrementValue.connect(onPmaxIncrementValue);
        secParamCreationPMAX.checkEndOfScaleForSpinnerWidget.connect(onPmaxEndOfScale);
        secParamCreationPMAX.setDownArrowBgColor.connect(onSetPmaxDownArrowBgColor);
        secParamCreationPMAX.setUpArrowBgColor.connect(onSetPmaxUpArrowBgColor);
        secParamCreationPMAX.selection.connect(onPmaxSelection);

        secParamCreationTrig.decrementValue.connect(onTrigWinDecrementValue);
        secParamCreationTrig.incrementValue.connect(onTrigWinIncrementValue);
        secParamCreationTrig.checkEndOfScaleForSpinnerWidget.connect(onTrigWinEndOfScale);
        secParamCreationTrig.setDownArrowBgColor.connect(onSetTrigWinDownArrowBgColor);
        secParamCreationTrig.setUpArrowBgColor.connect(onSetTrigWinUpArrowBgColor);


        secParamCreationEnd.decrementValue.connect(onEndOfBthDecrementValue);
        secParamCreationEnd.incrementValue.connect(onEndOfBthIncrementValue);
        secParamCreationEnd.checkEndOfScaleForSpinnerWidget.connect(onEndOfBthEndOfScale);
        secParamCreationEnd.setUpArrowBgColor.connect(onSetEndOfBthUpArrowBgColor);
        secParamCreationEnd.setDownArrowBgColor.connect(onSetEndOfBthDownArrowBgColor);

        secParamCreationFlowTrigger.setUniqueID("flowTrig_"+ventGrpId.selectedMode)
        secParamCreationTinSP.setUniqueID("Tinsp_"+ventGrpId.selectedMode)
        secParamCreationTPAUSE.setUniqueID("Tpause_"+ventGrpId.selectedMode)
        secParamCreationPMAX.setUniqueID("Pmax_"+ventGrpId.selectedMode)
        secParamCreationTrig.setUniqueID("TrigWin_"+ventGrpId.selectedMode)
        secParamCreationEnd.setUniqueID("Eob_"+ventGrpId.selectedMode)

    }  else if ((ventGrpId.selectedMode === "SIMVPCV") && (!ventGrpId.bCSBTrigger)){
        console.log("SIMV PCV !bCSBTrigger")
        secParamCreationFlowTrigger=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 0,
                                                                                "strValue": settingReader.read("FlowTrigger_sKey"),
                                                                                "strSecondaryParamLabel":flowTrigLabel,
                                                                                "strSecondaryParamUnit":flowTrigUnit,
                                                                                "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                                "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                                "strMoreSettingParamColor":moreSettingParamColor,
                                                                                "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                                "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                                "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                                "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                                "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                                "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                                "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                                "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                                "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                                "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                                "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                                "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                                "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                                "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                                "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                                "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                                "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                                "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                                "iSelectedStateWidth" : selectedStateWidth,
                                                                                "iSelectedStateHeight" :selectedStateHeight,
                                                                                "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                                "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                                "strTouchedStateColor" : touchedStateColor,
                                                                                "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                                "iArrowWidth" : arrowWidth ,
                                                                                "iArrowHeight" : arrowHeight
                                                                            });
        secParamCreationTinSP=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 1,
                                                                          "strValue": settingReader.read("Tinsp_sKey"),
                                                                          "strSecondaryParamLabel":tinspLabel,
                                                                          "strSecondaryParamUnit":tinspUnit,
                                                                          "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                          "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                          "strMoreSettingParamColor":moreSettingParamColor,
                                                                          "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                          "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                          "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                          "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                          "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                          "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                          "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                          "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                          "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                          "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                          "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                          "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                          "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                          "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                          "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                          "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                          "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                          "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                          "iSelectedStateWidth" : selectedStateWidth,
                                                                          "iSelectedStateHeight" :selectedStateHeight,
                                                                          "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                          "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                          "strTouchedStateColor" : touchedStateColor,
                                                                          "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                          "iArrowWidth" : arrowWidth ,
                                                                          "iArrowHeight" : arrowHeight
                                                                      });

        secParamCreationPMAX=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 2,
                                                                         "strValue": settingReader.read("Pmax_sKey"),
                                                                         "strSecondaryParamLabel":pmaxLabel,
                                                                         "strSecondaryParamUnit":pmaxUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     });
        secParamCreationTrig=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 3,
                                                                         "strValue": settingReader.read("Trig_Window_sKey"),
                                                                         "strSecondaryParamLabel":trigWinLabel,
                                                                         "strSecondaryParamUnit":trigWinUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     });
        secParamCreationEnd=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 4,
                                                                        "strValue": settingReader.read("EndofBreath_sKey"),
                                                                        "strSecondaryParamLabel":eobLabel,
                                                                        "strSecondaryParamUnit":eobUnit,
                                                                        "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                        "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                        "strMoreSettingParamColor":moreSettingParamColor,
                                                                        "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                        "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                        "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                        "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                        "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                        "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                        "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                        "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                        "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                        "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                        "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                        "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                        "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                        "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                        "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                        "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                        "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                        "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                        "iSelectedStateWidth" : selectedStateWidth,
                                                                        "iSelectedStateHeight" :selectedStateHeight,
                                                                        "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                        "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                        "strTouchedStateColor" : touchedStateColor,
                                                                        "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                        "iArrowWidth" : arrowWidth ,
                                                                        "iArrowHeight" : arrowHeight
                                                                    });
        secParamCreationFlowTrigger.decrementValue.connect(onFlowTrigDecrementValue);
        secParamCreationFlowTrigger.incrementValue.connect(onFlowTrigIncrementValue);
        secParamCreationFlowTrigger.checkEndOfScaleForSpinnerWidget.connect(onFlowTrigEndOfScale);
        secParamCreationFlowTrigger.setDownArrowBgColor.connect(onSetFlowTrigDownArrowBgColor);
        secParamCreationFlowTrigger.setUpArrowBgColor.connect(onSetFlowTrigUpArrowBgColor);


        secParamCreationTinSP.decrementValue.connect(onTinspDecrementValue);
        secParamCreationTinSP.incrementValue.connect(onTinspIncrementValue);
        secParamCreationTinSP.checkEndOfScaleForSpinnerWidget.connect(onTinspEndOfScale);
        secParamCreationTinSP.setDownArrowBgColor.connect(onSetTinspDownArrowBgColor);
        secParamCreationTinSP.setUpArrowBgColor.connect(onSetTinspUpArrowBgColor);
        secParamCreationTinSP.selection.connect(onTinspSelection);


        secParamCreationPMAX.decrementValue.connect(onPmaxDecrementValue);
        secParamCreationPMAX.incrementValue.connect(onPmaxIncrementValue);
        secParamCreationPMAX.checkEndOfScaleForSpinnerWidget.connect(onPmaxEndOfScale);
        secParamCreationPMAX.setDownArrowBgColor.connect(onSetPmaxDownArrowBgColor);
        secParamCreationPMAX.setUpArrowBgColor.connect(onSetPmaxUpArrowBgColor);
        secParamCreationPMAX.selection.connect(onPmaxSelection);

        secParamCreationTrig.decrementValue.connect(onTrigWinDecrementValue);
        secParamCreationTrig.incrementValue.connect(onTrigWinIncrementValue);
        secParamCreationTrig.checkEndOfScaleForSpinnerWidget.connect(onTrigWinEndOfScale);
        secParamCreationTrig.setDownArrowBgColor.connect(onSetTrigWinDownArrowBgColor);
        secParamCreationTrig.setUpArrowBgColor.connect(onSetTrigWinUpArrowBgColor);

        secParamCreationEnd.decrementValue.connect(onEndOfBthDecrementValue);
        secParamCreationEnd.incrementValue.connect(onEndOfBthIncrementValue);
        secParamCreationEnd.checkEndOfScaleForSpinnerWidget.connect(onEndOfBthEndOfScale);
        secParamCreationEnd.setUpArrowBgColor.connect(onSetEndOfBthUpArrowBgColor);
        secParamCreationEnd.setDownArrowBgColor.connect(onSetEndOfBthDownArrowBgColor);


        secParamCreationFlowTrigger.setUniqueID("flowTrig_"+ventGrpId.selectedMode)
        secParamCreationTinSP.setUniqueID("Tinsp_"+ventGrpId.selectedMode)
        secParamCreationPMAX.setUniqueID("Pmax_"+ventGrpId.selectedMode)
        secParamCreationTrig.setUniqueID("TrigWin_"+ventGrpId.selectedMode)
        secParamCreationEnd.setUniqueID("Eob_"+ventGrpId.selectedMode)
    }
    else if ((ventGrpId.selectedMode === "SIMVPCV") && (ventGrpId.bCSBTrigger)){
        console.log("SIMV PCV bCSBTrigger")
        secParamCreationFlowTrigger=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 0,
                                                                                "strValue": settingReader.read("FlowTrigger_sKey"),
                                                                                "strSecondaryParamLabel":flowTrigLabel,
                                                                                "strSecondaryParamUnit":flowTrigUnit,
                                                                                "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                                "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                                "strMoreSettingParamColor":moreSettingParamColor,
                                                                                "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                                "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                                "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                                "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                                "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                                "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                                "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                                "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                                "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                                "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                                "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                                "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                                "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                                "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                                "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                                "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                                "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                                "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                                "iSelectedStateWidth" : selectedStateWidth,
                                                                                "iSelectedStateHeight" :selectedStateHeight,
                                                                                "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                                "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                                "strTouchedStateColor" : touchedStateColor,
                                                                                "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                                "iArrowWidth" : arrowWidth ,
                                                                                "iArrowHeight" : arrowHeight
                                                                            });
        secParamCreationTinSP=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 1,
                                                                          "strValue": settingReader.read("Tinsp_sKey"),
                                                                          "strSecondaryParamLabel":tinspLabel,
                                                                          "strSecondaryParamUnit":tinspUnit,
                                                                          "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                          "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                          "strMoreSettingParamColor":moreSettingParamColor,
                                                                          "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                          "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                          "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                          "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                          "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                          "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                          "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                          "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                          "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                          "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                          "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                          "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                          "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                          "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                          "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                          "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                          "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                          "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                          "iSelectedStateWidth" : selectedStateWidth,
                                                                          "iSelectedStateHeight" :selectedStateHeight,
                                                                          "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                          "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                          "strTouchedStateColor" : touchedStateColor,
                                                                          "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                          "iArrowWidth" : arrowWidth ,
                                                                          "iArrowHeight" : arrowHeight
                                                                      });

        secParamCreationPMAX=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 2,
                                                                         "strValue": settingReader.read("Pmax_sKey"),
                                                                         "strSecondaryParamLabel":pmaxLabel,
                                                                         "strSecondaryParamUnit":pmaxUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     });
        secParamCreationTrig=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 3,
                                                                         "strValue": settingReader.read("Trig_Window_sKey"),
                                                                         "strSecondaryParamLabel":trigWinLabel,
                                                                         "strSecondaryParamUnit":trigWinUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     });
        secParamCreationEnd=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 4,
                                                                        "strValue": settingReader.read("EndofBreath_sKey"),
                                                                        "strSecondaryParamLabel":eobLabel,
                                                                        "strSecondaryParamUnit":eobUnit,
                                                                        "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                        "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                        "strMoreSettingParamColor":moreSettingParamColor,
                                                                        "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                        "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                        "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                        "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                        "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                        "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                        "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                        "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                        "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                        "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                        "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                        "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                        "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                        "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                        "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                        "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                        "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                        "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                        "iSelectedStateWidth" : selectedStateWidth,
                                                                        "iSelectedStateHeight" :selectedStateHeight,
                                                                        "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                        "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                        "strTouchedStateColor" : touchedStateColor,
                                                                        "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                        "iArrowWidth" : arrowWidth ,
                                                                        "iArrowHeight" : arrowHeight
                                                                    }) ;

        secParamCreationExitBackup=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 5,
                                                                               "strValue": settingReader.read("ExitBackup_sKey"),
                                                                               "strSecondaryParamLabel":exitBackupLabel,
                                                                               "strSecondaryParamUnit":exitBackupUnit,
                                                                               "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                               "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                               "strMoreSettingParamColor":moreSettingParamColor,
                                                                               "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                               "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                               "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                               "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                               "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                               "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                               "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                               "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                               "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                               "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                               "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                               "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                               "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                               "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                               "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                               "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                               "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                               "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                               "iSelectedStateWidth" : selectedStateWidth,
                                                                               "iSelectedStateHeight" :selectedStateHeight,
                                                                               "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                               "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                               "strTouchedStateColor" : touchedStateColor,
                                                                               "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                               "iArrowWidth" : arrowWidth ,
                                                                               "iArrowHeight" : arrowHeight
                                                                           });

        secParamCreationFlowTrigger.decrementValue.connect(onFlowTrigDecrementValue);
        secParamCreationFlowTrigger.incrementValue.connect(onFlowTrigIncrementValue);
        secParamCreationFlowTrigger.checkEndOfScaleForSpinnerWidget.connect(onFlowTrigEndOfScale);
        secParamCreationFlowTrigger.setDownArrowBgColor.connect(onSetFlowTrigDownArrowBgColor);
        secParamCreationFlowTrigger.setUpArrowBgColor.connect(onSetFlowTrigUpArrowBgColor);

        secParamCreationTinSP.decrementValue.connect(onTinspDecrementValue);
        secParamCreationTinSP.incrementValue.connect(onTinspIncrementValue);
        secParamCreationTinSP.checkEndOfScaleForSpinnerWidget.connect(onTinspEndOfScale);
        secParamCreationTinSP.setDownArrowBgColor.connect(onSetTinspDownArrowBgColor);
        secParamCreationTinSP.setUpArrowBgColor.connect(onSetTinspUpArrowBgColor);
        secParamCreationTinSP.selection.connect(onTinspSelection);


        secParamCreationPMAX.decrementValue.connect(onPmaxDecrementValue);
        secParamCreationPMAX.incrementValue.connect(onPmaxIncrementValue);
        secParamCreationPMAX.checkEndOfScaleForSpinnerWidget.connect(onPmaxEndOfScale);
        secParamCreationPMAX.setDownArrowBgColor.connect(onSetPmaxDownArrowBgColor);
        secParamCreationPMAX.setUpArrowBgColor.connect(onSetPmaxUpArrowBgColor);
        secParamCreationPMAX.selection.connect(onPmaxSelection);

        secParamCreationTrig.decrementValue.connect(onTrigWinDecrementValue);
        secParamCreationTrig.incrementValue.connect(onTrigWinIncrementValue);
        secParamCreationTrig.checkEndOfScaleForSpinnerWidget.connect(onTrigWinEndOfScale);
        secParamCreationTrig.setDownArrowBgColor.connect(onSetTrigWinDownArrowBgColor);
        secParamCreationTrig.setUpArrowBgColor.connect(onSetTrigWinUpArrowBgColor);

        secParamCreationEnd.decrementValue.connect(onEndOfBthDecrementValue);
        secParamCreationEnd.incrementValue.connect(onEndOfBthIncrementValue);
        secParamCreationEnd.checkEndOfScaleForSpinnerWidget.connect(onEndOfBthEndOfScale);
        secParamCreationEnd.setUpArrowBgColor.connect(onSetEndOfBthUpArrowBgColor);
        secParamCreationEnd.setDownArrowBgColor.connect(onSetEndOfBthDownArrowBgColor);

        secParamCreationExitBackup.decrementValue.connect(onExitBackupDecrementValue);
        secParamCreationExitBackup.incrementValue.connect(onExitBackupIncrementValue);
        secParamCreationExitBackup.checkEndOfScaleForSpinnerWidget.connect(onExitBackupEndOfScale);
        secParamCreationExitBackup.setDownArrowBgColor.connect(onSetExitBackupDownArrowBgColor);
        secParamCreationExitBackup.setUpArrowBgColor.connect(onSetExitBackupUpArrowBgColor);



        secParamCreationFlowTrigger.setUniqueID("flowTrig_"+ventGrpId.selectedMode)
        secParamCreationTinSP.setUniqueID("Tinsp_"+ventGrpId.selectedMode)
        secParamCreationPMAX.setUniqueID("Pmax_"+ventGrpId.selectedMode)
        secParamCreationTrig.setUniqueID("TrigWin_"+ventGrpId.selectedMode)
        secParamCreationEnd.setUniqueID("Eob_"+ventGrpId.selectedMode)
        secParamCreationExitBackup.setUniqueID("ExitBk_"+ventGrpId.selectedMode)
    }
    else if(ventGrpId.selectedMode === "PSVPro"){

        secParamCreationFlowTrigger=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 0,
                                                                                "strValue": settingReader.read("FlowTrigger_sKey"),
                                                                                "strSecondaryParamLabel":flowTrigLabel,
                                                                                "strSecondaryParamUnit":flowTrigUnit,
                                                                                "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                                "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                                "strMoreSettingParamColor":moreSettingParamColor,
                                                                                "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                                "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                                "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                                "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                                "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                                "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                                "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                                "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                                "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                                "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                                "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                                "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                                "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                                "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                                "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                                "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                                "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                                "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                                "iSelectedStateWidth" : selectedStateWidth,
                                                                                "iSelectedStateHeight" :selectedStateHeight,
                                                                                "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                                "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                                "strTouchedStateColor" : touchedStateColor,
                                                                                "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                                "iArrowWidth" : arrowWidth ,
                                                                                "iArrowHeight" : arrowHeight
                                                                            });
        secParamCreationTrig=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 1,
                                                                         "strValue": settingReader.read("Trig_Window_sKey"),
                                                                         "strSecondaryParamLabel":trigWinLabel,
                                                                         "strSecondaryParamUnit":trigWinUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     });
        secParamCreationEnd=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 2,
                                                                        "strValue": settingReader.read("EndofBreath_sKey"),
                                                                        "strSecondaryParamLabel":eobLabel,
                                                                        "strSecondaryParamUnit":eobUnit,
                                                                        "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                        "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                        "strMoreSettingParamColor":moreSettingParamColor,
                                                                        "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                        "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                        "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                        "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                        "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                        "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                        "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                        "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                        "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                        "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                        "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                        "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                        "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                        "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                        "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                        "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                        "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                        "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                        "iSelectedStateWidth" : selectedStateWidth,
                                                                        "iSelectedStateHeight" :selectedStateHeight,
                                                                        "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                        "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                        "strTouchedStateColor" : touchedStateColor,
                                                                        "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                        "iArrowWidth" : arrowWidth ,
                                                                        "iArrowHeight" : arrowHeight
                                                                    });
        secParamCreationPMAX=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 3,
                                                                         "strValue": settingReader.read("Pmax_sKey"),
                                                                         "strSecondaryParamLabel":pmaxLabel,
                                                                         "strSecondaryParamUnit":pmaxUnit,
                                                                         "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                         "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                         "strMoreSettingParamColor":moreSettingParamColor,
                                                                         "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                         "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                         "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                         "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                         "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                         "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                         "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                         "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                         "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                         "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                         "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                         "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                         "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                         "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                         "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                         "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                         "iSelectedStateWidth" : selectedStateWidth,
                                                                         "iSelectedStateHeight" :selectedStateHeight,
                                                                         "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                         "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                         "strTouchedStateColor" : touchedStateColor,
                                                                         "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                         "iArrowWidth" : arrowWidth ,
                                                                         "iArrowHeight" : arrowHeight
                                                                     });
        secParamCreationBackupTime=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 4,
                                                                               "strValue": settingReader.read("BackupTime_sKey"),
                                                                               "strSecondaryParamLabel":backupTimeLabel,
                                                                               "strSecondaryParamUnit":backupTimeUnit,
                                                                               "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                               "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                               "strMoreSettingParamColor":moreSettingParamColor,
                                                                               "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                               "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                               "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                               "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                               "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                               "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                               "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                               "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                               "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                               "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                               "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                               "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                               "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                               "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                               "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                               "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                               "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                               "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                               "iSelectedStateWidth" : selectedStateWidth,
                                                                               "iSelectedStateHeight" :selectedStateHeight,
                                                                               "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                               "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                               "strTouchedStateColor" : touchedStateColor,
                                                                               "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                               "iArrowWidth" : arrowWidth ,
                                                                               "iArrowHeight" : arrowHeight
                                                                           });
        secParamCreationTinSP=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 5,
                                                                          "strValue": settingReader.read("Tinsp_sKey"),
                                                                          "strSecondaryParamLabel":tinspLabel,
                                                                          "strSecondaryParamUnit":tinspUnit,
                                                                          "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                          "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                          "strMoreSettingParamColor":moreSettingParamColor,
                                                                          "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                          "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                          "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                          "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                          "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                          "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                          "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                          "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                          "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                          "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                          "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                          "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                          "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                          "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                          "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                          "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                          "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                          "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                          "iSelectedStateWidth" : selectedStateWidth,
                                                                          "iSelectedStateHeight" :selectedStateHeight,
                                                                          "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                          "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                          "strTouchedStateColor" : touchedStateColor,
                                                                          "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                          "iArrowWidth" : arrowWidth ,
                                                                          "iArrowHeight" : arrowHeight
                                                                      });
        secParamCreationExitBackup=moreMenuSecondaryComponent.createObject(moreMenuId.secParamParentId,{"iCompID": 6,
                                                                               "strValue": settingReader.read("ExitBackup_sKey"),
                                                                               "strSecondaryParamLabel":exitBackupLabel,
                                                                               "strSecondaryParamUnit":exitBackupUnit,
                                                                               "iMoreSettingParamWidth":moreSettingParamWidth,
                                                                               "iMoreSettingParamHeight":moreSettingParamHeight,
                                                                               "strMoreSettingParamColor":moreSettingParamColor,
                                                                               "iUnitlabelWidth":moreSettingParamLabelWidth,"iUnitlabelHeight":moreSettingParamLabelHeight,
                                                                               "iValueBoxWidth":moreSettingParamLabelValueWidth,"iValueBoxHeight" : moreSettingParamLabelValueHeight,
                                                                               "strValueBoxBorderColor": moreSettingParamBorderColor,"iValueBoxBorderWidth" : moreSettingParamBorderWidth,
                                                                               "iValueBoxRadius" : moreSettingParamBorderRadius,"strValueBoxColor" :moreStringParamLabelValueColor,
                                                                               "iMoreSettingParamLabelPixelSize" : moreStringParamLabelPixelSize,
                                                                               "strValueBoxColor" :moreStringParamLabelValueColor,
                                                                               "strMoreSettingParamActiveStateColor":moreSettingParamActiveStateColor,
                                                                               "strMoreSettingParamDisabledStateColor":moreSettingParamDisabledStateColor,
                                                                               "strMoreSettingParamEndOfScaleStateColor":moreSettingParamEndOfScaleStateColor,
                                                                               "strMoreSettingParamEndOfScaleStateBorderColor" : moreSettingParamEndOfScaleStateBorderColor,
                                                                               "strMoreSettingParamScrollStateBorderColor" : moreSettingParamScrollStateBorderColor,
                                                                               "strMoreSettingParamSelectedStateColor" : moreSettingParamSelectedStateColor,
                                                                               "strMoreSettingParamSelectedStateBorderColor" : moreSettingParamSelectedStateBorderColor,
                                                                               "strMoreSettingParamTouchedStateColor" : moreSettingParamTouchedStateColor,
                                                                               "strMoreSettingParamSpinnerUpEnabled" : moreSettingParamSpinnerUpEnabled,
                                                                               "strMoreSettingParamSpinnerUpDisabled" : moreSettingParamSpinnerUpDisabled,
                                                                               "strMoreSettingParamSpinnerDownEnabled" : moreSettingParamSpinnerDownEnabled,
                                                                               "strMoreSettingParamSpinnerDownDisabled" : moreSettingParamSpinnerDownDisabled,
                                                                               "iSelectedStateWidth" : selectedStateWidth,
                                                                               "iSelectedStateHeight" :selectedStateHeight,
                                                                               "iSelectedStateArrowWidth" : selectedStateArrowWidth ,
                                                                               "iSelectedStateArrowHeight" : selectedStateArrowHeight,
                                                                               "strTouchedStateColor" : touchedStateColor,
                                                                               "strSelectedStateArrowColor" : selectedStateArrowColor ,
                                                                               "iArrowWidth" : arrowWidth ,
                                                                               "iArrowHeight" : arrowHeight
                                                                           });
        secParamCreationFlowTrigger.decrementValue.connect(onFlowTrigDecrementValue);
        secParamCreationFlowTrigger.incrementValue.connect(onFlowTrigIncrementValue);
        secParamCreationFlowTrigger.checkEndOfScaleForSpinnerWidget.connect(onFlowTrigEndOfScale);
        secParamCreationFlowTrigger.setDownArrowBgColor.connect(onSetFlowTrigDownArrowBgColor);
        secParamCreationFlowTrigger.setUpArrowBgColor.connect(onSetFlowTrigUpArrowBgColor);

        secParamCreationTrig.decrementValue.connect(onTrigWinDecrementValue);
        secParamCreationTrig.incrementValue.connect(onTrigWinIncrementValue);
        secParamCreationTrig.checkEndOfScaleForSpinnerWidget.connect(onTrigWinEndOfScale);
        secParamCreationTrig.setDownArrowBgColor.connect(onSetTrigWinDownArrowBgColor);
        secParamCreationTrig.setUpArrowBgColor.connect(onSetTrigWinUpArrowBgColor);

        secParamCreationEnd.decrementValue.connect(onEndOfBthDecrementValue);
        secParamCreationEnd.incrementValue.connect(onEndOfBthIncrementValue);
        secParamCreationEnd.checkEndOfScaleForSpinnerWidget.connect(onEndOfBthEndOfScale);
        secParamCreationEnd.setUpArrowBgColor.connect(onSetEndOfBthUpArrowBgColor);
        secParamCreationEnd.setDownArrowBgColor.connect(onSetEndOfBthDownArrowBgColor);

        secParamCreationPMAX.decrementValue.connect(onPmaxDecrementValue);
        secParamCreationPMAX.incrementValue.connect(onPmaxIncrementValue);
        secParamCreationPMAX.checkEndOfScaleForSpinnerWidget.connect(onPmaxEndOfScale);
        secParamCreationPMAX.setDownArrowBgColor.connect(onSetPmaxDownArrowBgColor);
        secParamCreationPMAX.setUpArrowBgColor.connect(onSetPmaxUpArrowBgColor);
        secParamCreationPMAX.selection.connect(onPmaxSelection);

        secParamCreationBackupTime.decrementValue.connect(onBackupTimeDecrementValue);
        secParamCreationBackupTime.incrementValue.connect(onBackupTimeIncrementValue);
        secParamCreationBackupTime.checkEndOfScaleForSpinnerWidget.connect(onBackupTimeEndOfScale);
        secParamCreationBackupTime.setDownArrowBgColor.connect(onSetBackupTimeDownArrowBgColor);
        secParamCreationBackupTime.setUpArrowBgColor.connect(onSetBackupTimeUpArrowBgColor);

        secParamCreationTinSP.decrementValue.connect(onTinspDecrementValue);
        secParamCreationTinSP.incrementValue.connect(onTinspIncrementValue);
        secParamCreationTinSP.checkEndOfScaleForSpinnerWidget.connect(onTinspEndOfScale);
        secParamCreationTinSP.setDownArrowBgColor.connect(onSetTinspDownArrowBgColor);
        secParamCreationTinSP.setUpArrowBgColor.connect(onSetTinspUpArrowBgColor);
        secParamCreationTinSP.selection.connect(onTinspSelection);

        secParamCreationExitBackup.decrementValue.connect(onExitBackupDecrementValue);
        secParamCreationExitBackup.incrementValue.connect(onExitBackupIncrementValue);
        secParamCreationExitBackup.checkEndOfScaleForSpinnerWidget.connect(onExitBackupEndOfScale);
        secParamCreationExitBackup.setDownArrowBgColor.connect(onSetExitBackupDownArrowBgColor);
        secParamCreationExitBackup.setUpArrowBgColor.connect(onSetExitBackupUpArrowBgColor);


        secParamCreationFlowTrigger.setUniqueID("flowTrig_"+ventGrpId.selectedMode)
        secParamCreationTinSP.setUniqueID("Tinsp_"+ventGrpId.selectedMode)
        secParamCreationPMAX.setUniqueID("Pmax_"+ventGrpId.selectedMode)
        secParamCreationTrig.setUniqueID("TrigWin_"+ventGrpId.selectedMode)
        secParamCreationEnd.setUniqueID("Eob_"+ventGrpId.selectedMode)
        secParamCreationExitBackup.setUniqueID("ExitBk_"+ventGrpId.selectedMode)
        secParamCreationBackupTime.setUniqueID("BkTime_"+ventGrpId.selectedMode)
    }
}

//var prevValue;
function onVentSecParamEnterPressed(objectName) {
    console.log("enter pressed!!!")
    var settingMenuObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    var currentSecParam = getCompRef(settingMenuObj.secParamParentId, objectName)
    currentSecParam.prevValue = currentSecParam.strValue
    console.log("prev val" + currentSecParam.strValue)
    if(currentSecParam.state === "Active" || currentSecParam.state === "" || currentSecParam.state === "Scroll"){
        settingsBtnChangeState(currentSecParam,"Selected")
    } else {
        settingsBtnChangeState(currentSecParam,"Scroll")
        var newValue = currentSecParam.strValue;
        var labelid = currentSecParam.strSecondaryParamLabel;
        if (labelid === "Pmax_sKey") {
            settingReader.writeDataInFile("Ppeak_High", newValue)
            ObjCCValue.updateConstraintParam("Pmax_sKey",newValue)
        }
        if(labelid === "Tinsp_sKey") {
            ObjCCValue.updateConstraintParam("Tinsp_sKey",newValue)
        }
        if(labelid === "Tpause_sKey") {
            ObjCCValue.updateConstraintParam("Tpause_sKey",newValue)
        }

        settingReader.writeDataInFile(labelid, newValue)
        backupData.storeCriticalParams(labelid, newValue);
        objCSBValue.writeToCSB(labelid,newValue,"Secondary")
    }
    settingMenuObj.bTimerRunning = true
    settingMenuObj.restartTimer()
    console.log("timer from enter pressed")
}

function onSecParamTouched(objectName) {
    console.log("Touched State !!!!!")
    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("PrevValue !!!!",currentSecParam.prevValue)
    var isbtnselected = "false"
    // selectedComp.prevValue = selectedComp.strValue;
    console.log("onCompTouched....state is : " +currentSecParam.state +currentSecParam.iCompID)
    var length = secParamParentId.children.length;
    for(var i=0;i<length;i++){
        var currentComp = getCompRefByIndex(secParamParentId,i)
        if ((currentComp.state === "Selected" ||
             currentComp.state === "End of Scale") && (currentSecParam.iCompID !== i)) {
            isbtnselected = "true"
            currentComp.strValue = currentComp.prevValue;
            onSelectOtherKey(currentComp,currentSecParam)
        }
    }

    if(currentSecParam.state!=="Selected" && currentSecParam.state!=="End of Scale"){
        currentSecParam.focus =true
        settingsBtnChangeState(currentSecParam,"Touched")
    }
    var timeObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    timeObj.bTimerRunning = true
    timeObj.stopTimer()
    console.log("timer from touched comp")
}

function onSecParamSelected(objectName) {
    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("Selected State !!!" + currentSecParam.state)

    currentSecParam.prevValue = currentSecParam.strValue
    if(currentSecParam.state === "Touched"){
        settingsBtnChangeState(currentSecParam,"Selected")
    } else if(currentSecParam.state === "Selected" || currentSecParam.state === "End of Scale"){
        var newValue = currentSecParam.strValue;
        var labelid = currentSecParam.strSecondaryParamLabel;
        if (labelid === "Pmax_sKey") {
            console.log("writing Pmax into setting file!!!")
            settingReader.writeDataInFile("Ppeak_High", newValue)
            ObjCCValue.updateConstraintParam("Pmax_sKey",newValue)
        }
        if(labelid === "Tinsp_sKey") {
            ObjCCValue.updateConstraintParam("Tinsp_sKey",newValue)
        }
        if(labelid === "Tpause_sKey") {
            ObjCCValue.updateConstraintParam("Tpause_sKey",newValue)
        }
        settingReader.writeDataInFile(labelid, newValue)
        backupData.storeCriticalParams(labelid, newValue);
        objCSBValue.writeToCSB(labelid,newValue,"Secondary")
        console.log("the state now is" + currentSecParam.state)
        settingsBtnChangeState(currentSecParam,"Scroll")
    }
    var timeObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    timeObj.bTimerRunning = true
    timeObj.restartTimer()
    console.log("timer from param selected")

}

function settingsBtnChangeState(currentSecParam,stateName) {
    console.log("state changed to !!!!", stateName)
    var settingMenuObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    currentSecParam.state = stateName
    for(var i = 0; i < settingMenuObj.secParamParentId.children.length;i++){
        var siblingObj = getCompRef(settingMenuObj.secParamParentId, settingMenuObj.secParamParentId.children[i].objectName)
        if(siblingObj.state !== "Disabled"){
            if(siblingObj.state !== "Active" && i !== currentSecParam.iCompID){
                console.log("changing state of object"+siblingObj+ " " + siblingObj.state)
                siblingObj.state = "Active"
            }
        }
    }
}

function destroyMoreSettingMenu() {
    var ventGrpObj = getGroupRef("VentGroup")
    ventGrpObj.bIsSettingsMenuCreated = false
    var menuObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    menuObj.stopTimer()
    menuObj.destroy()
    var settingsBtnRef = getCompRef(ventGrpObj.ventParentId,"MoreSettingBtn")
    //settingsBtnRef.focus = true
    settingsBtnRef.state = settingsBtnRef.strVentPreviousState
    if(settingsBtnRef.strVentPreviousState === "Active" || settingsBtnRef.strVentPreviousState === "Preset" ){
        ventGrpObj.iPreviousIndex = settingsBtnRef.iCompID
        ventGrpObj.forceActiveFocus()
        ventGrpObj.focus = true
    } else {
       settingsBtnRef.focus = true
    }
}

function closeMenu() {
    console.log("close function called")
    var currentSecParam = getCompRef(secParamParentId, objectName)
    var length = secParamParentId.children.length;
    for(var i=0;i<length;i++){
        var currentComp = getCompRefByIndex(secParamParentId, i)
        if (currentComp.state === "Selected" ||
                currentComp.state === "End of Scale") {
        }
        currentComp.state = "Active"
    }
    moreMenuId.visible =false
    moreSettingId.focus = true
}

function onNavigateRightInSettingsMenu() {

    var ventGrpObj = getGroupRef("VentGroup")
    var menuObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    var closeComp = getCompRef(menuObj, "MoreSettingMenuClose")
    var settingsBtnRef = getCompRef(ventGrpObj.ventParentId,"MoreSettingBtn")
    closeComp.focus = true
    settingsBtnRef.resetScrollActiveTimer()
}

function moveClockwise(currentObjectName) {

    var settingsObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    var length = settingsObj.secParamParentId.children.length ;
    var currentIndex = 0;
    var currentComp = getCompRef(settingsObj.secParamParentId, currentObjectName)
    for(var i =0; i< length;i++){
        if(settingsObj.secParamParentId.children[i].objectName === currentObjectName) {
            break;
        } else {
            currentIndex++;
        }
    }
    if (currentIndex === length - 1 ) {
        console.log("length is length " + length + " "+currentIndex)
        onNavigateRightInSettingsMenu()
    } else {
        settingsObj.secParamParentId.children[(currentIndex + 1)%length].focus = true
        settingsObj.secParamParentId.children[(currentIndex + 1)%length].forceActiveFocus()
    }
    settingsObj.bTimerRunning = true
    settingsObj.restartTimer()
    console.log("timer from navigate right")
}

function moveAntiClockwise(currentObjectName) {

    var settingsObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    var length = settingsObj.secParamParentId.children.length;
    var currentIndex = 0;
    var currentComp = getCompRef(settingsObj.secParamParentId, currentObjectName)

    for(var i =0; i< length;i++){
        if(settingsObj.secParamParentId.children[i].objectName === currentObjectName){
            break;
        }else{
            currentIndex++;
        }
    }
    if (currentIndex === 0 ){
        onNavigateRightInSettingsMenu()
    }  else{
        settingsObj.secParamParentId.children[(currentIndex - 1)].focus = true
        settingsObj.secParamParentId.children[(currentIndex - 1)].forceActiveFocus()
    }
    settingsObj.bTimerRunning = true
    settingsObj.restartTimer()
    console.log("timer from navigate left")
}

function onSelectOtherKey(currentComp,SelectedComp) {

    /*  When the quick key is in selected state, when the user touches any other key ,
      set the customized border of previous selected indexed button invisible */
    if( currentComp.strType === "quickKeys" ) {
        currentComp.stopBlinking()
        SelectedComp.focus = true
        SelectedComp.forceActiveFocus()
        settingsBtnChangeState(SelectedComp,"Touched")
    }
}

function restartMenuTimeoutTimer() {

    var timeObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    timeObj.bTimerRunning = true
    timeObj.restartTimer()
}

