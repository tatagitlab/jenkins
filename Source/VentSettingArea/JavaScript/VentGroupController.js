Qt.include("qrc:/Source/Generic/JavaScript/MasterController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/TVController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/RRController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/RRMechController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/PEEPController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/IEController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/PinspController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/PsupportController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/VentModeController.js")
Qt.include("qrc:/Source/VentSettingArea/JavaScript/MoreSettingsController.js")

var statusComp;
var quickKeyComp;
var moreSettingComp;
var ventModeBtn;
var qKeyWidget1;
var qKeyWidget2;
var qKeyWidget3;
var qKeyWidget4;
var moreSettingbtn;
var grpName = "VentGroup";
var touchedGroupName ;
var isModeChange = false ;
var isModeMenuCreatedUsingK = false    // To handle case where menu is created using "K" press
var isSettingMenuCreatedUsingK = false
var isKeysPressedInSequence = false  //This is make sure that "2" is pressed first,so that 215/216 is in sequence
var sequenceList215 = [2,1,5]
var sequenceList216 = [2,1,6]
var sequenceList13 = [1,3]

//VentModeButton property
var ventGrpBtnState = "Active"
var ventState = "Vent"
var ventStatus = component.getComponentResourceString("Component","VentStatus","Status","ON")
var ventModeWidth =  parseInt(component.getComponentDimension("VentStatus", "width"))
var ventModeHeight =  parseInt(component.getComponentDimension("VentStatus", "height"))
var ventModeBorderWidth =  parseInt(component.getComponentXmlDataByTag("Component","VentStatus","BorderWidth"))
var ventStatusTextFontFamily = geFont.geFontStyle()
var ventStatusTextFontSize  = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "StatusTextPixelSize"))
var ventStatusTextFontColor = component.getComponentXmlDataByTag("Component","VentStatus", "StatusTextFontColor")
var ventStatusiconWidth = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "StatusIconWidth"))
var ventStatusiconHeight = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "StatusIconHeight"))
var ventModeBtnWidth = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "ModeWidth"))
var ventModeBtnHeight = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "ModeHeight"))
var ventModeBtnBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "ModeBorderWidth"))
var ventModeBtnRadius = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "ModeBorderRadius"))
var ventlabelFontFamily = geFont.geFontStyle()
var ventlabelFontPixelSize = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "ModePixelSize"))
var ventModeValueFontColor = component.getComponentXmlDataByTag("Component","VentStatus", "ModeValueFontColor")
var ventText1FontFamily = geFont.geFontStyle()
var ventText1FontPixelSize = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "ModeValuePixelSize"))
var ventModeBorderColor = component.getComponentXmlDataByTag("Component","VentStatus","BorderColor")
var ventModeColor = component.getComponentXmlDataByTag("Component","VentStatus", "StatusBackColor")
var ventModeBtnBorderColor = component.getComponentXmlDataByTag("Component","VentStatus", "ModeBorderColor")
var ventModeBtnColor = component.getComponentXmlDataByTag("Component","VentStatus", "Color")
var ventActiveStateColor = component.getComponentXmlDataByTag("Component","VentStatus", "Color")
var ventDisabledStateColor = component.getComponentXmlDataByTag("Component","VentStatus", "DisabledStateColor")
var ventDisabledStateFontColor = component.getComponentXmlDataByTag("Component","VentStatus", "DisabledStateFontColor")
var ventEndOfScaleStateColor = component.getComponentXmlDataByTag("Component","VentStatus", "EndOfScaleStateColor")
var ventEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("Component","VentStatus", "EndOfScaleStateBorderColor")
var ventScrollStateBorderColor = component.getComponentXmlDataByTag("Component","VentStatus", "ScrollStateBorderColor")
var ventSelectedStateColor = component.getComponentXmlDataByTag("Component","VentStatus", "SelectedStateColor")
var ventSelectedStateBorderColor = component.getComponentXmlDataByTag("Component","VentStatus", "SelectedStateBorderColor")
var ventTouchedStateColor = component.getComponentXmlDataByTag("Component","VentStatus", "TouchedStateColor")
var ventStateHeightChange = parseInt(component.getComponentXmlDataByTag("Component","VentStatus", "StateHeightChange"))
var ventIconOn = component.getComponentXmlDataByTag("Component","VentStatus", "VentIconOn")
var ventIconOff = component.getComponentXmlDataByTag("Component","VentStatus", "VentIconOff")
var ventArrowIcon = component.getComponentXmlDataByTag("Component","VentStatus", "VentArrowIcon")
var ventArrowDisableIcon = component.getComponentXmlDataByTag("Component","VentStatus", "VentArrowDisableIcon")
var ventArrowPresetIcon = component.getComponentXmlDataByTag("Component","VentStatus", "VentArrowPresetIcon")
//QuickKeys property
var quickKeysWidth = parseInt(component.getComponentDimension("QuickKey", "width"))
var quickKeysHeight = parseInt(component.getComponentDimension("QuickKey", "height"))
var quickKeysColor = component.getComponentXmlDataByTag("Component","QuickKey", "Color")
var quickKeysControlFontFamily = geFont.geFontStyle()//component.getComponentXmlDataByTag("QuickKeyButton", "VentControlFontFamily")
var quickKeysControlPixelSize = parseInt(component.getComponentXmlDataByTag("Component","QuickKey", "VentControlPixelSize"))
var quickKeysControlValueFontFamily = geFont.geFontStyle()//component.getComponentXmlDataByTag("QuickKeyButton", "VentControlValueFontFamily")
var quickKeysControlValuePixelSize = parseInt(component.getComponentXmlDataByTag("Component","QuickKey", "VentControlValuePixelSize"))
var quickKeysUnitFontFamily = geFont.geFontStyle()//component.getComponentXmlDataByTag("QuickKeyButton", "UnitFontFamily")
var quickKeysUnitPixelSize = parseInt(component.getComponentXmlDataByTag("Component","QuickKey", "UnitPixelSize"))
var quickkeyActiveStateColor = component.getComponentXmlDataByTag("Component","QuickKey", "Color")
var quickkeyDisabledStateColor = component.getComponentXmlDataByTag("Component","QuickKey", "DisabledStateColor")
var quickkeyDisabledStateFontColor = component.getComponentXmlDataByTag("Component","QuickKey", "DisabledStateFontColor")
var quickkeyEndOfScaleStateColor = component.getComponentXmlDataByTag("Component","QuickKey", "EndOfScaleStateColor")
var quickkeyEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("Component","QuickKey", "EndOfScaleStateBorderColor")
var quickkeyScrollStateBorderColor = component.getComponentXmlDataByTag("Component","QuickKey", "ScrollStateBorderColor")
var quickkeySelectedStateColor = component.getComponentXmlDataByTag("Component","QuickKey", "SelectedStateColor")
var quickkeySelectedStateBorderColor = component.getComponentXmlDataByTag("Component","QuickKey", "SelectedStateBorderColor")
var quickkeyTouchedStateColor = component.getComponentXmlDataByTag("Component","QuickKey", "TouchedStateColor")
var quickkeyStateHeightChange = parseInt(component.getComponentXmlDataByTag("Component","QuickKey", "StateHeightChange"))
var quickkeySpinnerUpEnabled = component.getComponentXmlDataByTag("Component","QuickKey", "QkSpinnerUpEnabled")
var quickkeySpinnerUpDisabled = component.getComponentXmlDataByTag("Component","QuickKey", "QkSpinnerUpDisabled")
var quickkeySpinnerDownDisabled = component.getComponentXmlDataByTag("Component","QuickKey", "QkSpinnerDownDisabled")
var quickkeySpinnerDownEnabled = component.getComponentXmlDataByTag("Component","QuickKey", "QkSpinnerDownEnabled")

//For quick key spinner
var quickeySpinnerArrowBtnHeight = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerArrowBtnHeight")
var quickeySpinnerArrowBtnWidth = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerArrowBtnWidth")
var quickkeySpinnerIconHeight = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerIconHeight")
var quickkeySpinnerIconWidth =  component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerIconWidth")
var quickkeySpinnerAreaHeight =  component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerAreaHeight")
var quickkeySpinnerAreaWidth  = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerAreaWidth")
var quickkeySpinnerSelStateColor = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerSelStateColor")
var quickkeySpinnerTchStateColor = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerTchStateColor")
var quickkeySpinnerScrlStateBorderColor = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerScrlStateBorderColor")
var quickkeySpinnerDisStateColor = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerDisStateColor")
var quickkeySpinnerSelStateBorderColor = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerSelStateBorderColor")
var quickkeySpinnerEoSStateColor = component.getComponentXmlDataByTag("Component","QuickKey","QkSpinnerEoSStateColor")

//QKSettingConfirmPopUp
var settingPopUpWidth = parseInt(component.getComponentDimension("QuickKeySettingConfirmPopUp", "width"))
var settingPopUpHeight = parseInt(component.getComponentDimension("QuickKeySettingConfirmPopUp", "height"))
var settingPopUpColor = component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "Color")
var popUpBtnHeight = parseInt(component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "PopUpBtnHeight"))
var popUpBtnWidth = parseInt(component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "PopUpBtnWidth"))
var popUpBtnColor = component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "PopUpBtnColor")
var popUpBtnRadius = parseInt(component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "PopUpBtnRadius"))
var popUpBtnMargin = parseInt(component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "PopUpBtnMargin"))
var settingConfirmLabel = component.getComponentResourceString("Component","QuickKeySettingConfirmPopUp", "SettingConfirmLabel","Confirm")
var settingBtnFontColor = component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "SettingBtnFontColor")
var settingBtnValuePixelSize = parseInt(component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "SettingBtnValuePixelSize"))
var settingCancelLabel = component.getComponentResourceString("Component","QuickKeySettingConfirmPopUp", "SettingCancelLabel","Cancel")
var popUpTriHeight = parseInt(component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "PopUpTriHeight"))
var popUpTriWidth = parseInt(component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "PopUpTriWidth"))
var popUpTriIconSource = component.getComponentXmlDataByTag("Component","QuickKeySettingConfirmPopUp", "PopUpTriIconSource")

//VentMoreSetting property
var moreLabel = component.getComponentResourceString("Component","MoreSettingBtn","Label","default")
var ventMoreSettingWidth = parseInt(component.getComponentDimension("MoreSettingBtn", "width"))
var ventMoreSettingHeight = parseInt(component.getComponentDimension("MoreSettingBtn", "height"))
var ventMoreSettingBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","MoreSettingBtn", "BorderWidth"))
var ventMoreSettingRadius = parseInt(component.getComponentXmlDataByTag("Component","MoreSettingBtn", "BorderRadius"))
var ventMoreSettingText1FontFamily = geFont.geFontStyle() //component.getComponentXmlDataByTag("MoreSetting", "LabelFontFamily")
var ventMoreSettingText1FontPixelSize = parseInt(component.getComponentXmlDataByTag("Component","MoreSettingBtn", "LabelPixelSize"))
var ventMoreSettingBorderColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "BorderColor")
var ventMoreSettingColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "Color")
var moreSettingActiveStateColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "Color")
var moreSettingDisabledStateColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "DisabledStateColor")
var moreSettingEndOfScaleStateColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "EndOfScaleStateColor")
var moreSettingEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "EndOfScaleStateBorderColor")
var moreSettingScrollStateBorderColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "ScrollStateBorderColor")
var moreSettingSelectedStateColor= component.getComponentXmlDataByTag("Component","MoreSettingBtn", "SelectedStateColor")
var moreSettingSelectedStateBorderColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "SelectedStateBorderColor")
var moreSettingTouchedStateColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "TouchedStateColor")
var moreSettingStateHeightChange = parseInt(component.getComponentXmlDataByTag("Component","MoreSettingBtn", "StateHeightChange"))
var moreDisabledStateFontColor = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "DisabledStateFontColor")
var moreIconDiabled = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "MoreIconDiabled")
var moreIconEnabled = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "MoreIconEnabled")
var moreIconPreset  = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "MoreIconPreset")
var moreIconTouched = component.getComponentXmlDataByTag("Component","MoreSettingBtn", "MoreIconTouched")

/*vent mode Confirmation popup*/
var popupWidth = parseInt(component.getComponentDimension("ModeConfirmPopup", "width"))
var popupHeight = parseInt(component.getComponentDimension("ModeConfirmPopup", "height"))
var popupRadius = parseInt(component.getComponentXmlDataByTag("Component","ModeConfirmPopup", "BorderRadius"))
var popupBorderColor =component.getComponentXmlDataByTag("Component","ModeConfirmPopup", "BorderColor")
var popupColor = component.getComponentXmlDataByTag("Component","ModeConfirmPopup", "Color")
var popupLabelFontFamily = geFont.geFontStyle() //component.getComponentXmlDataByTag("ModeConfirmPopup", "LabelFontFamily")
var popupTipTriangleSource = component.getComponentXmlDataByTag("Component","ModeConfirmPopup", "PopUpTriIconSource")
var popupCancelLabel = component.getComponentResourceString("Component","ModeConfirmPopup", "PopupCancelLabel","Cancel")
var popupCancelBorderColor = component.getComponentXmlDataByTag("Component","ModeConfirmPopup", "CancelBtnColor")
var popupLabelPixelSize = parseInt(component.getComponentXmlDataByTag("Component","ModeConfirmPopup", "LabelPixelSize"))
var popupFontColor = component.getComponentXmlDataByTag("Component","ModeConfirmPopup", "labelFontColor")


//creates the VentModeBtn, VentBtn, VentMoreSeetingBtn components

function createComponent() {
    console.log("inside Create component")
    statusComp = Qt.createComponent("qrc:Source/VentSettingArea/Qml/VentModeBtn.qml");
    quickKeyComp = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentBtn.qml")
    moreSettingComp = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentMoreSettingBtn.qml")
    if(statusComp.status === Component.Ready && quickKeyComp.status ===Component.Ready && moreSettingComp.status===Component.Ready){
        finishComponentCreation();
    }
    else
    {
        statusComp.statusChanged.connect(finishComponentCreation);
        quickKeyComp.statusChanged.connect(finishComponentCreation);
        moreSettingComp.statusChanged.connect(finishComponentCreation);
    }
}

//creates
function finishComponentCreation(){
    var modeLab = settingReader.read("MODE")
    backupData.validateParams("MODE",modeLab)
    var modeSelected;
    var ventModeLabel;
    var mode = modeLab.split("_").pop()

    //TO CHECK IN DOMAIN CONTROLLERS
    var getModeName = modeLab.split("_")
    modeSelected = getModeName[1]
    console.log("selcted mode is ->>>>>>>",modeSelected)
    var ventGrpRef = getCompRef (mainitem,"VentGroup")
    ventGrpRef.selectedMode = modeSelected

    ventModeLabel = component.getComponentResourceString("Component","VentStatus","Button",selectedMode)

    ventModeBtn = statusComp.createObject(mainrow,{ "iCompID":0,"strModeLabel":ventModeLabel ,"iVentModeWidth":ventModeWidth,"iVentModeHeight":ventModeHeight,
                                              "strBtnState":ventGrpBtnState,
                                              "strVentState":ventState,
                                              "strVentilationStatus":ventStatus,
                                              "iVentModeBorderWidth":ventModeBorderWidth,"strVentModeBorderColor":ventModeBorderColor,
                                              "strVentModeColor":ventModeColor,
                                              "strVentModeActiveStateColor":ventActiveStateColor,
                                              "strVentModeDisabledStateColor":ventDisabledStateColor,
                                              "strVentModeDisabledFontColor":ventDisabledStateFontColor,
                                              "strVentModeEndOfScaleStateColor" : ventEndOfScaleStateColor,
                                              "strVentModeEndOfScaleStateBorderColor":ventEndOfScaleStateBorderColor,
                                              "strVentModeScrollStateBorderColor":ventScrollStateBorderColor,
                                              "strVentModeSelectedStateColor":ventSelectedStateColor,
                                              "strVentModeSelectedStateBorderColor":ventSelectedStateBorderColor,
                                              "strVentModeTouchedStateColor":ventTouchedStateColor,
                                              "strVentIconOn" : ventIconOn,
                                              "strVentIconOff" : ventIconOff,
                                              "iVentModeStateHeightChange":ventStateHeightChange,
                                              "strStatusTextFontFamily":ventStatusTextFontFamily,
                                              "iStatusTextFontSize":ventStatusTextFontSize,
                                              "strStatusTextFontColor" : ventStatusTextFontColor,
                                              "iStatusiconWidth":ventStatusiconWidth,"iStatusiconHeight":ventStatusiconHeight,
                                              "iventModeBtnWidth":ventModeBtnWidth,"iventModeBtnHeight":ventModeBtnHeight,
                                              "strventModeBtnBorderColor":ventModeBtnBorderColor,"iventModeBtnBorderWidth":ventModeBtnBorderWidth,
                                              "strVentArrowIcon":ventArrowIcon,
                                              "strVentArrowDisableIcon":ventArrowDisableIcon,
                                              "strVentArrowPresetIcon":ventArrowPresetIcon,
                                              "iventModeBtnRadius":ventModeBtnRadius,"strventModeBtnColor":ventModeBtnColor,
                                              "strlabelFontFamily": ventlabelFontFamily,"ilabelFontPixelSize":ventlabelFontPixelSize,
                                              "strModeValueFontColor" : ventModeValueFontColor,
                                              "strText1FontFamily": ventText1FontFamily,"iText1FontPixelSize":ventText1FontPixelSize});
    qKeyWidget1 = quickKeyComp.createObject(mainrow,{"iCompID": 1,
                                                "strBtnState":ventGrpBtnState,
                                                "strVentState":ventState,
                                                "iVentWidth":quickKeysWidth,
                                                "iVentHeight":quickKeysHeight,
                                                "strVentcolor":quickKeysColor,
                                                "strFontFamilylabel":quickKeysControlFontFamily,"iPixelSizelabel":quickKeysControlPixelSize,
                                                "strFontFamilyText":quickKeysControlValueFontFamily,"iPixelSizeText":quickKeysControlValuePixelSize,
                                                "strFontFamilylabel1":quickKeysUnitFontFamily,"iPixelSizelabel1":quickKeysUnitPixelSize,
                                                "strVentActiveStateColor":quickkeyActiveStateColor,
                                                "strVentDisabledStateColor":quickkeyDisabledStateColor,
                                                "strDisabledStateFontColor":quickkeyDisabledStateFontColor,
                                                "strVentEndOfScaleStateColor" : quickkeyEndOfScaleStateColor,
                                                "strVentEndOfScaleStateBorderColor" : quickkeyEndOfScaleStateBorderColor,
                                                "strVentScrollStateBorderColor" : quickkeyScrollStateBorderColor,
                                                "strVentSelectedStateColor" : quickkeySelectedStateColor,
                                                "strVentSelectedStateBorderColor" : quickkeySelectedStateBorderColor,
                                                "strVentTouchedStateColor" : quickkeyTouchedStateColor,
                                                "iVentStateHeightChange" : quickkeyStateHeightChange,
                                                "strQuickkeySpinnerUpEnabled" : quickkeySpinnerUpEnabled,
                                                "strQuickkeySpinnerUpDisabled" : quickkeySpinnerUpDisabled,
                                                "strQuickkeySpinnerDownDisabled" : quickkeySpinnerDownDisabled,
                                                "strQuickkeySpinnerDownEnabled" : quickkeySpinnerDownEnabled,
                                                "iSettingPopUpWidth" : settingPopUpWidth,
                                                "iSettingPopUpHeight" : settingPopUpHeight,
                                                "strSettingPopUpColor" : settingPopUpColor,
                                                "iPopUpBtnHeight" : popUpBtnHeight,
                                                "iPopUpBtnWidth" : popUpBtnWidth,
                                                "strPopUpBtnColor" : popUpBtnColor,
                                                "iPopUpBtnRadius" : popUpBtnRadius,
                                                "iPopUpBtnMargin" : popUpBtnMargin,
                                                "strSettingConfirmLabel" : settingConfirmLabel,
                                                "strSettingBtnFontColor" : settingBtnFontColor,
                                                "iSettingBtnValuePixelSize" : settingBtnValuePixelSize,
                                                "strSettingCancelLabel" : settingCancelLabel,
                                                "iPopUpTriHeight" : popUpTriHeight,
                                                "iPopUpTriWidth" : popUpTriWidth,
                                                "strPopUpTriIconSource" : popUpTriIconSource,
                                                "iQuickeySpinnerArrowBtnHeight": quickeySpinnerArrowBtnHeight,
                                                "iQuickeySpinnerArrowBtnWidth" :quickeySpinnerArrowBtnWidth ,
                                                "iQuickkeySpinnerIconHeight": quickkeySpinnerIconHeight,
                                                "iQuickkeySpinnerIconWidth": quickkeySpinnerIconWidth,
                                                "iQuickkeySpinnerAreaHeight": quickkeySpinnerAreaHeight,
                                                "iQuickkeySpinnerAreaWidth" : quickkeySpinnerAreaWidth ,
                                                "strQuickkeySpinnerSelStateColor": quickkeySpinnerSelStateColor,
                                                "strQuickkeySpinnerTchStateColor": quickkeySpinnerTchStateColor ,
                                                "strQuickkeySpinnerScrlStateBorderColor" : quickkeySpinnerScrlStateBorderColor,
                                                "strQuickkeySpinnerDisStateColor": quickkeySpinnerDisStateColor ,
                                                "strQuickkeySpinnerSelStateBorderColor": quickkeySpinnerSelStateBorderColor,
                                                "strQuickkeySpinnerEoSStateColor": quickkeySpinnerEoSStateColor})
    qKeyWidget2 = quickKeyComp.createObject(mainrow,{"iCompID": 2,
                                                "strBtnState":ventGrpBtnState,
                                                "strVentState":ventState,
                                                "iVentWidth":quickKeysWidth,
                                                "iVentHeight":quickKeysHeight,
                                                "strVentcolor":quickKeysColor,
                                                "strFontFamilylabel":quickKeysControlFontFamily,"iPixelSizelabel":quickKeysControlPixelSize,
                                                "strFontFamilyText":quickKeysControlValueFontFamily,"iPixelSizeText":quickKeysControlValuePixelSize,
                                                "strFontFamilylabel1":quickKeysUnitFontFamily,"iPixelSizelabel1":quickKeysUnitPixelSize,
                                                "strVentActiveStateColor":quickkeyActiveStateColor,
                                                "strVentDisabledStateColor":quickkeyDisabledStateColor,
                                                "strDisabledStateFontColor":quickkeyDisabledStateFontColor,
                                                "strVentEndOfScaleStateColor" : quickkeyEndOfScaleStateColor,
                                                "strVentEndOfScaleStateBorderColor" : quickkeyEndOfScaleStateBorderColor,
                                                "strVentScrollStateBorderColor" : quickkeyScrollStateBorderColor,
                                                "strVentSelectedStateColor" : quickkeySelectedStateColor,
                                                "strVentSelectedStateBorderColor" : quickkeySelectedStateBorderColor,
                                                "strVentTouchedStateColor" : quickkeyTouchedStateColor,
                                                "iVentStateHeightChange" : quickkeyStateHeightChange,
                                                "strQuickkeySpinnerUpEnabled" : quickkeySpinnerUpEnabled,
                                                "strQuickkeySpinnerUpDisabled" : quickkeySpinnerUpDisabled,
                                                "strQuickkeySpinnerDownDisabled" : quickkeySpinnerDownDisabled,
                                                "strQuickkeySpinnerDownEnabled" : quickkeySpinnerDownEnabled,
                                                "iSettingPopUpWidth" : settingPopUpWidth,
                                                "iSettingPopUpHeight" : settingPopUpHeight,
                                                "strSettingPopUpColor" : settingPopUpColor,
                                                "iPopUpBtnHeight" : popUpBtnHeight,
                                                "iPopUpBtnWidth" : popUpBtnWidth,
                                                "strPopUpBtnColor" : popUpBtnColor,
                                                "iPopUpBtnRadius" : popUpBtnRadius,
                                                "iPopUpBtnMargin" : popUpBtnMargin,
                                                "strSettingConfirmLabel" : settingConfirmLabel,
                                                "strSettingBtnFontColor" : settingBtnFontColor,
                                                "iSettingBtnValuePixelSize" : settingBtnValuePixelSize,
                                                "strSettingCancelLabel" : settingCancelLabel,
                                                "iPopUpTriHeight" : popUpTriHeight,
                                                "iPopUpTriWidth" : popUpTriWidth,
                                                "strPopUpTriIconSource" : popUpTriIconSource,
                                                "iQuickeySpinnerArrowBtnHeight": quickeySpinnerArrowBtnHeight,
                                                "iQuickeySpinnerArrowBtnWidth" :quickeySpinnerArrowBtnWidth ,
                                                "iQuickkeySpinnerIconHeight": quickkeySpinnerIconHeight,
                                                "iQuickkeySpinnerIconWidth": quickkeySpinnerIconWidth,
                                                "iQuickkeySpinnerAreaHeight": quickkeySpinnerAreaHeight,
                                                "iQuickkeySpinnerAreaWidth" : quickkeySpinnerAreaWidth ,
                                                "strQuickkeySpinnerSelStateColor": quickkeySpinnerSelStateColor,
                                                "strQuickkeySpinnerTchStateColor": quickkeySpinnerTchStateColor ,
                                                "strQuickkeySpinnerScrlStateBorderColor" : quickkeySpinnerScrlStateBorderColor,
                                                "strQuickkeySpinnerDisStateColor": quickkeySpinnerDisStateColor ,
                                                "strQuickkeySpinnerSelStateBorderColor": quickkeySpinnerSelStateBorderColor,
                                                "strQuickkeySpinnerEoSStateColor": quickkeySpinnerEoSStateColor})
    qKeyWidget3 = quickKeyComp.createObject(mainrow,{"iCompID": 3,
                                                "strBtnState":ventGrpBtnState,
                                                "strVentState":ventState,
                                                "iVentWidth":quickKeysWidth,
                                                "iVentHeight":quickKeysHeight,
                                                "strVentcolor":quickKeysColor,
                                                "strFontFamilylabel":quickKeysControlFontFamily,"iPixelSizelabel":quickKeysControlPixelSize,
                                                "strFontFamilyText":quickKeysControlValueFontFamily,"iPixelSizeText":quickKeysControlValuePixelSize,
                                                "strFontFamilylabel1":quickKeysUnitFontFamily,"iPixelSizelabel1":quickKeysUnitPixelSize,
                                                "strVentActiveStateColor":quickkeyActiveStateColor,
                                                "strVentDisabledStateColor":quickkeyDisabledStateColor,
                                                "strDisabledStateFontColor":quickkeyDisabledStateFontColor,
                                                "strVentEndOfScaleStateColor" : quickkeyEndOfScaleStateColor,
                                                "strVentEndOfScaleStateBorderColor" : quickkeyEndOfScaleStateBorderColor,
                                                "strVentScrollStateBorderColor" : quickkeyScrollStateBorderColor,
                                                "strVentSelectedStateColor" : quickkeySelectedStateColor,
                                                "strVentSelectedStateBorderColor" : quickkeySelectedStateBorderColor,
                                                "strVentTouchedStateColor" : quickkeyTouchedStateColor,
                                                "iVentStateHeightChange" : quickkeyStateHeightChange,
                                                "strQuickkeySpinnerUpEnabled" : quickkeySpinnerUpEnabled,
                                                "strQuickkeySpinnerUpDisabled" : quickkeySpinnerUpDisabled,
                                                "strQuickkeySpinnerDownDisabled" : quickkeySpinnerDownDisabled,
                                                "strQuickkeySpinnerDownEnabled" : quickkeySpinnerDownEnabled,
                                                "iSettingPopUpWidth" : settingPopUpWidth,
                                                "iSettingPopUpHeight" : settingPopUpHeight,
                                                "strSettingPopUpColor" : settingPopUpColor,
                                                "iPopUpBtnHeight" : popUpBtnHeight,
                                                "iPopUpBtnWidth" : popUpBtnWidth,
                                                "strPopUpBtnColor" : popUpBtnColor,
                                                "iPopUpBtnRadius" : popUpBtnRadius,
                                                "iPopUpBtnMargin" : popUpBtnMargin,
                                                "strSettingConfirmLabel" : settingConfirmLabel,
                                                "strSettingBtnFontColor" : settingBtnFontColor,
                                                "iSettingBtnValuePixelSize" : settingBtnValuePixelSize,
                                                "strSettingCancelLabel" : settingCancelLabel,
                                                "iPopUpTriHeight" : popUpTriHeight,
                                                "iPopUpTriWidth" : popUpTriWidth,
                                                "strPopUpTriIconSource" : popUpTriIconSource,
                                                "iQuickeySpinnerArrowBtnHeight": quickeySpinnerArrowBtnHeight,
                                                "iQuickeySpinnerArrowBtnWidth" :quickeySpinnerArrowBtnWidth ,
                                                "iQuickkeySpinnerIconHeight": quickkeySpinnerIconHeight,
                                                "iQuickkeySpinnerIconWidth": quickkeySpinnerIconWidth,
                                                "iQuickkeySpinnerAreaHeight": quickkeySpinnerAreaHeight,
                                                "iQuickkeySpinnerAreaWidth" : quickkeySpinnerAreaWidth ,
                                                "strQuickkeySpinnerSelStateColor": quickkeySpinnerSelStateColor,
                                                "strQuickkeySpinnerTchStateColor": quickkeySpinnerTchStateColor ,
                                                "strQuickkeySpinnerScrlStateBorderColor" : quickkeySpinnerScrlStateBorderColor,
                                                "strQuickkeySpinnerDisStateColor": quickkeySpinnerDisStateColor ,
                                                "strQuickkeySpinnerSelStateBorderColor": quickkeySpinnerSelStateBorderColor,
                                                "strQuickkeySpinnerEoSStateColor": quickkeySpinnerEoSStateColor})
    qKeyWidget4 = quickKeyComp.createObject(mainrow,{"iCompID": 4,
                                                "strBtnState":ventGrpBtnState,
                                                "strVentState":ventState,
                                                "iVentWidth":quickKeysWidth,
                                                "iVentHeight":quickKeysHeight,
                                                "strVentcolor":quickKeysColor,
                                                "strFontFamilylabel":quickKeysControlFontFamily,"iPixelSizelabel":quickKeysControlPixelSize,
                                                "strFontFamilyText":quickKeysControlValueFontFamily,"iPixelSizeText":quickKeysControlValuePixelSize,
                                                "strFontFamilylabel1":quickKeysUnitFontFamily,"iPixelSizelabel1":quickKeysUnitPixelSize,
                                                "strVentActiveStateColor":quickkeyActiveStateColor,
                                                "strVentDisabledStateColor":quickkeyDisabledStateColor,
                                                "strDisabledStateFontColor":quickkeyDisabledStateFontColor,
                                                "strVentEndOfScaleStateColor" : quickkeyEndOfScaleStateColor,
                                                "strVentEndOfScaleStateBorderColor" : quickkeyEndOfScaleStateBorderColor,
                                                "strVentScrollStateBorderColor" : quickkeyScrollStateBorderColor,
                                                "strVentSelectedStateColor" : quickkeySelectedStateColor,
                                                "strVentSelectedStateBorderColor" : quickkeySelectedStateBorderColor,
                                                "strVentTouchedStateColor" : quickkeyTouchedStateColor,
                                                "iVentStateHeightChange" : quickkeyStateHeightChange,
                                                "strQuickkeySpinnerUpEnabled" : quickkeySpinnerUpEnabled,
                                                "strQuickkeySpinnerUpDisabled" : quickkeySpinnerUpDisabled,
                                                "strQuickkeySpinnerDownDisabled" : quickkeySpinnerDownDisabled,
                                                "strQuickkeySpinnerDownEnabled" : quickkeySpinnerDownEnabled,
                                                "iSettingPopUpWidth" : settingPopUpWidth,
                                                "iSettingPopUpHeight" : settingPopUpHeight,
                                                "strSettingPopUpColor" : settingPopUpColor,
                                                "iPopUpBtnHeight" : popUpBtnHeight,
                                                "iPopUpBtnWidth" : popUpBtnWidth,
                                                "strPopUpBtnColor" : popUpBtnColor,
                                                "iPopUpBtnRadius" : popUpBtnRadius,
                                                "iPopUpBtnMargin" : popUpBtnMargin,
                                                "strSettingConfirmLabel" : settingConfirmLabel,
                                                "strSettingBtnFontColor" : settingBtnFontColor,
                                                "iSettingBtnValuePixelSize" : settingBtnValuePixelSize,
                                                "strSettingCancelLabel" : settingCancelLabel,
                                                "iPopUpTriHeight" : popUpTriHeight,
                                                "iPopUpTriWidth" : popUpTriWidth,
                                                "strPopUpTriIconSource" : popUpTriIconSource,
                                                "iQuickeySpinnerArrowBtnHeight": quickeySpinnerArrowBtnHeight,
                                                "iQuickeySpinnerArrowBtnWidth" :quickeySpinnerArrowBtnWidth ,
                                                "iQuickkeySpinnerIconHeight": quickkeySpinnerIconHeight,
                                                "iQuickkeySpinnerIconWidth": quickkeySpinnerIconWidth,
                                                "iQuickkeySpinnerAreaHeight": quickkeySpinnerAreaHeight,
                                                "iQuickkeySpinnerAreaWidth" : quickkeySpinnerAreaWidth ,
                                                "strQuickkeySpinnerSelStateColor": quickkeySpinnerSelStateColor,
                                                "strQuickkeySpinnerTchStateColor": quickkeySpinnerTchStateColor ,
                                                "strQuickkeySpinnerScrlStateBorderColor" : quickkeySpinnerScrlStateBorderColor,
                                                "strQuickkeySpinnerDisStateColor": quickkeySpinnerDisStateColor ,
                                                "strQuickkeySpinnerSelStateBorderColor": quickkeySpinnerSelStateBorderColor,
                                                "strQuickkeySpinnerEoSStateColor": quickkeySpinnerEoSStateColor})
    moreSettingbtn = moreSettingComp.createObject(mainrow,{"iCompID":5,"strMoreSetting":moreLabel,
                                                      "strBtnState":ventGrpBtnState,
                                                      "strVentState":ventState,
                                                      "iMoreSettingWidth" : ventMoreSettingWidth,
                                                      "iMoreSettingHeight" : ventMoreSettingHeight,
                                                      "iMoreSettingBorderWidth" : ventMoreSettingBorderWidth,
                                                      "iMoreSettingRadius" : ventMoreSettingRadius,
                                                      "strMoreSettingBorderColor" : ventMoreSettingBorderColor,
                                                      "strMoreSettingColor" : ventMoreSettingColor,
                                                      "strMoreSettingText1FontFamily":ventMoreSettingText1FontFamily,
                                                      "iMoreSettingText1FontPixelSize":ventMoreSettingText1FontPixelSize,
                                                      "strMoreSettingActiveStateColor" : moreSettingActiveStateColor,
                                                      "strMoreSettingDisabledStateColor": moreSettingDisabledStateColor,
                                                      "strMoreSettingEndOfScaleStateColor": moreSettingEndOfScaleStateColor,
                                                      "strMoreSettingEndOfScaleStateBorderColor": moreSettingEndOfScaleStateBorderColor,
                                                      "strMoreSettingScrollStateBorderColor": moreSettingScrollStateBorderColor,
                                                      "strMoreSettingTouchedStateColor": moreSettingTouchedStateColor,
                                                      "iMoreSettingStateHeightChange" : moreSettingStateHeightChange,
                                                      "moreDisabledStateFontColor"  : moreDisabledStateFontColor,
                                                      "strMoreIconDiabled" : moreIconDiabled,
                                                      "strMoreIconEnabled" : moreIconEnabled,
                                                      "strMoreIconPreset" : moreIconPreset,
                                                      "strMoreIconTouched" : moreIconTouched})

    qKeyWidget1.incrementValue.connect(onTVIncrementValue);
    qKeyWidget1.decrementValue.connect(onTVDecrementValue);
    qKeyWidget1.checkEndOfScaleForSpinnerWidget.connect(onTVEndOfScale);
    qKeyWidget1.setUpArrowBgColor.connect(onSetTVUpArrowBgColor);
    qKeyWidget1.setDownArrowBgColor.connect(onSetTVDownArrowBgColor);
    qKeyWidget1.selection.connect(onTVSelection)

    qKeyWidget1.incrementValue.connect(onPinspIncrementValue);
    qKeyWidget1.decrementValue.connect(onPinspDecrementValue);
    qKeyWidget1.checkEndOfScaleForSpinnerWidget.connect(onPinspEndOfScale);
    qKeyWidget1.setUpArrowBgColor.connect(onSetPinspUpArrowBgColor);
    qKeyWidget1.setDownArrowBgColor.connect(onSetPinspDownArrowBgColor);

    qKeyWidget1.incrementValue.connect(onPsupportIncrementValue);
    qKeyWidget1.decrementValue.connect(onPsupportDecrementValue);
    qKeyWidget1.checkEndOfScaleForSpinnerWidget.connect(onPsupportEndOfScale);
    qKeyWidget1.setUpArrowBgColor.connect(onSetPsupportUpArrowBgColor);
    qKeyWidget1.setDownArrowBgColor.connect(onSetPsupportDownArrowBgColor);

    qKeyWidget2.incrementValue.connect(onRRIncrementValue);
    qKeyWidget2.decrementValue.connect(onRRDecrementValue);
    qKeyWidget2.checkEndOfScaleForSpinnerWidget.connect(onRREndOfScale);
    qKeyWidget2.setUpArrowBgColor.connect(onSetRRUpArrowBgColor);
    qKeyWidget2.setDownArrowBgColor.connect(onSetRRDownArrowBgColor);
    qKeyWidget2.selection.connect(onRRSelection);

    qKeyWidget2.incrementValue.connect(onPinspIncrementValue);
    qKeyWidget2.decrementValue.connect(onPinspDecrementValue);
    qKeyWidget2.checkEndOfScaleForSpinnerWidget.connect(onPinspEndOfScale);
    qKeyWidget2.setUpArrowBgColor.connect(onSetPinspUpArrowBgColor);
    qKeyWidget2.setDownArrowBgColor.connect(onSetPinspDownArrowBgColor);

    qKeyWidget2.incrementValue.connect(onRRMechIncrementValue);
    qKeyWidget2.decrementValue.connect(onRRMechDecrementValue);
    qKeyWidget2.checkEndOfScaleForSpinnerWidget.connect(onRRMechEndOfScale);
    qKeyWidget2.setUpArrowBgColor.connect(onSetRRMechUpArrowBgColor);
    qKeyWidget2.setDownArrowBgColor.connect(onSetRRMechDownArrowBgColor);
    qKeyWidget2.selection.connect(onRRMechSelection);

    qKeyWidget3.incrementValue.connect(onIEIncrementValue);
    qKeyWidget3.decrementValue.connect(onIEDecrementValue);
    qKeyWidget3.checkEndOfScaleForSpinnerWidget.connect(onIEEndOfScale);
    qKeyWidget3.setUpArrowBgColor.connect(onSetIEUpArrowBgColor);
    qKeyWidget3.setDownArrowBgColor.connect(onSetIEDownArrowBgColor);
    qKeyWidget3.selection.connect(onIESelection)

    qKeyWidget3.incrementValue.connect(onPsupportIncrementValue);
    qKeyWidget3.decrementValue.connect(onPsupportDecrementValue);
    qKeyWidget3.checkEndOfScaleForSpinnerWidget.connect(onPsupportEndOfScale);
    qKeyWidget3.setUpArrowBgColor.connect(onSetPsupportUpArrowBgColor);
    qKeyWidget3.setDownArrowBgColor.connect(onSetPsupportDownArrowBgColor);

    qKeyWidget3.incrementValue.connect(onRRIncrementValue);
    qKeyWidget3.decrementValue.connect(onRRDecrementValue);
    qKeyWidget3.checkEndOfScaleForSpinnerWidget.connect(onRREndOfScale);
    qKeyWidget3.setUpArrowBgColor.connect(onSetRRUpArrowBgColor);
    qKeyWidget3.setDownArrowBgColor.connect(onSetRRDownArrowBgColor);

    //RRMech functions connection
    qKeyWidget3.incrementValue.connect(onRRMechIncrementValue);
    qKeyWidget3.decrementValue.connect(onRRMechDecrementValue);
    qKeyWidget3.checkEndOfScaleForSpinnerWidget.connect(onRRMechEndOfScale);
    qKeyWidget3.setUpArrowBgColor.connect(onSetRRMechUpArrowBgColor);
    qKeyWidget3.setDownArrowBgColor.connect(onSetRRMechDownArrowBgColor);
    qKeyWidget3.selection.connect(onRRMechSelection);

    qKeyWidget4.incrementValue.connect(onPEEPIncrementValue);
    qKeyWidget4.decrementValue.connect(onPEEPDecrementValue);
    qKeyWidget4.checkEndOfScaleForSpinnerWidget.connect(onPEEPEndOfScale);
    qKeyWidget4.setUpArrowBgColor.connect(onSetPEEPUpArrowBgColor);
    qKeyWidget4.setDownArrowBgColor.connect(onSetPEEPDownArrowBgColor);
    qKeyWidget4.selection.connect(onPeepSelection)

    qKeyWidget1.setQKData.connect(onModeChangedTV)
    qKeyWidget1.setQKData.connect(onModeChangedPsupport)
    qKeyWidget1.setQKData.connect(onModeChangedPinsp)

    qKeyWidget2.setQKData.connect(onModeChangedRRMech)
    qKeyWidget2.setQKData.connect(onModeChangedRR)
    qKeyWidget2.setQKData.connect(onModeChangedPinsp)

    qKeyWidget3.setQKData.connect(onModeChangedRRMech)
    qKeyWidget3.setQKData.connect(onModeChangedPsupport)
    qKeyWidget3.setQKData.connect(onModeChangedIE)

    qKeyWidget4.setQKData.connect(onModeChangedPEEP)

    qKeyWidget1.setQKData(mode)
    qKeyWidget2.setQKData(mode)
    qKeyWidget3.setQKData(mode)
    qKeyWidget4.setQKData(mode)


    objCSBValue.writeVentModeToCSB(ventModeBtn.strModeLabel)
}

//*****************************MODE MENU CREATION*******************************

var ventMenuComponent;
var ventMenuObj;
var modeMenuTitle = component.getComponentResourceString("Group","VentModeMenu","ModeMenuLabel","default")
var ventModeMenuWidth = parseInt(layout.getGroupLayoutDataByTag("VentModeMenu","Dimension","width"));
var ventModeMenuHeight = parseInt(layout.getGroupLayoutDataByTag("VentModeMenu","Dimension","height"));
var ventModeMenuColor = component.getComponentXmlDataByTag("Group","VentModeMenu","Color")
var ventModeMenuBorderWidth = parseInt(component.getComponentXmlDataByTag("Group","VentModeMenu","BorderWidth"));
var ventModeMenuBorderColor = component.getComponentXmlDataByTag("Group","VentModeMenu","BorderColor");
var ventModeMenuRadius = parseInt(component.getComponentXmlDataByTag("Group","VentModeMenu","BorderRadius"));
var ventModeMenuLabelFontFamily = geFont.geFontStyle() //group.getComponentGroupDataByTag(groupnames,"VentMode","VentModeMenuGroup","ModeMenuLabelFontFamily");
var ventModeMenuLabelPixelsize = parseInt(component.getComponentXmlDataByTag("Group","VentModeMenu","ModeMenuLabelPixelsize"));
var closeBtnVentMenu = component.getComponentXmlDataByTag("Group","VentModeMenu","CloseBtnVentMenu");
var closeBtnTchVentMenu = component.getComponentXmlDataByTag("Group","VentModeMenu","CloseBtnTchVentMenu");
var ventMenuMinimizeBtn = component.getComponentXmlDataByTag("Group","VentModeMenu","VentMenuMinimizeBtn");
var ventMenuMaximizeBtn = component.getComponentXmlDataByTag("Group","VentModeMenu","VentMenuMaximizeBtn");


function createVentModeMenu(menuType) {

    console.log("inside mode menu creation")
    ventMenuComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentMenu.qml")
    if (ventMenuComponent.status === Component.Ready){
        finishVentMenuCreation(menuType);
    }else{
        ventMenuComponent.statusChanged.connect(finishVentMenuCreation(menuType));
    }
}

function finishVentMenuCreation(menuType) {

    console.log("finishVentMenuCreation")
    ventMenuObj = ventMenuComponent.createObject(mainitem,{"strMenuTitle":modeMenuTitle,
                                                     "state":menuType,
                                                     "iVentModeMenuWidth":ventModeMenuWidth,
                                                     "iVentModeMenuHeight":ventModeMenuHeight,
                                                     "strVentModeMenuColor":ventModeMenuColor,
                                                     "iVentModeMenuBorderWidth":ventModeMenuBorderWidth,
                                                     "strVentModeMenuBorderColor":ventModeMenuBorderColor,
                                                     "iVentModeMenuRadius":ventModeMenuRadius,
                                                     "ventModeMenuLabelFontFamily":ventModeMenuLabelFontFamily,
                                                     "iVentModeMenuLabelPixelsize":ventModeMenuLabelPixelsize,
                                                     "strcloseBtnVentMenu":closeBtnVentMenu,
                                                     "strcloseBtnTchVentMenu":closeBtnTchVentMenu,
                                                     "strventMenuMinimizeBtn": ventMenuMinimizeBtn,
                                                     "strventMenuMaximizeBtn": ventMenuMaximizeBtn
                                                 });
    var alarmGrpReference=getGroupRef("AlarmGroup")
    ventMenuObj.anchors.top = alarmGrpReference.bottom;
    ventMenuObj.anchors.topMargin = 5
    var ventGrpObj = getGroupRef("VentGroup")
    ventGrpObj.bIsModeMenuCreated = true   //to make sure that we will be destroying the menu only when it is created
    var ventModeRef = getCompRef(ventGrpObj.ventParentId,"ventModeBtn")
    ventMenuTypeChanged(ventModeRef.strVentMenuType)
}

//*****************************MORE SETTINGS MENU CREATION*******************************
var moreMenuComponent;
var moreMenuObj;

//MoreSettings Menu property
var moreSettingsLabel = component.getComponentResourceString("Group","MoreSettingsMenu","Label","default")
var moreSMode =component.getComponentResourceString("Group","MoreSettingsMenu","Mode","VCV")
var moreSettingsWidth = parseInt(layout.getGroupLayoutDataByTag("MoreSetting_VCV","Dimension","width"));
var moreSettingsHeight = parseInt(layout.getGroupLayoutDataByTag("MoreSetting_VCV","Dimension","height"));
var moreSettingsBorderWidth = parseInt(component.getComponentXmlDataByTag("Group","MoreSettingsMenu","BorderWidth"))
var moreSettingsBorderRadius =  parseInt(component.getComponentXmlDataByTag("Group","MoreSettingsMenu", "BorderRadius"))
var moreSettingsLabelFontFamily = geFont.geFontStyle() //group.getSecondaryComponentGroupDataByTag(strGroupnames,"VentMode","VentSetting", strMoreSMode, "MoreSettings", "MoreMenuGroup", "MoreSettingsLabelFontFamily")
var moreSettingsLabelPixelsize = parseInt(component.getComponentXmlDataByTag("Group","MoreSettingsMenu", "MoreSettingsLabelPixelsize"))
var moreSettingsBorderColor = component.getComponentXmlDataByTag("Group","MoreSettingsMenu","BorderColor")
var moreSettingsColor = component.getComponentXmlDataByTag("Group","MoreSettingsMenu","Color")


function createMoreSettingMenu() {

    console.log("inside more menu creation")
    moreMenuComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/MoreSettingsMenu.qml")
    if (moreMenuComponent.status === Component.Ready){
        finishMenuCreation();
    } else{
        moreMenuComponent.statusChanged.connect(finishMenuCreation());
    }
}

function finishMenuCreation() {

    moreMenuObj = moreMenuComponent.createObject(mainitem,{"strMoreSLabel":moreSettingsLabel,
                                                     "strMoreSMode":settingReader.read("MODE"),
                                                     "iMoreMenuWidth":moreSettingsWidth,
                                                     "iMoreMenuHeight":moreSettingsHeight,
                                                     "strMoreMenuBorderColor":moreSettingsBorderColor,
                                                     "iMoreMenuBorderWidth":moreSettingsBorderWidth,
                                                     "iMoreMenuRadius":moreSettingsBorderRadius,
                                                     "strMoreMenuColor":moreSettingsColor,
                                                     "strFontFamilyMoreSetLabel":moreSettingsLabelFontFamily,
                                                     "iPixelSizeMoreSet":moreSettingsLabelPixelsize
                                                 });
    var alarmGrpReference=getGroupRef("AlarmGroup")
    moreMenuObj.anchors.top = alarmGrpReference.bottom;
    moreMenuObj.anchors.topMargin = 5
    var ventGrpObj = getGroupRef("VentGroup")
    ventGrpObj.bIsSettingsMenuCreated = true  //to make sure that we will be destroying the menu only when it is created
}

//*******************************************
// Purpose: Handles key navigation (right) within the vent group
// Input Parameters: objectName of the quickkey/ventmodeBtn/moreSettingsBtn
// Description: Gets the current component objectName and set the focus to next enabled component on right navigation(comwheel)
//***********************************************/
function onNavigateRightInVentGroup(objectName) {

    var length = ventParentId.children.length;
    var currentIndex = 0;
    var currentComp = getCompRef(ventParentId, objectName)
    for(var i =0; i<length;i++){
        if(ventParentId.children[i].objectName === objectName){
            break;
        }else{
            currentIndex++;
        }
    }
    console.log("current index"+currentIndex+"length:"+length)

    if(currentIndex === length-1){
        //  ventParentId.children[0].focus = true
        /* For handling navigation across group once it reaches the last element
           of the current group(VenGroup) on clockwise rotation*/
        groupNavigatorOnRight("VentGroup","Right")
    } else{
        if(ventParentId.children[(currentIndex + 1)].state !== "Disabled"){
            ventParentId.children[currentIndex + 1].focus = true
        } else{
            ventParentId.children[(currentIndex + 2)/length].focus = true
        }
    }
    if (ventParentId.children[currentIndex].strVentState === "Vent") {
        ventParentId.children[currentIndex].state = "Active"
    } else if (ventParentId.children[currentIndex].strVentState === "Bag"){
        ventParentId.children[currentIndex].state = "Preset"
    }
}

//*******************************************
// Purpose: Handles key navigation (left) within the vent group
// Input Parameters: objectName of the quickkey/ventmodeBtn/moreSettingsBtn
// Description: Gets the current component objectName and set the focus to previous enabled component on left navigation(comwheel)
//***********************************************/
function onNavigateLeftInVentGroup(objectName) {

    var length = ventParentId.children.length;
    var currentIndex = 0;
    var currentComp = getCompRef(ventParentId, objectName)
    for(var i =0; i<length;i++){
        if(ventParentId.children[i].objectName === objectName){
            break;
        }else{
            currentIndex++;
        }
    }
    console.log("current index"+currentIndex+"length:"+length)
    if(currentIndex === 0  && ventParentId.children[length - 1].state !=="Disabled"){
        //ventParentId.children[length - 1].focus = true
        /* For handling navigation across group once it reaches the first element
           of the current group(VenGroup) on anticlockwise rotation*/
        groupNavigatorOnLeft("VentGroup","Left")
    } else if(ventParentId.children[currentIndex - 1].state !== "Disabled"){
        ventParentId.children[currentIndex - 1].focus = true
    } else {
        ventParentId.children[0].focus = true
    }
    if (ventParentId.children[currentIndex].strVentState === "Vent") {
        ventParentId.children[currentIndex].state = "Active"
    } else if (ventParentId.children[currentIndex].strVentState === "Bag"){
        ventParentId.children[currentIndex].state = "Preset"
    }
}

//*******************************************
// Purpose: Handles press of comwheel for QuickKeys
// Input Parameters: objectName of the quickkey
// Description: On comwheel press gets the objectName of the current component and sets the corresponding state(Scroll->Selected  or
// Selected->Scroll(on confirmation of value using comwheel)
//***********************************************/
function onEnterPressed(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)

    currentVentComp.strVentPreviousVal = currentVentComp.strValue;

    console.log("OnEnter Pressed->>>>>",currentVentComp.state)
    if(currentVentComp === "Disabled" || currentVentComp === "PresetDisabled") {
        return ;
    }
    if(currentVentComp.state === "Active" || currentVentComp.state === "Scroll" || currentVentComp.state === "Preset" || currentVentComp.state === "PresetScroll"){
        currentVentComp.strVentPreviousVal = currentVentComp.strValue;
        currentVentComp.resetScrollSpinnerProperties()
        changeState(currentVentComp,"Selected")
        /*On entering the selected state make spinner border highlight invisible*/

        //currentVentComp.bIsCustomBorderVisible = false ;
        if(currentVentComp.strVentPreviousVal === currentVentComp.strValue){
            if (ventParentId.children[0].bIsModeChangeConfirmed === true){
                currentVentComp.bIsTimerRunning = true
                currentVentComp.reset()
            }
        }
    } else {
        setTimerToStop(currentVentComp)
        if (ventParentId.children[0].bIsModeChangeConfirmed === false){
            onModeConfirmed()
            currentVentComp.resetScrollSpinnerProperties()
            if (currentVentComp.strVentState === "Vent"){
                changeState(currentVentComp,"Scroll")
            } else if (currentVentComp.strVentState === "Bag"){
                changeState(currentVentComp,"PresetScroll")
            }
        } else {
            currentVentComp.resetScrollSpinnerProperties()
            changeState(currentVentComp,currentVentComp.strVentPreviousState)
            if(currentVentComp.strVentPreviousState === "Active" || currentVentComp.strVentPreviousState === "Preset" ){
                var currentGroup = getGroupRef(grpName)
                currentGroup.iPreviousIndex = currentVentComp.iCompID
                currentGroup.forceActiveFocus()
                currentGroup.focus = true
            }
        }
        currentVentComp.bIsValueChanged = false
        /*On confirming the value of quick key make spinner border highlight invisible*/
    }
    if(currentVentComp.objectName === "quickKey1" && currentVentComp.bModeChanged){
        console.log("currentVentComp",currentVentComp.strValue)
        ObjCCValue.constraintChecker(false)
        updateValues_constraint(currentVentComp)
        ObjCCValue.setmodeChangeAction(true)
    }
    currentVentComp.bModeChanged = false
}


//*******************************************
// Purpose: get the updated value after the constraint is applied
// Input Parameters: objectName of the quickkey/ventmodeBtn/moreSettingsBtn
// Description: During mode change, on confirmation of mode constarint checker is run on the first quick key and the updated value is assigned.
//***********************************************/

function updateValues_constraint(currentVentComp) {
    //    var currentVentComp = getCompRef(ventParentId, objectName)
    var ventmodeRef =  getCompRef(ventParentId, "ventModeBtn")
    if(ventmodeRef.strModeLabel === "BtnMode_VCV" || ventmodeRef.strModeLabel === "BtnMode_SIMVVCV"){
        currentVentComp.strValue = ObjCCValue.getTVValue()
    }else if (ventmodeRef.strModeLabel === "BtnMode_PCV" || ventmodeRef.strModeLabel === "BtnMode_SIMVPCV"){
        currentVentComp.strValue = ObjCCValue.getPinspValue()
    }else if (ventmodeRef.strModeLabel === "BtnMode_PSVPro"){
        var psupport = ObjCCValue.getPsupportValue();
        if(psupport === 0){
            currentVentComp.strValue = "Off";
        }else{
            currentVentComp.strValue = ObjCCValue.getPsupportValue()
        }
    }
}

//*******************************************
// Purpose: Stops the inital timer(10s) and interrupted behavior(30s) on confirmation of quickkey value and
//writes the data to CSB,dual storage and settings file
// Input Parameters: Currently selected quickkey component
// Description: On confirmation of quickkey value , stops the initial and final timer and update those data
//to CSB, dual stotage and settings file.
//***********************************************/
function setTimerToStop(currentVentComp) {
    currentVentComp.stopInitialTimer()
    currentVentComp.stopFinalTimer()
    var newValue = currentVentComp.strValue;
    var labelid = currentVentComp.strName;
    settingReader.writeDataInFile(labelid, newValue)
    backupData.storeCriticalParams(labelid,newValue);
    objCSBValue.writeToCSB(labelid,newValue,"Primary")
    console.log("lable-->",labelid + "newValue--->" + newValue)
    ObjCCValue.updateConstraintParam(labelid,newValue) // update TV/ Pinsp
    if((selectedMode === "VCV" || selectedMode === "PCV")) {
        if(currentVentComp.objectName === "quickKey2" && currentVentComp.strName === "RR_qKey") {
            ObjCCValue.updateTinsp(true)
        } else if(currentVentComp.objectName === "quickKey3" && currentVentComp.strName === "IE_qKey") {
            ObjCCValue.updateTinsp(true)
        } else if (currentVentComp.objectName === "quickKey1") {
            ObjCCValue.updateTinsp(true)
        } else {
            console.log("Other Quickey Slected")
        }
    }
    if(selectedMode !== "PSVPro" && currentVentComp.objectName === "quickKey2") {
        ObjCCValue.updateRRControl()
    } else if(selectedMode === "PSVPro" && currentVentComp.objectName === "quickKey3") {
        ObjCCValue.updateRRControl()
    } else{
        console.log("Other Mode Selected")
    }
}


//*******************************************
// Purpose: Handles quickkey touch event
// Input Parameters: objectName of the quickkey
// Description: Gets the currently touched quickey component and handles the state change to "Touched"
// based on the state of the other vent component (if any of them is already selected/ menu is opened)
//***********************************************/
function onVentBtnTouched(objectName) {
    var SelectedComp = getCompRef(ventParentId, objectName)
    console.log("onCompTouched...state is : " +SelectedComp.state +SelectedComp+SelectedComp.iCompID)
    if(SelectedComp.state === "Active" || SelectedComp.state === "Preset" || SelectedComp.state === "Scroll" || SelectedComp.state === "PresetScroll"){
        setTouchedState(SelectedComp,SelectedComp.strVentState)
    }
}

//*******************************************
// Purpose: Sets the component state to "Touched"/"PresetTouched"
// Input Parameters: Current component and ventilation status(vent/bag)
//***********************************************/
function setTouchedState(SelectedComp,ventState){
    if (ventState === "Vent"){
        //changeState(SelectedComp,"Touched")
        SelectedComp.state = "Touched"
    } else if (ventState === "Bag"){
        //changeState(SelectedComp,"PresetTouched")
        SelectedComp.state = "PresetTouched"
    }
}

//*******************************************
// Purpose: Handles state change.
// Input Parameters: Selected state component and the other component which is touched
//***********************************************/
function onSelectionOfOtherkey(currentComp,SelectedComp) {
    console.log("on other key selected"+currentComp.strType+currentComp.state+currentComp)
    /*  When the quick key is in selected state, when the user touches any other key ,
      set the customized border of previous selected indexed button invisible */
    if(currentComp.strType === "quickKeys") {
        currentComp.resetScrollSpinnerProperties()
    }
    currentComp.stopBlinking()
    console.log("on other key selected...1"+SelectedComp.strType+SelectedComp.state)
    if(SelectedComp.strType === "quickKeys") {
        changeState(SelectedComp,"Selected")
        SelectedComp.focus = true
        SelectedComp.forceActiveFocus()
    } else {
        //changeState(SelectedComp,"Active")
        SelectedComp.state = "Active"
        currentComp.state = "Active"
    }
    console.log("on other key selected...2"+currentComp+currentComp.state+currentComp.color)
    //setTouchedState(SelectedComp,SelectedComp.strVentState)
}

//*******************************************
// Purpose: Handles quickkey selection using touch
// Input Parameters: objectName of the quickkey
// Description: Gets the currently touched quickey component and handles the state change from
// Touched->Selected or Selected-> previous state
//***********************************************/
function onVentBtnClicked(objectName) {

    var isSelected = "false"
    var currentVentComp = getCompRef(ventParentId, objectName)
    currentVentComp.strVentPreviousVal = currentVentComp.strValue;

    console.log("state inside onventBtnClicked"+currentVentComp.state)
    if(currentVentComp.state === "Touched" || currentVentComp.state === "PresetTouched") {
        var ventGrpObj = getGroupRef("VentGroup")
        console.log("bIsModeMenuCreated is ->>>>",ventGrpObj.bIsModeMenuCreated)
        if (ventGrpObj.bIsModeMenuCreated && !currentVentComp.bModeChanged) {
            destroyVentMenu();
        }
        if (ventGrpObj.bIsSettingsMenuCreated){
            destroyMoreSettingMenu()
        }
        for(var i =0 ;i<ventParentId.children.length ;i++){
            var currentComp = getCompRefByIndex(ventParentId, i)
            if(currentComp.state === "Selected" || currentComp.state === "End of Scale"){
                if((currentComp.bIsValueChanged) && (currentComp.bInitialInterruptedStateStarted) ){
                    isSelected = "true"
                    currentComp.strValue = currentComp.strVentPreviousVal;
                    //mediaPlayer.play()
                    onSelectionOfOtherkey(currentComp,currentVentComp)
                } else if((!currentComp.bIsValueChanged) && (!currentComp.bInitialInterruptedStateStarted)){
                    currentComp.strValue = currentComp.strVentPreviousVal;
                    isSelected = "true"
                    onSelectionOfOtherkey(currentComp,currentVentComp)
                } else if((currentComp.bIsValueChanged) && (!currentComp.bInitialInterruptedStateStarted)){
                    isSelected = "true"
                    currentComp.startBlinking()
                    currentComp.bIsValueChanged = false
                }
            }
        }
        if(isSelected === "false"){
            currentVentComp.focus =true
            currentVentComp.forceActiveFocus()
            changeState(currentVentComp,"Selected")
        }
        if(currentVentComp.state === "Selected" && currentVentComp.strVentPreviousVal === currentVentComp.strValue){
            if (ventParentId.children[0].bIsModeChangeConfirmed === true){
                currentVentComp.bIsTimerRunning = true
                currentVentComp.reset()
            }
        }
    } else if(currentVentComp.state === "Selected" || currentVentComp.state === "End of Scale"){
        setTimerToStop(currentVentComp)
        if (ventParentId.children[0].bIsModeChangeConfirmed === false){
            onModeConfirmed()
            currentVentComp.resetScrollSpinnerProperties()
            if (currentVentComp.strVentState === "Vent"){
                changeState(currentVentComp,"Scroll")
            } else if (currentVentComp.strVentState === "Bag"){
                changeState(currentVentComp,"PresetScroll")
            }
        } else {
            currentVentComp.resetScrollSpinnerProperties()
            currentVentComp.stopBlinking();
            currentVentComp.bIsValueChanged = false
            currentVentComp.strValue = currentVentComp.strVentPreviousVal ;
            changeState(currentVentComp,currentVentComp.strVentPreviousState)
            if(currentVentComp.strVentPreviousState === "Active" || currentVentComp.strVentPreviousState === "Preset") {
                var currentGroup = getGroupRef(grpName)
                currentGroup.iPreviousIndex = currentVentComp.iCompID
                currentGroup.forceActiveFocus()
                currentGroup.focus = true
            }
        }
    }
    if(currentVentComp.objectName === "quickKey1" && ventParentId.children[0].bIsModeChangeConfirmed === true) {
        console.log("currentVentComp",currentVentComp.strValue)
        ObjCCValue.constraintChecker(false)
        updateValues_constraint(currentVentComp)
        ObjCCValue.setmodeChangeAction(true)
    }
    currentVentComp.bModeChanged = false
}

//*******************************************
// Purpose: Handles state change for the Quickkeys
// Input Parameters: current commponent and state needs to be set
// Description: Change the current component state to based on the input
//***********************************************/
function changeState(currentVentComp,stateName) {

    var ventGrpRef = getGroupRef ("VentGroup")
    currentVentComp.state = stateName
    for(var i=0;i<ventGrpRef.ventParentId.children.length;i++){

        var siblingObj = getCompRef(ventGrpRef.ventParentId, ventGrpRef.ventParentId.children[i].objectName)
        if(ventGrpRef.ventParentId.children[0].bIsModeChangeConfirmed === false){
            //Checking whether vent status is Bag mode or vent mode
            if (siblingObj.strVentState === "Vent"){
                siblingObj.state = "Disabled"
            } else if (siblingObj.strVentState === "Bag"){
                siblingObj.state = "PresetDisabled"
            }
        } else{
            if(siblingObj.state !== "Active" && i !== currentVentComp.iCompID && siblingObj.state !== "Preset"){
                console.log("changing state of object"+siblingObj+siblingObj.state)
                //Checking whether vent status is Bag mode or vent mode
                if (siblingObj.strVentState === "Vent"){
                    siblingObj.state = "Active"
                } else if (siblingObj.strVentState === "Bag"){
                    siblingObj.state = "Preset"
                }
            }
        }
        currentVentComp.focus =true
        currentVentComp.state = stateName
    }
}

//*******************************************
// Purpose: Handles state change and timer chnages for the Quickkeys after the 30 second interrupted state time out
// Input Parameters: current commponent and value that needs to be set
// Description: Change the current component state and the value and the timer based on the input
//***********************************************/

function onInterruptedTimeout(objectName,strVentPreviousVal) {

    console.log("onInterruptedTimeout->>>>>>")
    var currentGroup = getGroupRef(grpName)
    var currentComp = getCompRef(ventParentId, objectName)
    currentComp.strValue = strVentPreviousVal
    currentComp.stopBlinking()
    if (currentComp.strVentState === "Vent"){
        currentComp.state = currentComp.strVentPreviousState
        //currentComp.state = "Active"
    } else if (currentComp.strVentState === "Bag"){
        currentComp.state = currentComp.strVentPreviousState
        //currentComp.state = "Preset"
    }
    if(currentComp.strVentPreviousState === "Active" || currentComp.strVentPreviousState === "Peset"){
        currentGroup.iPreviousIndex = currentComp.iCompID
        currentGroup.forceActiveFocus()
        currentGroup.focus = true
    }
}

//*******************************************
// Purpose: Setting the state of the quickkey to "Active" when value is not changed in selected state
// Input Parameters: objectName of the quickkey/ventmodeBtn/moreSettingsBtn
// Description: Handles 10s timer timeout when selected quickkey value is not changed for 10s
//***********************************************/
function onNoValueChangeInTenSeconds(objectName) {

    console.log(" inside no value changed in 10 s")
    var currentComp = getCompRef(ventParentId, objectName)
    var currentGroup = getGroupRef(grpName)
    setTimerToStop(currentComp)
    if (currentComp.strVentState === "Vent"){
        currentComp.state = currentComp.strVentPreviousState
    } else if (currentComp.strVentState === "Bag"){
        currentComp.state = currentComp.strVentPreviousState
    }
    if(currentComp.strVentPreviousState === "Active" || currentComp.strVentPreviousState === "Preset"){
        currentGroup.iPreviousIndex = currentComp.iCompID
        currentGroup.forceActiveFocus()
        currentGroup.focus = true
    }
}

//*******************************************
// Purpose: Set the focus to the previously selected child when the focus is on the group
// Input Parameters: index of the child which was last selected
//***********************************************/

function setFocusToChild(iPreviousIndex) {

    var currentComp = getCompRefByIndex(ventParentId, iPreviousIndex)
    currentComp.forceActiveFocus()
    currentComp.focus = true
}

//*******************************************
// Purpose: On confirming mode during mode change, destroying the menu,popup and setting states for vent component
// Description: While confirming mode in vent mode menu, setting the state of the quickkeys to "Active"/"Preset"
// and destroying the confirmation popup and vent menu.
//***********************************************/
function onModeConfirmed() {

    console.log("onModeConfirmed.....")
    var ventmodeRef =  getCompRef(ventParentId, "ventModeBtn")
    ventmodeRef.state = "Active"
    backupData.storeCriticalParams("MODE",ventmodeRef.strModeLabel)
    objCSBValue.writeVentModeToCSB(ventmodeRef.strModeLabel)

    if (ventmodeRef.strModeLabel === "BtnMode_SIMVPCV"){
        ventGrpId.bCSBTrigger = false;
    }

    for(var i = 2 ;i<ventParentId.children.length ;i++){
        // To check whether it is bag mode or vent mode
        if (ventmodeRef.strVentState === "Vent") {
            ventParentId.children[i].state = "Active"
        } else if (ventmodeRef.strVentState === "Bag") {
            ventParentId.children[i].state = "Preset"
        }
    }
    ventmodeRef.bIsModeChangeConfirmed = true
    destroyVentMenu()  //doing it here because once it'd destroying the object can't be accessed to change the mode params
    destroyModeConfirmPopup()
}


//*******************************************
// Purpose: handles the state change and timer changes after the 1 min inactivity timer times out.
// Description:
//***********************************************/
function onScrollActiveTimeOut(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    var ventGroupObj = getGroupRef(grpName)
    console.log("inside scroll active timeout"+currentVentComp+ventGroupObj+objectName)
    if (currentVentComp.strVentState === "Vent"){
        currentVentComp.state = "Active"
    } else if (currentVentComp.strVentState === "Bag"){
        currentVentComp.state = "Preset"
    }
    ventGroupObj.iPreviousIndex = currentVentComp.iCompID
    ventGroupObj.forceActiveFocus()
    ventGroupObj.focus = true
    currentVentComp.stopScrollActiveTimer()
}

//*******************************************
// Purpose: Handles vent mode button touch event
// Input Parameters: vent mode button objectname
// Description: Check whether any other vent component is in "Selected"/"End of Scale" ,based on that perform
// touch action on the mode button
//***********************************************/
function onVentModeTouched(objectName) {

    console.log("parent is and obj name " + ventParentId, objectName)
    var SelectedComp = getCompRef(ventParentId, objectName)
    var ventGrpRef = getCompRef (mainitem,"VentGroup")
    var isSelected = "false"
    if(SelectedComp.state!=="Touched"  && SelectedComp.state!== "Disabled" && SelectedComp.state!=="PresetTouched"  && SelectedComp.state!== "PresetDisabled"){
        setTouchedState(SelectedComp,SelectedComp.strVentState)
    }
    //    if(isSelected === "false"){
    //        setTouchedState(SelectedComp,SelectedComp.strVentState)
    //        isSelected = "true"
    //    }
}

//*******************************************
// Purpose: Handles vent mode button selection(creating vent mode menu) using touch
// Input Parameters: vent mode button objectname
// Description: Checks whether mode button is in "Touched" state, and creates the vent mode menu and destroys
// moresettings menu if it's already opened
//***********************************************/
function onVentModeSelected(objectName) {

    var isSelected = "false"
    var SelectedComp = getCompRef(ventParentId, objectName)

    for(var i =0 ;i<ventParentId.children.length ;i++){
        var currentComp = getCompRefByIndex(ventParentId, i)
        if(currentComp.state === "Selected" || currentComp.state === "End of Scale"){
            if((currentComp.bIsValueChanged) && (currentComp.bInitialInterruptedStateStarted) ){
                isSelected = "true"
                console.log("mode...1"+currentComp)
                currentComp.strValue = currentComp.strVentPreviousVal;
                createVentModeMenuOnTouch(SelectedComp)
                onSelectionOfOtherkey(currentComp,SelectedComp)

            }else if((!currentComp.bIsValueChanged) && (!currentComp.bInitialInterruptedStateStarted)){
                isSelected = "true"
                console.log("mode...2"+currentComp+currentComp.state)
                currentComp.strValue = currentComp.strVentPreviousVal;
                createVentModeMenuOnTouch(SelectedComp)
                onSelectionOfOtherkey(currentComp,SelectedComp)

            }else if((currentComp.bIsValueChanged) && (!currentComp.bInitialInterruptedStateStarted)){
                isSelected = "true"
                console.log("mode...3"+currentComp)
                currentComp.startBlinking()
                currentComp.bIsValueChanged = false
            }
        }
    }

    if(isSelected === "false"){
        createVentModeMenuOnTouch(SelectedComp)
    }
}

function createVentModeMenuOnTouch(SelectedComp) {
    var ventGrpRef = getCompRef (mainitem,"VentGroup")
    var ventModeRef = getCompRef(ventParentId,"ventModeBtn")
    if(SelectedComp.state === "Touched" || SelectedComp.state === "PresetTouched") {
        if (!ventGrpRef.bSelectModeBtn) {
            createVentModeMenu(ventModeRef.strVentMenuType)
            var ventGrpObj = getGroupRef("VentGroup")
            if (ventGrpObj.bIsSettingsMenuCreated){
                destroyMoreSettingMenu()
            }
            if (SelectedComp.strVentState === "Vent"){
                SelectedComp.state = "Active"
            } else if (SelectedComp.strVentState === "Bag"){
                SelectedComp.state = "Preset"
            }
            onVentModeEnterPressed()
        }else if(ventGrpRef.bSelectModeBtn) {
            if (SelectedComp.strVentState === "Vent"){
                SelectedComp.state = "Active"
            } else if (SelectedComp.strVentState === "Bag"){
                SelectedComp.state = "Preset"
            }
            ventGrpRef.bSelectModeBtn = false
        }
    } else if (SelectedComp.state === "Active" || SelectedComp.state === "Preset") {
        if(ventGrpRef.bSelectModeBtn){
            ventGrpRef.bSelectModeBtn = false
        }
    }
}

//*******************************************
// Purpose: Handles vent component changes on mode change in the menu(like setting the selected mode in the mode button label,
// disabling the quickkeys and setting the first quickkey to selected state
// Input Parameters: selected mode in the menu
// Description: Creates confirmation popup menu and set the vent component states
//***********************************************/
function onModeSelectionInMenu(mode) {

    console.log("in vent group controller.... for selection of different vent mode")
    console.log("mode is : " + mode)
    var ventmodeRef =  getCompRef(ventParentId, "ventModeBtn")
    var ventGrpRef = getCompRef (mainitem,"VentGroup")
    ventmodeRef.previousMode = ventmodeRef.strModeLabel
    ventGrpRef.selectedMode = mode.split("_").pop()
    ventGrpRef.strForRevertingPQKValue = ventParentId.children[1].strValue
    ventmodeRef.strModeLabel = mode
    var ventGrpLength = ventParentId.children.length;
    for(var childindex = 0; childindex < ventGrpLength ; childindex++){
        if(childindex === 1){
            //ventParentId.children[childindex].forceActiveFocus()
            ventParentId.children[childindex].focus = true
            ventParentId.children[childindex].state = "Selected"
            ventParentId.children[childindex].resetScrollSpinnerProperties()
            ventParentId.children[childindex].startInitialTimer()
            ventParentId.children[childindex].bModeChanged = true
            ventParentId.children[childindex].bIsValueChanged = false
        } else {
            // To check whether it is bag mode or vent mode
            if (ventmodeRef.strVentState === "Vent") {
                ventParentId.children[childindex].state = "Disabled"
            } else if (ventmodeRef.strVentState === "Bag") {
                ventParentId.children[childindex].state = "PresetDisabled"
            }
        }
    }
    createModeConfirmationPopup(ventmodeRef.strModeLabel)
    ventmodeRef.bIsModeChangeConfirmed = false
    ObjCCValue.setPrevMode(ventmodeRef.previousMode)
}

//*******************************************
// Purpose: When final timer is triggered during mode change , on timeout sets the primary QK value
// to the previous value and initiates mode cancellation
//***********************************************/
function resetPrimaryValues() {

    ventParentId.children[1].strValue = strForRevertingPQKValue
    onCancellingModeConfirmation()
}

//*******************************************
// Purpose: Handles more settings button touch event
// Input Parameters: more settings button objectname
// Description: Check whether any other vent component is in "Selected"/"End of Scale" ,based on that perform
// touch action on the more button
//***********************************************/
function onSettingsCompTouched(objectName) {

    var SelectedComp = getCompRef(ventParentId, objectName)
    var isSelected = "false"
    if(SelectedComp.state!=="Touched"  && SelectedComp.state!== "Disabled" && SelectedComp.state!=="PresetTouched"  && SelectedComp.state!== "PresetDisabled" ){
        setTouchedState(SelectedComp,SelectedComp.strVentState)
    }

}

//*******************************************
// Purpose: Handles more setting button selection(creating moresetting menu) using touch
// Input Parameters: more settings button objectname
// Description: Checks whether settings button is in "Touched" state, and creates the more settings menu and destroys
// vent mode menu if it's already opened
//***********************************************/
function onSettingsCompSelected(objectName) {
    var isSelected = "false"
    var SelectedComp = getCompRef(ventParentId, objectName)

    for(var i =0 ;i<ventParentId.children.length ;i++){
        var currentComp = getCompRefByIndex(ventParentId, i)
        if(currentComp.state === "Selected" || currentComp.state === "End of Scale"){
            if((currentComp.bIsValueChanged) && (currentComp.bInitialInterruptedStateStarted) ){
                isSelected = "true"
                console.log("more...1"+currentComp)
                currentComp.strValue = currentComp.strVentPreviousVal;
                createMoreSettingMenuOnTouch(SelectedComp)
                onSelectionOfOtherkey(currentComp,SelectedComp)

            }else if((!currentComp.bIsValueChanged) && (!currentComp.bInitialInterruptedStateStarted)){
                isSelected = "true"
                console.log("more...2"+currentComp+currentComp.state)
                currentComp.strValue = currentComp.strVentPreviousVal;
                createMoreSettingMenuOnTouch(SelectedComp)
                onSelectionOfOtherkey(currentComp,SelectedComp)

            }else if((currentComp.bIsValueChanged) && (!currentComp.bInitialInterruptedStateStarted)){
                isSelected = "true"
                console.log("more...3"+currentComp)
                currentComp.startBlinking()
                currentComp.bIsValueChanged = false
            }
        }
    }

    if(isSelected === "false"){
        createMoreSettingMenuOnTouch(SelectedComp)
    }
}

function createMoreSettingMenuOnTouch(SelectedComp){
    var ventGrpObj = getGroupRef("VentGroup")
    console.log("create more setting...."+SelectedComp.state)
    if(SelectedComp.state === "Touched"){
        SelectedComp.state = "Active"
        createMoreSettingMenu()
        if (ventGrpObj.bIsModeMenuCreated) {
            destroyVentMenu();
        }
    } else if (SelectedComp.state === "PresetTouched"){
        SelectedComp.state = "Preset"
        createMoreSettingMenu()
        if (ventGrpObj.bIsModeMenuCreated) {
            destroyVentMenu();
        }
    }
    var moreMenuObj = getCompRef(mainitem, "MoreSettingsMenuObj")
    moreMenuObj.secParamParentId.children[0].focus = true
    moreMenuObj.secParamParentId.children[0].forceActiveFocus();
}

/******************create mode confirmation popup**************/

var popupComponent;
var popupObj;

function createModeConfirmationPopup(mode) {

    console.log("createModeConfirmationPopup....")
    popupComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentModeConfirmPopup.qml");
    if(popupComponent.status === Component.Ready){
        finishModeConfirmationPopupCreation(mode)
    }else if (popupComponent.status === Component.Error){
        console.log("Error loading component:", popupComponent.errorString());
    }else{
        popupComponent.statusChanged.connect(finishModeConfirmationPopupCreation(mode));
    }
}

function finishModeConfirmationPopupCreation(mode) {

    var confirmText;
    if (mode === "BtnMode_VCV"){
        confirmText = component.getComponentResourceString("Component","ModeConfirmPopup", "ConfirmMode","VCV")
    } else if (mode === "BtnMode_PCV"){
        confirmText = component.getComponentResourceString("Component","ModeConfirmPopup", "ConfirmMode","PCV")
    } else if (mode === "BtnMode_SIMVVCV"){
        confirmText = component.getComponentResourceString("Component","ModeConfirmPopup", "ConfirmMode","SIMVVCV")
    }else if (mode === "BtnMode_SIMVPCV"){
        confirmText = component.getComponentResourceString("Component","ModeConfirmPopup", "ConfirmMode","SIMVPCV")
    }else if(mode === "BtnMode_PSVPro"){
        confirmText = component.getComponentResourceString("Component","ModeConfirmPopup", "ConfirmMode","PSVPro")
    }

    popupObj = popupComponent.createObject(mainitem,{"x":339,"y":547,
                                               "strConfirmationText":confirmText,
                                               "iPopupWidth":popupWidth,
                                               "iPopupHeight":popupHeight,
                                               "iPopupRadius":popupRadius,
                                               "strPopupColor":popupColor,
                                               "strPopupLabelFontFamily":popupLabelFontFamily,
                                               "strToolTipTriangleSource":popupTipTriangleSource,
                                               "strPopupCancel":popupCancelLabel,
                                               "strCancelBtnColor":popupCancelBorderColor,
                                               "iPopupLabelPixelSize":popupLabelPixelSize,
                                               "strPopupFontColor": popupFontColor});
}

//*******************************************
// Purpose: Destroys mode confirmation popup once mode is confirmed/cancelled/reverted to previous mode on other
// area touch/ reverted to previous mode when interrupted behavior triggered after 10s
//***********************************************/
function destroyModeConfirmPopup() {

    var popupRef =  getCompRef(mainitem, "ConfirmPopup")
    popupRef.destroy()
}

//*******************************************
// Purpose: Handles on cancellation of mode change, reverting to the previous mode and setting the states
// for the vent components
//***********************************************/
function onCancellingModeConfirmation() {

    console.log("onCancellingModeConfirmation...")
    var ventGrpRef = getGroupRef ("VentGroup")
    var ventGrpLength = ventGrpRef.ventParentId.children.length;
    var qkRef = getCompRef(ventGrpRef.ventParentId,"quickKey1")
    console.log("Before restting the mode changed flag :" ,qkRef.bModeChanged)
    qkRef.bModeChanged = false  //reset the mode change flag when the mode change is cancelled
    var ventmodeRef =  getCompRef(ventGrpRef.ventParentId, "ventModeBtn")
    ventmodeRef.strModeLabel = ventmodeRef.previousMode
    ventGrpRef.selectedMode = ventmodeRef.previousMode.split("_").pop()
    ObjCCValue.cancelControlSettingAction();
    console.log("on cancelling->>>>>>", ventGrpRef.selectedMode)
    onRevertSelectionToPreviousMode(ventmodeRef.previousMode)
    ventmodeRef.bIsModeChangeConfirmed = true
    destroyModeConfirmPopup()
    destroyVentMenu() //doing it here because once it'd destroying the object can't be accessed to change the mode params
    for(var i =0; i<ventGrpLength;i++){
        if (ventmodeRef.strVentState === "Vent"){
            ventGrpRef.ventParentId.children[i].state = "Active"
            ventGrpRef.iPreviousIndex = ventmodeRef.iCompID
        } else if (ventmodeRef.strVentState === "Bag") {
            ventGrpRef.ventParentId.children[i].state = "Preset"
            ventGrpRef.iPreviousIndex = ventmodeRef.iCompID
        }
    }

    ventGrpRef.focus = true //setting the focus back to group when mode change is cancelled
    qkRef.stopInitialTimer()
    qkRef.stopTimers()
    settingReader.writeDataInFile("MODE",ventmodeRef.previousMode)
}

/*********************OTHER GUI AREA TOUCH*****************************/
//*******************************************
// Purpose: When other group area is touched during mode change or in normal case scenerio,
// handles the vent group component behavior
// Description: Handles menu destruction / mode cahnge cancellation / interrupted behavior
// for the quickkeys based on the operation vent group is performing while touching
// on other group
//***********************************************/
function onOtherGrpTouched() {

    console.log("onOtherGUIAreaTouched......")
    var ventGrpRef = getGroupRef ("VentGroup")
    var qkRef = getCompRef(ventGrpRef.ventParentId,"quickKey1")
    var ventGroupLength = ventGrpRef.ventParentId.children.length;

    //Do not display checkout screen if any Menus are open or Any Vent Key is in selected state
    var doNotCreateCheckoutScreen = false ;

    //Do not launch checkout screen if alarm setup menu is open.
    var menuObj = getCompRef(mainitem, "AlarmSetupMenuObj")
    if (menuObj !== null && typeof(menuObj) !== "undefined") {
        doNotCreateCheckoutScreen = true ;
    }
    console.log("mode changed flag is : " +qkRef.bModeChanged)
    if (qkRef.bModeChanged) {
        doNotCreateCheckoutScreen = true ;
        console.log("on mode change!!!!!!!!!!!!!!!!!!!")
        console.log("bInitialInterruptedStateStarted->>>>",qkRef.bInitialInterruptedStateStarted,qkRef.bIsValueChanged)
        if (qkRef.bInitialInterruptedStateStarted && !qkRef.bIsValueChanged) {
            qkRef.bInitialInterruptedStateStarted = false
            onCancellingModeConfirmation()
        } else {
            qkRef.resetScrollSpinnerProperties()
            qkRef.stopInitialTimer()
            qkRef.bIsValueChanged = false
            qkRef.startFinalTimer()
        }
    }
    //To destroy vent mode menu on touch of other groups if it is open
    if (ventGrpRef.bIsModeMenuCreated && !qkRef.bModeChanged) {
        destroyVentMenu();
        mainitem.forceActiveFocus() // currently setting the focus back to main layout instead of that particular group
        mainitem.focus = true
        doNotCreateCheckoutScreen = true ;
    }

    //To destroy more settings menu on touch of other groups if it is open
    if (ventGrpRef.bIsSettingsMenuCreated && !qkRef.bModeChanged) {
        destroyMoreSettingMenu()
        mainitem.forceActiveFocus() // currently setting the focus back to main layout instead of that particular group
        mainitem.focus = true
        doNotCreateCheckoutScreen = true ;
    }

    /*When a Quick kay is in selected state ,If the value has not been changed and the user selects another GUI area
                   The Quick Key shall exit the “Selected” state and return to the previous state*/
    if (!qkRef.bModeChanged && (!ventGrpRef.bIsSettingsMenuCreated && !ventGrpRef.bIsModeMenuCreated)) {
        for (var ventComp = 1; ventComp < ventGroupLength - 1; ventComp ++) {
            var currentComp = getCompRef(ventGrpRef.ventParentId, ventGrpRef.ventParentId.children[ventComp].objectName)
            if(currentComp.state === "Selected" || currentComp.state === "End of Scale"){
                doNotCreateCheckoutScreen = true ;
                if((currentComp.bIsValueChanged) && (currentComp.bInitialInterruptedStateStarted) ) {
                    currentComp.strValue = currentComp.strVentPreviousVal;
                    // mediaPlayer.play()
                    if(currentComp.strType === "quickKeys") {
                        currentComp.resetScrollSpinnerProperties()
                    }
                    currentComp.stopBlinking()
                    //changeState(currentComp,"Scroll")
                    mainitem.focus = true
                } else if((!currentComp.bIsValueChanged) && (!currentComp.bInitialInterruptedStateStarted)) {
                    currentComp.strValue = currentComp.strVentPreviousVal;
                    if(currentComp.strType === "quickKeys") {
                        currentComp.resetScrollSpinnerProperties()
                    }
                    currentComp.stopBlinking()
                    //changeState(currentComp,"Scroll")
                    mainitem.focus = true
                } else if((currentComp.bIsValueChanged) && (!currentComp.bInitialInterruptedStateStarted)) {
                    currentComp.startBlinking()
                    currentComp.bIsValueChanged = false
                }
                // mainitem.focus = true
            }
        }
        var modeObj = getCompRef(ventGrpRef.ventParentId, "ventModeBtn")
        if (modeObj.strVentState === "Vent") {
            modeObj.state = "Active"
        } else if (modeObj.strVentState === "Bag") {
            modeObj.state = "Preset"
        }
    }

    if(doNotCreateCheckoutScreen === false && touchedGroupName === "checkout"){
        StandbyComp.destroyTherapyObject()
        StandbyComp.destroyStandbyObject()
        CheckoutComp.createChkoutComponent();
        touchedGroupName = "";
    }

    if(doNotCreateCheckoutScreen === false && touchedGroupName === "standby" )
    {
        var statusOne = StandbyComp.isStandbyObjectEmpty() ;
        var statusTwo = StandbyComp.isTherapyObjectEmpty() ;

        console.log("statusOne : " + statusOne )
        console.log("statusTwo : " + statusTwo )

        if( statusOne === true && statusTwo === true )
        {
            StandbyComp.createStandbyComponent()
        }

        else if( statusOne === true && statusTwo === false )
        {
            StandbyComp.createStandbyComponent() //
            StandbyComp.destroyTherapyObject()
        }
        else if( statusOne === false && statusTwo === true )
        {
            StandbyComp.createTherapyComponent()
            StandbyComp.destroyStandbyObject()
        }
        touchedGroupName = "";
    }
}

/*********************Short cut keys handling*****************************/

function shortcutKeysPress(currentKey, objName) {
    //Convert ascii format to String/character format
    var key = String.fromCharCode(currentKey)
    var ventGroupObj = getGroupRef("VentGroup")
    if (ventGroupObj.keySequenceList.indexOf(key) === -1) {
        ventGroupObj.keySequenceList.push(key);
    }
    console.log("KeySequenceList is ->>>",ventGroupObj.keySequenceList)
    console.log(">>>>...shortcutKeysPress.....>>>>",key)
    var currentComp
    if (objName.length !== 0) {
        currentComp = getCompRef(ventParentId, objName)
    }
    switch(key) {
    case 'C' :
    case 'c':
        destroyMenus(currentKey,objName)
        var name
        if (objName.length === 0){
            qKeyWidget1.focus = true
            name = qKeyWidget1.objectName
            qKeyWidget1.state = "Active"
            qKeyWidget1.strVentPreviousState = qKeyWidget1.state
        } else {
            var quickKey1Ref =  getCompRef(ventParentId, "quickKey1")
            name = quickKey1Ref.objectName
            if(currentComp.strVentPreviousState === "") {
                currentComp.strVentPreviousState = "Active"
            }
            if(currentComp.state === "Scroll" || currentComp.state === "PresetScroll") {
                quickKey1Ref.focus = true
                name = quickKey1Ref.objectName
            } else if(currentComp.state === "Selected") {
                if (quickKey1Ref.objectName === currentComp.objectName) {
                    name = objName
                } else {
                    break
                }
            }
        }
        ObjCCValue.constraintChecker(true);
        onEnterPressed(name)
        console.log("c pressed!!!")
        break;

    case 'D' :
    case 'd':
        if (objName.length !== 0) {
            if (currentComp.bModeChanged) {
                return
            }
        }
        destroyMenus(currentKey,objName)
        var name_d
        if (objName.length === 0){
            qKeyWidget2.focus = true
            name_d = qKeyWidget2.objectName
            qKeyWidget2.state = "Active"
            qKeyWidget2.strVentPreviousState = qKeyWidget2.state
        } else {
            var quickKey2Ref =  getCompRef(ventParentId, "quickKey2")
            name_d = quickKey2Ref.objectName
            if(currentComp.strVentPreviousState === "") {
                currentComp.strVentPreviousState = "Active"
            }
            if(currentComp.state === "Scroll" || currentComp.state === "PresetScroll") {
                quickKey2Ref.focus = true
                name_d = quickKey2Ref.objectName
            } else if(currentComp.state === "Selected") {
                if (quickKey2Ref.objectName === currentComp.objectName) {
                    name_d = objName
                } else {
                    break
                }
            }
        }
        onEnterPressed(name_d)
        console.log("d pressed!!!")
        break;

    case 'E' :
    case 'e':
        if (objName.length !== 0) {
            if (currentComp.bModeChanged) {
                return
            }
        }
        destroyMenus(currentKey,objName)
        var name_e
        if (objName.length === 0){
            qKeyWidget3.focus = true
            name_e = qKeyWidget3.objectName
            qKeyWidget3.state = "Active"
            qKeyWidget3.strVentPreviousState = qKeyWidget3.state
        } else {
            var quickKey3Ref =  getCompRef(ventParentId, "quickKey3")
            name_e = quickKey3Ref.objectName
            if(currentComp.strVentPreviousState === "") {
                currentComp.strVentPreviousState = "Active"
            }
            if(currentComp.state === "Scroll" || currentComp.state === "PresetScroll") {
                quickKey3Ref.focus = true
                name_e = quickKey3Ref.objectName
            } else if(currentComp.state === "Selected") {
                if (quickKey3Ref.objectName === currentComp.objectName) {
                    name_e = objName
                } else {
                    break
                }
            }
        }
        onEnterPressed(name_e)
        console.log("e pressed!!!")
        break;

    case 'F' :
    case 'f':
        if (objName.length !== 0) {
            if (currentComp.bModeChanged) {
                return
            }
        }
        destroyMenus(currentKey,objName)
        var name_f
        if (objName.length === 0){
            qKeyWidget4.focus = true
            name_f = qKeyWidget4.objectName
            qKeyWidget4.state = "Active"
            qKeyWidget4.strVentPreviousState = qKeyWidget4.state
        } else {
            var quickKey4Ref =  getCompRef(ventParentId, "quickKey4")
            name_f = quickKey4Ref.objectName
            if(currentComp.strVentPreviousState === "") {
                currentComp.strVentPreviousState = "Active"
            }
            if(currentComp.state === "Scroll" || currentComp.state === "PresetScroll") {
                quickKey4Ref.focus = true
                name_f = quickKey4Ref.objectName
            } else if(currentComp.state === "Selected") {
                if (quickKey4Ref.objectName === currentComp.objectName) {
                    name_f = objName
                } else {
                    break
                }
            }
        }
        onEnterPressed(name_f)
        console.log("f pressed!!!")
        break;

    case 'G' :
    case 'g':
        var ventGrpObj = getGroupRef("VentGroup")
        var ventModeBtnRef = getCompRef(ventParentId,"ventModeBtn")
        if (ventGrpObj.bIsModeMenuCreated) {
            if (objName.length !== 0) {
                if (currentComp.bModeChanged) {
                    //don't destroy
                }  else {
                    destroyVentMenu()
                    if (ventModeBtnRef.strVentState === "Vent") {
                        ventModeBtnRef.state = "Active"
                    } else if (ventModeBtnRef.strVentState === "Bag") {
                        ventModeBtnRef.state = "Preset"
                    }
                }
            } else {
                destroyVentMenu()
                if (ventModeBtnRef.strVentState === "Vent") {
                    ventModeBtnRef.state = "Active"
                } else if (ventModeBtnRef.strVentState === "Bag") {
                    ventModeBtnRef.state = "Preset"
                }
            }
        }
        var selected_obj = checkSelectedState()
        var checkEOSStateMoreSetting = checkEOSState()
        var ventGrp = getGroupRef("VentGroup")
        if (selected_obj  || checkEOSStateMoreSetting && !ventGrp.bIsSettingsMenuCreated){
            checkForShortcutPressed(currentKey)
        }
        else if (!ventGrp.bIsSettingsMenuCreated)
        {
            console.log("FROM G Pressed")
            createMoreSettingMenu()
        }
        else{
            console.log("g pressed!!!")
        }
        break;

    case 'V' :
    case 'v':
        var ventGrpForVent = getGroupRef("VentGroup")
        var settingsObj = getCompRef(ventParentId,"MoreSettingBtn")
        if (ventGrpForVent.bIsSettingsMenuCreated) {
            destroyMoreSettingMenu()
            if (settingsObj.strVentState === "Vent") {
                settingsObj.state = "Active"
            } else if (settingsObj.strVentState === "Preset") {
                settingsObj.state = "Preset"
            }
        }

        var checkSelectedStateVentMenu = checkSelectedState()
        var checkEOSStateVentMenu = checkEOSState()
        var ventModeRef = getCompRef(ventParentId,"ventModeBtn")
        if (checkSelectedStateVentMenu || checkEOSStateVentMenu && !ventGroupObj.bIsModeMenuCreated) {
            checkForShortcutPressed(currentKey)
        }
        else if (!ventGroupObj.bIsModeMenuCreated) {
            ventModeRef.strVentPreviousState = ventModeRef.state
            createVentModeMenu(ventModeRef.strVentMenuType)
            onVentModeEnterPressed()
        }
        else
        {
            console.log("v pressed!!!")
        }

        break;

    case 'K' :
    case 'k':
        console.log("k pressed!!!")

        if (isAnyMenuOpen()) {
            if (objName === "") {
                //To check if any menu is opened, if so on "k" press will acts as enter /select inside the menu
                handleShortcutSelectInsideMenu()
            } else {
                handleMenuCreationUsingShortcut(objName,currentComp)
            }
        } else {
            if (objName.length !== 0) {
                handleMenuCreationUsingShortcut(objName,currentComp)
            }
        }
        break;

    case '2':
        if (isAnyMenuOpen()) {
            if (objName.length !== 0) {
                if (currentComp.bModeChanged) {
                    currentComp.bKeySecondPressed = true;
                }
            } else {
                bKeyTwoPressedInsideMenu = true
            }
            break
        } else {
            if (objName === "") {
                bKeyTwoPressed = true;
            } else {
                currentComp.bKeySecondPressed = true;
            }
        }
        break;

    case '1' :
        if (isAnyMenuOpen()) {
            if (objName.length !== 0) {
                if (currentComp.bModeChanged) {
                    if (currentComp.bKeySecondPressed) {
                        isKeysPressedInSequence = true
                    }
                    currentComp.bKeyOnePressed = true;
                }
            } else {
                if (bKeyTwoPressedInsideMenu) {
                    isKeysPressedInSequence = true
                }
                bKeyOnePressedInsideMenu = true
            }
            break
        } else {
            if (objName.length === 0) {
                if(bKeyTwoPressed) {
                    bKeyOnePressed = true;
                    isKeysPressedInSequence = true
                } else {
                    if (bKeyTwoPressedInsideMenu) {
                        isKeysPressedInSequence = true
                    }
                    bKeyOnePressedInsideMenu = true
                }
            } else {
                currentComp.bKeyOnePressed = true;
                if (currentComp.bKeySecondPressed) {
                    isKeysPressedInSequence = true
                }
            }
        }
        break;

    case '5' :
        if (!arraysIdentical(ventGroupObj.keySequenceList,sequenceList215)) {
            ventGroupObj.keySequenceList.length = 0
            return
        } else {
            ventGroupObj.keySequenceList.length = 0
            if (isAnyMenuOpen()) {
                if (objName.length !== 0) {
                    if (currentComp.bModeChanged) {
                        currentComp.bKeyFifthPressed = true;
                        if (currentComp.bKeySecondPressed && currentComp.bKeyOnePressed && currentComp.bKeyFifthPressed && isKeysPressedInSequence) {
                            console.log("*****215 pressed for Quickkeys *******")
                            isKeysPressedInSequence =  false
                            currentComp.incrementValue(objName, currentComp.strName)
                            break
                        }
                    }
                } else {
                    //Handling "215" inside menu
                    bKeyFivePressedInsideMenu = true
                    if (bKeyTwoPressedInsideMenu && bKeyOnePressedInsideMenu && bKeyFivePressedInsideMenu && isKeysPressedInSequence) {
                        //To check if any menu is opened, if so on "k" press will acts as enter /select inside the menu
                        console.log("*****215 pressed inside menu*******")
                        isKeysPressedInSequence =  false
                        var ventGroupRef = getGroupRef("VentGroup")
                        if (ventGroupRef.bIsModeMenuCreated) {
                            var modeMenuRef = getCompRef(mainitem, "VentMenuObj")
                            var maxMinObjRef = getCompRef(modeMenuRef,"MaximizeMinimize")
                            var closeBtnObjRef = getCompRef(modeMenuRef,"ModeMenuClose")
                            for(var ventMode = 0;ventMode < modeMenuRef.menuModesID.children.length;ventMode++){
                                var currentModeBtn = getCompRef(modeMenuRef.menuModesID,modeMenuRef.menuModesID.children[ventMode].objectName)
                                if(currentModeBtn.state === "Scroll"){
                                    onNavigateRightMode(currentModeBtn.objectName)   //mapping to the same function as of keys.onreturnPressed
                                    break
                                }  else if(closeBtnObjRef.focus === true){
                                    onNavigateRightMode(closeBtnObjRef.objectName)
                                    break
                                } else if (maxMinObjRef.focus === true) {
                                    onNavigateRightMode(maxMinObjRef.objectName)
                                    modeMenuRef.restartTimer()
                                    break
                                }
                            }
                        } else if (ventGroupRef.bIsSettingsMenuCreated) {
                            var secParamMenuObj = getCompRef(mainitem, "MoreSettingsMenuObj")
                            var settingsCloseBtn = getCompRef(secParamMenuObj, "MoreSettingMenuClose")
                            for(var secParam= 0;secParam < secParamMenuObj.secParamParentId.children.length;secParam++) {
                                var currentParamObj = getCompRef(secParamMenuObj.secParamParentId,secParamMenuObj.secParamParentId.children[secParam].objectName)
                                if(currentParamObj.state === "Scroll"){
                                    moveClockwise(currentParamObj.objectName) //mapping to the same function as of key.rightPressed
                                    break
                                } else if (currentParamObj.state === "Selected" || currentParamObj.state === "End of Scale") {
                                    currentParamObj.incrementValue(currentParamObj.objectName,currentParamObj.strName)
                                    break
                                } else if (settingsCloseBtn.focus === true) {
                                    secParamMenuObj.secParamParentId.children[0].focus = true
                                    break
                                }
                            }
                        }
                    }
                }
            } else {
                if (objName.length === 0) {
                    bKeyFivePressed = true;
                } else {
                    currentComp.bKeyFifthPressed = true;
                    if (currentComp.bKeySecondPressed && currentComp.bKeyOnePressed && currentComp.bKeyFifthPressed && isKeysPressedInSequence) {
                        console.log("*****215 pressed for Quickkeys *******")
                        isKeysPressedInSequence =  false
                        if (currentComp.state === "Scroll" || currentComp.state === "PresetScroll") {
                            onNavigateRightInVentGroup(objName)
                            break
                        } else {
                            currentComp.incrementValue(objName, currentComp.strName)
                            break
                        }
                    }
                }
                if (bKeyTwoPressed && bKeyOnePressed && bKeyFivePressed && isKeysPressedInSequence) {
                    console.log("*****215 pressed for Scrolling in vent group*******")
                    isKeysPressedInSequence =  false
                    var ventGrpRef = getGroupRef("VentGroup")
                    var objIndex = getCurrentFocusedItem()
                    if ((ventGrpRef.focus !== true || ventGrpRef.focus === true) && iPreviousIndex === 0) {
                        onNavigateRightInVentGroup(ventParentId.children[objIndex].objectName)
                    } else {
                        setFocusToChild(iPreviousIndex)
                        iPreviousIndex = 0
                    }
                }
                isKeysPressedInSequence = false
            }
        }
        break;

    case '6' :
        //        if (!checkSelectedState() && checkScrollState() === "") {
        //            console.log("Nothing is selected or in scroll state")
        //            if(checkEOSState() === "") {
        //                return
        //            }
        //        }
        if (!arraysIdentical(ventGroupObj.keySequenceList,sequenceList216)) {
            ventGroupObj.keySequenceList.length = 0
            return
        } else {
            ventGroupObj.keySequenceList.length = 0
            if (isAnyMenuOpen()) {
                if (objName.length !== 0) {
                    if (currentComp.bModeChanged) {
                        currentComp.bKeySixthPressed = true;
                        if (currentComp.bKeySecondPressed && currentComp.bKeyOnePressed && currentComp.bKeySixthPressed && isKeysPressedInSequence) {
                            isKeysPressedInSequence =  false
                            console.log("*****216 pressed for Quickkeys *******")
                            currentComp.decrementValue(objName, currentComp.strName)
                            break
                        }
                    }
                } else {
                    //return
                    //Handling "216" inside menu
                    bKeyFivePressedInsideMenu = true
                    if (bKeyTwoPressedInsideMenu && bKeyOnePressedInsideMenu && bKeyFivePressedInsideMenu && isKeysPressedInSequence) {
                        //To check if any menu is opened, if so on "k" press will acts as enter /select inside the menu
                        isKeysPressedInSequence =  false
                        var groupRef = getGroupRef("VentGroup")
                        if (groupRef.bIsModeMenuCreated) {
                            var ventModeMenu = getCompRef(mainitem, "VentMenuObj")
                            var maxMin = getCompRef(ventModeMenu,"MaximizeMinimize")
                            var closeCompObj = getCompRef(ventModeMenu,"ModeMenuClose")
                            for(var ventModes = 0;ventModes < ventModeMenu.menuModesID.children.length;ventModes++){
                                var currentVentBtn = getCompRef(ventModeMenu.menuModesID,ventModeMenu.menuModesID.children[ventModes].objectName)
                                if(currentVentBtn.state === "Scroll"){
                                    onNavigateLeftMode(currentVentBtn.objectName)
                                    break//mapping to the same function as of keys.onreturnPressed
                                }  else if(closeCompObj.focus === true){
                                    onNavigateLeftMode(closeCompObj.objectName)
                                    break
                                } else if (maxMin.focus === true) {
                                    onNavigateLeftMode(maxMin.objectName)
                                    modeMenuRef.restartTimer()
                                    break
                                }
                            }
                        } else if (groupRef.bIsSettingsMenuCreated) {
                            var paramMenuObj = getCompRef(mainitem, "MoreSettingsMenuObj")
                            var settingsClose = getCompRef(paramMenuObj, "MoreSettingMenuClose")
                            for(var secParamBtn= 0;secParamBtn < paramMenuObj.secParamParentId.children.length;secParamBtn++) {
                                var currentParam = getCompRef(paramMenuObj.secParamParentId,paramMenuObj.secParamParentId.children[secParamBtn].objectName)
                                if(currentParam.state === "Scroll"){
                                    moveAntiClockwise(currentParam.objectName) //mapping to the same function as of key.rightPressed
                                    break
                                } else if (currentParam.state === "Selected" || currentParam.state === "End of Scale") {
                                    currentParam.decrementValue(currentParam.objectName,currentParam.strName)
                                    break
                                } else if (settingsClose.focus === true) {
                                    paramMenuObj.secParamParentId.children[paramMenuObj.secParamParentId.children.length-1].focus = true
                                    break
                                }
                            }
                        }
                    }
                }
            } else {
                if (objName.length === 0) {
                    bKeySixPressed = true;
                } else {
                    currentComp.bKeySixthPressed = true;
                    if (currentComp.bKeySecondPressed && currentComp.bKeyOnePressed && currentComp.bKeySixthPressed && isKeysPressedInSequence) {
                        isKeysPressedInSequence =  false
                        if (currentComp.state === "Scroll" || currentComp.state === "PresetScroll") {
                            onNavigateLeftInVentGroup(objName)
                            break
                        } else {
                            currentComp.decrementValue(objName, currentComp.strName)
                            break
                        }
                    }
                }
                if (bKeyTwoPressed && bKeyOnePressed && bKeySixPressed && isKeysPressedInSequence) {
                    console.log("*****216 pressed for Scrolling in vent group*******")
                    isKeysPressedInSequence =  false
                    if ((ventGroupObj.focus !== true || ventGroupObj.focus === true) && iPreviousIndex === 0) {
                        onNavigateLeftInVentGroup(ventParentId.children[0].objectName)
                    } else {
                        setFocusToChild(iPreviousIndex)
                        iPreviousIndex = 0
                    }
                }
                isKeysPressedInSequence = false
            }
        }
        break;

    case '3':
        if (!arraysIdentical(ventGroupObj.keySequenceList,sequenceList13)) {
            ventGroupObj.keySequenceList.length = 0
            return
        } else {
            if (isAnyMenuOpen()) {
                if (objName === "") {
                    bKeyThreePressedInsideMenu = true
                    if (bKeyOnePressedInsideMenu && bKeyThreePressedInsideMenu) {
                        //To check if any menu is opened, if so on "k" press will acts as enter /select inside the menu
                        handleShortcutSelectInsideMenu()
                    }
                } else {
                    currentComp.bKeyThreePressed = true
                    if (currentComp.bKeyOnePressed && currentComp.bKeyThreePressed) {
                        handleMenuCreationUsingShortcut(objName,currentComp)
                    }
                }
            } else {
                if (objName.length !== 0) {
                    currentComp.bKeyThreePressed = true
                    if (currentComp.bKeyOnePressed && currentComp.bKeyThreePressed) {
                        handleMenuCreationUsingShortcut(objName,currentComp)
                    }
                }
            }
        }
        break;
    case 'I':
    case 'A':
        var isVentkeySelected = checkSelectedState()
        var isVentkeyEOSstate = checkEOSState()
        if (isVentkeySelected  || isVentkeyEOSstate){
            checkForShortcutPressed(currentKey)

        } else{
            console.log("FROM A Pressed")
            fromGroups(currentKey)
        }
        break
    case 'p':
    case 'P':
        console.log("shortcutPressMain!! -  case P - SIMVPCV backup")
        var currentMode = getCurrentMode();
        if(currentMode === "BtnMode_VCV" || currentMode === "BtnMode_PCV" || currentMode === "BtnMode_SIMVVCV"){
            return;
        }
        if(currentMode === "BtnMode_PSVPro" )
        {
            ventGroupObj.bIsBackModeTriggered = true ;
            updatedModeLabelSIMVPCV();
            changeModeToSIMVPCV();
        }

        break

    case 'q':
    case 'Q':
        console.log("shortcutPressMain!! -  case Q - PSVPro backup")
        currentMode = getCurrentMode();
        if(currentMode === "BtnMode_VCV" || currentMode === "BtnMode_PCV" || currentMode === "BtnMode_SIMVVCV"){
            return;
        }
        if((currentMode === "BtnMode_SIMVPCV") &&   ventGroupObj.bIsBackModeTriggered === true){
            updatedModeLabelPSVPro();
            changeModeToPSVPro();
        }
        break;
    }
}

function checkScrollState() {
    var scrollObjectName = ""
    for(var i=0;i<ventParentId.children.length;i++){
        if (ventParentId.children[i].state === "Scroll" || ventParentId.children[i].state === "PresetScroll") {
            scrollObjectName = ventParentId.children[i].objectName
            if (i !== 0 && i !== 5){
                ventParentId.children[i].strVentPreviousState = ventParentId.children[i].state
            }
            break;
        }
    }
    return scrollObjectName
}

function checkEOSState() {
    var eosObjectName
    for(var i=0;i<ventParentId.children.length;i++){
        if (ventParentId.children[i].state === "End of Scale") {
            eosObjectName = true
            break;
        }
    }
    return eosObjectName
}

function checkSelectedState() {
    var selectedObjectState = false
    for(var i=0;i<ventParentId.children.length;i++){
        if (ventParentId.children[i].state === "Selected") {
            selectedObjectState = true;
            break;
        }
    }
    return selectedObjectState
}

function getCurrentFocusedItem() {
    for(var i=0;i<ventParentId.children.length;i++){
        if (ventParentId.children[i].state === "Scroll" || ventParentId.children[i].state === "PresetScroll") {
            break;
        }
    }
    if (i >= ventParentId.children.length) {
        i = ventParentId.children.length - 1
    }
    return i
}

function isAnyMenuOpen() {
    var menusOpen = false
    var ventGrpObj = getGroupRef("VentGroup")
    if (ventGrpObj.bIsModeMenuCreated || ventGrpObj.bIsSettingsMenuCreated) {
        menusOpen = true
    }
    return menusOpen
}

function shortcutKeysRelease(currentKey, objName) {

    var key = String.fromCharCode(currentKey)
    console.log("************SHORTCUT RELEASE IN VENT GROUP**********",key)
    var ventGroupObj = getGroupRef("VentGroup")
    var currentComp
    if (objName.length !== 0) {
        currentComp = getCompRef(ventParentId, objectName)
    }
    switch(key) {
    case '2':
        if (isAnyMenuOpen()) {
            if (objName.length !== 0) {
                if (currentComp.bModeChanged) {
                    currentComp.bKeySecondPressed = false
                }
            } else {
                bKeyTwoPressedInsideMenu = false
            }
        } else {
            if (objName === "") {
                bKeyTwoPressed = false;
            } else {
                currentComp.bKeySecondPressed  = false
            }
        }
        break;
    case '1':
        if (isAnyMenuOpen()) {
            if (objName.length !== 0) {
                if (currentComp.bModeChanged) {
                    currentComp.bKeyOnePressed = false
                    bKeyOnePressedInsideMenu = false
                }
                currentComp.bKeyOnePressed = false
            } else {
                bKeyOnePressedInsideMenu = false
                //currentComp.bKeyOnePressed = false
            }
            break
        } else {
            if (objName.length === 0) {
                if (bKeyTwoPressed) {
                    bKeyOnePressed = false
                    bKeyOnePressedInsideMenu = false
                } else {
                    bKeyOnePressedInsideMenu = false
                }
                // currentComp.bKeyOnePressed = false
                bKeyOnePressedInsideMenu = false
            } else {
                currentComp.bKeyOnePressed = false
                bKeyOnePressedInsideMenu = false
            }
        }
        ventGroupObj.keySequenceList.length = 0
        break;
    case '5':
        if (isAnyMenuOpen()) {
            if (objName.length !== 0) {
                if (currentComp.bModeChanged) {
                    currentComp.bKeyFifthPressed = false
                }
            } else {
                bKeyFivePressedInsideMenu = false
            }
        } else {
            if (objName === "") {
                bKeyFivePressed = false;
            } else {
                currentComp.bKeyFifthPressed = false
            }
        }
        break;
    case '6':
        if (isAnyMenuOpen()) {
            if (objName.length !== 0) {
                if (currentComp.bModeChanged) {
                    currentComp.bKeySixthPressed = false
                }
            } else {
                bKeySixPressedInsideMenu = false
            }
        } else {
            if (objName === "") {
                bKeySixPressed = false;
            } else {
                currentComp.bKeySixthPressed = false
            }
        }
        break
    case '3':
        if (objName === "") {
            bKeyThreePressedInsideMenu = false
        } else {
            currentComp.bKeyThreePressed = false
        }
        break;
    }
}

function destroyMenus(currentKey, objName) {
    var key = String.fromCharCode(currentKey)
    var currentComp
    if (objName.length !== 0) {
        currentComp = getCompRef(ventParentId, objName)
        if (currentComp.bModeChanged && (key == 'c' || key =='C')) {
            return
        }
    }
    var ventGrpObj = getGroupRef("VentGroup")
    if (ventGrpObj.bIsModeMenuCreated) {
        var isAPressed = true
        if(key == 'A' || key =='a'){
            destroyVentMenu(isAPressed);
        }
        else{
            destroyVentMenu()
        }
    }

    if (ventGrpObj.bIsSettingsMenuCreated){
        destroyMoreSettingMenu()
    }
}

//*******************************************
// Purpose: Handles shortcut("13"/"k") inside the menu(selecting sec param/ selecting mode)
//***********************************************/
function handleShortcutSelectInsideMenu() {

    //To check if any menu is opened, if so on "k" press will acts as enter /select inside the menu
    var ventGrpRef = getGroupRef("VentGroup")
    console.log("Is menu open???",ventGrpRef.bIsModeMenuCreated)
    if (ventGrpRef.bIsModeMenuCreated && isModeMenuCreatedUsingK === false) {
        console.log(">>>VENT MODE MENU OPEN<<<<<<<")
        var modeMenuObj = getCompRef(mainitem, "VentMenuObj")
        var maxMinRef = getCompRef(modeMenuObj,"MaximizeMinimize")
        var closeBtnRef = getCompRef(modeMenuObj,"ModeMenuClose")
        for(var i=0;i<modeMenuObj.menuModesID.children.length;i++){
            var siblingObj = getCompRef(modeMenuObj.menuModesID,modeMenuObj.menuModesID.children[i].objectName)
            if(siblingObj.focus === true){
                onSelectMode(siblingObj.objectName, siblingObj.strModeName) //mapping to the same function as of keys.onreturnPressed
            }
        }
        if(closeBtnRef.focus === true){
            inModeMenuEnterPressed(closeBtnRef.objectName)
        } else if (maxMinRef.focus === true) {
            inModeMenuEnterPressed(maxMinRef.objectName)
            modeMenuObj.restartTimer()
        }
    } else if (ventGrpRef.bIsSettingsMenuCreated && isSettingMenuCreatedUsingK === false) {
        console.log(">>>SETTINGS MENU OPEN<<<<<<<")
        var settingMenuObj = getCompRef(mainitem, "MoreSettingsMenuObj")
        var closeCompRef = getCompRef(settingMenuObj, "MoreSettingMenuClose")
        for(var comp = 0;comp < settingMenuObj.secParamParentId.children.length;comp++) {
            var currentObj = getCompRef(settingMenuObj.secParamParentId,settingMenuObj.secParamParentId.children[comp].objectName)
            if(currentObj.state === "Scroll" || currentObj.state === "Selected" || currentObj.state === "End of Scale"){
                onVentSecParamEnterPressed(currentObj.objectName) //mapping to the same function as of keys.onreturnPressed
            }
        }
        if (closeCompRef.focus === true) {
            destroyMoreSettingMenu()
            settingMenuObj.restartTimer()
        }
    }
}

//*******************************************
// Purpose: his function handles the creation of Vent mode menu/more settings menu for shortcut key "k"/"13" press
// (based on which btn(ventmodeBtn/moresettingsBtn) has focus
//***********************************************/
function handleMenuCreationUsingShortcut(objName,currentComp) {

    var selectedStateObj = checkSelectedState()
    var eosStateObj = checkEOSState()
    if (selectedStateObj) {
        console.log("selectedStateObj->>>>",selectedStateObj)
        onEnterPressed(objName)
        currentComp.bKeyOnePressed = false
    } else if (eosStateObj) {
        onEnterPressed(objName)
        currentComp.bKeyOnePressed = false
    } else {
        var scrollStateObj = checkScrollState()
        if (scrollStateObj !== "") {
            if (scrollStateObj === "ventModeBtn") {
                var ventModeObj = getCompRef(ventParentId, scrollStateObj)
                ventModeObj.strVentPreviousState = ventModeObj.state
                createVentModeMenu(ventModeObj.strVentMenuType)
                onVentModeEnterPressed()
                isModeMenuCreatedUsingK = true
                //to make sure that mode btn remains in preset mode /active mode when mode menu is opened
                if (ventModeObj.strVentState === "Vent") {
                    ventModeObj.state = "Active"
                } else if (ventModeObj.strVentState === "Preset"){
                    ventModeObj.state = "Preset"
                }
            } else if (scrollStateObj === "MoreSettingBtn") {
                var moreMenuObj = getCompRef(ventParentId, scrollStateObj)
                moreMenuObj.strVentPreviousState = moreMenuObj.state
                createMoreSettingMenu()
                isSettingMenuCreatedUsingK = true
                //to make sure that mode btn remains in preset mode /active mode when more settings menu is opened
                if (moreMenuObj.strVentState === "Vent") {
                    moreMenuObj.state = "Active"
                } else if (moreMenuObj.strVentState === "Preset"){
                    moreMenuObj.state = "Preset"
                }
            } else {
                onEnterPressed(scrollStateObj)
                currentComp.bKeyOnePressed = false
            }
        }
    }
    if (currentComp !== "undefined") {
        currentComp.bKeyOnePressed = false
    }
}

//**********************SWITCHING BETWEEN VENT AND BAG ***********************
//*******************************************
// Purpose: Switching between ventilation On/Off based on the switching in CSB
// Input Parameters: vent state to be set(Vent/Bag)
// Description: Based on the data emitted by the CSB , the vent status will be changed and accordingly
// the vent components states will be set
//***********************************************/
function switchVentToBag(ventState) {
    console.log("switchVentToBag->>>>.>",ventState)
    var ventGrpObj = getGroupRef(grpName)
    var length = ventGrpObj.ventParentId.children.length;
    // To make sure that any user action is cancelled before switching (bag/vent) modes
    if (ventGrpObj.bIsModeMenuCreated) {
        destroyVentMenu();
    }
    if (ventGrpObj.bIsSettingsMenuCreated) {
        destroyMoreSettingMenu()
    }
    var qkRef = getCompRef(ventGrpObj.ventParentId,"quickKey1")
    // if mode change is inprogress and user switches (bag/vent) that action needs to be cancelled
    // and reverted to previous mode
    if (qkRef.bModeChanged) {
        onCancellingModeConfirmation()
    }
    for (var comp =0 ;comp <length;comp++) {
        if (ventState === "Bag") {
            console.log("------Vent Off------")
            ventGrpObj.ventParentId.children[comp].strVentState = "Bag"
            changeState(ventGrpObj.ventParentId.children[comp],"Preset")
            var ventModeReff = getCompRef(ventGrpObj.ventParentId,"ventModeBtn")
            ventModeReff.strVentilationStatus = ventModeReff.strVentStatusOff
            ventModeReff.strVentswitch = "Off"
        } else if (ventState === "Vent") {
            console.log("------Vent On------")
            ventGrpObj.ventParentId.children[comp].strVentState = "Vent"
            changeState(ventGrpObj.ventParentId.children[comp],"Active")
            var ventModeRef = getCompRef(ventGrpObj.ventParentId,"ventModeBtn")
            ventModeRef.strVentilationStatus = ventModeRef.strVentStatusOn
            ventModeRef.strVentswitch = "On"
        }
        if (comp !== 0 && comp !== 5){
            ventGrpObj.ventParentId.children[comp].strValue = ventGrpObj.ventParentId.children[comp].strVentPreviousVal
            ventGrpObj.ventParentId.children[comp].bIsTimerRunning = false
            ventGrpObj.ventParentId.children[comp].bIsTotalTimerRunning = false
            ventGrpObj.ventParentId.children[comp].stopTimers()
        }
    }
    ventGrpObj.focus = true
}


function changeModeToSIMVPCV()
{
    console.log("Backup mode SIMVPCV : ");
    objCSBValue.writeVentModeToCSB("BtnMode_SIMVPCV")
    settingReader.writeDataInFile("MODE","BtnMode_SIMVPCV")
    backupData.storeCriticalParams("MODE", "BtnMode_SIMVPCV")
    var ventGrpObj = getGroupRef("VentGroup")
    ventGrpObj.selectedMode = "SIMVPCV"
    ventGrpObj.bCSBTrigger = true
    ventGrpObj.backmode("SIMVPCV")

    if (ventGrpObj.bIsSettingsMenuCreated) {
        destroyMoreSettingMenu()
    } else if (ventGrpObj.bIsModeMenuCreated) {
        destroyVentMenu();
    }

    ventGrpObj.focus = true;
}

function updatedModeLabelSIMVPCV() {
    console.log("Backup mode SIMVPCV Mode: ");
    var ventGrpObj = getGroupRef("VentGroup")
    var ventmodeRef =  getCompRef(ventGrpObj.ventParentId, "ventModeBtn")
    ventmodeRef.state = "Active"
    ventmodeRef.strModeLabel = "BtnMode_SIMVPCV";
}

function changeModeToPSVPro() {
    console.log("Current mode is :" + getCurrentMode())
    objCSBValue.writeVentModeToCSB("BtnMode_PSVPro")
    settingReader.writeDataInFile("MODE","BtnMode_PSVPro")
    backupData.storeCriticalParams("MODE", "BtnMode_PSVPro")
    var ventGrpObj = getGroupRef("VentGroup")
    ventGrpObj.selectedMode = "PSVPro"
    ventGrpObj.backmode("PSVPro")

    if (ventGrpObj.bIsSettingsMenuCreated) {
        destroyMoreSettingMenu()
    } else if (ventGrpObj.bIsModeMenuCreated) {
        destroyVentMenu()
    }
    ventGrpObj.focus = true;
}

function updatedModeLabelPSVPro() {
    var ventGrpObj = getGroupRef("VentGroup")
    var ventmodeRef =  getCompRef(ventGrpObj.ventParentId, "ventModeBtn")
    ventmodeRef.state = "Active"
    ventmodeRef.strModeLabel = "BtnMode_PSVPro";
}

function getCurrentMode() {
    var ventGrpObj = getGroupRef("VentGroup")
    var ventmodeRef =  getCompRef(ventGrpObj.ventParentId, "ventModeBtn")
    return (ventmodeRef.strModeLabel)
}

function checkForShortcutPressed(currentKey) {
    var ventModeRef = getCompRef(ventParentId,"ventModeBtn")
    var moreSettingRef = getCompRef(ventParentId,"MoreSettingBtn")
    for(var i = 1; i<ventParentId.children.length-1; i++) {
        var currentComp = getCompRefByIndex(ventParentId, i)
        if( i == 1 && (ventParentId.children[1].state === "Selected" || ventParentId.children[1].state === "End of Scale") && ventParentId.children[1].bModeChanged === true) {
            onOtherGrpTouched()
            if (!ventParentId.children[1].bModeChanged) {
                if (currentKey === 65) {
                    fromGroups(Qt.Key_A);
                } else if (currentKey === 71) {

                    moreSettingRef.strVentPreviousState = moreSettingRef.state
                    createMoreSettingMenu()
                } else if (currentKey === 86)  {
                    ventModeRef.strVentPreviousState = ventModeRef.state
                    createVentModeMenu(ventModeRef.strVentMenuType)
                    onVentModeEnterPressed()
                }
                else{
                    console.log("Invalid Key Pressed")
                }
            }
        }
        if((currentComp.state === "Selected" || currentComp.state === "End of Scale")
                && currentComp.bIsValueChanged === true && currentComp.bInitialInterruptedStateStarted === false && currentComp.bModeChanged === false){
            console.log("If Value Changed and Not Interupted")
            currentComp.startBlinking()
            //currentComp.bIsValueChanged = false
            currentComp.bInitialInterruptedStateStarted = true
        } else if((currentComp.state === "Selected" || currentComp.state === "End of Scale")
                  && currentComp.bIsValueChanged === false && currentComp.bInitialInterruptedStateStarted === false){
            console.log("If Value Not Changed Not Interupted")
            currentComp.strValue = currentComp.strVentPreviousVal;
            if (currentKey === 65) {
                fromGroups(Qt.Key_A);
            } else if (currentKey === 71) {
                moreSettingRef.strVentPreviousState = moreSettingRef.state
                createMoreSettingMenu()
            } else if (currentKey === 86) {
                ventModeRef.strVentPreviousState = ventModeRef.state
                createVentModeMenu(ventModeRef.strVentMenuType)
                onVentModeEnterPressed()
            }
            else{
                console.log("Invalid Key Pressed")
            }
            currentComp.stopInitialTimer()
        } else if ((currentComp.state === "Selected" || currentComp.state === "End of Scale")
                   && currentComp.bIsValueChanged === true && currentComp.bInitialInterruptedStateStarted === true) {
            currentComp.strValue = currentComp.strVentPreviousVal;
            if (currentKey === 65) {
                console.log("If Value is Changed and Interupted ")
                currentComp.stopBlinking()
                fromGroups(Qt.Key_A);
            } else if (currentKey === 71) {

                moreSettingRef.strVentPreviousState = moreSettingRef.state
                currentComp.stopBlinking()
                createMoreSettingMenu()
            } else if (currentKey === 86){
                ventModeRef.strVentPreviousState = ventModeRef.state
                currentComp.stopBlinking()
                createVentModeMenu(ventModeRef.strVentMenuType)
                onVentModeEnterPressed()
            }else{
                console.log("Invalid Key Pressed")
            }
            currentComp.stopInitialTimer()
        }
    }
}

//*******************************************
// Purpose: To check if the arrays are identical (for checking correct key sequence)
// to perform shortcut key handling
// Input Parameters: Actual key sequence and the key sequence pressed
//***********************************************/
function arraysIdentical(actualSequence, storedSequence) {
    console.log("actualSequence->>",actualSequence)
    console.log("storedSequence->>",storedSequence)
    var length = actualSequence.length;
    if (length !== storedSequence.length) {
        return false;
    }
    while (length--) {
        if (parseInt(actualSequence[length]) !== parseInt(storedSequence[length])) {
            return false;
        }
    }
    return true;
}

//*******************************************
// Purpose: Handles setting the focus to the current group's first element when
// right rotated from the previous group's last element
//***********************************************/
function groupChangeOnRight() {
    console.log("VentGroupController->>>Right")
    var ventGrpRef = getGroupRef("VentGroup")
    var modeBtnRef = getCompRef(ventGrpRef.ventParentId,"ventModeBtn")
    modeBtnRef.focus = true
}

//*******************************************
// Purpose: Handles setting the focus to the current group's last element when
// left rotated from the previous group's first element
//***********************************************/
function groupChangeOnLeft() {
    console.log("VentGroupController->>>Left")
    var ventGrpRef = getGroupRef("VentGroup")
    var moreBtnRef = getCompRef(ventGrpRef.ventParentId,"MoreSettingBtn")
    moreBtnRef.focus = true
}


/**************************************************
  Purpose:Sets which group is clicked to open checkout menu
  Description:When the other group is clicked , do not open checkout menu,open only on
             clicking  the settings icon
  *************************************************/
function whichGroupIsClicked(groupName) {
    console.log("checkoutButtonClicked called :")
    console.log("ObjectName is :" + objectName)
   touchedGroupName = groupName ;
}
