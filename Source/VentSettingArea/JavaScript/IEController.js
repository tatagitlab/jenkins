/* fetch the range list and the step size for I:E */

var ieRangeList;
var ie_min_step1;
var ie_max_step1;
var ie_stepSize1;
var ie_min_step_pt5;
var ie_max_step_pt5;
var ie_stepSizept5;
var iemin = 0.5;
var iemax = 8;
ieRangeList = physicalparam.getRangeData("I:E", "");
console.log("list...IE"+ieRangeList)
ie_min_step1 = ieRangeList[0]
ie_max_step1 = ieRangeList[1]
ie_stepSize1 = ieRangeList[2]
ie_min_step_pt5 = ieRangeList[3]
ie_max_step_pt5 = ieRangeList[4]
ie_stepSizept5 = ieRangeList[5]

/* function to fetch the updated min and max values after the contraint check when the IE quick key is selected*/

function onIESelection() {
    console.log("onIESelection!!! ")
    ie_min_step1 = ObjCCValue.getFinalIEMIN()
    ie_max_step_pt5 = ObjCCValue.getFinalIEMAX()
}

/*Increment step Logic for I:E*/

function clkHandleIE(m_insExp) {

    var finalIEValue;
    var inspiration;
    var expiration;
    var splitValue = m_insExp.split(":")
    inspiration = splitValue[0]
    expiration = splitValue[1]
    if(inspiration === "1") {
        expiration = parseFloat(expiration)+ parseFloat(ie_stepSizept5)
    } else if(inspiration === "2") {
        inspiration = parseInt(inspiration)-parseInt(ie_stepSize1)
    }
    finalIEValue = inspiration + ":"+expiration
    return finalIEValue
}

/*Decrement step Logic for I:E*/

function antClkHandleIE(m_insExp) {

    var finalIEValue;
    var inspiration;
    var expiration;
    var splitValue = m_insExp.split(":")
    inspiration = splitValue[0]
    expiration = splitValue[1]
    if(inspiration === "1" && expiration === "1") {
        inspiration = parseFloat(inspiration)+ parseInt(ie_stepSize1)
    }
    if(expiration !== "1") {
        expiration = parseFloat(expiration) - parseFloat(ie_stepSizept5)
    }
    finalIEValue = inspiration + ":"+expiration
    return finalIEValue
}

//*******************************************
// Purpose: handler for state change during value increment of I:E
// Input Parameters: object name of the quickey - I:E
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onIEIncrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("IE: clicked on widget for increment:"+currentVentComp+strName+selectedMode)
    if(objectName === "quickKey3" && (selectedMode === "VCV" || selectedMode === "PCV")){
        console.log("inside increment IE")
        var numerator = parseInt(currentVentComp.strValue.split(":")[0])
        var denominator = parseFloat(currentVentComp.strValue.split(":")[1])
        var ratio_fraction = parseFloat(numerator/denominator)
        var temp_maxfraction = parseFloat(1/8);
        var numerator_max = parseFloat(ie_max_step_pt5.split(":")[0])
        var denominator_max = parseFloat(ie_max_step_pt5.split(":")[1])
        temp_maxfraction = parseFloat(numerator_max/denominator_max)

        if(ratio_fraction > temp_maxfraction && (currentVentComp.state === "Selected" || currentVentComp.state === "End of Scale")){
            currentVentComp.strValue = clkHandleIE(currentVentComp.strValue)
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
            currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of I:E
// Input Parameters: object name of the quickey - I:E
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onIEDecrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("IE: clicked on widget for decrement:"+currentVentComp+strName+selectedMode)
    if(objectName === "quickKey3" && (selectedMode === "VCV" || selectedMode === "PCV")){
        console.log("inside decrement IE")
        var numerator = parseInt(currentVentComp.strValue.split(":")[0])
        var denominator = parseFloat(currentVentComp.strValue.split(":")[1])
        var ratio_fraction = parseFloat(numerator/denominator)
        var temp_minfraction = parseFloat(2/1)
        var numerator_min = parseFloat(ie_min_step1.split(":")[0])
        var denominator_min = parseFloat(ie_min_step1.split(":")[1])
        temp_minfraction = parseFloat(numerator_min/denominator_min)
        if(ratio_fraction < temp_minfraction) {
            currentVentComp.strValue = antClkHandleIE(currentVentComp.strValue)
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
            currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - I:E
//***********************************************/

function onIEEndOfScale(objectName) {

    console.log("checking end of scale IE")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if(objectName === "quickKey3" && (selectedMode === "VCV" || selectedMode === "PCV")){

        if(currentVentComp.strValue === ie_min_step1 && currentVentComp.strValue === ie_max_step_pt5) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled
        } else if(currentVentComp.strValue === ie_min_step1) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled
        } else if(currentVentComp.strValue === ie_max_step_pt5) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        } else if(currentVentComp.strValue !== ie_min_step1 && currentVentComp.strValue !== ie_max_step_pt5) {
            currentVentComp.strQuickKeyUpArrowSource=currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - I:E
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetIEUpArrowBgColor(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting up arrow bg IE"+currentVentComp.strValue)
    if(objectName === "quickKey3" && (selectedMode === "VCV" || selectedMode === "PCV")){
        if(currentVentComp.strValue === ie_max_step_pt5) {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - I:E
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetIEDownArrowBgColor(objectName) {

    console.log("setting down arrow bg IE")
    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting down arrow bg IE"+currentVentComp.strValue)
    if(objectName === "quickKey3" && (selectedMode === "VCV" || selectedMode === "PCV")){
        if(currentVentComp.strValue === ie_min_step1) {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler gets called when the mode changes to connect I:E controller to the corresponding quick key widget based on mode
// Input Parameters: current mode
// Description: takes the current mode as input and based on the input connects the I:E controller to the corresponding widget
//***********************************************/

function onModeChangedIE(currentMode) {

    var objref;
    console.log("current mode in IE ctrl",currentMode)
    if((currentMode === "VCV") || currentMode === "PCV") {
        var ieValue = settingReader.read("IE_qKey")
        objref=getCompRef(ventParentId,"quickKey3");
        objref.setLabelofWidget("IE_qKey");
        objref.setUnitofWidget("NA_unit")
        backupData.validateParams("IE_qKey",ieValue)
        objref.setQKValue(ieValue)
        ObjCCValue.setCurrentMode(currentMode)
        ObjCCValue.updateConstraintParam("IE_qKey",ieValue)
        objref.setUniqueID("qkIE_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        objref.bIsValueChanged = false
    }
}
