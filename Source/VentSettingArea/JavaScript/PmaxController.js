/* fetch the range list and the step size for pmax */

var pmaxRangeList;
var pmaxMin;
var pmaxMax;
var pmaxStepSize;
pmaxRangeList = physicalparam.getRangeData("Pmax", "CmH2o");
console.log("list...Pmax"+pmaxRangeList)
pmaxMin = pmaxRangeList[0]
pmaxMax = pmaxRangeList[1]
pmaxStepSize = pmaxRangeList[2]

/* function to fetch the updated min and max values after the contraint check when the IE quick key is selected*/

function onPmaxSelection() {
    console.log("onPmaxSelection!!! ")
    pmaxMin = ObjCCValue.getFinalPmaxMIN()
    pmaxMax = ObjCCValue.getFinalPmaxMAX()
}

/*Increment step logic for Pmax*/

function clkHandlPmax(iPmax) {

    console.log("range inside function"+pmaxRangeList)
    var strPpeakLow = settingReader.read("Ppeak_Low")
    console.log(strPpeakLow,pmaxMin,iPmax)
    if (parseInt(strPpeakLow) < parseInt(pmaxMin)) {
        strPpeakLow = pmaxMin
    }
    if((iPmax >= parseInt(strPpeakLow)) && (iPmax < parseInt(pmaxMax))) {
        iPmax += parseInt(pmaxStepSize) ;
    }
    return iPmax;
}


/*Decrement step logic for Pmax*/

function antClkHandlPmax(iPmax) {

    var strPpeakLow = settingReader.read("Ppeak_Low")
    if (parseInt(strPpeakLow) < parseInt(pmaxMin)) {
        strPpeakLow = pmaxMin
    }
    if((iPmax > parseInt(strPpeakLow)) && (iPmax <= parseInt(pmaxMax))) {
        iPmax -= parseInt(pmaxStepSize) ;
    }
    return iPmax;
}

//*******************************************
// Purpose: handler for state change during value increment of Pmax
// Input Parameters: object name of the quickey - Pmax
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onPmaxIncrementValue(objectName, strName) {

    console.log("inside inc")
    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("Pmax: clicked on widget for increment:"+objectName+currentSecParam+strName+selectedMode)
    if((objectName === "secParam0" && (selectedMode === "VCV" || selectedMode === "PCV" )) || (objectName === "secParam3" && (selectedMode === "SIMVVCV"  || selectedMode === "PSVPro")) ||(objectName === "secParam2" && selectedMode === "SIMVPCV")) {
        console.log("inside increment PMax")
        if(currentSecParam.strValue < parseInt(pmaxMax)){
            currentSecParam.strValue= parseInt(clkHandlPmax(parseInt(currentSecParam.strValue)))
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of Pmax
// Input Parameters: object name of the quickey - Pmax
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onPmaxDecrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("Pmax: clicked on widget for decrement:"+objectName+currentSecParam+strName+selectedMode)
    var pmaxValue = parseInt(currentSecParam.strValue)
    if((objectName === "secParam0" && (selectedMode === "VCV" || selectedMode === "PCV" )) || (objectName === "secParam3" && (selectedMode === "SIMVVCV"  || selectedMode === "PSVPro")) ||(objectName === "secParam2" && selectedMode === "SIMVPCV")) {
        console.log("inside decrement PMax")
        if(currentSecParam.strValue > parseInt(pmaxMin)){
            currentSecParam.strValue= parseInt(antClkHandlPmax(parseInt(currentSecParam.strValue)))
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Pmax
//***********************************************/

function onPmaxEndOfScale(objectName) {

    console.log("checking end of scale Pmax")
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam0" && (selectedMode === "VCV" || selectedMode === "PCV" )) || (objectName === "secParam3" && (selectedMode === "SIMVVCV"  || selectedMode === "PSVPro")) ||(objectName === "secParam2" && selectedMode === "SIMVPCV")) {
        if(parseInt(currentVentComp.strValue) === parseInt(pmaxMin)) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownDisabled
        } else if(parseInt(currentVentComp.strValue) === parseInt(pmaxMax)) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        } else if(parseInt(currentVentComp.strValue) !== parseInt(pmaxMin) && parseInt(currentVentComp.strValue) !== parseInt(pmaxMax)) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Pmax
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetPmaxUpArrowBgColor(objectName) {
    console.log("Pmax bg color"+objectName)
    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting up arrow bg Pmax"+currentVentComp)
    if((objectName === "secParam0" && (selectedMode === "VCV" || selectedMode === "PCV" )) || (objectName === "secParam3" && (selectedMode === "SIMVVCV"  || selectedMode === "PSVPro")) ||(objectName === "secParam2" && selectedMode === "SIMVPCV")) {
        if(parseInt(currentVentComp.strValue) === parseInt(pmaxMax)) {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Pmax
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetPmaxDownArrowBgColor(objectName) {

    console.log("Pmax bg color"+objectName)
    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting down arrow bg Pmax"+currentVentComp.strValue)
    console.log("pmaxMin bg Pmax"+pmaxMin)
    if((objectName === "secParam0" && (selectedMode === "VCV" || selectedMode === "PCV" )) || (objectName === "secParam3" && (selectedMode === "SIMVVCV"  || selectedMode === "PSVPro")) ||(objectName === "secParam2" && selectedMode === "SIMVPCV")) {
        if(parseInt(currentVentComp.strValue) === parseInt(pmaxMin)) {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
