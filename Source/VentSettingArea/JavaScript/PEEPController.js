/* fetch the range list and the step size for PEEP */

var peepRangeList;
var peepMin;
var peepMax;
var peepStepSize;
peepRangeList = physicalparam.getRangeData("PEEP", "cmH2O");
console.log("list...PEEP"+peepRangeList)
peepMin = peepRangeList[0]
peepMax = peepRangeList[1]
peepStepSize = peepRangeList[2]

/* function to fetch the updated min and max values after the contraint check when the IE quick key is selected*/

function onPeepSelection() {
    console.log("onPeepSelection!!! ")
    peepMax = ObjCCValue.getFinalPeepMAX()
    console.log("peepMax",peepMax)
}

/*Increment step Logic for PEEP*/
function clkHandlePeep(m_peep) {

    // var range = peepRangeList
    if(m_peep.localeCompare("Off") !== 0 && (m_peep < parseInt(peepMax))) {
        m_peep =parseInt(m_peep)+parseInt(peepStepSize);
        m_peep = m_peep.toString()
    } else if(m_peep.localeCompare("Off") === 0){
        m_peep =parseInt(peepMin);
    }
    return m_peep
}

/*Decrement step Logic for PEEP*/
function antClkHandlePeep(m_peep) {

    // var range = peepRangeList
    if(m_peep.localeCompare("Off") === 0) return;

    if((m_peep <= parseInt(peepMax)) && (m_peep > parseInt(peepMin))) {
        m_peep -=parseInt(peepStepSize);
        m_peep = m_peep.toString()
    } else if (m_peep === peepMin) {
        m_peep = "Off"
    }
    return m_peep
}

//*******************************************
// Purpose: handler for state change during value increment of I:E
// Input Parameters: object name of the quickey - I:E
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onPEEPIncrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("PEEP: clicked on widget for increment:"+objectName+currentVentComp+strName+selectedMode)
    if(objectName === "quickKey4"){
        console.log("inside increment PEEP")

        if(currentVentComp.strValue < parseInt(peepMax) || currentVentComp.strValue === "Off"){
            currentVentComp.strValue = clkHandlePeep(currentVentComp.strValue)
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
	    currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of I:E
// Input Parameters: object name of the quickey - I:E
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onPEEPDecrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("PEEP: clicked on widget for decrement:"+currentVentComp+strName+selectedMode)
    if(objectName === "quickKey4"){
        console.log("inside decrement PEEP")
        if(currentVentComp.strValue >= parseInt(peepMin)){
            currentVentComp.strValue = antClkHandlePeep(currentVentComp.strValue)
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
	    currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - I:E
//***********************************************/

function onPEEPEndOfScale(objectName) {

    console.log("checking end of scale PEEP")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if(objectName === "quickKey4"){

        if(currentVentComp.strValue === parseInt(peepMax) && currentVentComp.strValue === "Off") {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled
        } else if(currentVentComp.strValue === "Off") {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled

        } else if(parseInt(currentVentComp.strValue) === parseInt(peepMax)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        } else if(currentVentComp.strValue !== "Off" && parseInt(currentVentComp.strValue) !== parseInt(peepMax)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - I:E
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetPEEPUpArrowBgColor(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting up arrow bg PEEP"+currentVentComp.strValue)
    if(objectName === "quickKey4"){
        if(parseInt(currentVentComp.strValue) === parseInt(peepMax)) {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - I:E
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetPEEPDownArrowBgColor(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting down arrow bg PEEP"+currentVentComp.strValue)
    if(objectName === "quickKey4"){
        if(currentVentComp.strValue === "Off") {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler gets called when the mode changes to connect I:E controller to the corresponding quick key widget based on mode
// Input Parameters: current mode
// Description: takes the current mode as input and based on the input connects the I:E controller to the corresponding widget
//***********************************************/

function onModeChangedPEEP(currentMode) {

    var objref;
    console.log("current mode in PEEP ctrl",currentMode)
    if((currentMode === "VCV") || currentMode === "PCV" || currentMode === "SIMVVCV" || currentMode === "SIMVPCV" || currentMode === "PSVPro") {
        var peepValue = settingReader.read("PEEP_qKey")
        objref=getCompRef(ventParentId,"quickKey4");
        objref.setLabelofWidget("PEEP_qKey");
        objref.setUnitofWidget("cmH2O_unit")
        backupData.validateParams("PEEP_qKey",peepValue)
        ObjCCValue.updateConstraintParam("PEEP_qKey",peepValue)
        objref.setQKValue(peepValue)
        objref.setUniqueID("qkPEEP_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        objref.bIsValueChanged = false
    }    
}
