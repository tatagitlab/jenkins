/* fetch the range list and the step size for RR Mech */

var rrMechRangeList;
var rrMechMin;
var rrMechMax;
var rrMechStepSize;
rrMechRangeList = physicalparam.getRangeData("RRMech", "/min");
rrMechMin = rrMechRangeList [0]
rrMechMax = rrMechRangeList[1]
rrMechStepSize = rrMechRangeList[2]

/* function to fetch the updated min and max values after the contraint check when the IE quick key is selected*/

function onRRMechSelection() {
    if ( selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" || selectedMode === "PSVPro" ) {
        console.log("onSpontRRSelection!!! ")
        rrMechMin = ObjCCValue.getFinalSpontRRMIN()
        rrMechMax = ObjCCValue.getFinalSpontRRMAX()
    }
}

/*Increment step Logic for RRMech*/

function clkHandleMechRespiration(m_mechResp) {

    console.log("rangelist inisde rr mech increment"+rrMechRangeList)
    if((m_mechResp>=parseInt(rrMechMin)) && (m_mechResp < parseInt(rrMechMax))) {
        m_mechResp += parseInt(rrMechStepSize) ;
    }
    return m_mechResp;
}

/*Decrement step Logic for RR*/

function antClkMechRespiration(m_mechResp) {

    if((m_mechResp>parseInt(rrMechMin)) && (m_mechResp <= parseInt(rrMechMax))) {
        m_mechResp -= parseInt(rrMechStepSize) ;
    }
    return m_mechResp;
}

//*******************************************
// Purpose: handler for state change during value increment of RR Mech
// Input Parameters: object name of the quickey - RR Mech
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onRRMechIncrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("RR: clicked on widget for increment:"+currentVentComp+strName+selectedMode)
    if((objectName === "quickKey3" && selectedMode === "PSVPro") || (objectName === "quickKey2" && selectedMode === "SIMVVCV") || (objectName === "quickKey2" && selectedMode === "SIMVPCV")){
        console.log("inside increment RRMech")
        if(parseInt(currentVentComp.strValue) < parseInt(rrMechMax)){
            currentVentComp.strValue = parseInt(clkHandleMechRespiration(parseInt(currentVentComp.strValue)))
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
	    currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
        var ventmodeObj =  getCompRef(ventParentId, "VentModeBtn")
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of RR Mech
// Input Parameters: object name of the quickey - RR Mech
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onRRMechDecrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    if((objectName === "quickKey3" && selectedMode === "PSVPro") || (objectName === "quickKey2" && selectedMode === "SIMVVCV") || (objectName === "quickKey2" && selectedMode === "SIMVPCV")){
        if(parseInt(currentVentComp.strValue) > parseInt(rrMechMin) ){
            currentVentComp.strValue = parseInt(antClkMechRespiration(parseInt(currentVentComp.strValue)))
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
	    currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
        var ventmodeObj =  getCompRef(ventParentId, "VentModeBtn")
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - RR Mech
//***********************************************/

function onRRMechEndOfScale(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    if((objectName === "quickKey3" && selectedMode === "PSVPro") || (objectName === "quickKey2" && selectedMode === "SIMVVCV") || (objectName === "quickKey2" && selectedMode === "SIMVPCV")){

        if(parseInt(currentVentComp.strValue) === parseInt(rrMechMin) && parseInt(currentVentComp.strValue) === parseInt(rrMechMax)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled
        } else if(parseInt(currentVentComp.strValue) === parseInt(rrMechMin)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled
        } else if(parseInt(currentVentComp.strValue) === parseInt(rrMechMax)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        } else if(parseInt(currentVentComp.strValue) !== parseInt(rrMechMin) && parseInt(currentVentComp.strValue) !== parseInt(rrMechMax)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - RR Mech
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetRRMechUpArrowBgColor(objectName) {

    console.log("setting up arrow bg RRMech")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if((objectName === "quickKey3" && selectedMode === "PSVPro") || (objectName === "quickKey2" && selectedMode === "SIMVVCV") || (objectName === "quickKey2" && selectedMode === "SIMVPCV")){
        if(parseInt(currentVentComp.strValue) === parseInt(rrMechMax)) {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - RR Mech
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetRRMechDownArrowBgColor(objectName) {

    console.log("setting up arrow bg RRMech")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if((objectName === "quickKey3" && selectedMode === "PSVPro") || (objectName === "quickKey2" && selectedMode === "SIMVVCV") || (objectName === "quickKey2" && selectedMode === "SIMVPCV")){
        if(parseInt(currentVentComp.strValue) === parseInt(rrMechMin)) {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler gets called when the mode changes to connect RR Mech controller to the corresponding quick key widget based on mode
// Input Parameters: current mode
// Description: takes the current mode as input and based on the input connects the RR Mech controller to the corresponding widget
//***********************************************/

function onModeChangedRRMech(currentMode) {
    var objref;
    console.log("onModeChanged current mode :" + currentMode)
    var rrMechValue = settingReader.read("RRMech_qKey")
    console.log("rrMechValue :" + rrMechValue )
    if(currentMode === "PSVPro") {
        objref=getCompRef(ventParentId,"quickKey3");
        objref.setLabelofWidget("RRMech_qKey");
        objref.setUnitofWidget("min_unit")
      //  backupData.validateParams("RRMech_qKey",rrMechValue)
        objref.setQKValue(rrMechValue)
        objref.setUniqueID("qkRRMech_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        objref.bIsValueChanged = false
    } else if(currentMode === "SIMVVCV" || currentMode === "SIMVPCV") {
        objref=getCompRef(ventParentId,"quickKey2");
        objref.setLabelofWidget("RRMech_qKey");
        objref.setUnitofWidget("min_unit")
      //  backupData.validateParams("RRMech_qKey",rrMechValue)
        objref.setQKValue(rrMechValue)
        objref.setUniqueID("qkRRMech_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        objref.bIsValueChanged = false
    }

    ObjCCValue.setCurrentMode(currentMode)
    if(selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" || selectedMode === "PSVPro"){
        ObjCCValue.updateConstraintParam("RRMech_qKey",rrMechValue)
    }

}
