#ifndef LAYOUTDOMREADER_H
#define LAYOUTDOMREADER_H

#include <QObject>
#include <QDomDocument>
#include <QDomElement>
#include <QString>

//This class is used to read XML contents for the layout
class LayoutDomReader : public QObject
{
    Q_OBJECT
public:
    LayoutDomReader(bool errorStatus = false ,QString errorString = "no error");
    Q_INVOKABLE void init(QString xmlfilepath, QString schemapath);
    Q_INVOKABLE int getGroupLength ();
    Q_INVOKABLE QString getGroupName (int index);
    Q_INVOKABLE QString getGroupLayoutDataByTag (QString type, QString groupparam, QString groupattribute = "");
    Q_INVOKABLE QString getErrorString();
    Q_INVOKABLE bool getErrorStat();
    Q_INVOKABLE int getComponentLength(QString type);
    Q_INVOKABLE QString getComponentType (QString type,int index);

private :
    QDomDocument layoutdocument;
    QDomElement layoutelement;
    bool m_errorStat ;
    QString m_errorStr ;

};
#endif // QDOMREADER_H
