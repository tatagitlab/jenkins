#ifndef PHYSICALPARAMDOMREADER_H
#define PHYSICALPARAMDOMREADER_H
#include <QObject>
#include <QString>
#include <QDomDocument>

class PhysicalParamDomReader : public QObject
{
    Q_OBJECT

public:
    PhysicalParamDomReader(bool errorStatus = false, QString errorString = "no error");
    Q_INVOKABLE void init(QString xmlfilepath, QString schemapath);
    Q_INVOKABLE QString getParamData(QString physicalparam, QString param);
    Q_INVOKABLE QString getErrorString();
    Q_INVOKABLE bool getErrorStat();
    Q_INVOKABLE QStringList getRangeData(QString physicalparam, QString pawunit);
    Q_INVOKABLE QString getAlarmData(QString physicalparam, QString alarmcode);
    Q_INVOKABLE QStringList getAlarmInstructionData(QString physicalparam, QString alarmcode);

    QDomDocument physicalparamdocument;
    QDomElement physicalparamelement;
    bool m_errorStat ;
    QString m_errorStr ;
};

#endif // BACKEND_DOM_H
