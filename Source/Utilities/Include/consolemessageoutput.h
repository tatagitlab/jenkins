#ifndef CONSOLEMESSAGEOUTPUT_H
#define CONSOLEMESSAGEOUTPUT_H

#include <QString>
//To support Console logs on Linux, this handler is used.
//Logs will be written in /var/log/messages* path
#if defined(__linux__) || defined(__FreeBSD__)   /* Linux & FreeBSD */
void consoleMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg) ;
QString consoleLogFilePath;
#endif

#endif // CONSOLEMESSAGEOUTPUT_H
