#ifndef COMPONENTDOMREADER_H
#define COMPONENTDOMREADER_H
#include <../Source/Utilities/Include/componentdomreader.h>
#include <QObject>
#include <QString>
#include <QDomDocument>

class ComponentDomReader : public QObject
{
    Q_OBJECT
public:
    ComponentDomReader(bool errorStatus = false, QString errorString = "no error");
    Q_INVOKABLE void init(QString xmlfilepath, QString schemapath);
    Q_INVOKABLE QString getComponentXmlDataByTag(QString styleType, QString type, QString attribute);
    Q_INVOKABLE QString getErrorString();
    Q_INVOKABLE bool getErrorStat();
    Q_INVOKABLE QString getComponentResourceString(QString styleType,QString compType, QString compStrLabel, QString stringId);
    Q_INVOKABLE QString getComponentDimension(QString compType, QString attributeName);
    Q_INVOKABLE QString getComponentStylingBasedOnContext(QString compType,QString context, QString attribute);

private:
    QDomDocument componentdocument;
    bool m_errorStat;
    QString m_errorStr;

};

#endif // COMPONENTDOMREADER_H
