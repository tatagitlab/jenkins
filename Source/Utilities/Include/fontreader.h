#ifndef FONTREADER_H
#define FONTREADER_H
#include <QObject>

class Fontreader :  public QObject
{
    Q_OBJECT
public:
    Fontreader();
    void addGEFontToDB();
    Q_INVOKABLE QString geFontStyle() const ;

private:
    QString dbFont;
};

#endif // FONTREADER_H
