#include "../Source/Utilities/Include/componentdomreader.h"
#include <QtCore>
#include <QtXml/QDomDocument>
#include <QDebug>
#include <QXmlSchema>
#include <QXmlSchemaValidator>

ComponentDomReader::ComponentDomReader(bool errorStatus, QString errorString):m_errorStat(errorStatus),m_errorStr(errorString){

}

/**
 * @brief ComponentDomReader::init - Method to load the xml file and schema validation
 * @param xmlfilepath
 * @param schemapath
 */
void ComponentDomReader::init(QString xmlfilepath, QString schemapath)
{
    try{
        //xml schema validation for the component
        m_errorStat = false;
        m_errorStr = "no error";
        QFileInfo sf(schemapath);
        QString schemaext = sf.suffix();
        QFileInfo xf(xmlfilepath);
        QString xmlext = xf.suffix();
        if (schemaext != "xsd")
        {
            m_errorStr = "not a valid xsd file";
            throw m_errorStr;
        }
        if (xmlext != "xml")
        {
            m_errorStr = "not a valid xml file";
            throw m_errorStr;
        }
        QFile schemafile(schemapath);
        QFile file(xmlfilepath);
        if (!schemafile.exists())
        {
            m_errorStr = "schema file does not exist";
            throw m_errorStr;
        }
        if (!file.exists())
        {
            m_errorStr = "file does not exist";
            throw m_errorStr;
        }
        if (!schemafile.open(QIODevice::ReadOnly))
        {
            m_errorStr = "could not read schema file in parser";
            throw m_errorStr;
        }
        if (!file.open(QIODevice::ReadOnly))
        {
            m_errorStr = "could not read xml file in parser";
            throw m_errorStr;
        }
        QXmlSchema schema;
        schema.load(&schemafile, QUrl::fromLocalFile(schemafile.fileName()));
        if (schema.isValid())
        {
            QXmlSchemaValidator validator (schema);
            if (validator.validate (&file, QUrl::fromLocalFile(file.fileName())))
            {
                file.close();
            }
            else
            {
                m_errorStr = "xml does not confirm with the schema componentStyling";
                throw m_errorStr;
            }
        }
        else
        {
            m_errorStr = "schema is invalid componentStyling";
            throw m_errorStr;
        }

        file.open(QIODevice::ReadOnly);
        if (!componentdocument.setContent(&file))
        {
            m_errorStr = "could not load component xml file in parser";
            file.close();
            throw m_errorStr;
        }
        schemafile.close();
    }
    catch (QString errorStr)
    {
        qDebug()<<errorStr;
        m_errorStat = true;
    }
}

/**
 * @brief ComponentDomReader::getComponentXmlDataByTag - Method to read styling for the individual component by its tag. Returns the styling value of the specified tag.
 * @param styleType
 * @param type - type of the component ex."VentMode"
 * @param attribute - required tag ex."Height"
 * @return componentparam: value of the tag
 */
QString ComponentDomReader::getComponentXmlDataByTag (QString styleType, QString type, QString attribute)
{
    QString componentparam = "";

    QDomNode component;
    try{
        if (!m_errorStat)
        {

            QDomElement docElem = componentdocument.documentElement();
            if (styleType == "Component")
            {
                component = docElem.firstChild().firstChild();
            }
            else if (styleType == "Group")
            {
                QDomNode compStyle = docElem.firstChild();
                component = compStyle.nextSibling().firstChild();
            }
            while(!component.isNull())
            {
                if(component.toElement().attribute("type") == type)
                {
                    componentparam = component.firstChildElement(attribute).text();
                    break;
                }
                component = component.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }

    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }
    return componentparam;
}

/**
 * @brief ComponentDomReader::getErrorString - returns error message whenever there is
 * an error in XML or schema validation
 * @return
 */
QString ComponentDomReader::getErrorString()
{
    return m_errorStr;
}

/**
 * @brief ComponentDomReader::getErrorStat - Returns a boolean value which gives whether an
 * error occurred or not
 * @return
 */
bool ComponentDomReader::getErrorStat()
{
    return m_errorStat;
}

/**
 * @brief ComponentDomReader::getComponentResourceString - Method to read resource string ID for the individual component
 * @param styleType - styling type ed. "Component","Group"
 * @param compType - type of the component eg."VentStatus"
 * @param compStrLabel - Label type eg."Unit"
 * @param stringId - component's label state for which string ID needs to be fetched
 * @return - corresponding resource string ID based on the above inputs
 */
QString ComponentDomReader::getComponentResourceString(QString styleType,QString compType, QString compStrLabel, QString stringId)
{   
    QString resourceString = "";
    QDomNode component;
    try{
        if (!m_errorStat)
        {
            QDomElement docElem = componentdocument.documentElement();
            if (styleType == "Component")
            {
                component = docElem.firstChild().firstChild();
            }
            else if (styleType == "Group")
            {
                QDomNode compStyle = docElem.firstChild();
                component = compStyle.nextSibling().firstChild();
            }

            while(!component.isNull())
            {
                if(component.toElement().attribute("type") == compType)
                {
                    QDomElement compLabel = component.firstChildElement(compStrLabel);
                    QDomNode resourceStr = compLabel.firstChild();
                    while(!resourceStr.isNull())
                    {
                        if (resourceStr.toElement().attribute("id") == stringId){
                            resourceString = resourceStr.firstChildElement().text();
                            break;
                        }
                        resourceStr = resourceStr.nextSibling();
                    }
                }
                component = component.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }

    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }
    return resourceString;
}

/**
 * @brief ComponentDomReader::getComponentDimension - Method to read component's dimension (width and height)
 * @param compType - type of the component eg."ModeMenuButton"
 * @param attributeName - Width/Height
 * @return - corresponding value based on the component and attribute
 */
QString ComponentDomReader::getComponentDimension(QString compType, QString attributeName)
{
    QString dimension = "";
    try{
        if (!m_errorStat)
        {
            QDomElement docElem = componentdocument.documentElement();
            QDomNode component =  docElem.firstChild().firstChild();
            while(!component.isNull())
            {
                if(component.toElement().attribute("type") == compType)
                {
                    dimension = component.toElement().attribute(attributeName);
                    break;
                }
                component = component.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }

    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }
    return dimension;
}

/**
 * @brief ComponentDomReader::getComponentStylingBasedOnContext - Method to read the styling
 * of a component based on the context
 * @param compType - type of the component
 * @param context - context type eg. "Menu","Popup"
 * @param attribute - property that needs to be read eg."Color"
 * @return - corresponding value of the attribute based on the context
 */
QString ComponentDomReader::getComponentStylingBasedOnContext(QString compType, QString context, QString attribute)
{
    QString compStyleParam = "";
    try{
        if (!m_errorStat)
        {
            QDomElement docElem = componentdocument.documentElement();
            QDomNode component =  docElem.firstChild().firstChild();
            while(!component.isNull())
            {
                if(component.toElement().attribute("type") == compType)
                {
                    QDomElement contextLabel = component.firstChildElement("Context");
                    while(!contextLabel.isNull())
                    {
                        if (contextLabel.toElement().attribute("type") == context)
                        {
                            QDomNode contextLbl = contextLabel.firstChild();
                            while(!contextLbl.isNull())
                            {
                                compStyleParam = contextLabel.firstChildElement(attribute).text();
                                break;
                            }
                            contextLbl = contextLbl.nextSibling();
                        }
                        contextLabel = contextLabel.nextSiblingElement();
                    }
                }
                component = component.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }

    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }
    return compStyleParam;
}
