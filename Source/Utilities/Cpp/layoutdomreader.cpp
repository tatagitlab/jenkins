#include "../Source/Utilities/Include/layoutdomreader.h"
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#include <QFile>
#include <QDebug>
#include <QtCore>
#include <QtXml/QDomDocument>

LayoutDomReader::LayoutDomReader (bool errorStatus, QString errorString):m_errorStat(errorStatus),m_errorStr(errorString)
{
}

/**
 * @brief LayoutDomReader::init-Method to load the xml file and schema validation
 * @param xmlfilepath
 * @param schemapath
 */
void LayoutDomReader::init(QString xmlfilepath, QString schemapath)
{

    try{
        //xml schema validation for the layout
        m_errorStat = false;
        m_errorStr = "no error";
        QFileInfo sf(schemapath);
        QString schemaext = sf.suffix();
        QFileInfo xf(xmlfilepath);
        QString xmlext = xf.suffix();
        if (schemaext != "xsd")
        {
            m_errorStr = "not a valid xsd file";
            throw m_errorStr;
        }
        if (xmlext != "xml")
        {
            m_errorStr = "not a valid xml file";
            throw m_errorStr;
        }
        QFile schemafile(schemapath);
        QFile file(xmlfilepath);
        if (!schemafile.exists())
        {
            m_errorStr = "schema file does not exist";
            throw m_errorStr;
        }
        if (!file.exists())
        {
            m_errorStr = "file does not exist";
            throw m_errorStr;
        }
        if (!schemafile.open(QIODevice::ReadOnly))
        {
            m_errorStr = "could not read schema file in parser";
            throw m_errorStr;
        }
        if (!file.open(QIODevice::ReadOnly))
        {
            m_errorStr = "could not read xml file in parser";
            throw m_errorStr;
        }

        QXmlSchema schema;
        schema.load(&schemafile, QUrl::fromLocalFile(schemafile.fileName()));
        if (schema.isValid())
        {
            QXmlSchemaValidator validator (schema);
            if (validator.validate (&file, QUrl::fromLocalFile(file.fileName())))
            {
                file.close();
            }
            else
            {
                m_errorStr = "xml does not confirm with the schema valueLayout";
                throw m_errorStr;
            }
        }
        else
        {
            m_errorStr = "schema is invalid valueLayout";
            throw m_errorStr;
        }
        file.open(QIODevice::ReadOnly);
        if (!layoutdocument.setContent(&file))
        {
            m_errorStr = "could not load component xml file in parser";
            file.close();
            throw m_errorStr;
        }
        layoutelement = layoutdocument.documentElement();
        file.close();
    }
    catch (QString errorStr)
    {
        qDebug()<<errorStr;
        m_errorStat = true;
    }
}

/**
 * @brief LayoutDomReader::getGroupLength - Method to get the length of groups in layout.xml
 * @return length of the groups in layout.xml
 */

int LayoutDomReader::getGroupLength ()
{
    QDomNodeList grouplist;
    int grouplength = 0;
    try{
        if (!m_errorStat)
        {
            grouplist = layoutelement.childNodes();
            grouplength = grouplist.length();
        }
        else
        {
            throw m_errorStr;
        }
    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return grouplength;
}

/**
 * @brief LayoutDomReader::getGroupName - Method to get the name of the group at the position index
 * @param index - position of the group in layout.xml
 * @return - returns the name of the group at the position index
 */

QString LayoutDomReader::getGroupName (int index)
{
    QString groupname;
    try{
        if (!m_errorStat)
        {
            if(!layoutelement.isNull())
            {
                QDomNodeList grouplist = layoutelement.childNodes();
                QDomNode component = grouplist.at(index);
                groupname = component.toElement().attribute("type");
                int groupLen = getGroupLength();
                if(index >= 0 && index <= groupLen-1)
                {
                    qDebug()<<"The index is valid";
                }
                else{
                    m_errorStr = "The index is invalid";
                    throw m_errorStr;
                }


            }
            else
            {
                qDebug()<<"layout xml did not return anything";
            }
        }
        else
        {
            throw m_errorStr;
        }
    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return groupname;

}

/**
 * @brief LayoutDomReader::getGroupLayoutDataByTag - Method to get the dimension for the groups
 * @param type - name of the group
 * @param groupparam - name of the dimension tag
 * @param groupattribute - name of the parameter
 * @return - returns the dimension param value
 */

QString LayoutDomReader::getGroupLayoutDataByTag (QString type, QString groupparam, QString groupattribute)
{
    QString groupvalue =  "";
    try{
        if (!m_errorStat)
        {
            QDomElement group =  layoutelement.firstChildElement("Group");

            while (!group.isNull())
            {
                if (group.attribute("type") == type)
                {
                    QDomElement param = group.firstChildElement(groupparam);

                    groupvalue = param.attribute(groupattribute);
                    break;
                }
                group = group.nextSiblingElement("Group");
            }
        }
        else
        {
            throw m_errorStr;
        }
    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return groupvalue;

}

/**
 * @brief LayoutDomReader::getErrorString - Method to get the error string in case of error in schema validation or file corruption.
 * @return returns the errorString from schema validation or file corruption.
 */

QString LayoutDomReader::getErrorString()
{
    return m_errorStr;
}

/**
 * @brief LayoutDomReader::getErrorStat - Method to get the error status in case of error in schema validation or file corruption.
 * @return returns the error status in case of error in schema validation or file corruption.
 */
bool LayoutDomReader::getErrorStat()
{
    return m_errorStat;
}

/**
 * @brief LayoutDomReader::getComponentLength - Method to get the sub component length of the group
 * @param type - group name
 * @return returns the sub component length of the group
 */

int LayoutDomReader::getComponentLength(QString type)
{
    QDomNodeList component;
    int compLen = 0;
    try{

        if (!m_errorStat)
        {
            QDomElement group =  layoutelement.firstChildElement("Group");

            while (!group.isNull())
            {
                if (group.attribute("type") == type){

                    component = group.elementsByTagName("Component");
                    compLen = component.length();
                    break;
                }
                group = group.nextSiblingElement("Group");
            }
        }
        else
        {
            throw m_errorStr;
        }
    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return compLen;
}

/**
 * @brief LayoutDomReader::getComponentType - Method to get the sub component type
 * @param type - group name
 * @param index - position of the sub component
 * @return - returns the type of the sub component at the position index within the group
 */

QString LayoutDomReader::getComponentType (QString type ,int index)
{
    QDomNodeList component;
    QString compName;

    try{

        if (!m_errorStat)
        {
            QDomElement group =  layoutelement.firstChildElement("Group");

            while (!group.isNull())
            {
                if (group.attribute("type") == type){

                    component = group.elementsByTagName("Component");
                    QDomNode compType = component.at(index);
                    compName = compType.toElement().attribute("type");
                }
                group = group.nextSiblingElement("Group");
            }
        }
        else
        {
            throw m_errorStr;
        }

    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return compName;
}
