#include "../Source/Utilities/Include/checkoutreader.h"

Checkoutreader::Checkoutreader(QObject *parent) : QObject(parent)
{
    m_errorStat = false ;
    m_errorStr = "no error" ;
}

/**
 * @brief checkoutreader::init - Method to validate XML and it's corresponding schema
 * @param xmlfilepath - Where XML file lies
 * @param schemapath - where XML Schema lies
 */
void Checkoutreader::init(QString xmlfilepath, QString schemapath)
{
    qDebug()<<"Inside the init of Checkout calibration";

    try{
        //xml schema validation for the component
        QFileInfo sf(schemapath);
        QString schemaext = sf.suffix();
        QFileInfo xf(xmlfilepath);
        QString xmlext = xf.suffix();
        if (schemaext != "xsd")
        {
            m_errorStr = "not a valid xsd file";
            throw m_errorStr;
        }
        if (xmlext != "xml")
        {
            m_errorStr = "not a valid xml file";
            throw m_errorStr;
        }
        QFile schemafile(schemapath);
        QFile file(xmlfilepath);
        if (!schemafile.exists())
        {
            m_errorStr = "schema file does not exist";
            throw m_errorStr;
        }
        if (!file.exists())
        {
            m_errorStr = "file does not exist";
            throw m_errorStr;
        }
        if (!schemafile.open(QIODevice::ReadOnly))
        {
            m_errorStr = "could not read schema file in parser";
            throw m_errorStr;
        }
        if (!file.open(QIODevice::ReadOnly))
        {
            schemafile.close();
            m_errorStr = "could not read xml file in parser";
            throw m_errorStr;
        }

        QXmlSchema schema;
        schema.load(&schemafile, QUrl::fromLocalFile(schemafile.fileName()));
        if (schema.isValid())
        {
            QXmlSchemaValidator validator (schema);
            if (validator.validate (&file, QUrl::fromLocalFile(file.fileName())))
            {
                file.close();
                schemafile.close();
            }
            else
            {
                file.close();
                schemafile.close();
                m_errorStr = "xml does not confirm with the schema componentStyling";
                throw m_errorStr;
            }
        }
        else
        {
            file.close();
            schemafile.close();
            m_errorStr = "schema is invalid componentStyling";
            throw m_errorStr;
        }
        file.open(QIODevice::ReadOnly);
        if (!checkoutDocument.setContent(&file))
        {
            m_errorStr = "could not load component xml file in parser";
            file.close();
            throw m_errorStr;
        }
        else{
            file.close();
        }

    }
    catch (QString errorStr)
    {

        qDebug()<<"error string inside the checkoutreader is : "<<errorStr;
        m_errorStat = true;
        return;
    }
}

/**
 * @brief checkoutreader::getErrorString - Returns the error string
 * @return
 */
QString Checkoutreader::getErrorString() const
{
    return m_errorStr;
}

/**
 * @brief checkoutreader::getListString - This method is used to return the list
 * @return
 */
QStringList Checkoutreader::getListOfString() const
{
    return m_listOfString;
}

/**
 * @brief checkoutreader::getErrorStat - Returns the error status
 * @return
 */
bool Checkoutreader::getErrorStat()
{
    return m_errorStat;
}

/**
 * @brief checkoutreader::ReadXMLData -  Returns the value of XML data element
 * @param rootItem - STarting element of the XML file
 * @param testType - Type of test,such as fulltest,circuit leak test etc
 * @param attributeName - element's attribute
 * @return
 */
QString Checkoutreader::ReadXMLData(QString rootItem, QString testType, QString attributeName)
{
    QDomElement calibration = checkoutDocument.firstChildElement(rootItem);
    QDomNodeList calibrationList = calibration.childNodes();
    int numOfCalibrations = calibrationList.size();
    qDebug()<<"no of calibrations : "<<numOfCalibrations;
    int index = 0 ;
    while (index < calibrationList.length())
    {
        QDomNode nodeItem = calibrationList.at(index);
        if(nodeItem.nodeName() == testType)
        {
            QString lastPassedTime =  nodeItem.firstChildElement(attributeName).attribute("text");
            return lastPassedTime;
        }
        index++;
    }
    return "" ;
}

/**
 * @brief checkoutreader::readXMLContents - This method is used to read xml contents into list.
 */
void Checkoutreader::readXMLContents()
{
    QDomElement docElem = checkoutDocument.documentElement();
    QDomElement General = docElem.firstChild().toElement();
    QDomElement SubGeneral;

    while (General.hasChildNodes())
    {
        SubGeneral=General.firstChild().toElement();
        qDebug()<<"Sub Grouptag: "<< General.tagName();

        while (SubGeneral.hasAttributes() && (SubGeneral.nodeName() != NULL))
        {
            qDebug()<<"text : "<< SubGeneral.attribute("text");
            m_listOfString.append(SubGeneral.attribute("text"));
            SubGeneral = SubGeneral.nextSiblingElement();
        }
        General = General.nextSiblingElement();
    }
    qDebug()<<"length: "<< m_listOfString.length();
}

/**
 * @brief checkoutreader::checkForLastCheckoutDone -Verify if the checkout has been done in last 24 hours
 */
bool Checkoutreader::isCheckoutDoneBeforeTwentyFourHours()
{
    try
    {
        QString time_format = "dd-MM-yyyy HH:mm";
        QString currentTimeString = QDateTime::currentDateTime().toString(time_format);
        QDateTime currentTime = QDateTime::fromString(currentTimeString,time_format);

        QString lastCheckoutDoneOnString = ReadXMLData("CheckoutCalibTestStatus","FullTest","lastPassedTime");
        QDateTime lastCheckoutDoneOn = QDateTime::fromString(lastCheckoutDoneOnString,time_format);

        if(currentTime.isValid() == false || lastCheckoutDoneOn.isValid() == false)
        {
            m_errorStr = "Date and time format read is not proper,please check XML time format dd-mm-yyyy HH:MM";
            throw m_errorStr;
        }
        float noOfHours = (currentTime.toTime_t() - lastCheckoutDoneOn.toTime_t())/3600;//convert ms to hours
        qDebug()<<"noOfHours"<<noOfHours;
        qDebug()<<"Current Time "<<currentTime;
        qDebug()<<"last checkout done on "<<lastCheckoutDoneOn;
        if(noOfHours >= MAX_CHECKOUT_HOURS)
        {
            return false;
        }
    }

    catch(QString exception)
    {
        qDebug()<<exception ;
        m_errorStat = true;
    }
    return true;
}
