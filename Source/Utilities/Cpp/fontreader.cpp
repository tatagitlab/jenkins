#include "../Source/Utilities/Include/fontreader.h"
#include <QFontDatabase>
#include <QDebug>


Fontreader::Fontreader()
{
    addGEFontToDB();
}

/**
 * @brief Fontreader::addGEFontToDB - Add GEInspiraSans font to QFontDatabase
 */
void Fontreader::addGEFontToDB()
{
    QFontDatabase fontDB;
    fontDB.addApplicationFont(":/Config/Assets/GEInspiraSans-Regular-v02_0.ttf");

    foreach(QString geFont, fontDB.families())
    {
        if( geFont == "GE Inspira Sans"){
            dbFont = geFont;
        }
    }
}

/**
 * @brief Fontreader::geFontStyle - Set Font to GEInspiraSans Regular
 * @return
 */
QString Fontreader::geFontStyle() const
{
    return dbFont;
}
