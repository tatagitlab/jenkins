Qt.include("qrc:/Source/Generic/JavaScript/MasterController.js")

var standbyComponent = null ;
var standbyObject = null ;

var therapyComponent = null
var therapyObject = null

var standbyWidth = 900
var standbyHeight = 620

var therapyWidth = 700
var therapyHeight = 620
//var ventModeMenuWidth = parseInt(layout.getGroupLayoutDataByTag("VentModeMenu","Dimension","width"));


var StandbyScreenWidth  = parseInt(layout.getGroupLayoutDataByTag("Standby","Dimension","width"))
var StandbyScreenHeight = parseInt(layout.getGroupLayoutDataByTag("Standby","Dimension","height"))
var StandbyTherapyScreenWidth  = parseInt(layout.getGroupLayoutDataByTag("StandbyTherapy","Dimension","width"))
var StandbyTherapyScreenHeight = parseInt(layout.getGroupLayoutDataByTag("StandbyTherapy","Dimension","height"))

var TextFontSize        = parseInt(component.getComponentXmlDataByTag("Component","Standby","TextFontSize"))
var TestBtnWidth        = parseInt(component.getComponentXmlDataByTag("Component","Standby","TestBtnWidth"))
var TestBtnHeight       = parseInt(component.getComponentXmlDataByTag("Component","Standby","TestBtnHeight"))
var StatusBtnWidth      = parseInt(component.getComponentXmlDataByTag("Component","Standby","StatusBtnWidth"))
var StatusBtnHeight     = parseInt(component.getComponentXmlDataByTag("Component","Standby","StatusBtnHeight"))
var LastPassedBtnWidth  = parseInt(component.getComponentXmlDataByTag("Component","Standby","LastPassedBtnWidth"))
var LastPassedBtnHeight = parseInt(component.getComponentXmlDataByTag("Component","Standby","LastPassedBtnHeight"))
var LogBtnWidth         = parseInt(component.getComponentXmlDataByTag("Component","Standby","LogBtnWidth"))
var LogBtnHeight        = parseInt(component.getComponentXmlDataByTag("Component","Standby","LogBtnHeight"))
var GridBtnWidth        = parseInt(component.getComponentXmlDataByTag("Component","Standby","GridBtnWidth"))
var GridBtnHeight       = parseInt(component.getComponentXmlDataByTag("Component","Standby","GridBtnHeight"))
var LockScrnBtnWidth    = parseInt(component.getComponentXmlDataByTag("Component","Standby","LockScrnBtnWidth"))
var LockScrnBtnHeight   = parseInt(component.getComponentXmlDataByTag("Component","Standby","LockScrnBtnHeight"))
var CaseDefaultBtnWidth = parseInt(component.getComponentXmlDataByTag("Component","Standby","CaseDefaultBtnWidth"))
var CaseDefaultBtnHeight= parseInt(component.getComponentXmlDataByTag("Component","Standby","CaseDefaultBtnHeight"))
var DefaultBtnWidth     = parseInt(component.getComponentXmlDataByTag("Component","Standby","DefaultBtnWidth"))
var DefaultBtnHeight    = parseInt(component.getComponentXmlDataByTag("Component","Standby","DefaultBtnHeight"))
var CloseBtnWidth       = parseInt(component.getComponentXmlDataByTag("Component","Standby","CloseBtnWidth"))
var CloseBtnHeight      = parseInt(component.getComponentXmlDataByTag("Component","Standby","CloseBtnHeight"))
var LockIconWidth       = parseInt(component.getComponentXmlDataByTag("Component","Standby","LockIconWidth"))
var LockIconHeight      = parseInt(component.getComponentXmlDataByTag("Component","Standby","LockIconHeight"))

var BtnBorderColor     = component.getComponentXmlDataByTag("Component","Standby","BtnBorderColor")
var NormalBtnBgColor   = component.getComponentXmlDataByTag("Component","Standby","NormalBtnBgColor")
var DefaultBtnBgColor  = component.getComponentXmlDataByTag("Component","Standby","DefaultBtnBgColor")
var LogBtnIcon         = component.getComponentXmlDataByTag("Component","Standby","LockScreenBtnIcon")
var LockScreenBtnIcon  = component.getComponentXmlDataByTag("Component","Standby","LockScreenBtnIcon")
var CaseDefaultBtnIcon = component.getComponentXmlDataByTag("Component","Standby","CaseDefaultBtnIcon")
var CloseBtnIcon       = component.getComponentXmlDataByTag("Component","Standby","CloseBtnIcon")
var DefaultSetupBtnIcon= component.getComponentXmlDataByTag("Component","Standby","DefaultSetupBtnIcon")

var StandbyScreenColor = component.getComponentXmlDataByTag("Component","Standby","StandbyScreenColor")
var StandbyScreenBorderColor = component.getComponentXmlDataByTag("Component","Standby","StandbyScreenBorderColor")
var StandbyScreenBorderWidth = component.getComponentXmlDataByTag("Component","Standby","StandbyScreenBorderWidth")
var StandbyScreenBorderRadius = component.getComponentXmlDataByTag("Component","Standby","StandbyScreenBorderRadius")
var StatusLabelfontSize = component.getComponentXmlDataByTag("Component","Standby","StatusLabelfontSize")
var StatusLabelfontColor = component.getComponentXmlDataByTag("Component","Standby","StatusLabelfontColor")
var SystemLabelLeftMargin = component.getComponentXmlDataByTag("Component","Standby","SystemLabelLeftMargin")
var SystemLabelTopMargin = component.getComponentXmlDataByTag("Component","Standby","SystemLabelTopMargin")
var TestTypeRectBorderWidth = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectBorderWidth")
var TestTypeRectScrollStateBorderWidth = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectScrollStateBorderWidth")
var TestTypeRectBorderRadius = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectBorderRadius")
var TestTypeRectColor = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectColor")
var TestTypeRectBorderColor = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectBorderColor")
var TestTypeRectTouchStateColor = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectTouchStateColor")
var TestTypeRectDisableStateColor = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectDisableStateColor")
var TestTypeRectFontColor = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectFontColor")
var TestTypeRectFontSize = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectFontSize")
var TestTypeRectScrollStateBorderColor = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectScrollStateBorderColor")
var TestResultTextFontColor = component.getComponentXmlDataByTag("Component","Standby","TestResultTextFontColor")
var TestResultTextFontsize = component.getComponentXmlDataByTag("Component","Standby","TestResultTextFontsize")
var BatteryTextFontColor = component.getComponentXmlDataByTag("Component","Standby","BatteryTextFontColor")
var BatteryTextFontsize = component.getComponentXmlDataByTag("Component","Standby","BatteryTextFontsize")
var BatteryStatusTextFontColor = component.getComponentXmlDataByTag("Component","Standby","BatteryStatusTextFontColor")
var BatteryStatusTextFontsize = component.getComponentXmlDataByTag("Component","Standby","BatteryStatusTextFontsize")
var WaterGasLabelTextFontColor = component.getComponentXmlDataByTag("Component","Standby","WaterGasLabelTextFontColor")
var WaterGasTextFontsize = component.getComponentXmlDataByTag("Component","Standby","WaterGasTextFontsize")
var WaterGasValueTextFontColor = component.getComponentXmlDataByTag("Component","Standby","WaterGasValueTextFontColor")
var WaterGasValueTextFontsize = component.getComponentXmlDataByTag("Component","Standby","WaterGasValueTextFontsize")
var LogBtnRadius = component.getComponentXmlDataByTag("Component","Standby","LogBtnRadius")
var LogBtnBorderWidth = component.getComponentXmlDataByTag("Component","Standby","LogBtnBorderWidth")
var LogBtnTopMargin = component.getComponentXmlDataByTag("Component","Standby","LogBtnTopMargin")
var LogBtnLeftMargin = component.getComponentXmlDataByTag("Component","Standby","LogBtnLeftMargin")
var LogBtnScrollStateBorderWidth = component.getComponentXmlDataByTag("Component","Standby","LogBtnScrollStateBorderWidth")
var LogBtnBorderColor = component.getComponentXmlDataByTag("Component","Standby","LogBtnBorderColor")
var LogBtnScrollStateBorderColor = component.getComponentXmlDataByTag("Component","Standby","LogBtnScrollStateBorderColor")
var LogBtnTextFontColor = component.getComponentXmlDataByTag("Component","Standby","LogBtnTextFontColor")
var LogBtnTextFontsize = component.getComponentXmlDataByTag("Component","Standby","LogBtnTextFontsize")
var LogLableLeftMargin = component.getComponentXmlDataByTag("Component","Standby","LogLableLeftMargin")
var LockScrnRadius  = component.getComponentXmlDataByTag("Component","Standby","LockScrnRadius")
var LockScrnBorderColor = component.getComponentXmlDataByTag("Component","Standby","LockScrnBorderColor")
var LockScrnScrollStateBorderColor = component.getComponentXmlDataByTag("Component","Standby","LockScrnScrollStateBorderColor")
var LockScrnTextFontColor = component.getComponentXmlDataByTag("Component","Standby","LockScrnTextFontColor")
var LockScrnTextFontsize = component.getComponentXmlDataByTag("Component","Standby","LockScrnTextFontsize")
var DefaultSetUpBtnTxtFontSize = component.getComponentXmlDataByTag("Component","Standby","DefaultSetUpBtnTxtFontSize")
var WallGasLabelWidth = component.getComponentXmlDataByTag("Component","Standby","WallGasLabelWidth")
var CloseBtnRightMargin = component.getComponentXmlDataByTag("Component","Standby","CloseBtnRightMargin")
var CloseBtnTopMargin = component.getComponentXmlDataByTag("Component","Standby","CloseBtnTopMargin")
var CloseBtnSrc = component.getComponentXmlDataByTag("Component","Standby","CloseBtnSrc")
var TestTypeRectGap = component.getComponentXmlDataByTag("Component","Standby","TestTypeRectGap")
var TestTopMargin= component.getComponentXmlDataByTag("Component","Standby","TestTopMargin")
var TestLeftMargin= component.getComponentXmlDataByTag("Component","Standby","TestLeftMargin")
var StatusTypeLeftMargin= component.getComponentXmlDataByTag("Component","Standby","StatusTypeLeftMargin")
var StatusTypeTopMargin= component.getComponentXmlDataByTag("Component","Standby","StatusTypeTopMargin")
var StatusTypeLabelGap= component.getComponentXmlDataByTag("Component","Standby","StatusTypeLabelGap")
var LastStatusDoneLabelGap= component.getComponentXmlDataByTag("Component","Standby","LastStatusDoneLabelGap")
var LastStatusDoneTopMargin= component.getComponentXmlDataByTag("Component","Standby","LastStatusDoneTopMargin")
var LastStatusDoneLeftMargin = component.getComponentXmlDataByTag("Component","Standby","LastStatusDoneLeftMargin")
var BatteryLabelTopMargin= component.getComponentXmlDataByTag("Component","Standby","BatteryLabelTopMargin")
var BatteryLabelLeftMargin= component.getComponentXmlDataByTag("Component","Standby","BatteryLabelLeftMargin")
var BatteryStatusLeftMargin= component.getComponentXmlDataByTag("Component","Standby","BatteryStatusLeftMargin")
var WaterGasSuppliesLeftMargin= component.getComponentXmlDataByTag("Component","Standby","WaterGasSuppliesLeftMargin")
var WaterGasSuppliesTopMargin= component.getComponentXmlDataByTag("Component","Standby","WaterGasSuppliesTopMargin")
var WaterGasSuppliesLabelXGap= component.getComponentXmlDataByTag("Component","Standby","WaterGasSuppliesLabelXGap")
var WaterGasSuppliesLabelYGap= component.getComponentXmlDataByTag("Component","Standby","WaterGasSuppliesLabelYGap")
var WaterGasSuppliesUnitYGap= component.getComponentXmlDataByTag("Component","Standby","WaterGasSuppliesUnitYGap")
var LockScreenLeftMargin= component.getComponentXmlDataByTag("Component","Standby","LockScreenLeftMargin")
var LockScreenBottomMargin= component.getComponentXmlDataByTag("Component","Standby","LockScreenBottomMargin")
var LockBtnSrc= component.getComponentXmlDataByTag("Component","Standby","LockBtnSrc")
var SeperatorTopMargin= component.getComponentXmlDataByTag("Component","Standby","SeperatorTopMargin")
var SeperatorWidth= component.getComponentXmlDataByTag("Component","Standby","SeperatorWidth")
var SeperatorLeftMargin= component.getComponentXmlDataByTag("Component","Standby","SeperatorLeftMargin")
var SeperatorColor= component.getComponentXmlDataByTag("Component","Standby","SeperatorColor")
var SeperatorBorderColor= component.getComponentXmlDataByTag("Component","Standby","SeperatorBorderColor")
var CaseDefaultLeftMargin= component.getComponentXmlDataByTag("Component","Standby","CaseDefaultLeftMargin")
var CaseDefaultTopMargin= component.getComponentXmlDataByTag("Component","Standby","CaseDefaultTopMargin")
var MutExclLeftMargin= component.getComponentXmlDataByTag("Component","Standby","MutExclLeftMargin")
var MutExclTopMargin= component.getComponentXmlDataByTag("Component","Standby","MutExclTopMargin")
var MutExclBtnGap= component.getComponentXmlDataByTag("Component","Standby","MutExclBtnGap")
var CaseDefaultSrc= component.getComponentXmlDataByTag("Component","Standby","CaseDefaultSrc")
var Custom1Src= component.getComponentXmlDataByTag("Component","Standby","Custom1Src")
var Custom2Src= component.getComponentXmlDataByTag("Component","Standby","Custom2Src")
var CustomNormalFontColor= component.getComponentXmlDataByTag("Component","Standby","CustomNormalFontColor")
var DefaultBtnRightMargin= component.getComponentXmlDataByTag("Component","Standby","DefaultBtnRightMargin")
var DefaultBtnBottomMargin= component.getComponentXmlDataByTag("Component","Standby","DefaultBtnBottomMargin")
var DefaultBtnImageSrc= component.getComponentXmlDataByTag("Component","Standby","DefaultBtnImageSrc")
var WaterGasSuppliesUnitLeftMargin = component.getComponentXmlDataByTag("Component","Standby","WaterGasSuppliesUnitLeftMargin")
var WaterGasSuppliesValuesLeftMargin = component.getComponentXmlDataByTag("Component","Standby","WaterGasSuppliesValuesLeftMargin")
var CaseDefaultLabelColor = component.getComponentXmlDataByTag("Component","Standby","CaseDefaultLabelColor")


function resizeScreenForTherapy(){
    standbyWidth = 700
    standbyHeight = 620
   // standbyObject.
}

function resizeScreenForStandby(){
    standbyWidth = 900
    standbyHeight = 620
  //  standbyObje().
}

function createStandbyComponent(){
    if( standbyObject != null && standbyObject != undefined ){
        return ;
    }

    standbyComponent = Qt.createComponent("qrc:Source/Standby/Qml/StandbyComponent.qml");
    if(standbyComponent.status === Component.Ready) {
        finishStandbyComponentCreation();
    }
    else if(standbyComponent.status === Component.Error) {
        console.log("Error loading component: " + standbyComponent.errorString());
    }
    else {
        standbyComponent.statusChanged.connect(finishStandbyComponentCreation);
    }
}

function createTherapyComponent(){

    if( therapyObject != null && therapyObject != undefined ){
        return ;
    }

    therapyComponent = Qt.createComponent("qrc:Source/Standby/Qml/TherapyComponent.qml");
    if(therapyComponent.status === Component.Ready) {
        finishTherapyComponentCreation();
    }
    else if(therapyComponent.status === Component.Error) {
        console.log("Error loading component: " + therapyComponent.errorString());
    }
    else {
        therapyComponent.statusChanged.connect(finishTherapyComponentCreation);
    }

}

function finishStandbyComponentCreation(){
    standbyObject = standbyComponent.createObject(mainitem,{
                                                      "iStandbyWidth":StandbyScreenWidth,
                                                      "iStandbyHeight":StandbyScreenHeight,
                                                      "strStandbyScreenColor":StandbyScreenColor,
                                                      "strStandbyScreenBorderColor":StandbyScreenBorderColor,
                                                      "iStandbyScreenBorderWidth":StandbyScreenBorderWidth,
                                                      "iStandbyScreenBorderRadius":StandbyScreenBorderRadius,
                                                      "iStatusLabelfontSize":StatusLabelfontSize,
                                                      "strStatusLabelfontColor":StatusLabelfontColor,
                                                      "iSystemLabelLeftMargin":SystemLabelLeftMargin,
                                                      "iSystemLabelTopMargin":SystemLabelTopMargin,
                                                      "iTestTypeRectBorderWidth":TestTypeRectBorderWidth,
                                                      "iTestTypeRectScrollStateBorderWidth":TestTypeRectScrollStateBorderWidth,
                                                      "iTestTypeRectBorderRadius":TestTypeRectBorderRadius,
                                                      "strTestTypeRectColor":TestTypeRectColor,
                                                      "strTestTypeRectBorderColor":TestTypeRectBorderColor,
                                                      "strTestTypeRectTouchStateColor":TestTypeRectTouchStateColor,
                                                      "strTestTypeRectDisableStateColor":TestTypeRectDisableStateColor,
                                                      "strTestTypeRectFontColor":TestTypeRectFontColor,
                                                      "iTestTypeRectFontSize":TestTypeRectFontSize,
                                                      "strTestTypeRectScrollStateBorderColor":TestTypeRectScrollStateBorderColor,
                                                      "strTestResultTextFontColor":TestResultTextFontColor,
                                                      "iTestResultTextFontsize":TestResultTextFontsize,
                                                      "strBatteryTextFontColor":BatteryTextFontColor,
                                                      "iBatteryTextFontsize":BatteryTextFontsize,
                                                      "strBatteryStatusTextFontColor":BatteryStatusTextFontColor,
                                                      "iBatteryStatusTextFontsize":BatteryStatusTextFontsize,
                                                      "strWaterGasLabelTextFontColor":WaterGasLabelTextFontColor,
                                                      "iWaterGasTextFontsize":WaterGasTextFontsize,
                                                      "strWaterGasValueTextFontColor":WaterGasValueTextFontColor,
                                                      "iWaterGasValueTextFontsize":WaterGasValueTextFontsize,
                                                      "iLogBtnRadius":LogBtnRadius,
                                                      "iLogBtnBorderWidth":LogBtnBorderWidth,
                                                      "iLogBtnTopMargin":LogBtnTopMargin,
                                                      "iLogBtnLeftMargin":LogBtnLeftMargin,
                                                      "iLogBtnScrollStateBorderWidth":LogBtnScrollStateBorderWidth,
                                                      "strLogBtnBorderColor":LogBtnBorderColor,
                                                      "strLogBtnScrollStateBorderColor":LogBtnScrollStateBorderColor,
                                                      "strLogBtnTextFontColor":LogBtnTextFontColor,
                                                      "iLogBtnTextFontsize":LogBtnTextFontsize,
                                                      "iLogLableLeftMargin":LogLableLeftMargin,
                                                      "iLockScrnRadius":LockScrnRadius,
                                                      "strLockScrnBorderColor":LockScrnBorderColor,
                                                      "strLockScrnScrollStateBorderColor":LockScrnScrollStateBorderColor,
                                                      "strLockScrnTextFontColor":LockScrnTextFontColor,
                                                      "iLockScrnTextFontsize":LockScrnTextFontsize,
                                                      "iDefaultSetUpBtnTxtFontSize":DefaultSetUpBtnTxtFontSize,
                                                      "iWaterGasSuppliesUnitLeftMargin":WaterGasSuppliesUnitLeftMargin,
                                                      "wallGasLabelWidth":WallGasLabelWidth,
                                                      "icloseBtnRightMargin":CloseBtnRightMargin,
                                                      "icloseBtnTopMargin":CloseBtnTopMargin,
                                                      "strCloseBtnSrc":CloseBtnSrc,
                                                      "itestTypeRectGap":TestTypeRectGap,
                                                      "itestTopMargin":TestTopMargin,
                                                      "itestLeftMargin":TestLeftMargin,
                                                      "iStatusTypeLeftMargin":StatusTypeLeftMargin,
                                                      "iStatusTypeTopMargin":StatusTypeTopMargin,
                                                      "iStatusTypeLabelGap":StatusTypeLabelGap,
                                                      "iLastStatusDoneLabelGap":LastStatusDoneLabelGap,
                                                      "iLastStatusDoneTopMargin":LastStatusDoneTopMargin,
                                                      "iLastStatusDoneLeftMargin":LastStatusDoneLeftMargin,
                                                      "iBatteryLabelTopMargin":BatteryLabelTopMargin,
                                                      "iBatteryLabelLeftMargin":BatteryLabelLeftMargin,
                                                      "iBatteryStatusLeftMargin":BatteryStatusLeftMargin,
                                                      "iWaterGasSuppliesValuesLeftMargin":WaterGasSuppliesValuesLeftMargin,
                                                      "iWaterGasSuppliesLeftMargin":WaterGasSuppliesLeftMargin,
                                                      "iWaterGasSuppliesTopMargin":WaterGasSuppliesTopMargin,
                                                      "iWaterGasSuppliesLabelXGap":WaterGasSuppliesLabelXGap,
                                                      "iWaterGasSuppliesLabelYGap":WaterGasSuppliesLabelYGap,
                                                      "iWaterGasSuppliesUnitYGap":WaterGasSuppliesUnitYGap,
                                                      "iLockScreenLeftMargin":LockScreenLeftMargin,
                                                      "iLockScreenBottomMargin":LockScreenBottomMargin,
                                                      "strLockBtnSrc":LockBtnSrc,
                                                      "iSeperatorTopMargin":SeperatorTopMargin,
                                                      "iSeperatorWidth":SeperatorWidth,
                                                      "iSeperatorLeftMargin":SeperatorLeftMargin,
                                                      "strSeperatorColor":SeperatorColor,
                                                      "strSeperatorBorderColor":SeperatorBorderColor,
                                                      "iCaseDefaultLeftMargin":CaseDefaultLeftMargin,
                                                      "iCaseDefaultTopMargin":CaseDefaultTopMargin,
                                                      "iMutExclLeftMargin":MutExclLeftMargin,
                                                      "iMutExclTopMargin":MutExclTopMargin,
                                                      "iMutExclBtnGap":MutExclBtnGap,
                                                      "strCaseDefaultSrc":CaseDefaultSrc,
                                                      "strCustom1Src":Custom1Src,
                                                      "strCustom2Src":Custom2Src,
                                                      "strCustomNormalFontColor":CustomNormalFontColor,
                                                      "iDefaultBtnRightMargin":DefaultBtnRightMargin,
                                                      "iDefaultBtnBottomMargin":DefaultBtnBottomMargin,
                                                      "strDefaultBtnImageSrc":DefaultBtnImageSrc,
                                                       "strCaseDefaultLabelColor":CaseDefaultLabelColor,
                                                      "iTextFontSize" : TextFontSize ,
                                                      "iTestBtnWidth" : TestBtnWidth ,
                                                      "iTestBtnHeight" : TestBtnHeight ,
                                                      "iStatusBtnWidth" : StatusBtnWidth,
                                                      "iStatusBtnHeight" : StatusBtnHeight,
                                                      "iLastPassedBtnWidth" : LastPassedBtnWidth,
                                                      "iLastPassedBtnHeight" : LastPassedBtnHeight,
                                                      "iLogBtnWidth" : LogBtnWidth,
                                                      "iLogBtnHeight" : LogBtnHeight,
                                                      "iGridBtnWidth" : GridBtnWidth,
                                                      "iGridBtnHeight" : GridBtnHeight,
                                                      "iLockScrnBtnWidth" : LockScrnBtnWidth,
                                                      "iLockScrnBtnHeight" : LockScrnBtnHeight,
                                                      "iCaseDefaultBtnWidth" : CaseDefaultBtnWidth,
                                                      "iCaseDefaultBtnHeight" : CaseDefaultBtnHeight,
                                                      "iDefaultBtnWidth" : DefaultBtnWidth,
                                                      "iDefaultBtnHeight" : DefaultBtnHeight,
                                                      "iCloseBtnWidth" : CloseBtnWidth,
                                                      "iCloseBtnHeight" : CloseBtnHeight,
                                                      "strNormalBtnBgColor" : NormalBtnBgColor,
                                                      "strDefaultBtnBgColor" : DefaultBtnBgColor,
                                                      "strLogBtnIcon" : LogBtnIcon,
                                                      "strLockScreenBtnIcon" : LockScreenBtnIcon,
                                                      "strCaseDefaultBtnIcon" : CaseDefaultBtnIcon,
                                                      "strCloseBtnIcon" : CloseBtnIcon,
                                                      "strDefaultSetupBtnIcon" : DefaultSetupBtnIcon,
                                                      //"strBtnBorderColor" : BtnBorderColor,
                                                      "iLockIconWidth" : LockIconWidth,
                                                      "iLockIconHeight" : LockIconHeight
                                            })

//    if(therapyObject !== null || therapyObject !== undefined ){
//        therapyObject.destroy();
//    }

    standbyObject.standbyCloseBtnClicked.connect(destroyStandbyObject)
    var alrmGroupRef = getGroupRef("AlarmGroup")
    standbyObject.anchors.top = alrmGroupRef.bottom
}

function finishTherapyComponentCreation(){
    therapyObject = therapyComponent.createObject(mainitem,{
                                                      "iStandbyWidth":StandbyTherapyScreenWidth,
                                                      "iStandbyHeight":StandbyTherapyScreenHeight,
                                                      "strStandbyScreenColor":StandbyScreenColor,
                                                      "strStandbyScreenBorderColor":StandbyScreenBorderColor,
                                                      "iStandbyScreenBorderWidth":StandbyScreenBorderWidth,
                                                      "iStandbyScreenBorderRadius":StandbyScreenBorderRadius,
                                                      "iStatusLabelfontSize":StatusLabelfontSize,
                                                      "strStatusLabelfontColor":StatusLabelfontColor,
                                                      "iSystemLabelLeftMargin":SystemLabelLeftMargin,
                                                      "iSystemLabelTopMargin":SystemLabelTopMargin,
                                                      "iTestTypeRectBorderWidth":TestTypeRectBorderWidth,
                                                      "iTestTypeRectScrollStateBorderWidth":TestTypeRectScrollStateBorderWidth,
                                                      "iTestTypeRectBorderRadius":TestTypeRectBorderRadius,
                                                      "strTestTypeRectColor":TestTypeRectColor,
                                                      "strTestTypeRectBorderColor":TestTypeRectBorderColor,
                                                      "strTestTypeRectTouchStateColor":TestTypeRectTouchStateColor,
                                                      "strTestTypeRectDisableStateColor":TestTypeRectDisableStateColor,
                                                      "strTestTypeRectFontColor":TestTypeRectFontColor,
                                                      "iTestTypeRectFontSize":TestTypeRectFontSize,
                                                      "strTestTypeRectScrollStateBorderColor":TestTypeRectScrollStateBorderColor,
                                                      "strTestResultTextFontColor":TestResultTextFontColor,
                                                      "iTestResultTextFontsize":TestResultTextFontsize,
                                                      "strBatteryTextFontColor":BatteryTextFontColor,
                                                      "iBatteryTextFontsize":BatteryTextFontsize,
                                                      "strBatteryStatusTextFontColor":BatteryStatusTextFontColor,
                                                      "iBatteryStatusTextFontsize":BatteryStatusTextFontsize,
                                                      "strWaterGasLabelTextFontColor":WaterGasLabelTextFontColor,
                                                      "iWaterGasTextFontsize":WaterGasTextFontsize,
                                                      "strWaterGasValueTextFontColor":WaterGasValueTextFontColor,
                                                      "iWaterGasValueTextFontsize":WaterGasValueTextFontsize,
                                                      "iLogBtnRadius":LogBtnRadius,
                                                      "iLogBtnBorderWidth":LogBtnBorderWidth,
                                                      "iLogBtnTopMargin":LogBtnTopMargin,
                                                      "iLogBtnLeftMargin":LogBtnLeftMargin,
                                                      "iLogBtnScrollStateBorderWidth":LogBtnScrollStateBorderWidth,
                                                      "strLogBtnBorderColor":LogBtnBorderColor,
                                                      "strLogBtnScrollStateBorderColor":LogBtnScrollStateBorderColor,
                                                      "strLogBtnTextFontColor":LogBtnTextFontColor,
                                                      "iLogBtnTextFontsize":LogBtnTextFontsize,
                                                      "iLogLableLeftMargin":LogLableLeftMargin,
                                                      "iLockScrnRadius":LockScrnRadius,
                                                      "strLockScrnBorderColor":LockScrnBorderColor,
                                                      "strLockScrnScrollStateBorderColor":LockScrnScrollStateBorderColor,
                                                      "strLockScrnTextFontColor":LockScrnTextFontColor,
                                                      "iLockScrnTextFontsize":LockScrnTextFontsize,
                                                      "iDefaultSetUpBtnTxtFontSize":DefaultSetUpBtnTxtFontSize,
                                                      "iWaterGasSuppliesUnitLeftMargin":WaterGasSuppliesUnitLeftMargin,
                                                      "wallGasLabelWidth":WallGasLabelWidth,
                                                      "icloseBtnRightMargin":CloseBtnRightMargin,
                                                      "icloseBtnTopMargin":CloseBtnTopMargin,
                                                      "strCloseBtnSrc":CloseBtnSrc,
                                                      "itestTypeRectGap":TestTypeRectGap,
                                                      "itestTopMargin":TestTopMargin,
                                                      "itestLeftMargin":TestLeftMargin,
                                                      "iStatusTypeLeftMargin":StatusTypeLeftMargin,
                                                      "iStatusTypeTopMargin":StatusTypeTopMargin,
                                                      "iStatusTypeLabelGap":StatusTypeLabelGap,
                                                      "iLastStatusDoneLabelGap":LastStatusDoneLabelGap,
                                                      "iLastStatusDoneTopMargin":LastStatusDoneTopMargin,
                                                      "iLastStatusDoneLeftMargin":LastStatusDoneLeftMargin,
                                                      "iBatteryLabelTopMargin":BatteryLabelTopMargin,
                                                      "iBatteryLabelLeftMargin":BatteryLabelLeftMargin,
                                                      "iBatteryStatusLeftMargin":BatteryStatusLeftMargin,
                                                      "iWaterGasSuppliesValuesLeftMargin":WaterGasSuppliesValuesLeftMargin,
                                                      "iWaterGasSuppliesLeftMargin":WaterGasSuppliesLeftMargin,
                                                      "iWaterGasSuppliesTopMargin":WaterGasSuppliesTopMargin,
                                                      "iWaterGasSuppliesLabelXGap":WaterGasSuppliesLabelXGap,
                                                      "iWaterGasSuppliesLabelYGap":WaterGasSuppliesLabelYGap,
                                                      "iWaterGasSuppliesUnitYGap":WaterGasSuppliesUnitYGap,
                                                      "iLockScreenLeftMargin":LockScreenLeftMargin,
                                                      "iLockScreenBottomMargin":LockScreenBottomMargin,
                                                      "strLockBtnSrc":LockBtnSrc,
                                                      "iSeperatorTopMargin":SeperatorTopMargin,
                                                      "iSeperatorWidth":SeperatorWidth,
                                                      "iSeperatorLeftMargin":SeperatorLeftMargin,
                                                      "strSeperatorColor":SeperatorColor,
                                                      "strSeperatorBorderColor":SeperatorBorderColor,
                                                      "iCaseDefaultLeftMargin":CaseDefaultLeftMargin,
                                                      "iCaseDefaultTopMargin":CaseDefaultTopMargin,
                                                      "iMutExclLeftMargin":MutExclLeftMargin,
                                                      "iMutExclTopMargin":MutExclTopMargin,
                                                      "iMutExclBtnGap":MutExclBtnGap,
                                                      "strCaseDefaultSrc":CaseDefaultSrc,
                                                      "strCustom1Src":Custom1Src,
                                                      "strCustom2Src":Custom2Src,
                                                      "strCustomNormalFontColor":CustomNormalFontColor,
                                                      "iDefaultBtnRightMargin":DefaultBtnRightMargin,
                                                      "iDefaultBtnBottomMargin":DefaultBtnBottomMargin,
                                                      "strDefaultBtnImageSrc":DefaultBtnImageSrc,
                                                       "strCaseDefaultLabelColor":CaseDefaultLabelColor,
                                                      "iTextFontSize" : TextFontSize ,
                                                      "iTestBtnWidth" : TestBtnWidth ,
                                                      "iTestBtnHeight" : TestBtnHeight ,
                                                      "iStatusBtnWidth" : StatusBtnWidth,
                                                      "iStatusBtnHeight" : StatusBtnHeight,
                                                      "iLastPassedBtnWidth" : LastPassedBtnWidth,
                                                      "iLastPassedBtnHeight" : LastPassedBtnHeight,
                                                      "iLogBtnWidth" : LogBtnWidth,
                                                      "iLogBtnHeight" : LogBtnHeight,
                                                      "iGridBtnWidth" : GridBtnWidth,
                                                      "iGridBtnHeight" : GridBtnHeight,
                                                      "iLockScrnBtnWidth" : LockScrnBtnWidth,
                                                      "iLockScrnBtnHeight" : LockScrnBtnHeight,
                                                      "iCaseDefaultBtnWidth" : CaseDefaultBtnWidth,
                                                      "iCaseDefaultBtnHeight" : CaseDefaultBtnHeight,
                                                      "iDefaultBtnWidth" : DefaultBtnWidth,
                                                      "iDefaultBtnHeight" : DefaultBtnHeight,
                                                      "iCloseBtnWidth" : CloseBtnWidth,
                                                      "iCloseBtnHeight" : CloseBtnHeight,
                                                      "strNormalBtnBgColor" : NormalBtnBgColor,
                                                      "strDefaultBtnBgColor" : DefaultBtnBgColor,
                                                      "strLogBtnIcon" : LogBtnIcon,
                                                      "strLockScreenBtnIcon" : LockScreenBtnIcon,
                                                      "strCaseDefaultBtnIcon" : CaseDefaultBtnIcon,
                                                      "strCloseBtnIcon" : CloseBtnIcon,
                                                      "strDefaultSetupBtnIcon" : DefaultSetupBtnIcon,
                                                      //"strBtnBorderColor" : BtnBorderColor,
                                                      "iLockIconWidth" : LockIconWidth,
                                                      "iLockIconHeight" : LockIconHeight
                                                  })
//    if(standbyObject !== null || standbyObject !== undefined ){
//        standbyObject.destroy();
//    }
    therapyObject.therapyCloseBtnClicked.connect(destroyTherapyObject)
    var alrmGroupRef = getGroupRef("AlarmGroup")
    therapyObject.anchors.top = alrmGroupRef.bottom
}

function isStandbyObjectEmpty(){
    if( standbyObject == null || standbyObject == undefined ) {
        return true;
    }
    else {
        return false ;
    }
}

function isTherapyObjectEmpty(){
    if( therapyObject == null || therapyObject == undefined ) {
        return true;
    }
    else {
        return false ;
    }
}

function destroyStandbyObject()
{
    if(!isStandbyObjectEmpty()){
        standbyObject.destroy()
        standbyObject = null;
    }
}

function destroyTherapyObject()
{
    if(!isTherapyObjectEmpty()){
        therapyObject.destroy()
        therapyObject = null;
    }
}

/*******************************************
Purpose:move clock wise direction
Description:Used to provide navigation within a screen.When the user presses right navigation key
or uses comwheel to navigate clock wise
***********************************************/
function moveClockwise(currentObjectName) {
    console.log("In moveClockwise; currentObjectName:",currentObjectName)
    var standByObj = getCompRef(mainitem, "standbyObject")

    if (currentObjectName === "closeBtnObject") {

        //  var caseDefaultComp = getCompRef(standByObj, "caseDefaultBtnObject")
        console.log("In moveClockwise; currentObjectName:",standByObj.exclusiveColumn.children[0].objectName)
        //    var caseDefaultComp = standByObj.exclusiveColumn.children[0].objectName;

        //var caseDefaultComp = getCompRef(standByObj, standByObj.exclusiveColumn.children[0].objectName)

        standByObj.exclusiveColumn.children[0].focus = true
        standByObj.exclusiveColumn.children[0].forceActiveFocus();
    }
    else if (currentObjectName === "caseDefaultBtnObject") {
        standByObj.exclusiveColumn.children[1].focus = true
        standByObj.exclusiveColumn.children[1].forceActiveFocus();    }
    else if (currentObjectName === "caseCustom_1BtnObject") {
        standByObj.exclusiveColumn.children[2].focus = true
        standByObj.exclusiveColumn.children[2].forceActiveFocus();
    }
    else if (currentObjectName === "caseCustom_2BtnObject") {
        var defaultBtnComp = getCompRef(standByObj, "defaultBtnObject")
        defaultBtnComp.focus = true
        defaultBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "defaultBtnObject") {
        var lockBtnComp = getCompRef(standByObj, "lockBtnObject")
        lockBtnComp.focus = true
        lockBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "lockBtnObject") {
        standByObj.testColumn.children[2].focus = true
        standByObj.testColumn.children[2].forceActiveFocus();
    }
    else if (currentObjectName === "caliberationObject") {
        standByObj.testColumn.children[1].focus = true
        standByObj.testColumn.children[1].forceActiveFocus();
    }
    else if (currentObjectName === "circuitLeakTestObject") {
        standByObj.testColumn.children[0].focus = true
        standByObj.testColumn.children[0].forceActiveFocus();
    }
    else if (currentObjectName === "fullTestObject") {
        var logBtnComp = getCompRef(standByObj, "logBtnObject")
        logBtnComp.focus = true
        logBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "logBtnObject" || currentObjectName === "standbyObject") {
        var closeBtnComp = getCompRef(standByObj, "closeBtnObject")
        closeBtnComp.focus = true
        closeBtnComp.forceActiveFocus()
    }

}


/*******************************************
Purpose:move Anticlock wise direction
Description:Used to provide navigation within a screen.When the user presses right navigation key
or uses comwheel to navigate clock wise
***********************************************/
function moveAntiClockwise(currentObjectName) {
    console.log("In moveClockwise; currentObjectName:",currentObjectName)
    var standByObj = getCompRef(mainitem, "standbyObject")

    if (currentObjectName === "closeBtnObject") {

        //  var caseDefaultComp = getCompRef(standByObj, "caseDefaultBtnObject")
        //console.log("In moveClockwise; currentObjectName:",standByObj.exclusiveColumn.children[0].objectName)
        //    var caseDefaultComp = standByObj.exclusiveColumn.children[0].objectName;

        //var caseDefaultComp = getCompRef(standByObj, standByObj.exclusiveColumn.children[0].objectName)

     //   standByObj.exclusiveColumn.children[0].focus = true
       // standByObj.exclusiveColumn.children[0].forceActiveFocus();
        var logBtnComp = getCompRef(standByObj, "logBtnObject")
        logBtnComp.focus = true
        logBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "caseDefaultBtnObject") {
     //   standByObj.exclusiveColumn.children[1].focus = true
      //  standByObj.exclusiveColumn.children[1].forceActiveFocus();
        var closeBtnComp = getCompRef(standByObj, "closeBtnObject")
        closeBtnComp.focus = true
        closeBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "caseCustom_1BtnObject") {
        standByObj.exclusiveColumn.children[0].focus = true
        standByObj.exclusiveColumn.children[0].forceActiveFocus();
    }
    else if (currentObjectName === "caseCustom_2BtnObject") {
        standByObj.exclusiveColumn.children[1].focus = true
        standByObj.exclusiveColumn.children[1].forceActiveFocus();
        //var defaultBtnComp = getCompRef(standByObj, "defaultBtnObject")
        //defaultBtnComp.focus = true
        //defaultBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "defaultBtnObject") {
 //       var lockBtnComp = getCompRef(standByObj, "lockBtnObject")
   //     lockBtnComp.focus = true
     //   lockBtnComp.forceActiveFocus()
        standByObj.exclusiveColumn.children[2].focus = true
        standByObj.exclusiveColumn.children[2].forceActiveFocus();

    }
    else if (currentObjectName === "lockBtnObject") {
//        standByObj.testColumn.children[2].focus = true
  //      standByObj.testColumn.children[2].forceActiveFocus();
        var defaultBtnComp = getCompRef(standByObj, "defaultBtnObject")
        defaultBtnComp.focus = true
        defaultBtnComp.forceActiveFocus()

    }
    else if (currentObjectName === "caliberationObject") {
        //standByObj.testColumn.children[1].focus = true
        //standByObj.testColumn.children[1].forceActiveFocus();
               var lockBtnComp = getCompRef(standByObj, "lockBtnObject")
               lockBtnComp.focus = true
               lockBtnComp.forceActiveFocus()

    }
    else if (currentObjectName === "circuitLeakTestObject") {
        standByObj.testColumn.children[2].focus = true
        standByObj.testColumn.children[2].forceActiveFocus();
    }
    else if (currentObjectName === "fullTestObject") {
        standByObj.testColumn.children[1].focus = true
        standByObj.testColumn.children[1].forceActiveFocus();


    }
    else if (currentObjectName === "logBtnObject" || currentObjectName === "standbyObject") {
        standByObj.testColumn.children[0].focus = true
        standByObj.testColumn.children[0].forceActiveFocus();

//        var closeBtnComp = getCompRef(standByObj, "closeBtnObject")
//        closeBtnComp.focus = true
//        closeBtnComp.forceActiveFocus()
//        var logBtnComp = getCompRef(standByObj, "logBtnObject")
//        logBtnComp.focus = true
//        logBtnComp.forceActiveFocus()
    }
}

/*******************************************
Purpose:move clock wise direction
Description:Used to provide navigation within a screen.When the user presses right navigation key
or uses comwheel to navigate clock wise
***********************************************/
function moveClockwiseForTherapy(currentObjectName) {
    console.log("In moveClockwise; currentObjectName:",currentObjectName)
    var standByObj = getCompRef(mainitem, "standbyTherapyObject")

    if (currentObjectName === "closeBtnObject") {

        //  var caseDefaultComp = getCompRef(standByObj, "caseDefaultBtnObject")
     //   console.log("In moveClockwise; currentObjectName:",standByObj.exclusiveColumn.children[0].objectName)
        //    var caseDefaultComp = standByObj.exclusiveColumn.children[0].objectName;

        //var caseDefaultComp = getCompRef(standByObj, standByObj.exclusiveColumn.children[0].objectName)

       // standByObj.exclusiveColumn.children[0].focus = true
        //standByObj.exclusiveColumn.children[0].forceActiveFocus();
        var lockBtnComp = getCompRef(standByObj, "lockBtnObject")
        lockBtnComp.focus = true
        lockBtnComp.forceActiveFocus()

    }
    else if (currentObjectName === "caseDefaultBtnObject") {
        standByObj.exclusiveColumn.children[1].focus = true
        standByObj.exclusiveColumn.children[1].forceActiveFocus();    }
    else if (currentObjectName === "caseCustom_1BtnObject") {
        standByObj.exclusiveColumn.children[2].focus = true
        standByObj.exclusiveColumn.children[2].forceActiveFocus();
    }
    else if (currentObjectName === "caseCustom_2BtnObject") {
        var defaultBtnComp = getCompRef(standByObj, "defaultBtnObject")
        defaultBtnComp.focus = true
        defaultBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "defaultBtnObject") {
      //  var lockBtnComp = getCompRef(standByObj, "lockBtnObject")
        //lockBtnComp.focus = true
        //lockBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "lockBtnObject") {
        standByObj.testColumn.children[2].focus = true
        standByObj.testColumn.children[2].forceActiveFocus();
    }
    else if (currentObjectName === "caliberationObject") {
        standByObj.testColumn.children[1].focus = true
        standByObj.testColumn.children[1].forceActiveFocus();
    }
    else if (currentObjectName === "circuitLeakTestObject") {
        standByObj.testColumn.children[0].focus = true
        standByObj.testColumn.children[0].forceActiveFocus();
    }
    else if (currentObjectName === "fullTestObject") {
        var logBtnComp = getCompRef(standByObj, "logBtnObject")
        logBtnComp.focus = true
        logBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "logBtnObject" || currentObjectName === "standbyTherapyObject") {
        var closeBtnComp = getCompRef(standByObj, "closeBtnObject")
        closeBtnComp.focus = true
        closeBtnComp.forceActiveFocus()
    }

}


/*******************************************
Purpose:move Anticlock wise direction
Description:Used to provide navigation within a screen.When the user presses right navigation key
or uses comwheel to navigate clock wise
***********************************************/
function moveAntiClockwiseForTherapy(currentObjectName) {
    console.log("In moveClockwise; currentObjectName:",currentObjectName)
    var standByObj = getCompRef(mainitem, "standbyTherapyObject")

    if (currentObjectName === "closeBtnObject") {

        //  var caseDefaultComp = getCompRef(standByObj, "caseDefaultBtnObject")
        //console.log("In moveClockwise; currentObjectName:",standByObj.exclusiveColumn.children[0].objectName)
        //    var caseDefaultComp = standByObj.exclusiveColumn.children[0].objectName;

        //var caseDefaultComp = getCompRef(standByObj, standByObj.exclusiveColumn.children[0].objectName)

     //   standByObj.exclusiveColumn.children[0].focus = true
       // standByObj.exclusiveColumn.children[0].forceActiveFocus();
        var logBtnComp = getCompRef(standByObj, "logBtnObject")
        logBtnComp.focus = true
        logBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "caseDefaultBtnObject") {
     //   standByObj.exclusiveColumn.children[1].focus = true
      //  standByObj.exclusiveColumn.children[1].forceActiveFocus();
        //var closeBtnComp = getCompRef(standByObj, "closeBtnObject")
        //closeBtnComp.focus = true
        //closeBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "caseCustom_1BtnObject") {
        standByObj.exclusiveColumn.children[0].focus = true
        standByObj.exclusiveColumn.children[0].forceActiveFocus();
    }
    else if (currentObjectName === "caseCustom_2BtnObject") {
        standByObj.exclusiveColumn.children[1].focus = true
        standByObj.exclusiveColumn.children[1].forceActiveFocus();
        //var defaultBtnComp = getCompRef(standByObj, "defaultBtnObject")
        //defaultBtnComp.focus = true
        //defaultBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "defaultBtnObject") {
 //       var lockBtnComp = getCompRef(standByObj, "lockBtnObject")
   //     lockBtnComp.focus = true
     //   lockBtnComp.forceActiveFocus()
        standByObj.exclusiveColumn.children[2].focus = true
        standByObj.exclusiveColumn.children[2].forceActiveFocus();

    }
    else if (currentObjectName === "lockBtnObject") {
//        standByObj.testColumn.children[2].focus = true
  //      standByObj.testColumn.children[2].forceActiveFocus();
//        var defaultBtnComp = getCompRef(standByObj, "defaultBtnObject")
//        defaultBtnComp.focus = true
//        defaultBtnComp.forceActiveFocus()

        var closeBtnComp = getCompRef(standByObj, "closeBtnObject")
        closeBtnComp.focus = true
        closeBtnComp.forceActiveFocus()
    }
    else if (currentObjectName === "caliberationObject") {
        //standByObj.testColumn.children[1].focus = true
        //standByObj.testColumn.children[1].forceActiveFocus();
               var lockBtnComp = getCompRef(standByObj, "lockBtnObject")
               lockBtnComp.focus = true
               lockBtnComp.forceActiveFocus()

    }
    else if (currentObjectName === "circuitLeakTestObject") {
        standByObj.testColumn.children[2].focus = true
        standByObj.testColumn.children[2].forceActiveFocus();
    }
    else if (currentObjectName === "fullTestObject") {
        standByObj.testColumn.children[1].focus = true
        standByObj.testColumn.children[1].forceActiveFocus();


    }
    else if (currentObjectName === "logBtnObject" || currentObjectName === "standbyTherapyObject") {
        standByObj.testColumn.children[0].focus = true
        standByObj.testColumn.children[0].forceActiveFocus();

//        var closeBtnComp = getCompRef(standByObj, "closeBtnObject")
//        closeBtnComp.focus = true
//        closeBtnComp.forceActiveFocus()
//        var logBtnComp = getCompRef(standByObj, "logBtnObject")
//        logBtnComp.focus = true
//        logBtnComp.forceActiveFocus()
    }
}
