import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.0
import "qrc:/Source/Standby/JavaScript/StandbyController.js" as StandByCntrl
import "qrc:/Source/DefaultSetup/JavaScript/MainDefaultController.js" as DefaultCtrl
import "qrc:/Source/DefaultSetup/JavaScript/PasswordPageController.js" as PwdCtrl

Rectangle {
    id:standby
    objectName : "standbyObject"
    property int iStandbyWidth
    property int iStandbyHeight
    property string strStandbyScreenColor : "pink"
    property string strStandbyScreenBorderColor : "black"
    property int iStandbyScreenBorderWidth : 1
    property int iStandbyScreenBorderRadius : 3

    //System label info
    property int iStatusBtnWidth
    property int iStatusBtnHeight
    property int iStatusLabelfontSize : 18
    property string strStatusLabelfontColor : "Black"
    property int iSystemLabelLeftMargin: 50
    property int iSystemLabelTopMargin: 15

    property int iTestBtnWidth
    property int iTestBtnHeight
    property int iTestTypeRectBorderWidth: 1
    property int iTestTypeRectScrollStateBorderWidth: 2
    property int iTestTypeRectBorderRadius : 5
    property string strTestTypeRectColor: "white"
    property string strTestTypeRectBorderColor: "orange"
    property string strTestTypeRectTouchStateColor: "black"
    property string strTestTypeRectDisableStateColor: "black"
    property string strTestTypeRectFontColor: "black"
    property int iTestTypeRectFontSize: 10
    property string strTestTypeRectScrollStateBorderColor: "blue"


    // property int iTestTypeRectWidth: 150
    // property int iTestTypeRectHeight: 50
    //    property int iTestTypeRectScrollStateBorderColor
    //    property int iTestTypeRectActiveStateBorderColor


    property string strTestResultTextFontColor: "black"
    property int iTestResultTextFontsize: 20

    property string strBatteryTextFontColor: "black"
    property int iBatteryTextFontsize: 20

    property string strBatteryStatusTextFontColor: "black"
    property int iBatteryStatusTextFontsize: 20


    property string strWaterGasLabelTextFontColor: "black"
    property int iWaterGasTextFontsize: 20

    property string strWaterGasValueTextFontColor: "black"
    property int iWaterGasValueTextFontsize: 20


    property int iLogBtnRadius : 3
    property int iLogBtnHeight
    property int iLogBtnWidth
    property int iLogBtnBorderWidth: 1
    property int iLogBtnTopMargin: 70
    property int iLogBtnLeftMargin: 5
    property int iLogBtnScrollStateBorderWidth: 2
    property string strLogBtnBorderColor: "orange"
    property string strLogBtnScrollStateBorderColor: "blue"
    property string strLogBtnTextFontColor: "black"
    property int iLogBtnTextFontsize: 20

    property int iLogLableLeftMargin: 20


    property int iLockScrnBtnWidth
    property int iLockScrnBtnHeight
    property int iLockScrnRadius : 10
    property string strLockScrnBorderColor: "blue"
    property string strLockScrnScrollStateBorderColor: "orange"
    property string strLockScrnTextFontColor: "black"
    property int iLockScrnTextFontsize: 20

    // property int iStandbyScreenWidth
    //property int iStandbyScreenHeight
    //  property int iTextFontColor
    property int iTextFontSize
    property int iLastPassedBtnWidth
    property int iLastPassedBtnHeight
    // property int iStatusBtnWidth
    // property int iStatusBtnHeight
    property int iGridBtnWidth
    property int iGridBtnHeight
    // property int iLockScrnBtnWidth
    //  property int iLockScrnBtnHeight
    //  property int iLogBtnWidth
    //  property int iLogBtnHeight
    property int iCaseDefaultBtnWidth
    property int iCaseDefaultBtnHeight

    property int iDefaultBtnWidth
    property int iDefaultBtnHeight

    property int iCloseBtnWidth
    property int iCloseBtnHeight

    property int iLockIconWidth
    property int iLockIconHeight

    property int wallGasLabelWidth: 200

    //property string strCaseDefaultRectColor: "white"
    //property string strCaseDefaultBorderColor: "black"
    //property string strCaseDefaultTouchStateColor: "black"
    //property string strCaseDefaultDisableStateColor: "black"
    //property string strCaseDefaultEnableStateColor: "black"
    //property string strCaseDefaultRectFontColor: "white"
    //property int iCaseDefaultRectFontSize: 10

    //property string strlockBtnRectColor: "white"
    //property string strlockBtnBorderColor: "black"
    //property string strlockBtnTouchStateColor: "black"
    //property string strlockBtnDisableStateColor: "black"

    //property string strDefaultBtnRectColor: "white"
    //property string strDefaultBtnBorderColor: "black"
    //property string strDefaultBtnTouchStateColor: "black"
    //property string strDefaultBtnDisableStateColor: "black"

    property string strNormalBtnBgColor
    property string strDefaultBtnBgColor
    property string strLogBtnIcon
    property string strLockScreenBtnIcon
    property string strCaseDefaultBtnIcon
    //  property string strCloseBtnIcon
    property string strDefaultSetupBtnIcon
    // property string strBtnBorderColor


    property var testColumn: testTypeColumn
    property var exclusiveColumn: mutuallyExclusiveBtn

    property int icloseBtnRightMargin: 15
    property int icloseBtnTopMargin: 15
    property string strCloseBtnSrc: "qrc:/Config/Assets/icon_close_checkout.png"
    property int itestTypeRectGap: 6
    property int itestTopMargin: 70
    property int itestLeftMargin: 50

    property int iStatusTypeLeftMargin: 5
    property int iStatusTypeTopMargin: 70
    property int iStatusTypeLabelGap: 6



    property  int iLastStatusDoneLabelGap: 4
    property  int iLastStatusDoneTopMargin: 70

    property int iBatteryLabelTopMargin: 20
    property int iBatteryLabelLeftMargin: 50

    property  int iBatteryStatusLeftMargin: 20

    property  int iWaterGasSuppliesLeftMargin: 30
    property  int iWaterGasSuppliesTopMargin: 20
    property  int iWaterGasSuppliesLabelXGap: 5
    property  int iWaterGasSuppliesLabelYGap: 2
    property int iWaterGasSuppliesUnitYGap: 15

    property int iLockScreenLeftMargin: 15
    property int iLockScreenBottomMargin: 15
    property string strLockBtnSrc: ""


    property int iSeperatorTopMargin: 50
    property int iSeperatorWidth: 50
    property int iSeperatorLeftMargin: 600
    property string strSeperatorColor: "black"
    property string strSeperatorBorderColor: "black"

    property int iCaseDefaultLeftMargin: 15
    property int iCaseDefaultTopMargin: 100

    property int iMutExclLeftMargin: 25
    property int iMutExclTopMargin: 25
    property int iMutExclBtnGap: 0
    property string strCaseDefaultSrc: ""
    property string strCustom1Src: ""
    property string strCustom2Src: ""
    //property string strCustomNormalColor : "white"
    property string strCustomNormalFontColor : "black"

    property int iDefaultBtnRightMargin: 15
    property int iDefaultBtnBottomMargin: 15
    property string strDefaultBtnImageSrc: ""

    width: iStandbyWidth
    height: iStandbyHeight
    color: strStandbyScreenColor
    border.width: iStandbyScreenBorderWidth
    border.color: strStandbyScreenBorderColor
    radius: iStandbyScreenBorderRadius
    focus: true

    signal standbyCloseBtnClicked() ;

    //    Rectangle{
    //        width: iStatusBtnWidth
    //        height: iStatusBtnHeight
    //        anchors.left:parent.left
    //        anchors.leftMargin: 30
    //        anchors.top: parent.top
    //        anchors.topMargin: 15



    Label{
        //           anchors.centerIn: parent
        text:qsTrId("System")
        font.pixelSize: iStatusLabelfontSize
        fontSizeMode: Text.Fit
        color: strStatusLabelfontColor
        width: iStatusBtnWidth
        height: iStatusBtnHeight
        anchors.left:parent.left
        anchors.leftMargin: iSystemLabelLeftMargin
        anchors.top: parent.top
        anchors.topMargin: iSystemLabelTopMargin
        horizontalAlignment: "AlignHCenter"
    }

    //        Label {
    //            id: idComplianceStatusTime
    //            text: listcontents[iCOMPLIANCE_TEST_DATE_TIME]
    //            font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
    //            color: strInstructionTxtFontColor
    //            horizontalAlignment: "AlignHCenter"
    //            width : 25
    //        }
    //    }

    //  property string strCloseBtnNormalColor: "white"
    // property string strCloseBtnTouchColor: "blue"

    Rectangle{
        id:closeButton
        objectName : "closeBtnObject"
        width: iCloseBtnWidth
        height: iCloseBtnHeight
        border.color: activeFocus ? strTestTypeRectScrollStateBorderColor : strTestTypeRectBorderColor;
        anchors.right: parent.right
        anchors.rightMargin: icloseBtnRightMargin
        anchors.top: parent.top
        anchors.topMargin: icloseBtnTopMargin
        color: idCloseBtnArea.pressed ?strTestTypeRectTouchStateColor : strTestTypeRectColor
                        border.width: activeFocus ? iLogBtnScrollStateBorderWidth : iLogBtnBorderWidth;
        Image{
            anchors.fill: parent
            source: strCloseBtnSrc
            MouseArea{
                id : idCloseBtnArea
                anchors.fill: parent
                //                onClicked: {

                //                }
                onPressed: {
                }
                onReleased: {
                    standbyCloseBtnClicked()
                }
            }
        }
        Keys.onRightPressed:{
            console.log("Keys.onRightPressed")
            StandByCntrl.moveClockwise("closeBtnObject");

        }
        Keys.onLeftPressed:{
            console.log("Keys.onLeftPressed")
            StandByCntrl.moveAntiClockwise("closeBtnObject");
        }

        Keys.onPressed:{
            console.log("Keys.onPressed")
        }
        Keys.onReleased:{
            console.log("Keys.onReleased")
        }
    }


    ColumnLayout{
        id:testTypeColumn
        spacing: itestTypeRectGap
        //        y: 70

        anchors.top : parent.top
        anchors.topMargin: itestTopMargin
        anchors.left:parent.left
        anchors.leftMargin: itestLeftMargin
        Rectangle{
            id:fullTestRectangle
            objectName : "fullTestObject"
            width: iTestBtnWidth
            height: iTestBtnHeight
            radius: iTestTypeRectBorderRadius
            antialiasing: true
            color: idTestTypeArea.pressed ? strTestTypeRectTouchStateColor : strTestTypeRectColor
            border.width: activeFocus ? iTestTypeRectScrollStateBorderWidth : iTestTypeRectBorderWidth;
            border.color: activeFocus ? strTestTypeRectScrollStateBorderColor : strTestTypeRectBorderColor;
            Text{
                anchors.centerIn: parent
                text:qsTrId("Full_Test")/*"Full Test"*/
                font.pixelSize: iTestTypeRectFontSize
                fontSizeMode: Text.Fit
                color: strTestTypeRectFontColor
            }
            MouseArea{
                id : idTestTypeArea
                anchors.fill: parent
                onClicked: {}
            }
            Keys.onRightPressed:{
                console.log("Keys.onRightPressed")
                StandByCntrl.moveClockwise("fullTestObject");

            }
            Keys.onLeftPressed:{
                console.log("Keys.onLeftPressed")
                StandByCntrl.moveAntiClockwise("fullTestObject");
            }

            Keys.onPressed:{
                console.log("Keys.onPressed")
            }
            Keys.onReleased:{
                console.log("Keys.onReleased")
            }
        }
        Rectangle{
            objectName : "circuitLeakTestObject"
            width: iTestBtnWidth
            height: iTestBtnHeight
            radius: iTestTypeRectBorderRadius ;
            antialiasing: true
            color: idTestTypeArea1.pressed ? strTestTypeRectTouchStateColor : strTestTypeRectColor
            border.width: activeFocus ? iTestTypeRectScrollStateBorderWidth : iTestTypeRectBorderWidth;
            //border.color: sTestTypeRectBorderColor
            border.color: activeFocus ? strTestTypeRectScrollStateBorderColor : strTestTypeRectBorderColor;
            Text{
                anchors.centerIn: parent
                text:qsTrId("Circuit_Leak_Test")/*"Circuit Leak Test"*/
                font.pixelSize: iTestTypeRectFontSize
                fontSizeMode: Text.Fit
                color: strTestTypeRectFontColor
            }
            MouseArea{
                id :idTestTypeArea1
                anchors.fill: parent
                onClicked: {}
            }
            Keys.onRightPressed:{
                console.log("Keys.onRightPressed")
                StandByCntrl.moveClockwise("circuitLeakTestObject");

            }
            Keys.onLeftPressed:{
                console.log("Keys.onLeftPressed")
                StandByCntrl.moveAntiClockwise("circuitLeakTestObject");
            }

            Keys.onPressed:{
                console.log("Keys.onPressed")
            }
            Keys.onReleased:{
                console.log("Keys.onReleased")
            }
        }
        Rectangle{
            id:calibrations
            objectName : "caliberationObject"
            width: iTestBtnWidth
            height: iTestBtnHeight
            radius: iTestTypeRectBorderRadius ;
            antialiasing: true
            color: idTestTypeArea2.pressed ? strTestTypeRectTouchStateColor : strTestTypeRectColor
            border.width: activeFocus ? iTestTypeRectScrollStateBorderWidth : iTestTypeRectBorderWidth;
            //border.color: sTestTypeRectBorderColor
            border.color: activeFocus ? strTestTypeRectScrollStateBorderColor : strTestTypeRectBorderColor;
            Text{
                anchors.centerIn: parent
                text:qsTrId("Calibrations")/*"Calibrations"*/
                font.pixelSize: iTestTypeRectFontSize
                fontSizeMode: Text.Fit
                color: strTestTypeRectFontColor
            }
            MouseArea{
                id :idTestTypeArea2
                anchors.fill: parent
                onClicked: {}
            }

            Keys.onRightPressed:{
                console.log("Keys.onRightPressed")
                StandByCntrl.moveClockwise("caliberationObject");

            }
            Keys.onLeftPressed:{
                console.log("Keys.onLeftPressed")
                StandByCntrl.moveAntiClockwise("caliberationObject");

            }

            Keys.onPressed:{
                console.log("Keys.onPressed")
            }
            Keys.onReleased:{
                console.log("Keys.onReleased")
            }
        }

        //        Rectangle{
        //            id:batteryInfo
        //            width: iTestBtnWidth
        //            height: iTestBtnHeight
        //            radius: 5 ; antialiasing: true
        //            color: sTestTypeRectColor
        //            anchors.top: calibrations.bottom
        //            anchors.topMargin: 20
        //            border.width: 0

        //        }

    }

    Label{
        //            anchors.centerIn: parent
        id : idBatteryLabel
        text:qsTrId("Battery")/*"Battery"*/
        font.pixelSize: iBatteryTextFontsize
        fontSizeMode: Text.Fit
        color:strBatteryTextFontColor
        anchors.top: testTypeColumn.bottom
        anchors.topMargin: iBatteryLabelTopMargin
        anchors.left: parent.left
        anchors.leftMargin: iBatteryLabelLeftMargin
        horizontalAlignment: "AlignHCenter"
    }


    ColumnLayout{
        id:statusTypeColumn
        spacing: iStatusTypeLabelGap
        //y: 70
        anchors{
            left:testTypeColumn.right
            leftMargin: iStatusTypeLeftMargin
            top : parent.top
            topMargin: iStatusTypeTopMargin
        }
        //        Rectangle{
        //            id:status
        //            width: iStatusBtnWidth
        //            height: iStatusBtnHeight
        //            color: sTestTypeRectColor
        //            border.width: 0
        Label{
            //                anchors.centerIn: parent
            text:qsTrId("Pass")/*"Pass"*/
            font.pixelSize: iTestResultTextFontsize
            //            fontSizeMode: Text.Fit
            color: strTestResultTextFontColor
            width: iStatusBtnWidth
            height: iStatusBtnHeight
            horizontalAlignment: "AlignHCenter"
        }
        //        }
        //        Rectangle{
        //            width: iStatusBtnWidth
        //            height: iStatusBtnHeight
        //            color: sTestTypeRectColor
        //            border.width: 0
        Label{
            //                anchors.centerIn: parent
            text:qsTrId("Pass")/*"Pass"*/
            font.pixelSize: iTestResultTextFontsize
            fontSizeMode: Text.Fit
            color: strTestResultTextFontColor
            width: iStatusBtnWidth
            height: iStatusBtnHeight
            horizontalAlignment: "AlignHCenter"
        }

        //    }
        //        Rectangle{
        //            id:thirdStatusComponent
        //            width: iStatusBtnWidth
        //            height: iStatusBtnHeight
        //            color: sTestTypeRectColor
        //            border.width: 0
        Label{
            id:thirdStatusComponent
            //  anchors.centerIn: parent
            text:qsTrId("Done")/*"Done"*/
            font.pixelSize: iTestResultTextFontsize
            //fontSizeMode: Text.Fit
            color: strTestResultTextFontColor
            width: iStatusBtnWidth
            height: iStatusBtnHeight
            horizontalAlignment: "AlignHCenter"
        }
        //}
        //        }
        //        Rectangle{
        //            width: iTestBtnWidth
        //            height: iTestBtnHeight
        //            color: sTestTypeRectColor
        //            border.width: 0
        //            anchors.top: thirdStatusComponent.bottom
        //            anchors.topMargin: 20


    }


    Label{
        width: iTestBtnWidth
        height: iTestBtnHeight
        anchors.top: idBatteryLabel.top
        //anchors.topMargin: 20
        anchors.left: idBatteryLabel.right
        anchors.leftMargin: iBatteryStatusLeftMargin
        //                anchors.centerIn: parent
        text: "50" + "% " + qsTrId("Charging") /*"50% Charging"*/
        font.pixelSize: iBatteryStatusTextFontsize
        //   fontSizeMode: Text.Fit
        color: strBatteryStatusTextFontColor
        horizontalAlignment: "AlignHCenter"
    }

    property int iLastStatusDoneLeftMargin: 30
    ColumnLayout{
        id:lastTestDone
        spacing: iLastStatusDoneLabelGap

        anchors{
            left:statusTypeColumn.right
            leftMargin: iLastStatusDoneLeftMargin
            top : parent.top
            topMargin: iLastStatusDoneTopMargin
        }
        //        Rectangle{
        //            width: iLastPassedBtnWidth
        //            height: iLastPassedBtnHeight
        //            color: sTestTypeRectColor
        //            border.width: 0
        Label{
            width: iLastPassedBtnWidth
            height: iLastPassedBtnHeight

            //anchors.centerIn: parent
            text: "6" + qsTrId("h_ago")/*"6h ago"*/
            font.pixelSize: iTestResultTextFontsize
            //fontSizeMode: Text.Fit
            color: strTestResultTextFontColor
            horizontalAlignment: "AlignHCenter"
        }
        //}
        //        Rectangle{
        //            width: iLastPassedBtnWidth
        //            height: iLastPassedBtnHeight
        //            color: sTestTypeRectColor
        //            border.width: 0
        Label{
            width: iLastPassedBtnWidth
            height: iLastPassedBtnHeight
            //  anchors.centerIn: parent
            text:"1" + qsTrId("h_ago")/*"1h ago"*/
            font.pixelSize: iTestResultTextFontsize
            fontSizeMode: Text.Fit
            color: strTestResultTextFontColor
            horizontalAlignment: "AlignHCenter"
        }
        //        }
        //Rectangle{
        //width: iLastPassedBtnWidth
        //height: iLastPassedBtnHeight
        //  color: sTestTypeRectColor
        // border.width: 0
        Label{
            width: iLastPassedBtnWidth
            height: iLastPassedBtnHeight

            //                anchors.centerIn: parent
            text: "6" + qsTrId("days_ago")/*"6 days ago"*/
            font.pixelSize: iTestResultTextFontsize
            fontSizeMode: Text.Fit
            color: strTestResultTextFontColor
            horizontalAlignment: "AlignHCenter"
        }
        //}
    }
   // property int iWallGasBtnTopMargin: 50
    property int  iWaterGasSuppliesUnitLeftMargin: 200
  //  property int  iWaterGasSuppliesUnitTopMargin: 10
    ColumnLayout{
        anchors.left: parent.left
        anchors.top: idWaterValuesGrid.top
        spacing: iWaterGasSuppliesLabelYGap
        Label {
            id:wallGasBtn
            width: wallGasLabelWidth
            height: iGridBtnHeight
            leftPadding:  iWaterGasSuppliesLeftMargin
            text: qsTrId("Wall_Gas_supplies")/*qsTr("Wall Gas Supplies")*/
            color: strWaterGasLabelTextFontColor
            font.pixelSize:  iWaterGasTextFontsize
            horizontalAlignment: "AlignHCenter"
            fontSizeMode: Text.Fit
        }


        Label {
            width: iTestBtnWidth
            height: iTestBtnHeight
            leftPadding:  iWaterGasSuppliesUnitLeftMargin
            horizontalAlignment: "AlignHCenter"
            text: qsTrId("psi")
            color: strWaterGasValueTextFontColor
            font.pixelSize: iWaterGasValueTextFontsize
            fontSizeMode: Text.Fit
        }
    }

    property int iWaterGasSuppliesValuesLeftMargin: 160
    GridLayout{
        id : idWaterValuesGrid
        columns: 3
        columnSpacing: iWaterGasSuppliesLabelXGap
        rowSpacing: iWaterGasSuppliesLabelYGap
        rows:2
        anchors.left: idBatteryLabel.right
        anchors.leftMargin: iWaterGasSuppliesValuesLeftMargin
        anchors.top: idBatteryLabel.bottom
        anchors.topMargin: iWaterGasSuppliesTopMargin
        Label {
            width: iGridBtnWidth
            height: iGridBtnHeight
            text: qsTrId("O2")
            font.pixelSize: iWaterGasTextFontsize
            fontSizeMode: Text.Fit
            color: strWaterGasLabelTextFontColor
            horizontalAlignment: "AlignHCenter"
        }
        Label {
            width: iGridBtnWidth
            height: iGridBtnHeight
            text: qsTrId("Air")/*("Air")*/
            font.pixelSize: iWaterGasTextFontsize
            fontSizeMode: Text.Fit
            color: strWaterGasLabelTextFontColor
            horizontalAlignment: "AlignHCenter"
        }
        Label {
            text: qsTrId("N2O")
            fontSizeMode: Text.Fit
            color: strWaterGasLabelTextFontColor
            horizontalAlignment: "AlignHCenter"
            font.pixelSize: iWaterGasTextFontsize
        }
        Label {
            id : idO2Value
            width: iGridBtnWidth
            height: iGridBtnHeight
            text: qsTr("134")
            fontSizeMode: Text.Fit
            color: strWaterGasValueTextFontColor
            font.pixelSize: iWaterGasValueTextFontsize
            horizontalAlignment: "AlignHCenter"
        }
        Label {
            width: iGridBtnWidth
            height: iGridBtnHeight
            text: qsTr("563")
            fontSizeMode: Text.Fit
            color: strWaterGasValueTextFontColor
            font.pixelSize: iWaterGasValueTextFontsize
            horizontalAlignment: "AlignHCenter"
        }
        Label {
            width: iGridBtnWidth
            height: iGridBtnHeight
            text: qsTr("0")
            fontSizeMode: Text.Fit
            color: strWaterGasValueTextFontColor
            font.pixelSize: iWaterGasValueTextFontsize
            horizontalAlignment: "AlignHCenter"
        }
    }

    Rectangle{
        id:logButton
        objectName : "logBtnObject"
        width: iLogBtnWidth
        height: iLogBtnHeight
        radius: iLogBtnRadius
        anchors.top : parent.top
        anchors.topMargin: iLogBtnTopMargin
        antialiasing: true
        border.color: activeFocus ? strLogBtnScrollStateBorderColor: strLogBtnBorderColor ;
        border.width: activeFocus ? iLogBtnScrollStateBorderWidth : iLogBtnBorderWidth;
        anchors.left: lastTestDone.right
        anchors.leftMargin: iLogBtnLeftMargin
        color: idLogbtnArea.pressed ? strTestTypeRectTouchStateColor : strTestTypeRectColor
        Image{
            anchors.fill: parent
            source: strLogBtnIcon//strLockBtnSrc
            MouseArea{
                id : idLogbtnArea
                anchors.fill: parent
                onClicked: {
                    console.log("Log Menu clicked !");
                }
            }
        }
        Keys.onRightPressed:{
            console.log("Keys.onRightPressed")
            StandByCntrl.moveClockwise("logBtnObject");

        }
        Keys.onLeftPressed:{
            console.log("Keys.onLeftPressed")
            StandByCntrl.moveAntiClockwise("logBtnObject");
        }

        Keys.onPressed:{
            console.log("Keys.onPressed")
        }
        Keys.onReleased:{
            console.log("Keys.onReleased")
        }
    }



    Label{
        text:qsTrId("Log") /*"Log"*/
        font.pixelSize: iLogBtnTextFontsize
        fontSizeMode: Text.Fit
        anchors.top: logButton.bottom
        anchors.left: lastTestDone.right
        anchors.leftMargin: iLogLableLeftMargin
        color: strLogBtnTextFontColor
    }



    Rectangle{
        id:lockScreenBtn
        objectName : "lockBtnObject"
        width: iLockScrnBtnWidth
        height: iLockScrnBtnHeight
        //        border.color: "black"
        border.color: activeFocus ? strLockScrnScrollStateBorderColor: strLockScrnBorderColor ;
        radius: iLockScrnRadius
        antialiasing: true
        color: idlckArea.pressed ? strTestTypeRectTouchStateColor : strTestTypeRectColor
                border.width: activeFocus ? iLogBtnScrollStateBorderWidth : iLogBtnBorderWidth;
        anchors{
            left: parent.left
            leftMargin: iLockScreenLeftMargin
            bottom: parent.bottom
            bottomMargin: iLockScreenBottomMargin
        }

        Image{
            id:lockImage
            width: iLockIconWidth
            height: iLockIconHeight
            anchors.left: parent.left
            source: strLockScreenBtnIcon//strLockBtnSrc
        }
        Text{
            anchors.left: lockImage.left
            anchors.centerIn: parent
            text:qsTrId("Lock_Screen")
            font.pixelSize: iLockScrnTextFontsize
            fontSizeMode: Text.Fit
            color: strLockScrnTextFontColor
        }
        MouseArea{
            id : idlckArea
            anchors.fill: parent
            onClicked: {
                console.log("lock Screen button is clicked ")
            }
        }
        Keys.onRightPressed:{
            console.log("Keys.onRightPressed")
            StandByCntrl.moveClockwise("lockBtnObject");

        }
        Keys.onLeftPressed:{
            console.log("Keys.onLeftPressed")
            StandByCntrl.moveAntiClockwise("lockBtnObject");
        }

        Keys.onPressed:{
            console.log("Keys.onPressed")
        }
        Keys.onReleased:{
            console.log("Keys.onReleased")
        }
    }

    property int iDefaultSetUpBtnTxtFontSize: 20
    Rectangle{
        id:defaultSettingsBtn
        objectName : "defaultBtnObject"
        width: iDefaultBtnWidth
        height: iDefaultBtnHeight
        //        border.color: "black"
        border.color: activeFocus ? strLockScrnScrollStateBorderColor: strLockScrnBorderColor ;
        radius: iLockScrnRadius
        antialiasing: true
        color: idDefaultBtnArea.pressed ? strTestTypeRectTouchStateColor : strTestTypeRectColor
                border.width: activeFocus ? iLogBtnScrollStateBorderWidth : iLogBtnBorderWidth;
        anchors{
            right: parent.right
            rightMargin: iDefaultBtnRightMargin
            bottom: parent.bottom
            bottomMargin: iDefaultBtnBottomMargin
        }

        Image{
            id:defaultSettings
            width: iLockIconWidth
            height: iLockIconHeight
            anchors.right: parent.right
//            anchors.top: parent
//            anchors.bottom: parent

            source: strDefaultSetupBtnIcon//strDefaultBtnImageSrc
        }
        Text{
            anchors.left: lockImage.left
            anchors.centerIn: parent
            text:qsTrId("Defaults_Setup")
            font.pixelSize: iDefaultSetUpBtnTxtFontSize
            fontSizeMode: Text.Fit
        }
        MouseArea{
            id : idDefaultBtnArea
            anchors.fill: parent
            onClicked: {
                console.log("Default setup button is clicked ")
                PwdCtrl.createPasswordPageComponent();
                DefaultCtrl.hideComponent();
            }
        }
        Keys.onRightPressed:{
            console.log("Keys.onRightPressed")
            StandByCntrl.moveClockwise("defaultBtnObject");

        }
        Keys.onLeftPressed:{
            console.log("Keys.onLeftPressed")
            StandByCntrl.moveAntiClockwise("defaultBtnObject");

        }

        Keys.onPressed:{
            console.log("Keys.onPressed")
        }
        Keys.onReleased:{
            console.log("Keys.onReleased")
        }

    }


    Rectangle{
        id:separator
        width: 1
        height: parent.height - 50
        anchors.top: parent.top
        anchors.topMargin: iSeperatorTopMargin
        anchors.left: parent.left
        anchors.leftMargin: iSeperatorLeftMargin

        color: strSeperatorColor
        //x:600
        border.width: iSeperatorWidth
        border.color: strSeperatorBorderColor
    }

    property string strCaseDefaultLabelColor: "black"
    Label{
        id:caseDefaults
        text:qsTrId("Case_Defaults")
        font.pixelSize: iTextFontSize
        fontSizeMode: Text.Fit
        anchors.left:separator.left
        anchors.leftMargin: iCaseDefaultLeftMargin
        anchors.top: parent.top
        anchors.topMargin: iCaseDefaultTopMargin
        color:strCaseDefaultLabelColor
    }



    ColumnLayout{
        id:mutuallyExclusiveBtn
        spacing: iMutExclBtnGap
        anchors.left: separator.left
        anchors.leftMargin: iMutExclLeftMargin
        anchors.top:caseDefaults.bottom
        anchors.topMargin: iMutExclTopMargin

        Rectangle{
            objectName : "caseDefaultBtnObject"
            width: iCaseDefaultBtnWidth
            height: iCaseDefaultBtnHeight
            color: strDefaultBtnBgColor
            //            border.color: "black"
            border.color: activeFocus ? strTestTypeRectScrollStateBorderColor : strTestTypeRectBorderColor;
                    border.width: activeFocus ? iLogBtnScrollStateBorderWidth : iLogBtnBorderWidth;
            Image{
                width: iLockIconWidth
                height: iLockIconHeight
                anchors.left: parent.left
                source: strCaseDefaultBtnIcon//strCaseDefaultSrc
            }
            Text{
                text:qsTrId("Default")
                font.pixelSize: iTextFontSize
                fontSizeMode: Text.Fit
                anchors.centerIn: parent
                font.bold: true
                color: strCustomNormalFontColor

            }
            Keys.onRightPressed:{
                console.log("Keys.onRightPressed")
                StandByCntrl.moveClockwise("caseDefaultBtnObject");

            }
            Keys.onLeftPressed:{
                console.log("Keys.onLeftPressed")
                StandByCntrl.moveAntiClockwise("caseDefaultBtnObject");

            }

            Keys.onPressed:{
                console.log("Keys.onPressed")
            }
            Keys.onReleased:{
                console.log("Keys.onReleased")
            }
        }

        Rectangle{
            objectName : "caseCustom_1BtnObject"
            width: iCaseDefaultBtnWidth
            height: iCaseDefaultBtnHeight
            color: strNormalBtnBgColor
            border.color: activeFocus ? strTestTypeRectScrollStateBorderColor : strTestTypeRectBorderColor;
                    border.width: activeFocus ? iLogBtnScrollStateBorderWidth : iLogBtnBorderWidth;
            Image{
                width: iLockIconWidth
                height: iLockIconHeight
                anchors.left: parent.left
                source: strCaseDefaultBtnIcon//strCustom1Src
            }
            Text{
                text:qsTrId("Custom_1")
                font.pixelSize: iTextFontSize
                fontSizeMode: Text.Fit
                anchors.centerIn: parent
                color: strCustomNormalFontColor
            }
            Keys.onRightPressed:{
                console.log("Keys.onRightPressed")
                StandByCntrl.moveClockwise("caseCustom_1BtnObject");

            }
            Keys.onLeftPressed:{
                console.log("Keys.onLeftPressed")
                StandByCntrl.moveAntiClockwise("caseCustom_1BtnObject");
            }

            Keys.onPressed:{
                console.log("Keys.onPressed")
            }
            Keys.onReleased:{
                console.log("Keys.onReleased")
            }
        }
        Rectangle{
            objectName : "caseCustom_2BtnObject"
            width: iCaseDefaultBtnWidth
            height: iCaseDefaultBtnHeight
            color: strNormalBtnBgColor
            border.color: activeFocus ? strTestTypeRectScrollStateBorderColor : strTestTypeRectBorderColor;
                    border.width: activeFocus ? iLogBtnScrollStateBorderWidth : iLogBtnBorderWidth;
            Image{
                width: iLockIconWidth
                height: iLockIconHeight
                anchors.left: parent.left
                source: strCaseDefaultBtnIcon //strCustom2Src
            }
            Text{
                text:qsTrId("Custom_2")
                font.pixelSize: iTextFontSize
                fontSizeMode: Text.Fit
                anchors.centerIn: parent
                color: strCustomNormalFontColor
            }
            Keys.onRightPressed:{
                console.log("Keys.onRightPressed")
                StandByCntrl.moveClockwise("caseCustom_2BtnObject");

            }
            Keys.onLeftPressed:{
                console.log("Keys.onLeftPressed")
                StandByCntrl.moveAntiClockwise("caseCustom_2BtnObject");
            }

            Keys.onPressed:{
                console.log("Keys.onPressed")
            }
            Keys.onReleased:{
                console.log("Keys.onReleased")
            }
        }
    }
    Keys.onRightPressed:{
        console.log("Keys.onRightPressed")
        StandByCntrl.moveClockwise("standbyObject");

    }
    Keys.onLeftPressed:{
        console.log("Keys.onLeftPressed")
        StandByCntrl.moveAntiClockwise("standbyObject");
    }

    Keys.onPressed:{
        console.log("Keys.onPressed")
    }
    Keys.onReleased:{
        console.log("Keys.onReleased")
    }

    //    MouseArea{
    //        anchors.fill: parent
    //        onClicked: {
    //            console.log("on clicked")
    //        }
    //    }
    Component.onCompleted:{
        standby.forceActiveFocus()
        console.log("Component Created : \n ")
    }
}
