import QtQuick 2.9
import QtQuick.Controls 2.2
import "qrc:/Source/Checkout/JavaScript/CheckoutResController.js" as CheckoutResCntrl

Rectangle {
    id : idCheckOutResultScreen
    objectName: "CheckoutResScreen"

    property int iActionBtnBorderWidth
    property int iActionBtnRadius
    property int iCloseBtnWidth
    property int iCloseBtnHeight
    property int iMenuFontSize
    property int iStatusIconWidth
    property int iStatusIconHeight
    property int iInstructionTxtFontSize
    property string strMenuFontColor: "white"
    property string strInstructionTxtFontColor: "white"
    property string strActionBtnBgColor
    property string strActionBtnBorderColor
    property string strScrollStateBorderColor
    property string strChkoutAreaColor
    property string strPassIconSource
    property string strFailIconSource
    property string strConditionalPassIconSource
    property string strFontFamilylabel: geFont.geFontStyle()
    property string strCloseBtnSource
    property string strCloseBtnTchSource
    property var listcontents


    anchors.fill: parent
    color: strChkoutAreaColor
    z: 10

    MouseArea{
        anchors.fill: parent
        onClicked: {
        }
    }
    //**********************************************************
    // Purpose:
    // This function is used to update labels with xml list data.
    //**********************************************************
    function updateTextWithListValues() {
        idVentLeakTestStatus.text = listcontents[4]
        idVentLeakTestStatusTime.text = listcontents[5]
        idComplianceTestStatus.text = listcontents[8]
        idComplianceStatusTime.text = listcontents[9]
        idLeftVepoTestStatus.text = listcontents[12]
        idLeftVepoStatusTime.text = listcontents[13]
        idRightVepoTestStatus.text = listcontents[16]
        idRightVepoStatusTime.text = listcontents[17]
        idGasControlsTestStatus.text = listcontents[20]
        idGasControlsStatusTime.text = listcontents[21]
        idCircuitLeakTestStatus.text = listcontents[26]
        idCircuitLeakStatusTime.text = listcontents[27]
        idEquipmentStatus.text = listcontents[23]
        idEquipmentStatusTime.text = listcontents[24]
    }
    //**********************************************************
    // Purpose:
    // This function is used to update status icon.
    // Input parameters:
    // testStatus : It holds the status of test
    // statusIcon : It holds the image of status icon
    //**********************************************************
    function updateStatusIcon(testStatus, statusIcon) {
        console.log("testStatus, statusIcon",testStatus, statusIcon)
        if (testStatus === "Pass") {
            statusIcon.source = strPassIconSource
        } else if (testStatus === "Fail") {
            statusIcon.source = strFailIconSource;
        } else if (testStatus === "ConditionalPass") {
            statusIcon.source = strConditionalPassIconSource;
        } else if (testStatus === "Skipped") {
            statusIcon.visible = false;
        }
    }

    Rectangle {
        id: closeBtn
        objectName: "CheckoutResScreenClose"
        height: iCloseBtnHeight
        width: iCloseBtnWidth
        color: closeMouseArea.pressed ? "#4565A6" : strActionBtnBgColor
        anchors{right: parent.right;rightMargin: 15;top: parent.top;topMargin: 15}
        border.color: activeFocus ? strScrollStateBorderColor : strActionBtnBorderColor
        border.width: iActionBtnBorderWidth
        radius: iActionBtnRadius
        property  bool bPressed: false

        Image {
            id: closeBtnIcon
            source: strCloseBtnSource
            anchors.centerIn: parent
        }
        MouseArea {
            id: closeMouseArea
            anchors.fill: closeBtn

            onPressed: {
                console.log("close btn pressed")
                closeBtnIcon.source = strCloseBtnTchSource
                closeBtn.bPressed = true
                closeBtn.border.width = 0
                console.log("bPressed",bPressed)
            }
            onReleased: {
                console.log("close btn released")
                if (closeBtn.bPressed === true) {
                    closeBtn.bPressed = false;
                    closeBtn.border.width = 2
                    closeBtnIcon.source = strCloseBtnSource
                    CheckoutResCntrl.destroyCheckoutResScreen()
                }
            }
        }

        Keys.onRightPressed: {
            //Do nothing
        }
        Keys.onLeftPressed: {
            //Do nothing
        }
        Keys.onReturnPressed: {
            CheckoutResCntrl.destroyCheckoutResScreen()
        }
    }

    Label {
        id: idTestMenuTitle
        text: qsTrId("CheckOut_testRes_title")
        font{pixelSize: iMenuFontSize;family: strFontFamilylabel}
        color: strMenuFontColor
        anchors{left: parent.left;leftMargin: 20;top: parent.top;topMargin: 25}
    }

    Label {
        id: idVentLeakTest
        text: qsTrId("VentLeak_testRes_title") + ",< 150 ml/min"
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 70;top: parent.top;topMargin: 100}
    }
    Image {
        id: idVentStatusIcon
        source: strPassIconSource
        width: iStatusIconWidth
        height: iStatusIconHeight
        anchors{left: parent.left;leftMargin: 20;top: parent.top;topMargin: 95}
    }
    Label {
        id: idVentLeakTestStatus
        text: listcontents[4]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 625;top: parent.top;topMargin: 100}
        width : 10
        horizontalAlignment: "AlignHCenter"
    }
    Label {
        id: idVentLeakTestStatusTime
        text: listcontents[5]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 825;top: parent.top;topMargin: 100}
        horizontalAlignment: "AlignHCenter"
        width : 25
    }
    Image {
        id: idComplianceStatusIcon
        source: strPassIconSource
        width: iStatusIconWidth
        height: iStatusIconHeight
        anchors{left: parent.left;leftMargin: 20;top: parent.top;topMargin: 155}
    }
    Label {
        id: idComplianceTest
        text: qsTrId("Compilance_testRes_title") + "1 ml/cmH20"
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 70;top: parent.top;topMargin: 160}
    }
    Label {
        id: idComplianceTestStatus
        text: listcontents[8]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 625;top: parent.top;topMargin: 160}
        horizontalAlignment: "AlignHCenter"
        width : 10
    }
    Label {
        id: idComplianceStatusTime
        text: listcontents[9]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 825;top: parent.top;topMargin: 160}
        horizontalAlignment: "AlignHCenter"
        width : 25
    }
    Image {
        id: idLeftVepoStatusIcon
        source: strPassIconSource
        width: iStatusIconWidth
        height: iStatusIconHeight
        anchors{left: parent.left;leftMargin: 20;top: parent.top;topMargin: 215}
    }
    Label {
        id: idLeftVepoTest
        text: qsTrId("LeftVepo_testRes_title") + ",< 50 ml/min"
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 70;top: parent.top;topMargin: 220}
    }
    Label {
        id: idLeftVepoTestStatus
        text: listcontents[12]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 625;top: parent.top;topMargin: 220}
        horizontalAlignment: "AlignHCenter"
        width : 10
    }
    Label {
        id: idLeftVepoStatusTime
        text: listcontents[13]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 825;top: parent.top;topMargin: 220}
        width : 25
        horizontalAlignment: "AlignHCenter"
    }
    Image {
        id: idRightVepoStatusIcon
        source: strPassIconSource
        width: iStatusIconWidth
        height: iStatusIconHeight
        anchors{left: parent.left;leftMargin: 20;top: parent.top;topMargin: 275}
    }
    Label {
        id: idRightVepoTest
        text: qsTrId("RightVepo_testRes_title")
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 70;top: parent.top;topMargin: 280}
    }
    Label {
        id: idRightVepoTestStatus
        text: listcontents[16]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 625;top: parent.top;topMargin: 280}
        horizontalAlignment: "AlignHCenter"
        width : 10
    }
    Label {
        id: idRightVepoStatusTime
        text: listcontents[17]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 825;top: parent.top;topMargin: 280}
        horizontalAlignment: "AlignHCenter"
        width : 25
    }
    Image {
        id: idGasControlsIcon
        source: strPassIconSource
        width: iStatusIconWidth
        height: iStatusIconHeight
        anchors{left: parent.left;leftMargin: 20;top: parent.top;topMargin: 335}
    }
    Label {
        id: idGasControlsTest
        text: qsTrId("GasControls_testRes_title")
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 70;top: parent.top;topMargin: 340}
    }
    Label {
        id: idGasControlsTestStatus
        text: listcontents[20]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 625;top: parent.top;topMargin: 340}
        width : 10
        horizontalAlignment: "AlignHCenter"
    }
    Label {
        id: idGasControlsStatusTime
        text: listcontents[21]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 825;top: parent.top;topMargin: 340}
        horizontalAlignment: "AlignHCenter"
        width : 25
    }
    Image {
        id: idCircuitLeakIcon
        source: strPassIconSource
        width: iStatusIconWidth
        height: iStatusIconHeight
        anchors{left: parent.left;leftMargin: 20;top: parent.top;topMargin: 395}
    }
    Label {
        id: idCircuitLeakTest
        text: qsTrId("CircuitLeak_testRes_title") + ", < 150 ml/min"
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 70;top: parent.top;topMargin: 400}
    }
    Label {
        id: idCircuitLeakTestStatus
        text: listcontents[26]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 625;top: parent.top;topMargin: 400}
        width : 10
        horizontalAlignment: "AlignHCenter"
    }
    Label {
        id: idCircuitLeakStatusTime
        text: listcontents[27]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 825;top: parent.top;topMargin: 400}
        horizontalAlignment: "AlignHCenter"
        width : 25
    }
    Image {
        id: idEquipmentIcon
        source: strPassIconSource
        width: iStatusIconWidth
        height: iStatusIconHeight
        anchors{left: parent.left;leftMargin: 20;top: parent.top;topMargin: 455}
    }
    Label {
        id: idEquipmentTest
        text: qsTrId("EquipCheck_testRes_title")
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 70;top: parent.top;topMargin: 460}
    }
    Label {
        id: idEquipmentStatus
        text: listcontents[23]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 625;top: parent.top;topMargin: 460}
        horizontalAlignment: "AlignHCenter"
        width : 10
    }
    Label {
        id: idEquipmentStatusTime
        text: listcontents[24]
        font{pixelSize: iInstructionTxtFontSize;family: strFontFamilylabel}
        color: strInstructionTxtFontColor
        anchors{left: parent.left;leftMargin: 825;top: parent.top;topMargin: 460}
        horizontalAlignment: "AlignHCenter"
        width : 25
    }
    Component.onCompleted: {
        console.log("strCloseBtnSource" + strCloseBtnSource);
        closeBtn.focus = true
        checkoutReader.readXMLContents()
        listcontents = checkoutReader.getListOfString();
        updateTextWithListValues();
        updateStatusIcon(idVentLeakTestStatus.text, idVentStatusIcon)
        updateStatusIcon(idComplianceTestStatus.text, idComplianceStatusIcon)
        updateStatusIcon(idLeftVepoTestStatus.text, idLeftVepoStatusIcon)
        updateStatusIcon(idRightVepoTestStatus.text, idRightVepoStatusIcon)
        updateStatusIcon(idGasControlsTestStatus.text, idGasControlsIcon)
        updateStatusIcon(idCircuitLeakTestStatus.text, idCircuitLeakIcon)
        updateStatusIcon(idEquipmentStatus.text, idEquipmentIcon)
    }

    Component.onDestruction: {
        console.log("strPassIconSource" + strPassIconSource)
        console.log("strFailIconSource" + strFailIconSource)
        var buttonGrpRef = getGroupRef("ButtonGroup")
        console.log("***Destruction****",buttonGrpRef.strIconPreviousState)
        if (buttonGrpRef.strIconPreviousState === "Scroll") {
            buttonGrpRef.buttongrpnam.children[0].focus = true
        } else if (buttonGrpRef.strIconPreviousState === "Touched") {
            buttonGrpRef.focus = true
        } else {
            var ventGrpReference=getGroupRef("VentGroup")
            ventGrpReference.focus = true
        }
    }
}
