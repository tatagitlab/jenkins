import QtQuick 2.9
import QtQuick.Controls 2.2
import "qrc:/Source/Checkout/JavaScript/CheckoutController.js" as CheckoutCntrl
import "qrc:/Source/Checkout/JavaScript/CheckoutResController.js" as CheckoutResCntrl

Rectangle{
    property var instructionRectId: instructRectangle
    property var titleRectId: otherEquipTitle
    property var skipBtnId: skipBtn
    property var closeBtnId: equipTestClose
    objectName: "OtherEquipListObj"

    property int iActionBtnWidth
    property int iActionBtnHeight
    property int iActionBtnBorderWidth
    property int iActionBtnRadius
    property int iScrollStateBorderWidth
    property int iFontSize
    property int iInstructionTextArea
    property int iTitleFontSize
    property int iChkoutAreaHeight
    property int iChkoutAreaWidth
    property int iTitleAreaHeight
    property int iTitleAreaWidth
    property int iChkoutInstAreaHeight
    property int iChkoutInstAreaWidth
    property int iChkoutPicAreaHeight
    property int iChkoutPicAreaWidth
    property int iCurrentPag
    property int iPriorTestCheckInstructionWidth
    property int iPriorTestCheckInstructionHeight
    property int iPriorTestCheckInstructionLeftMargin
    property int iConfirmButtonLeftMargin
    property int iConfirmButtonBottomMargin
    property int iSkipButtonLeftMargin
    property int iSkipButtonBottomMargin
    property int iSubTitleTextPixelSize
    property int iSubTitleTextTopMargin
    property int iSubTitleTextLeftMargin
    property int iPreCheckListTopMargin
    property int iCloseButtonWidth
    property int iCloseButtonHeight
    property int iCloseButtonRadius
    property int iCloseButtonTopMargin
    property int iCloseButtonRightMargin
    property int iTitleTextTopMargin
    property int iTitleTextLeftMargin
    property int iCurrentPage
    property int iTroubleshootTipWidth: 520
    property int iTroubleshootTipHeight: 670
    property int iTroubleshootTipBorderWidth: 1
    property int iTroubleshootTipRightMargin: 20
    property int iTroubleshootTipBottomMargin: 20
    property int iFirstPriorCheckInstructionTopMargin
    property int iSecondPriorCheckInstructionTopMargin
    property int iThirdPriorCheckInstructionTopMargin
    property int iFourthPriorCheckInstructionTopMargin

    property string strPreCheckListRectColor
    property string strChkoutAreaColor
    property string strFontColor
    property string strActionBtnBgColor
    property string strActionBtnBorderColor
    property string strCloseBtnSource
    property string strCloseBtnTchSource
    property string strScrollStateBorderColor
    property string strTroubleshootTipBorderColor: "white"

    visible: true
    width: iChkoutAreaWidth
    height: iChkoutAreaHeight
    id: otherEquipcheckoutScreen
    color: strChkoutAreaColor
    z: 3

    MouseArea{
        anchors.fill: parent
        onClicked: {
        }
    }

    Rectangle{
        id: otherEquipTitle
        objectName: "OtherEquipTitleObj"
        property string strTitle

        height: iTitleAreaHeight
        width: iTitleAreaWidth
        color: strChkoutAreaColor
        function setFullTestTitle(title) {
            strTitle = title
        }

        Text {
            id: titleText
            width:parent.width
            height:parent.height
            text: qsTrId(otherEquipTitle.strTitle)
            font.pixelSize: iTitleFontSize
            color:strFontColor
            anchors.top:parent.top
            anchors.topMargin: iTitleTextTopMargin
            anchors.left:parent.left
            anchors.leftMargin: iTitleTextLeftMargin
        }
        Rectangle {
            id: equipTestClose
            objectName: "OtherEquipTestClose"
            height: iCloseButtonWidth
            width: iCloseButtonHeight
            color: otherEquipCloseArea.pressed ? "#4565A6" : strActionBtnBgColor
            anchors.top: otherEquipTitle.top
            anchors.topMargin: iCloseButtonTopMargin
            anchors.right: otherEquipTitle.right
            anchors.rightMargin: iCloseButtonRightMargin
            border.color: activeFocus ? strScrollStateBorderColor : strActionBtnBorderColor
            border.width: activeFocus ? iScrollStateBorderWidth : iActionBtnBorderWidth
            radius: iCloseButtonRadius
            Image {
                id: closeBtnIcon
                source: strCloseBtnSource
                anchors.centerIn: parent
            }
            MouseArea {
                id: otherEquipCloseArea
                anchors.fill: equipTestClose

                onPressed: {
                    equipTestClose.forceActiveFocus()
                    equipTestClose.border.width = 0

                }
                onReleased: {
                    equipTestClose.border.width = iActionBtnBorderWidth
                    CheckoutCntrl.destroyCheckoutScreen();
                    var ventGrpReference=getGroupRef("VentGroup")
                    var ventModeObject = getCompRef(ventGrpReference.ventParentId, "ventModeBtn")
                    ventModeObject.focus = true
                }
            }
            Keys.onRightPressed: {
                CheckoutCntrl.moveClockwise(equipTestClose.objectName,otherEquipcheckoutScreen.objectName)
            }

            Keys.onLeftPressed: {
                CheckoutCntrl.moveAntiClockwise(equipTestClose.objectName,otherEquipcheckoutScreen.objectName)
            }

            Keys.onReturnPressed: {
                CheckoutCntrl.destroyPriorCheckScreen()
                CheckoutCntrl.destroyCheckoutScreen();
                var ventGrpReference=getGroupRef("VentGroup")
                var ventModeObject = getCompRef(ventGrpReference.ventParentId, "ventModeBtn")
                ventModeObject.focus = true
            }
        }


    }
    Rectangle{
        id: instructRectangle
        objectName: "InstRecObj"
        property string strSubTitle

        height: iChkoutInstAreaHeight
        width: iChkoutInstAreaWidth
        color: strChkoutAreaColor
        anchors.top: otherEquipTitle.bottom

        function setSubTitle(subTitle) {
            strSubTitle = subTitle
        }
        Text {
            id: subTitleText
            text: qsTrId(instructRectangle.strSubTitle)
            font.pixelSize: iSubTitleTextPixelSize
            color: strFontColor
            anchors.top:otherEquipcheckoutScreen.top
            anchors.topMargin: iSubTitleTextTopMargin
            anchors.left: parent.left
            anchors.leftMargin: iSubTitleTextLeftMargin
        }
        Rectangle{
            id: preCheck
            objectName: "OtherEquipChecklist"
            property string strFullTestFirstInst
            property string strFullTestSecInst
            property string strFullTestThirdInst
            property string strFullTestFourthInst

            color: strPreCheckListRectColor
            anchors.top: subTitleText.bottom
            anchors.topMargin: iPreCheckListTopMargin

            /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setFullTestFirstInst(firstInst) {
                strFullTestFirstInst = firstInst
            }

              /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setFullTestSecInst(secInst) {
                strFullTestSecInst = secInst
            }

            /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setFullTestThirdInst(thirdInst) {
                strFullTestThirdInst = thirdInst
            }

            /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setFullTestFourthInst(fourthInst) {
                strFullTestFourthInst = fourthInst
            }

            /*******************************************
             Purpose:Set focus on confirm button
             Description:Sets the focus to confirm button of bypass checkout Popup
             ***********************************************/
            function setFocusOnConfirmButton() {
                otherEquipConfirmBtn.focus = true
                otherEquipConfirmBtn.forceActiveFocus();
            }

            Label {
                id: firstInst
                objectName: "firstInst"
                text: qsTrId(preCheck.strFullTestFirstInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iFirstPriorCheckInstructionTopMargin
            }
            Label {
                id: secInst

                text: qsTrId(preCheck.strFullTestSecInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: firstInst.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iSecondPriorCheckInstructionTopMargin
            }
            Label {
                id: thirdInst

                text:qsTrId(preCheck.strFullTestThirdInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: secInst.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iThirdPriorCheckInstructionTopMargin
            }
            Label {
                id: fourthInst

                text:qsTrId(preCheck.strFullTestFourthInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: thirdInst.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iFourthPriorCheckInstructionTopMargin
            }
        }
        Rectangle{
            id:otherEquipConfirmBtn
            objectName: "OtherEquipConfirmObj"
            height: iActionBtnHeight
            width:  iActionBtnWidth
            color: confirmBtnMouseArea.pressed ? "#4565A6" : "#2e2e2e"
            border.color: activeFocus ? strScrollStateBorderColor : strActionBtnBorderColor
            border.width: activeFocus ? iScrollStateBorderWidth : iActionBtnBorderWidth
            radius: iActionBtnRadius
            anchors.bottom: instructRectangle.bottom
            anchors.bottomMargin: iConfirmButtonBottomMargin
            anchors.left: instructRectangle.left
            anchors.leftMargin: iConfirmButtonLeftMargin
            z:10
            MouseArea {
                id: confirmBtnMouseArea
                anchors.fill: otherEquipConfirmBtn

                onPressed: {
                    otherEquipConfirmBtn.border.width = 0
                }
                onReleased: {
                    otherEquipConfirmBtn.border.width = 2
                    objCSBValue.writeOnCSB(81,0) //To remove Checkout alarm
                    CheckoutResCntrl.createChkoutResComponent()
                }
            }

            Text {
                id: confirmText
                text: qsTr("Confirm")
                anchors.centerIn: parent
                font.pixelSize: iFontSize
                color: strFontColor
            }

            Keys.onRightPressed: {
                CheckoutCntrl.moveClockwise(otherEquipConfirmBtn.objectName,otherEquipcheckoutScreen.objectName)
            }
            Keys.onLeftPressed: {
                CheckoutCntrl.moveAntiClockwise(otherEquipConfirmBtn.objectName,otherEquipcheckoutScreen.objectName)
            }
            Keys.onReturnPressed: {
                objCSBValue.writeOnCSB(81,0) //To remove Checkout alarm
                CheckoutResCntrl.createChkoutResComponent()
            }
        }
        Rectangle{
            id:skipBtn
            objectName: "OtherEquipSkipObj"
            height: iActionBtnHeight
            width: iActionBtnWidth
            color: skipBtnMouseArea.pressed ? "#4565A6" : strActionBtnBgColor
            border.color: activeFocus ? strScrollStateBorderColor : strActionBtnBorderColor
            border.width: activeFocus ? iScrollStateBorderWidth : iActionBtnBorderWidth
            radius: iActionBtnRadius
            anchors.bottom: instructRectangle.bottom
            anchors.bottomMargin: iSkipButtonBottomMargin
            anchors.left: otherEquipConfirmBtn.right
            anchors.leftMargin: iSkipButtonLeftMargin
            visible: true
            z:5
            Text {
                id: skipText
                text: qsTr("Skip")
                anchors.centerIn: parent
                font.pixelSize: iFontSize
                color: strFontColor

            }
            MouseArea {
                id: skipBtnMouseArea
                anchors.fill: skipBtn
                onPressed: {
                    skipBtn.border.width = 0

                }
                onReleased: {
                    CheckoutResCntrl.createChkoutResComponent()
                    skipBtn.border.width = 2
                }

            }
            Keys.onRightPressed: {
                CheckoutCntrl.moveClockwise(skipBtn.objectName,otherEquipcheckoutScreen.objectName)
            }
            Keys.onLeftPressed: {
                CheckoutCntrl.moveAntiClockwise(skipBtn.objectName,otherEquipcheckoutScreen.objectName)
            }
            Keys.onReturnPressed: {
                CheckoutResCntrl.createChkoutResComponent()
            }
        }

    }

    Component.onCompleted: {
        CheckoutCntrl.fullTestTitle();
        otherEquipConfirmBtn.focus = true
        otherEquipConfirmBtn.forceActiveFocus();
    }

    Component.onDestruction: {
        var buttonGrpRef = getGroupRef("ButtonGroup")
        console.log("***Destruction****",buttonGrpRef.strIconPreviousState)
        if (buttonGrpRef.strIconPreviousState === "Scroll") {
            buttonGrpRef.buttongrpnam.children[0].focus = true
        } else if (buttonGrpRef.strIconPreviousState === "Touched") {
            buttonGrpRef.focus = true
        } else {
            var ventGrpReference=getGroupRef("VentGroup")
            ventGrpReference.focus = true
        }
    }
    Rectangle {
        id : troubleshootTip
        width: iTroubleshootTipWidth //520
        height: iTroubleshootTipHeight //670
        color: strChkoutAreaColor
        border.width: iTroubleshootTipBorderWidth // 1
        border.color: strTroubleshootTipBorderColor//"white"
        anchors.right: otherEquipcheckoutScreen.right
        anchors.rightMargin: iTroubleshootTipRightMargin
        anchors.bottom: otherEquipcheckoutScreen.bottom
        anchors.bottomMargin: iTroubleshootTipBottomMargin
    }
}


