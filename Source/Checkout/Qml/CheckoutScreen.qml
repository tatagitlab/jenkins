import QtQuick 2.9
import QtQuick.Controls 2.2
import "qrc:/Source/Checkout/JavaScript/CheckoutController.js" as CheckoutCntrl
import "qrc:/Source/Checkout/JavaScript/CheckoutResController.js" as CheckoutResCntrl

Rectangle{
    property var instructionRectId: instructionRect
    property var titleRectId: titleRect
    property var skipBtnId: skipBtn
    property var closeBtnId: closeBtn

    property int iActionBtnWidth
    property int iActionBtnHeight
    property int iActionBtnBorderWidth
    property int iActionBtnRadius
    property int iScrollStateBorderWidth
    property int iFontSize
    property int iInstructionTextArea
    property int iTitleFontSize
    property int iChkoutAreaHeight
    property int iChkoutAreaWidth
    property int iTitleAreaHeight
    property int iTitleAreaWidth
    property int iChkoutInstAreaHeight
    property int iChkoutInstAreaWidth
    property int iChkoutPicAreaHeight
    property int iChkoutPicAreaWidth
    property int iCurrentPag
    property int iByPassPopupWidth
    property int iByPassPopupHeight
    property int iPopupColumnTextPixelSize
    property int iPopupColumnTextPixelHeight
    property int iPopupColumnTextPixelLeftMargin
    property int iByPassSubButtonWidth
    property int iByPassSubButtonHeight
    property int iByPassSubButtonTopMargin
    property int iByPassSubButtonLeftMargin
    property int iByPassSubButtonPixelSize
    property int iPriorTestCheckInstructionWidth
    property int iPriorTestCheckInstructionHeight
    property int iPriorTestCheckInstructionLeftMargin
    property int iConfirmButtonLeftMargin
    property int iConfirmButtonBottomMargin
    property int iSkipButtonLeftMargin
    property int iSkipButtonBottomMargin
    property int iSubTitleTextPixelSize
    property int iSubTitleTextTopMargin
    property int iSubTitleTextLeftMargin
    property int iPreCheckListTopMargin
    property int iCloseButtonWidth
    property int iCloseButtonHeight
    property int iCloseButtonRadius
    property int iCloseButtonTopMargin
    property int iCloseButtonRightMargin
    property int iTitleTextTopMargin
    property int iTitleTextLeftMargin
    property int iCurrentPage

    property string strPreCheckListRectColor
    property string strByPassSubButtonColor
    property string strByPassSubButtonBorderColor
    property string strChkoutAreaColor
    property string strFontColor
    property string strPopUpBgColor
    property string strPopUpFontColor
    property string strActionBtnBgColor
    property string strActionBtnBorderColor
    property string strCloseBtnSource
    property string strCloseBtnTchSource
    property string strScrollStateBorderColor

    visible: true
    width: iChkoutAreaWidth
    height: iChkoutAreaHeight
    id: checkoutScreen
    color: strChkoutAreaColor
    z: 3

    MouseArea {
        anchors.fill: parent
        onClicked: {
        }
    }

    Popup{
        id: popup
        width: iByPassPopupWidth
        height: iByPassPopupHeight
        modal: true
        padding: 0
        x: Math.round((parent.width - width) / 2)
        y: Math.round((parent.height - height) / 2)
        closePolicy: Popup.NoAutoClose

        Rectangle {
            width: popup.width
            height: popup.height
            anchors.fill: parent
            color: strPopUpBgColor
        }
        Column {
            id: popupCol
            Text {
                id: popupText1
                text: qsTr("Bypass checkout")
                color: strPopUpFontColor
                font.pixelSize: iPopupColumnTextPixelSize
                height:iPopupColumnTextPixelHeight
                anchors.left: parent.left
                anchors.leftMargin: iPopupColumnTextPixelLeftMargin
            }
            Text {
                id: popupText2
                text: qsTr("Always perform the checkout before using the system on patient.")
                color: strPopUpFontColor
                font.pixelSize: iPopupColumnTextPixelSize
                height:iPopupColumnTextPixelHeight
                anchors.left: parent.left
                anchors.leftMargin: iPopupColumnTextPixelLeftMargin
            }
            Text {
                id: popupText3
                text: qsTr("The Bypass will be logged on the event log.")
                color: strPopUpFontColor
                font.pixelSize: iPopupColumnTextPixelSize
                height: iPopupColumnTextPixelHeight
                anchors.left: parent.left
                anchors.leftMargin: iPopupColumnTextPixelLeftMargin
            }
        }

        Rectangle {
            id: confirmBypassBtn
            width: iByPassSubButtonWidth
            height: iByPassSubButtonHeight
            anchors.top: popupCol.bottom
            anchors.topMargin: iByPassSubButtonTopMargin
            anchors.left: parent.left
            anchors.leftMargin: iByPassSubButtonLeftMargin
            color: strByPassSubButtonColor
            focus: true
            border.color: activeFocus ? strByPassSubButtonBorderColor : strActionBtnBorderColor
            Text {
                text: "Confirm ByPass"
                font.pixelSize: iByPassSubButtonPixelSize
                color: strPopUpFontColor
                anchors.centerIn: parent
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    CheckoutCntrl.fullTestTitle()
                    iCurrentPage = 2;
                    popup.close()
                    objCSBValue.writeOnCSB(80, 15) //User selected bypass button
                }

            }
            Keys.onLeftPressed: {
                cancelBypassBtn.focus = true
                cancelBypassBtn.forceActiveFocus()
            }
            Keys.onRightPressed: {
                cancelBypassBtn.focus = true
                cancelBypassBtn.forceActiveFocus()
            }
            Keys.onReturnPressed: {
                objCSBValue.writeOnCSB(80, 15) //User selected bypass button
                CheckoutCntrl.fullTestTitle()
                iCurrentPage = 2;
                popup.close()
            }

        }
        Rectangle {
            id: cancelBypassBtn
            width: iByPassSubButtonWidth
            height: iByPassSubButtonHeight
            anchors.top: popupCol.bottom
            anchors.topMargin: iByPassSubButtonTopMargin
            anchors.left: confirmBypassBtn.right
            anchors.leftMargin: iByPassSubButtonLeftMargin
            color: strByPassSubButtonColor
            border.color: activeFocus ? strByPassSubButtonBorderColor : strActionBtnBorderColor
            Text {

                text: "Cancel"
                font.pixelSize: iByPassSubButtonPixelSize
                color: strPopUpFontColor
                anchors.centerIn: parent
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    popup.close()
                    confirmBtn.focus = true
                    confirmBtn.forceActiveFocus();
                }
            }
            Keys.onLeftPressed: {
                confirmBypassBtn.focus = true
                confirmBypassBtn.forceActiveFocus()
            }
            Keys.onRightPressed: {
                confirmBypassBtn.focus = true
                confirmBypassBtn.forceActiveFocus()
            }
            Keys.onReturnPressed: {
                popup.close()
                confirmBtn.focus = true
                confirmBtn.forceActiveFocus();
            }
        }
    }
    Rectangle{
        id: titleRect
        objectName: "titleRectObj"
        property string strTitle

        height: iTitleAreaHeight
        width: iTitleAreaWidth
        color: strChkoutAreaColor

        function setTitle(title) {
            console.log("Title is set :" + title)
            strTitle = title
        }
        function setFullTestTitle(title) {
            strTitle = title
        }

        Text {
            id: titleText
            width:parent.width
            height:parent.height
            text: qsTrId(titleRect.strTitle)
            font.pixelSize: iTitleFontSize
            color:strFontColor
            anchors.top:parent.top
            anchors.topMargin: iTitleTextTopMargin
            anchors.left:parent.left
            anchors.leftMargin: iTitleTextLeftMargin
        }
        Rectangle {
            id: closeBtn
            objectName: "CheckoutScreenClose"
            height: iCloseButtonWidth
            width: iCloseButtonHeight
            color: strActionBtnBgColor
            anchors.top: titleRect.top
            anchors.topMargin: iCloseButtonTopMargin
            anchors.right: titleRect.right
            anchors.rightMargin: iCloseButtonRightMargin
            border.color: activeFocus ? strScrollStateBorderColor : strActionBtnBorderColor
            border.width: activeFocus ? iScrollStateBorderWidth : iActionBtnBorderWidth
            radius: iCloseButtonRadius
            Image {
                id: closeBtnIcon
                source: strCloseBtnSource
                anchors.centerIn: parent
            }
            MouseArea {
                id: closeMouseArea
                anchors.fill: closeBtn
                
                onPressed: {
                    console.log("pop up Pressed")

                }
                onReleased: {
                    console.log("pop up Released")
                    popup.open()
                    popup.forceActiveFocus()
                }
            }
            Keys.onRightPressed: {
                CheckoutCntrl.moveClockwise(closeBtn.objectName,iCurrentPage)
            }

            Keys.onLeftPressed: {
                CheckoutCntrl.moveAntiClockwise(closeBtn.objectName,iCurrentPage)
            }

            Keys.onReturnPressed: {
                console.log("pop up Key pressed")
                popup.open()
                popup.forceActiveFocus()
            }
        }


    }
    Rectangle{
        id: instructionRect
        objectName: "InstructRectObj"
        property string strSubTitle

        height: iChkoutInstAreaHeight
        width: iChkoutInstAreaWidth
        color: strChkoutAreaColor
        anchors.top: titleRect.bottom

        function setSubTitle(subTitle) {
            strSubTitle = subTitle
        }

        function setFullTestSubTitle(subTitle) {
            strSubTitle = subTitle
            subTitleText.text = qsTrId(instructionRect.strSubTitle)
        }
        Text {
            id: subTitleText
            text: qsTrId(instructionRect.strSubTitle)
            font.pixelSize: iSubTitleTextPixelSize
            color: strFontColor
            anchors.top:checkoutScreen.top
            anchors.topMargin: iSubTitleTextTopMargin
            anchors.left: parent.left
            anchors.leftMargin: iSubTitleTextLeftMargin
        }
        Rectangle{
            id: preCheck
            objectName: "preChecklist"
            property string strPriorFirstInst
            property string strPriorSecInst
            property string strPriorThirdInst
            property string strPriorFourthInst
            property string strPriorFifthInst
            property string strPriorSixthInst
            property string strFullTestFirstInst
            property string strFullTestSecInst
            property string strFullTestThirdInst
            property string strFullTestFourthInst

            color: strPreCheckListRectColor
            anchors.top: subTitleText.bottom
            anchors.topMargin: iPreCheckListTopMargin

            function setPriorFirstInst(firstInst) {
                strPriorFirstInst = firstInst
            }

            function setPriorSecInst(secInst) {
                strPriorSecInst = secInst
            }

            function setPriorThirdInst(thirdInst) {
                strPriorThirdInst = thirdInst
            }

            function setPriorFourthInst(fourthInst) {
                strPriorFourthInst = fourthInst
            }

            function setPriorFifthInst(fifthInst) {
                strPriorFifthInst = fifthInst
            }

            function setPriorSixthInst(sixthInst) {
                strPriorSixthInst = sixthInst
            }


            function setFullTestFirstInst(firstInst) {
                strFullTestFirstInst = firstInst
                inst1.text = qsTrId(preCheck.strFullTestFirstInst)


            }
            function setFullTestSecInst(secInst) {
                strFullTestSecInst = secInst
                inst2.text = qsTrId(preCheck.strFullTestSecInst)

            }

            function setFullTestThirdInst(thirdInst) {
                strFullTestThirdInst = thirdInst
                inst3.text = qsTrId(preCheck.strFullTestThirdInst)

            }

            function setFullTestFourthInst(fourthInst) {

                strFullTestFourthInst = fourthInst
                inst4.text = qsTrId(preCheck.strFullTestFourthInst)

            }

            /*******************************************
             Purpose:Set focus on confirm button
             Description:Sets the focus to confirm button of bypass checkout Popup
             ***********************************************/
            function setFocusOnConfirmButton() {
                confirmBtn.focus = true
                confirmBtn.forceActiveFocus();
            }

            Label {
                id: inst1
                objectName: "firstInst"
                width: iPriorTestCheckInstructionWidth
                height: iPriorTestCheckInstructionHeight
                text: qsTrId(preCheck.strPriorFirstInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin

            }
            Label {
                id: inst2
                width: iPriorTestCheckInstructionWidth
                height: iPriorTestCheckInstructionHeight
                text: qsTrId(preCheck.strPriorSecInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: inst1.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
            }
            Label {
                id: inst3
                width: iPriorTestCheckInstructionWidth
                height: iPriorTestCheckInstructionHeight
                text:qsTrId(preCheck.strPriorThirdInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: inst2.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
            }
            Label {
                id: inst4
                width: iPriorTestCheckInstructionWidth
                height: iPriorTestCheckInstructionHeight
                text:qsTrId(preCheck.strPriorFourthInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: inst3.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
            }
            Label {
                id: inst5
                width:iPriorTestCheckInstructionWidth
                height: iPriorTestCheckInstructionHeight
                text:qsTrId(preCheck.strPriorFifthInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: inst4.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
            }
            Label {
                id: inst6
                width: iPriorTestCheckInstructionWidth
                height: iPriorTestCheckInstructionHeight
                text:qsTrId(preCheck.strPriorSixthInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: inst5.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
            }
        }
        Rectangle{
            id:confirmBtn
            objectName: "confirmBtnObj"
            height: iActionBtnHeight
            width:  iActionBtnWidth
            color: strActionBtnBgColor
            border.color: activeFocus ? strScrollStateBorderColor : strActionBtnBorderColor
            border.width: activeFocus ? iScrollStateBorderWidth : iActionBtnBorderWidth

            radius: iActionBtnRadius
            anchors.bottom: instructionRect.bottom
            anchors.bottomMargin: iConfirmButtonBottomMargin
            anchors.left: instructionRect.left
            anchors.leftMargin: iConfirmButtonLeftMargin

            Text {
                id: confirmText
                text: qsTr("Confirm")
                anchors.centerIn: parent
                font.pixelSize: iFontSize
                color: strFontColor
            }
            MouseArea {
                id: confirmBtnMouseArea
                anchors.fill: confirmBtn

                onPressed: {
                    console.log("On Confirm button pressed")

                }
                onReleased: {
                    console.log("On Confirm button released")
                    if (iCurrentPage === 1)
                    {
                        console.log("iCurrentPage",iCurrentPage)
                        objCSBValue.writeOnCSB(80,0) //User selected checkout
                        CheckoutCntrl.fullTestTitle()
                        iCurrentPage = 2;
                    }else if (iCurrentPage === 2)
                    {
                        console.log("iCurrentPage",iCurrentPage)
                        objCSBValue.writeOnCSB(81,0) //To remove Checkout alarm
                        CheckoutResCntrl.createChkoutResComponent()
                        iCurrentPage = 3;
                    }else
                    {
                        //Do nothing
                    }
                }
            }
            Keys.onRightPressed: {
                CheckoutCntrl.moveClockwise(confirmBtn.objectName, iCurrentPage)
            }
            Keys.onLeftPressed: {
                CheckoutCntrl.moveAntiClockwise(confirmBtn.objectName, iCurrentPage)
            }
            Keys.onReturnPressed: {
                console.log("On Confirm button released")
                if (iCurrentPage === 1)
                {
                    console.log("iCurrentPage",iCurrentPage)
                    objCSBValue.writeOnCSB(80,0) //User selected checkout
                    CheckoutCntrl.fullTestTitle()
                    iCurrentPage = 2;
                }else if (iCurrentPage === 2)
                {
                    console.log("iCurrentPage",iCurrentPage)
                    objCSBValue.writeOnCSB(81,0) //To remove Checkout alarm
                    CheckoutResCntrl.createChkoutResComponent()
                    iCurrentPage = 3;
                }else
                {
                    //Do nothing
                }
            }
        }
        Rectangle{
            id:skipBtn
            objectName: "skipBtnObj"
            height: iActionBtnHeight
            width: iActionBtnWidth
            color: strActionBtnBgColor
            border.color: activeFocus ? strScrollStateBorderColor : strActionBtnBorderColor
            border.width: activeFocus ? iScrollStateBorderWidth : iActionBtnBorderWidth
            radius: iActionBtnRadius
            anchors.bottom: instructionRect.bottom
            anchors.bottomMargin: iSkipButtonBottomMargin
            anchors.left: confirmBtn.right
            anchors.leftMargin: iSkipButtonLeftMargin
            visible: false
            Text {
                id: skipText
                text: qsTr("Skip")
                anchors.centerIn: parent
                font.pixelSize: iFontSize
                color: strFontColor

            }
            MouseArea {
                id: skipBtnMouseArea
                anchors.fill: skipBtn
                onPressed: {
                    console.log("On Skip button Pressed")

                }
                onReleased: {
                    console.log("On Skip button Released")
                    CheckoutResCntrl.createChkoutResComponent()
                }

            }
            Keys.onRightPressed: {
                CheckoutCntrl.moveClockwise(skipBtn.objectName,iCurrentPage)
            }
            Keys.onLeftPressed: {
                CheckoutCntrl.moveAntiClockwise(skipBtn.objectName,iCurrentPage)
            }
            Keys.onReturnPressed: {
                CheckoutResCntrl.createChkoutResComponent()
            }
        }
    }

    Component.onCompleted: {
        CheckoutCntrl.preCheckTitle();
        iCurrentPage = 1; // Set the current page
        console.log("iCurrentPage",iCurrentPage)
        confirmBtn.focus = true
        confirmBtn.forceActiveFocus();
    }

    Component.onDestruction: {
        console.log("on destruction of screen")
        CheckoutResCntrl.destroyCheckoutResScreen();
        var ventGrpReference=getGroupRef("VentGroup")
        var ventModeObject = getCompRef(ventGrpReference.ventParentId, "ventModeBtn")
        ventModeObject.focus = true
    }
}
