var checkoutResComp;
var checkoutResObj = null;
var chkoutAreaHeight = parseInt(layout.getGroupLayoutDataByTag("Checkout","Dimension","height"));
var chkoutAreaWidth = parseInt(layout.getGroupLayoutDataByTag("Checkout","Dimension","width"));
var individualCheckPassImage = component.getComponentXmlDataByTag("Component","CheckoutComponent","IndividualCheckPassImage")
var individualCheckFailImage = component.getComponentXmlDataByTag("Component","CheckoutComponent","IndividualCheckFailImage")
var individualCheckCondPassImage = component.getComponentXmlDataByTag("Component","CheckoutComponent","IndividualCheckCondPassImage")
var actionBtnBgColor = component.getComponentXmlDataByTag("Component","CheckoutComponent","ActionBtnBgColor")
var actionBtnBorderColor = component.getComponentXmlDataByTag("Component","CheckoutComponent","ActionBtnBorderColor")
var closeBtnBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","CheckoutComponent","ActionBtnBorderWidth"))
var actionBtnRadius = parseInt(component.getComponentXmlDataByTag("GroComponentup","CheckoutComponent","ActionBtnRadius"))
var scrollStateBorderColor = component.getComponentXmlDataByTag("Component","CheckoutComponent","ScrollStateBorderColor")
var chkoutAreaColor = component.getComponentXmlDataByTag("Group","Checkout","ChkAreaColor")
var menuFontSize = parseInt(component.getComponentXmlDataByTag("Group","Checkout","ChkMenuFontSize"))
var statusIconWidth = parseInt(component.getComponentXmlDataByTag("Group","Checkout","ChkStatusIcnWidth"))
var statusIconHeight = parseInt(component.getComponentXmlDataByTag("Group","Checkout","ChkStatusIcnHeight"))
var menuFontColor = component.getComponentXmlDataByTag("Group","Checkout","ChkMenuFontColor")
var instructionTxtFontSize = parseInt(component.getComponentXmlDataByTag("Group","Checkout","ChkInstTextFontSize"))
var instructionTxtFontColor = component.getComponentXmlDataByTag("Group","Checkout","ChkInstTextFontColor")
var closeButtonWidth = component.getComponentXmlDataByTag("Component","CheckoutComponent","CloseButtonWidth")
var closeButtonHeight = component.getComponentXmlDataByTag("Component","CheckoutComponent","CloseButtonHeight")
var closeButtonRadius = component.getComponentXmlDataByTag("Component","CheckoutComponent","CloseButtonRadius")
var closeBtnSource = component.getComponentXmlDataByTag("Component","CheckoutComponent","CloseBtnCheckout")
var closeBtnTchSource = component.getComponentXmlDataByTag("Component","CheckoutComponent","CloseBtnTouchedCheckout")


console.log("individualCheckPassImage" + individualCheckPassImage)
//*******************************************
// Purpose:
// This function is used to create CheckoutResults component
//***********************************************/
function createChkoutResComponent() {
    console.log("individualCheckPassImage",individualCheckPassImage)

    console.log("inside Create component checkout Res")
    checkoutResComp = Qt.createComponent("qrc:Source/Checkout/Qml/CheckoutResults.qml");

    if(checkoutResComp.status === Component.Ready) {
        finishChkoutResComponentCreation();
    }else if(checkoutResComp.status === Component.Error) {
        console.log("Error loading component:", checkoutResComp.errorString());
    }
    else {
        checkoutResComp.statusChanged.connect(finishChkoutResComponentCreation);
    }
}

//*******************************************
// Purpose:
// This function is used to create CheckoutResults object
//***********************************************/
function finishChkoutResComponentCreation() {
    console.log("finishChkoutResComponentCreation")
    checkoutResObj = checkoutResComp.createObject(mainitem,{"width": chkoutAreaWidth,
                                                            "height": chkoutAreaHeight,
                                                            "iCloseBtnHeight":closeButtonHeight,
                                                            "iCloseBtnWidth":closeButtonWidth,
                                                            "strPassIconSource": individualCheckPassImage,
                                                            "strFailIconSource": individualCheckFailImage,
                                                            "strConditionalPassIconSource": individualCheckCondPassImage,
                                                            "strActionBtnBgColor": actionBtnBgColor,
                                                            "strActionBtnBorderColor": actionBtnBorderColor,
                                                            "iActionBtnBorderWidth": closeBtnBorderWidth,
                                                            "iActionBtnRadius": closeButtonRadius,
                                                            "strScrollStateBorderColor": scrollStateBorderColor,
                                                            "strChkoutAreaColor": chkoutAreaColor,
                                                            "iMenuFontSize": menuFontSize,
                                                            "iStatusIconWidth":statusIconWidth,
                                                            "iStatusIconHeight":statusIconHeight,
                                                            "strMenuFontColor":menuFontColor,
                                                            "iInstructionTxtFontSize":instructionTxtFontSize,
                                                            "strInstructionTxtFontColor":instructionTxtFontColor,
                                                            "strCloseBtnSource":closeBtnSource,
                                                            "strCloseBtnTchSource":closeBtnTchSource
                                                  })
}

//*******************************************
// Purpose:
// This function is used to delete CheckoutScreen object
//***********************************************/
function destroyCheckoutScreen() {
    console.log("destroyCheckoutScreen")
    var checkoutScreenObj = getCompRef(mainitem, "CheckoutScreenObj")
    if (checkoutScreenObj !== null && typeof(checkoutScreenObj) !== "undefined") {
        checkoutScreenObj.destroy()
    }
}

//*******************************************
// Purpose:
// This function is used to delete CheckoutResults object
//***********************************************/
function destroyCheckoutResScreen() {
    console.log("destroyCheckoutResScreen")
    var checkoutResScreenObj = getCompRef(mainitem, "CheckoutResScreen")
    if (checkoutResScreenObj !== null && typeof(checkoutResScreenObj) !== "undefined") {
        checkoutResScreenObj.destroy()
    }
    var checkoutScreenObj = getCompRef(mainitem, "OtherEquipListObj")
    var priorCheckObj = getCompRef(mainitem, "CheckoutScreenObj")

    if (checkoutScreenObj !== null && typeof(checkoutScreenObj) !== "undefined") {
        checkoutScreenObj.destroy()
    }
    if (priorCheckObj !== null && typeof(priorCheckObj) !== "undefined") {
        priorCheckObj.destroy()
    }
}

