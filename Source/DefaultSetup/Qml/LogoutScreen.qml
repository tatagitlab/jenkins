import QtQuick 2.0
import QtQuick.Controls 2.2

import "qrc:/../Source/DefaultSetup/JavaScript/LogoutScreenController.js" as LogoutScreenCtrl
import "qrc:/../Source/DefaultSetup/JavaScript/MainDefaultController.js" as DfltCtrl
import "qrc:/../Source/DefaultSetup/JavaScript/PasswordPageController.js" as PwdCtrl

Rectangle {

    id: logoutScreenrect

    objectName: "pwd"
    property int iLogoutPageWidth
    property int iLogoutPageHeight
    property int iPopupHeigth
    property int iPopupWidth
    property int iPopupTextFontSize
    property int iPopupX
    property int iPopupY
    property int iLogoutRectangleHeight
    property int iLogoutRectangleWidth
    property int iButtonHeigth
    property int iButtonWidth
    property int iButtonBorderWidth
    property int iButtonBorderRadius
    property int iButtonTextFontSize
    property string strButtonBorderColor
    property int iButtonTopMargin

    property string strConfirmBtnMouseClickClr: "grey"

    signal creat()

    width: iLogoutPageWidth
    height: iLogoutPageHeight
    visible:false

    onCreat:popup.open();

    Component.onCompleted: {
        creat()
    }
    Popup {
        id: popup
        x: iPopupX
        y: iPopupY
        width: iPopupWidth
        height: iPopupHeigth
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside| Popup.CloseOnReleaseOutside
        Text{
            id:popuptxt
            anchors.horizontalCenter: parent.horizontalCenter
            text:qsTr("Confirm setting changes?")
            font.pixelSize: iPopupTextFontSize
        }
        Rectangle{
            id:confirmButton
            anchors.top:popuptxt.bottom
            anchors.topMargin: iButtonTopMargin
            anchors.horizontalCenter: popup.width/4
            width:iButtonWidth
            height:iButtonHeigth
            border.width:iButtonBorderWidth
            border.color: strButtonBorderColor
            radius:iButtonBorderRadius
            Text{
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text:qsTr("Confirm")
                font.pixelSize: iButtonTextFontSize
            }
            MouseArea{
                anchors.fill: parent
                onClicked:{
                    confirmButton.color=strConfirmBtnMouseClickClr;
                    logoutScreenrect.destroy();
                    DfltCtrl.destroyMainDefault();
                    PwdCtrl.destroyPswdPage();

                }
            }
        }
        Rectangle{
            id:discardButton
            anchors.top:popuptxt.bottom
            anchors.topMargin: iButtonTopMargin
            anchors.left:confirmButton.right
            anchors.leftMargin: 30
            width:iButtonWidth
            height:iButtonHeigth
            border.width:iButtonBorderWidth
            border.color: strButtonBorderColor
            radius:iButtonBorderRadius
            Text{
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text:qsTr("Discard changes")
                font.pixelSize:iButtonTextFontSize
            }
            MouseArea{
                anchors.fill:parent
                onClicked:{

                }
            }
        }
    }
}
