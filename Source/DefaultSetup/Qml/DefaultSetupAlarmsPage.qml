import QtQuick 2.0
import QtQuick.Controls 2.2
import QtMultimedia 5.8
import "qrc:/../Source/DefaultSetup/JavaScript/DefaultAlarmSetupController.js" as AlarmSetupController
import "qrc:/../Source/Generic/JavaScript/AlarmLogMenuController.js" as AlarmLogController
import "qrc:/../Source/ButtonBarArea/Qml"

Rectangle {
    id: defaultAlarmSetupMenu
    property var alarmMenuId: defaultAlarmSetupMenu
    //parent property

    property int iAlarmMenuBorderWidth
    property int iAlarmMenuHeight
    property int iAlarmMenuRadius
    property int iAlarmMenuWidth
    property int iPixelSizeAlarmSetup
    property int iPixelSizeLimitLabel
    property string strAlarmBorderColor
    property string strAlarmMenuBgColor
    property string strAlarmMenuLabel
    property string strAlarmParamScrollStateBorderColor
    property string strFontFamilyAlarmSet: geFont.geFontStyle()
    property string strFontColor: "#787878"
    property string strLimitLabel1
    property string strLimitLabel2
    property string strAlarmTchColor
    property string strAlarmParamPpeak
    property string strAlarmParamPpeakUnit
    property string strAlarmParamMV
    property string strAlarmParamMVUnit
    property string strAlarmParamTVexp
    property string strAlarmParamTVexpUnit
    property string strAlarmParamO2
    property string strAlarmParamO2Unit
    property string strAlarmParamApneaDelay
    property string strAlarmParamApneaDelayUnit
    property string strAlarmParamAlarm
    property string strAlarmParamAlarmUnit
    property int iAlarmSetupParamFontSize
    property string strAlarmSetupParamFontColor
    property var prevValue
    property int iAlarmLabelTopMargin
    property int iAlarmLabelLeftMargin
    property int iDefaultRightMargin
    property int iDefaultUnitTopMargin
    property int iAlarmVolumeLabelTopMargin
    property int iPpeakLabelTopMargin
    property int iMVLabelTopMargin
    property int iTVexpLabelTopMargin
    property int iO2labelTopMargin
    property int iApneaDelayLabelBottomMargin

    property int iApeneaLabelTopMargin

    signal alarmSetupMenuClosed();

    width: parent.width
    height: parent.height
    color: strAlarmMenuBgColor
    visible: true

    MouseArea {
        anchors.fill: parent
        onClicked: {
            defaultAlarmSetupMenu.focus = true;
            defaultAlarmSetupMenu.forceActiveFocus();
        }
    }

    Label {
        id: alarmMenuLabel
        text: qsTrId(strAlarmMenuLabel)
        anchors.top: parent.top
        anchors.topMargin: iAlarmLabelTopMargin
        anchors.left: parent.left
        anchors.leftMargin: iAlarmLabelLeftMargin
        font.family: strFontFamilyAlarmSet
        font.pixelSize: iPixelSizeAlarmSetup
        color: strFontColor
    }

    Label {
        id: lowLimitLabel
        objectName: "lowLimitLabelObj"
        text: qsTrId(strLimitLabel1)

        font.family: strFontFamilyAlarmSet
        font.pixelSize:  iPixelSizeLimitLabel
        color: strFontColor
    }

    Label {
        id: highLimitLabel
        text: qsTrId(strLimitLabel2)
        objectName: "highLimitLabelObj"

        font.family: strFontFamilyAlarmSet
        font.pixelSize: iPixelSizeLimitLabel
        color: strFontColor
    }

    Label {
        id: paramPpeakLabel
        text: qsTrId(strAlarmParamPpeak)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        font.family: strFontFamilyAlarmSet
        anchors.top: parent.top
        anchors.topMargin: iPpeakLabelTopMargin
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramPpeakUnit
        text: qsTrId(strAlarmParamPpeakUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramPpeakLabel.bottom
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: iDefaultUnitTopMargin
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramMVLabel
        text: qsTrId(strAlarmParamMV)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: parent.top
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: iMVLabelTopMargin
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramMVUnit
        text: qsTrId(strAlarmParamMVUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramMVLabel.bottom
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: iDefaultUnitTopMargin
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramTVexpLabel
        text: qsTrId(strAlarmParamTVexp)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: parent.top
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: iTVexpLabelTopMargin
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramTVexpUnit
        text: qsTrId(strAlarmParamTVexpUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.topMargin: iDefaultUnitTopMargin
        font.family: strFontFamilyAlarmSet
        anchors.top: paramTVexpLabel.bottom
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramO2Label
        text: qsTrId(strAlarmParamO2)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: parent.top
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: iO2labelTopMargin
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramO2Unit
        text: qsTrId(strAlarmParamO2Unit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramO2Label.bottom
        anchors.topMargin: iDefaultUnitTopMargin
        font.family: strFontFamilyAlarmSet
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramAlarmLabel
        text: qsTrId(strAlarmParamAlarm)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: parent.top
        anchors.topMargin: iAlarmVolumeLabelTopMargin
        font.family: strFontFamilyAlarmSet
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramAlarmUnit
        text: qsTrId(strAlarmParamAlarmUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramAlarmLabel.bottom
        anchors.topMargin: iDefaultUnitTopMargin
        font.family: strFontFamilyAlarmSet
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramApneaDelayLabel
        text: qsTrId(strAlarmParamApneaDelay)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramO2Unit.bottom
        anchors.topMargin: iApeneaLabelTopMargin
        font.family: strFontFamilyAlarmSet
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }

    Label {
        id: paramApneaDelayUnit
        text: qsTrId(strAlarmParamApneaDelayUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramApneaDelayLabel.bottom
        anchors.bottomMargin: iDefaultUnitTopMargin
        font.family: strFontFamilyAlarmSet
        anchors.right: parent.right
        anchors.rightMargin: iDefaultRightMargin
    }



    Component.onCompleted: {
        AlarmSetupController.createAlarmSetupMenuComponents()
        defaultAlarmSetupMenu.focus=true;
    }
    Keys.onRightPressed: {
        defaultAlarmSetupMenu.children[16].focus = true
        defaultAlarmSetupMenu.children[16].forceActiveFocus();
    }
    Keys.onLeftPressed: {
        defaultAlarmSetupMenu.children[16].focus = true
        defaultAlarmSetupMenu.children[16].forceActiveFocus();
    }
    Component.onDestruction: {
        AlarmLogController.destroyAlarmLogMenu();
    }
}
