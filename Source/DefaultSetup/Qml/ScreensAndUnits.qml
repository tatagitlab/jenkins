import QtQuick 2.0
import QtQuick.Layouts 1.3
import "qrc:/../Source/DefaultSetup/JavaScript/DropdownwithLabelController.js" as LabelDropBox
import "qrc:/../Source/DefaultSetup/JavaScript/ScreensAndUnitsController.js" as ScreensAndUnits

Rectangle {
    id:screenandunits
    property string strFirstDropdownLabel
    property string strSecondDropdownLabel
    property string strThirdDropdownLabel
    property int iScreenLeftMargin
    property string strScreenSubTitleText
    property int iScreenSubTitlePixelSize
    property int iScreenSubTitleTopMargin
    property int iScreenSubTitleLeftMargin
    property int iScreensDropdownWidth
    property int iScreensDropdownHeight
    property int iPawUnitsRectTopMargin
    property int iPawUnitsRectLeftMargin
    property int iPawUnitsObjTopMargin
    property int iPawUnitsObjLeftMargin
    property int iVolumeRectTopMargin
    property int iVolumeRectLeftMargin
    property int iVolumeObjTopMargin
    property int iVolumeObjLeftMargin
    property int iSpiroRectTopMargin
    property int iSpiroRectLeftMargin
    property int iSpiroObjTopMargin
    property int iSpiroObjLeftMargin

    property var arrPawUnits: []
    property var arrVolumeWaveform: []
    property var arrSpiroType: []

    property var pawUnitsRef
    property var volumeRef
    property var spiroRef
    property var pawDropdownRef
    property var volumeDropdownRef
    property var spiroDropdownRef
    property int iScreensColumnSpacing
    property var screenColumnId: screenUnitsColumn

    //*******************************************
    // Purpose: To close the other dropdown when one is selected
    // Description: closes the other dropdown's when one dropdown is selected
    //***********************************************/
    function dropdownTouched(){
        ScreensAndUnits.changeStateIndropdownBtnOnTouch();
    }

    //*******************************************
    // Purpose: set focus to the screens and units page
    // Description: set focus to the screens and units page when dropdown element or
    // dropdown box is selected
    //***********************************************/
    function settingScreenPageFocus()
    {
        screenandunits.focus = true;
        screenandunits.forceActiveFocus();
    }

    width: parent.width
    height: parent.height

    MouseArea {
        anchors.fill: parent
        onClicked: {
           var PawDropdownRef = getCompRef(pawUnitsRef,"combobox");
            var VolumeDropdownRef = getCompRef(volumeRef,"combobox");
            var SpiroDropdownRef = getCompRef(spiroRef,"combobox");
            PawDropdownRef.state = "noDropdown";
            VolumeDropdownRef.state = "noDropdown";
            SpiroDropdownRef.state = "noDropdown";
            screenandunits.focus = true;
            screenandunits.forceActiveFocus();
        }
    }

    Rectangle {
        id:screensubtitle
        width: screensAndUnitsRectTitle.width
        height: screensAndUnitsRectTitle.height
        anchors {
           left: parent.left
           top: parent.top
           topMargin: iScreenSubTitleTopMargin
           leftMargin: iScreenSubTitleLeftMargin
        }

        Text {
            id: screensAndUnitsRectTitle
            text: qsTr(strScreenSubTitleText)
            font.pixelSize: iScreenSubTitlePixelSize
        }

    }

        Column {
            id:screenUnitsColumn
            spacing: iScreensColumnSpacing
            anchors.top: screensubtitle.bottom
            anchors.topMargin: iScreenSubTitleTopMargin
            anchors.left: parent.left
            anchors.leftMargin: iPawUnitsObjLeftMargin

        }

        Component.onCompleted: {
            LabelDropBox.createDropdownwithLabelComponent(arrPawUnits,strFirstDropdownLabel,"screenandunits","PawUnitsObj",30);
            LabelDropBox.createDropdownwithLabelComponent(arrVolumeWaveform,strSecondDropdownLabel,"screenandunits","VolumeWaveformObj",20);
            LabelDropBox.createDropdownwithLabelComponent(arrSpiroType,strThirdDropdownLabel,"screenandunits","SpiroTypeObj",10);
            pawUnitsRef = getCompRef(screenUnitsColumn,"PawUnitsObj");
            volumeRef = getCompRef(screenUnitsColumn,"VolumeWaveformObj");
            spiroRef = getCompRef(screenUnitsColumn,"SpiroTypeObj");
             pawDropdownRef = getCompRef(pawUnitsRef,"combobox");
             volumeDropdownRef = getCompRef(volumeRef,"combobox");
             spiroDropdownRef = getCompRef(spiroRef,"combobox");

            screenandunits.focus = true;
            screenandunits.forceActiveFocus();
        }

        Keys.onRightPressed: {
            if(screenandunits.focus === true) {
                pawDropdownRef.children[0].focus = true;
                pawDropdownRef.children[0].forceActiveFocus = true;

            }
            else if( pawDropdownRef.children[0].focus === true) {
                volumeDropdownRef.children[0].focus = true;
                volumeDropdownRef.children[0].forceActiveFocus = true;
            }
            else if(volumeDropdownRef.children[0].focus === true) {
                spiroDropdownRef.children[0].focus = true;
                spiroDropdownRef.children[0].forceActiveFocus = true;
            }
            else if(spiroDropdownRef.children[0].focus === true) {
                pawDropdownRef.children[0].focus = true;
                pawDropdownRef.children[0].forceActiveFocus = true;
            }
        }

        Keys.onLeftPressed: {
            if(screenandunits.focus === true) {
                pawDropdownRef.children[0].focus = true;
                pawDropdownRef.children[0].forceActiveFocus = true;
            }

            else if(spiroDropdownRef.children[0].focus === true) {
                volumeDropdownRef.children[0].focus = true;
                volumeDropdownRef.children[0].forceActiveFocus = true;
            }
            else if(volumeDropdownRef.children[0].focus === true) {
                pawDropdownRef.children[0].focus = true;
                pawDropdownRef.children[0].forceActiveFocus = true;
            }
            else if( pawDropdownRef.children[0].focus === true) {
                spiroDropdownRef.children[0].focus = true;
                spiroDropdownRef.children[0].forceActiveFocus = true;
            }
        }


}
