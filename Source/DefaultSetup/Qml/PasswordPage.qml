import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.3
import QtQuick.VirtualKeyboard 2.1

import "qrc:/Source/DefaultSetup/Qml/content"
import "../../DefaultSetup/JavaScript/MainDefaultController.js" as DefaultCtrl


Rectangle{
    id: wind
    z:10
    property int iPasswordPageWidth
    property int iPasswordPageHeight
    property int iDefaultSetupTopMargin
    property int iDefaultSetupLeftMargin
    property int iDefaultSetupButtonHeight
    property int iDefaultSetupButtonWidth
    property int iCancelButtonHeight
    property int iCancelButtonWidth
    property int iTitlePixelSize
    property int iTitleMargin
    property int iRectangleBorderWidth
    property int iPwdMaxLength
    property int iPwdMinlength
    property string strRectangleBordercolor
    property string strButtonTextColor

    //*******************************************
    // Purpose:Checks if the entered paswword is correct or not
    // InputParameter: password
    // Description: when the password is entered by the user,
    // this function will check if it is correct or not
    //***********************************************/
    function checkPwd(password)
    {
        var accept=false;
        var result=false;
        var len=password.length
        if(len>iPwdMinlength && len<iPwdMaxLength){
            accept=true;
            if(password==="JARVIS")
                return true;
        }
        return false;
    }

    visible: true
    width: iPasswordPageWidth
    height: iPasswordPageHeight

    Rectangle{
        id:bottomRect
        width:parent.width
        height:parent.height
        border.color:strRectangleBordercolor
        border.width: iRectangleBorderWidth
        anchors.bottom:parent.bottom

        Rectangle {
            width:100
            height:40
            Text{
                text: qsTr("Case default setup")
            font.pixelSize: iTitlePixelSize
            }
            anchors.top:parent.top
            anchors.left:parent.left
            anchors.margins:iTitleMargin
        }
        InputPanel{
            id:keyboardRect
            width:parent.width - 20
            height:parent.height/2
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom:parent.bottom
            anchors.margins: 10
            //color:"lightgrey"
        }
        Label {
            id:pwdTxtFieldLabel
            text: qsTr("Enter password")
            font.pixelSize: iTitlePixelSize
            color: "black"
            anchors.verticalCenter:pwdTxtField.verticalCenter
            anchors.right:pwdTxtField.left
            anchors.margins:iTitleMargin
        }

        TextField {
            id: pwdTxtField
            onTextChanged: pwdIncorrect.visible=false;
            anchors.horizontalCenter: keyboardRect.horizontalCenter
            anchors.bottom: keyboardRect.top
            anchors.bottomMargin: 50
            height: iDefaultSetupButtonHeight
            width: keyboardRect.width/2
            echoMode: TextInput.Password
            focus:true
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhPreferLowercase | Qt.ImhSensitiveData | Qt.ImhNoPredictiveText
            Component.onCompleted: {
                forceActiveFocus();
            }

            onAccepted:{
                var res=checkPwd(text)
                if(res===true){
                    DefaultCtrl.createDefaultSetupComponent();
                    //wind.destroy();
                }
                else
                    pwdIncorrect.visible=true
            }

        }
        Label{
            id:pwdIncorrect
            width:100
            height:20
            color: "red"
            visible: false
            text:qsTr("Password Incorrect")
            anchors.top :pwdTxtField.bottom
            anchors.horizontalCenter:parent.horizontalCenter
        }



        Rectangle{
            id:cancelButton
            height:iCancelButtonHeight
            width:iCancelButtonWidth
            border.color:strRectangleBordercolor
            border.width: iRectangleBorderWidth
            anchors.right: parent.right
            anchors.top:parent.top
            anchors.margins:iTitleMargin
            Text{
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text:qsTr("Cancel")
            }
            MouseArea
            {
                anchors.fill:parent
                onClicked: {
                    wind.destroy();
                    DefaultCtrl.destroyMainDefault();
                }
            }
        }
    }
}

