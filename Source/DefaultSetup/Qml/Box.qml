import QtQuick 2.0

Rectangle{
    id: ventBtnRect
    property alias txtValue: txt.text
    property alias colorOfBox: ventBtnRect.color

    width:90
    height: 50
    color: "darkgray"
    border.color: "black"
    border.width: 0.5

    Text {
        id: txt
        anchors.centerIn: parent
        text: qsTr("Hello!!!")
    }
}
