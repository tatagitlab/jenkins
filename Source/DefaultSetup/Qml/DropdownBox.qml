import QtQuick 2.0

Rectangle {
    id:dropdownBox

    property var dropdownValues;
//    property variant items:arry
    property alias selectedItem: chosenItemText.text;
    property alias selectedIndex: listView.currentIndex;
    property int iDropdownBoxWidth
    property int iDropdownBoxHeight
    property string strDropdownBorderColor
    property int iChosenItemTextmargin
    property string strChosenItemFontFamily: geFont.geFontStyle()
    property int iChosenItemFontSize
    property string strImageSource
    property int iImageHeight
    property int iImageWidth
    property int iImageTopMargin
    property int iDropdownHeight
    property int iDropdownMargin
    property int iDropdownTextMargin
    property int iImageRightMargin

    signal comboClicked;

    //*******************************************
    // Purpose: getting the dropdown values from main default page
    // InputParameter: dropdownvalues
    // Description: gets the dropdown values passed from the Main default page
    //***********************************************/

    function getValue(dropdownvalues)
    {
        dropdownValues = dropdownvalues;
    }

    width: iDropdownBoxWidth;
    height: iDropdownBoxHeight;
    smooth:true;

    Rectangle {
        id:chosenItem
        width:parent.width;
        height:dropdownBox.height;
        border.color: chosenItem.focus?"yellow":strDropdownBorderColor
        smooth:true;
        Text {
            id:chosenItemText
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text:qsTr(dropdownBox.dropdownValues[0]);
            font.family: strChosenItemFontFamily
            font.pointSize: iChosenItemFontSize;
            smooth:true
        }
        Image {
            id: dropdownSymbol
            source: strImageSource
            width: iImageHeight
            height: iImageWidth
            anchors.right: chosenItem.right
            anchors.rightMargin: iImageRightMargin
            anchors.top: chosenItem.top
            anchors.topMargin: iImageTopMargin
        }

        MouseArea {
            anchors.fill: parent;
            onClicked: {
                dropdownBox.state = dropdownBox.state==="dropDown"?"":"dropDown"
                listView.focus = true;
                listView.forceActiveFocus();
            }
        }
        Keys.onReturnPressed: {
            dropdownBox.state = dropdownBox.state==="dropDown"?"":"dropDown"
            listView.focus = true;
            listView.forceActiveFocus();

        }
    }

    Rectangle {
        id:dropDown
        width:dropdownBox.width;
        height:iDropdownHeight;
        clip:true;
        anchors.top: chosenItem.bottom;
        anchors.margins: iDropdownMargin;

        ListView {
            id:listView
            height:iDropdownBoxHeight*dropdownBox.dropdownValues.length;
            model: dropdownBox.dropdownValues
            currentIndex: 0
            highlight:highlight
            delegate: Item{
                width:dropdownBox.width;
                height: dropdownBox.height;

                Text {
                    text: modelData
                    font.family: strChosenItemFontFamily
                    anchors.top: parent.top;
                    anchors.left: parent.left;
                    anchors.margins: iDropdownTextMargin;

                }
                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
                        dropdownBox.state = ""
                        var prevSelection = chosenItemText.text
                        chosenItemText.text = modelData
                        if(chosenItemText.text !== prevSelection){
                            dropdownBox.comboClicked();
                        }
                    }
                }
                Keys.onPressed: {
                    event.accepted = true;
                    if(event.key===Qt.Key_Left){
                        if(listView.currentIndex>0){
                            listView.currentIndex=listView.currentIndex-1
                        }
                        else if(listView.currentIndex===0){
                            listView.currentIndex=dropdownBox.dropdownValues.length-1
                        }
                    }
                    if(event.key===Qt.Key_Right){
                        if(dropdownBox.dropdownValues.length-1>listView.currentIndex){
                            listView.currentIndex=listView.currentIndex+1
                        }
                        else if(dropdownBox.dropdownValues.length-1===listView.currentIndex){
                            listView.currentIndex= 0
                        }
                    }
                    if(event.key===Qt.Key_Return){
                        console.log("ënter pressed");
                        dropdownBox.state = ""
                        var prevSelection = chosenItemText.text
                        chosenItemText.text = modelData
                        if(chosenItemText.text !== prevSelection){
                            dropdownBox.comboClicked();
                        }
                        listView.currentIndex = 0
                        chosenItem.focus = true;
                        chosenItem.forceActiveFocus();
                    }
                }
            }
        }
    }


    Component {
        id: highlight
        Rectangle {
            width:dropdownBox.width;
            height:dropdownBox.height;
            color: "lightgray";
        }
    }

    states: [State {
            name: "dropDown";
            PropertyChanges { target: dropDown; height:iDropdownBoxHeight*dropdownBox.dropdownValues.length }
        },
        State {
            name: "noDropDown";
            PropertyChanges { target: dropDown; height: 0 }
        }]

    transitions: Transition {
        NumberAnimation { target: dropDown; properties: "height"; easing.type: Easing.OutExpo; duration: 1000 }
    }
}

