var alarmVolumeRangeList;
var alarmVolumeMin;
var alarmVolumeMax;
var alarmVolumeStepSize;
alarmVolumeRangeList = physicalparam.getRangeData("Alarm_Volume", "s")
alarmVolumeMin = alarmVolumeRangeList[0]
alarmVolumeMax = alarmVolumeRangeList[1]
alarmVolumeStepSize = alarmVolumeRangeList[2]

/*Increment step Logic for Alarm Volume */
function clkHandleAlarmVolume(m_alarmVol) {
        m_alarmVol += parseInt(alarmVolumeStepSize) ;
        return m_alarmVol;

    }


/*Decrement step Logic for Alarm Volume */
function antClkHandleAlarmVolume(m_alarmVol){
    m_alarmVol -= parseInt(alarmVolumeStepSize) ;
    return m_alarmVol;
}

function onAlarmVolumeIncrementValue(objectName) {
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if(objectName === "AlarmVolumeObj"){
        if(parseInt(currentAlarmSetupComp.strValue) < parseInt(alarmVolumeMax)){
            currentAlarmSetupComp.strValue = parseInt(clkHandleAlarmVolume(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onAlarmVolumeDecrementValue(objectName) {
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if(objectName === "AlarmVolumeObj"){
        if(parseInt(currentAlarmSetupComp.strValue) > parseInt(alarmVolumeMin)){
            currentAlarmSetupComp.strValue = parseInt(antClkHandleAlarmVolume(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onAlarmVolumeEndOfScale(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "AlarmVolumeObj" ))
    {
        if(currentVentComp.strValue === alarmVolumeMin) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon
        } else if(currentVentComp.strValue === alarmVolumeMax) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(currentVentComp.strValue !== alarmVolumeMin && currentVentComp.strValue !== alarmVolumeMax) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetAlarmVolumeUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "AlarmVolumeObj" )){
        if(parseInt(currentVentComp.strValue) === parseInt(alarmVolumeMax) ){
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

function onSetAlarmVolumeDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "AlarmVolumeObj" )){
        if(parseInt(currentVentComp.strValue) === parseInt(alarmVolumeMin) ){
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
