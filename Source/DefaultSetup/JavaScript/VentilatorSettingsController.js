var comp;
var vent;

var subTitleHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","VentSubTitleHeight");
var subTitleTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","VentSubTitleTopMargin");
var subTitleLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","VentSubTitleLeftMargin");
var subTitleText = component.getComponentXmlDataByTag("Component","DefaultSetup","VentSubTitleText");
var subTitleTextSize = component.getComponentXmlDataByTag("Component","DefaultSetup","VentSubTitleTextSize");
var progressBarBtnWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","VentProgressBarBtnWidth");
var progressBarBtnHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","VentProgressBarBtnHeight");
var progressBarPressedColor = component.getComponentXmlDataByTag("Component","DefaultSetup","VentProgressBarPressedColor");
var progressBarColor = component.getComponentXmlDataByTag("Component","DefaultSetup","VentProgressBarColor");
var progressBarBorderColor = component.getComponentXmlDataByTag("Component","DefaultSetup","VentProgressBarBorderColor");
var progressBarBorderWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","VentProgressBarBorderWidth");
var gridViewWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","VentGridViewWidth");
var gridViewheight = component.getComponentXmlDataByTag("Component","DefaultSetup","VentGridViewHeight");
var cellWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","VentGridCellWidth");
var cellHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","VentGridCellHeight");
var headerWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","VentHeaderWidth");
var headerHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","VentHeaderHeight");
var headerColor = component.getComponentXmlDataByTag("Component","DefaultSetup","VentHeaderColor");
var labelViewWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","VentLabelViewWidth");
var labelViewHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","VentLabelViewHeight");
var labelViewYvalue = component.getComponentXmlDataByTag("Component","DefaultSetup","VentLabelViewYvalue");
var labelRectWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","VentLabelRectWidth");
var labelRectHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","VentLabelRectHeight");
var labelRectTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","VentLabelRectTopMargin");
var labelRectRightMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","VentLabelRectRightMargin");
var borderFocusColor = component.getComponentXmlDataByTag("Component","DefaultSetup","VentBorderFocusColor");
var borderColor = component.getComponentXmlDataByTag("Component","DefaultSetup","VentBorderColor");
var spinnerWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","VentSpinnerWidth");
var spinnerHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","VentSpinnerHeight");
var spinnerBorderWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","VentSpinnerBorderWidth");
var downArrowBtnEnabled = component.getComponentXmlDataByTag("Component","DefaultSetup","VentDownArrowBtnEnabled");
var downArrowBtnTouched = component.getComponentXmlDataByTag("Component","DefaultSetup","VentDownArrowBtnTouched");
var downarrowBtnDisabled = component.getComponentXmlDataByTag("Component","DefaultSetup","VentDownArrowBtnDisabled");
var upArrowBtnEnabled = component.getComponentXmlDataByTag("Component","DefaultSetup","VentUpArrowBtnEnabled");
var upArrowBtnTouched = component.getComponentXmlDataByTag("Component","DefaultSetup","VentUpArrowBtnTouched");
var upArrowBtnDisabled = component.getComponentXmlDataByTag("Component","DefaultSetup","VentUpArrowBtnDisabled");

function createVentObjects() {
    comp = Qt.createComponent("qrc:/Source/DefaultSetup/Qml/VentilatorSettings.qml");
    if (comp.status === Component.Ready)
        finishCreation();
    else
        comp.statusChanged.connect(finishCreation);
    return 3;
}

function finishCreation() {
    /*if(vent===null || vent==="undefined")
       {*/
    if (comp.status === Component.Ready) {
        vent = comp.createObject(rightrect, {"x": 0, "y": 0, "z":10,
                                            "iSubTitleHeight":subTitleHeight,
                                            "iSubTitleTopmargin":subTitleTopMargin,
                                            "iSubTitleLeftmargin":subTitleLeftMargin,
                                            "strSubTitleText":subTitleText,
                                            "iSubTitleTextSize":subTitleTextSize,
                                            "iProgressBarBtnWidth":progressBarBtnWidth,
                                            "iProgressBarBtnHeight":progressBarBtnHeight,
                                            "strProgressBarPressedColor":progressBarPressedColor,
                                            "strProgressBarColor":progressBarColor,
                                            "strProgressBarBorderColor":progressBarBorderColor,
                                            "iProgressbarBorderWidth":progressBarBorderWidth,
                                            "iGridViewWidth":gridViewWidth,
                                            "iGridViewHeight":gridViewheight,
                                            "iCellWidth":cellWidth,
                                            "iCellHeight":cellHeight,
                                            "iHeaderWidth":headerWidth,
                                            "iHeaderHeight":headerHeight,
                                            "strHeaderColor":headerColor,
                                            "iLabelViewWidth":labelViewWidth,
                                            "iLabelViewHeight":labelViewHeight,
                                            "iLabelViewYvalue":labelViewYvalue,
                                            "iLabelRectWidth":labelRectWidth,
                                            "ilabelRectHeight":labelRectHeight,
                                            "iLabelRectTopMargin":labelRectTopMargin,
                                            "iLabelRectRightMargin":labelRectRightMargin,
                                            "strBorderFocusColor":borderFocusColor,
                                            "strBorderColor":borderColor,
                                            "iSpinnerWidth":spinnerWidth,
                                            "iSpinnerHeight":spinnerHeight,
                                            "iSpinnerBorderWidth":spinnerBorderWidth,
                                            "strDownArrowBtnEnabled":downArrowBtnEnabled,
                                            "strDownArrowBtnTouched":downArrowBtnTouched,
                                            "strDownArrowBtnDisabled":downarrowBtnDisabled,
                                            "strUpArrowBtnEnabled":upArrowBtnEnabled,
                                            "strUpArrowBtnTouched":upArrowBtnTouched,
                                            "strUpArrowBtnDisabled":upArrowBtnDisabled
                                 });
        console.log("vent Value in creation: ",vent);}

    else if (comp.status === Component.Error) {
        console.log("Error loading component:", comp.errorString());
    }
}
//}


function hideVent(){

    console.log("vent Value in destroy : ",vent);
    vent.destroy();
    //vent=null;
    console.log("vent status(vent Destroyed`) : ",vent.status);

}

function changeOnVentTouch(index){
    var currentComp = getCompRefByIndex(mainGrid, index)
    console.log("index:"+index);
    console.log("currentComp:"+currentComp.state);

}


function destroySpinner() {
    console.log("CAME INSIDE DESTROY SPINNER");
    var destroyMainSpinnerRef =   getCompRef(win,"SpinnerObjParam");
    console.log("Destroy spinner ref ",destroyMainSpinnerRef);
    if (destroyMainSpinnerRef !== null && typeof(destroyMainSpinnerRef) !== "undefined")
    {
        console.log("destroying destroyMainDefaultRef");
        destroyMainSpinnerRef.destroy();
        spinner=null;
    } else {
        console.log("destroyMainSpinner deletion : destroyMainSpinnerRef === null");
    }
}

var max;
var min;
var step;
var val;
var of;

var initial;
function tv_UpHandle(inc)
{
    var tv=parseInt(inc)
    min=20; max=1500;

    if(tv>=20 && tv<100 )
        return 5+tv;
    if(tv>=100 && tv<300 )
        return 10+tv;
    if(tv>=300 && tv<1000 )
        return 25+tv;
    if(tv>=1000 && tv<1500 )
        return 50+tv;

    if(tv>=max || tv<=min)
        return tv;

}
function tv_DownHandle(inc)
{
    var tv=parseInt(inc)

    if(tv>20 && tv<=100 )
        return tv-5;
    if(tv>100 && tv<=300 )
        return tv-10;
    if(tv>300 && tv<=1000 )
        return tv-25;
    if(tv>1000 && tv<=1500 )
        return tv-50;
    if(tv>=max && tv<=min)
        return tv;

    if( tv<=min || tv>=max)
        return tv;
}
function rr_UpHandle(inc,index)
{
    min=4;max=99;
    val=parseInt(inc)
    var spont = inc.split('(');
    console.log("split[0] : ",spont[0]," Split[1] : ",spont[1])
    if(spont[1]===undefined)    {
        if(val>=min && val<max)
            return val+1;
        else
            return val;}
    else
    {
        if(val>=min && val<max)
            return (val+1)+"(spont)";
        else
            return  val+"(spont)";
    }
}
function rr_DownHandle(inc,index)
{
    min=4;max=99;
    val=parseInt(inc)
    var spont = inc.split('(');
    console.log("split[0] : ",spont[0]," Split[1] : ",spont[1])
    if(spont[1]===undefined)    {
        if(val>min && val<=max)
            return val-1;
        else
            return val;}
    else
    {
        if(val>min && val<=max)
            return (val-1)+"(spont)";
        else
            return  val+"(spont)";
    }
}
function ie_UpHandle(inc)
{
    console.log(inc);
    min=1;max=8;step=0.5;
    var split = inc.split(':');
    var l=parseInt(split[0]);
    var r=parseFloat(split[1]);
    if(r>=min && r<max)
     {   if(l===2)
        {l=1;r=0.5;}
            return l+":"+(step+r);}
    else
        return l+":"+r

}
function ie_DownHandle(inc)
{
    console.log(inc);
    min=1;max=8;step=0.5;
    var split = inc.split(':');
    var l=parseInt(split[0]);
    var r=parseFloat(split[1]);
    if(r===min)
    {   console.log("l : ",l);
        return "2"+":"+r }  //  for 2:1 value
    if(r>min && r<=max)
        return l+":"+(r-step);
    else
        return l+":"+r
}
function pinsp_UpHandle(inc)
{
    min=5;max=50;step=1;
    val=parseInt(inc)
    if(val>=min && val<max)
        return step+val;
    else
        return val;
}
function pinsp_DownHandle(inc)
{
    min=5;max=50;step=1;
    val=parseInt(inc)
    if(val>min && val<=max)
        return val-step;
    else
        return val;
}
function pmax_UpHandle(inc)
{
    min=10;max=99;step=1;
    val=parseInt(inc)
    if(val>=min && val<max)
        return step+val;
    else
        return val;
}
function pmax_DownHandle(inc)
{
    min=10;max=99;step=1;
    val=parseInt(inc)
    if(val>min && val<=max)
        return val-step;
    else
        return val;
}
function peep_UpHandle(inc)
{
    console.log(inc);
    min=3;max=25;step=1;
    of="Off";
    if (!(inc.localeCompare(of)))
        val=3
    else val=parseInt(inc);

    if(val>=min && val<max)
        return val+step;
    else
        return val;
}
function peep_DownHandle(inc)
{
    initial="4"
    min=4;max=25;step=1;
    console.log("inc  :",inc)
    of="Off";
    if ((!(inc.localeCompare(of))) || (!(inc.localeCompare(initial))))
    {      val="Off"
        return val;}
    else
    {
        val=parseInt(inc)
        if(val>min && val<=max)
            return val-step;
        else
            return val;}
}
function psupp_UpHandle(inc)
{
    min=2;max=40;step=1;
    val=parseInt(inc)
    if(val>=min && val<max)
        return step+val;
    else
        return val;
}
function psupp_DownHandle(inc)
{
    min=2;max=40;step=1;
    val=parseInt(inc)
    if(val>min && val<=max)
        return val-step;
    else
        return val;
}
function tpause_UpHandle(inc)
{
    initial="5"
    console.log(inc);
    min=5;max=60;step=5;
    of="Off";
    if (!(inc.localeCompare(of)))
        return initial;
    else{ val=parseInt(inc);

        if(val>=min && val<max)
            return val+step;
        else
            return val;}
}
function tpause_DownHandle(inc)
{
    initial="5"
    min=5;max=60;step=5;
    console.log("inc  :",inc)
    of="Off";
    if ((!(inc.localeCompare(of))) || (!(inc.localeCompare(initial))))
    {      val="Off"
        return val;}
    else
    {
        val=parseInt(inc)
        if(val>min && val<=max)
            return val-step;
        else
            return val;}
}
function inspirationtime_UpHandle(inc)
{
    min=0.2;max=5.0;step=0.1;
    val=parseFloat(inc)
    console.log(val)
    if(val>=min && val<max)
    {val=val+step;
        return val.toFixed(1);}
    else
    {
        return val.toFixed(1);}
}
function inspirationtime_DownHandle(inc)
{
    min=0.2;max=5.0;step=0.1;
    val=parseFloat(inc)
    console.log(val)
    if(val>min && val<=max)
    { val=val-step;
        return val.toFixed(1);}
    else
    {
        return val.toFixed(1);}
}
function triggerwindow_UpHandle(inc)
{
    min=5;max=80;step=5;
    val=parseInt(inc)
    if(val>=min && val<max)
        return val+step;
    else
        return val;
}
function triggerwindow_DownHandle(inc)
{
    min=5;max=80;step=5;
    val=parseInt(inc)
    if(val>min && val<=max)
        return val-step;
    else
        return val;
}
function flowtrigger_UpHandle(inc) //test for decimal nos
{

    min=0.2;max=3;step=0.2;
    val=parseFloat(inc)
    console.log("Vlaue in flow trigger : ",val)
    if(val>=min && val<max)
    {val=val+(step);
        return val.toFixed(1);}
    else
    {
        val=val+(step);
        return val.toFixed(1);}
}
function flowtrigger_DownHandle(inc)
{
    min=0.2;max=3;step=0.2;
    val=parseFloat(inc)
    console.log(val)
    if(val>min && val<=max)
    { val=val-(step);
        return val.toFixed(1);}
    else
    {
        val=val+(step);
        return val.toFixed(1);}
}
function endofbreath_UpHandle(inc)
{
    min=30;max=75;step=5;
    val=parseInt(inc)
    if(val>=min && val<max)
        return val+step;
    else
        return val;
}
function endofbreath_DownHandle(inc)
{
    min=30;max=75;step=5;
    val=parseInt(inc)
    if(val>min && val<=max)
        return val-step;
    else
        return val;
}
function backuptime_UpHandle(inc)
{
    min=1;max=30;step=1;
    val=parseInt(inc)
    if(val>=min && val<max)
        return val+step;
    else
        return val;
}
function backuptime_DownHandle(inc)
{
    min=1;max=30;step=1;
    val=parseInt(inc)
    if(val>min && val<=max)
        return val-step;
    else
        return val;
}
function exitbackupspontbreaths_UpHandle(inc)
{
    min=1;max=5;step=1;
    val=parseInt(inc)
    if(val>=min && val<max)
        return val+step;
    else
        return val;
}
function exitbackupspontbreaths_DownHandle(inc)
{
    min=1;max=5;step=1;
    val=parseInt(inc)
    if(val>min && val<=max)
        return val-step;
    else
        return val;
}





