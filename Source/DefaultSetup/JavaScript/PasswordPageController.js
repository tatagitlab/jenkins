var PasswordPageComponent;
var PasswordPageObj;

var passwordPageWidth= parseInt(component.getComponentDimension("PasswordPage","width"));
var passwordPageHeight= parseInt(component.getComponentDimension("PasswordPage","height"));
var defaultSetupTopMargin = component.getComponentXmlDataByTag("Component","PasswordPage","DefaultSetupTopMargin");
var defaultSetupLeftMargin= component.getComponentXmlDataByTag("Component","PasswordPage","DefaultSetupLeftMargin");
var defaultSetupButtonHeight= component.getComponentXmlDataByTag("Component","PasswordPage","DefaultSetupButtonHeight");
var defaultSetupButtonWidth= component.getComponentXmlDataByTag("Component","PasswordPage","DefaultSetupButtonWidth");
var cancelButtonHeight= component.getComponentXmlDataByTag("Component","PasswordPage","CancelButtonHeight");
var cancelButtonWidth= component.getComponentXmlDataByTag("Component","PasswordPage","CancelButtonWidth");
var titlePixelSize= component.getComponentXmlDataByTag("Component","PasswordPage","TitlePixelSize");
var titleMargin= component.getComponentXmlDataByTag("Component","PasswordPage","TitleMargin");
var rectangleBorderWidth= component.getComponentXmlDataByTag("Component","PasswordPage","RectangleBorderWidth");
var pwdMaxLength= component.getComponentXmlDataByTag("Component","PasswordPage","PwdMaxLength");
var pwdMinlength= component.getComponentXmlDataByTag("Component","PasswordPage","PwdMinlength");

var rectangleBordercolor= component.getComponentXmlDataByTag("Component","PasswordPage","RectangleBordercolor");
var buttonTextColor= component.getComponentXmlDataByTag("Component","PasswordPage","ButtonTextColor");



function createPasswordPageComponent() {
    console.log("createPasswordPageComponent....")
    PasswordPageComponent = Qt.createComponent("qrc:/Source/DefaultSetup/Qml/PasswordPage.qml");
    if (PasswordPageComponent.status === Component.Ready)
        finishCreation();
    else
        PasswordPageComponent.statusChanged.connect(finishCreation);
    return 1;
}

function finishCreation() {
    console.log("finishCreation..")
    if (PasswordPageComponent.status === Component.Ready) {
        PasswordPageObj = PasswordPageComponent.createObject(mainitem, {"x": 0, "y": 0,
                                                                 "objectName":"PswdPageParam",
                                                             "iPasswordPageWidth":passwordPageWidth,
                                                             "iPasswordPageHeight":passwordPageHeight,
                                                             "iDefaultSetupTopMargin":defaultSetupTopMargin,
                                                             "iDefaultSetupLeftMargin":defaultSetupLeftMargin,
                                                             "iDefaultSetupButtonHeight":defaultSetupButtonHeight,
                                                             "iDefaultSetupButtonWidth":defaultSetupButtonWidth,
                                                             "iCancelButtonHeight":cancelButtonHeight,
                                                             "iCancelButtonWidth":cancelButtonWidth,
                                                             "iTitlePixelSize":titlePixelSize,
                                                             "iTitleMargin":titleMargin,
                                                             "iRectangleBorderWidth":rectangleBorderWidth,
                                                             "iPwdMaxLength":pwdMaxLength,
                                                             "iPwdMinlength":pwdMinlength,
                                                             "strRectangleBordercolor":rectangleBordercolor,
                                                             "strButtonTextColor":buttonTextColor});
        console.log("CREATED SVENT SETTINGS \n");
        if (PasswordPageObj === null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (PasswordPageComponent.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", PasswordPageComponent.errorString());
    }
}

function hideComponent(){
    PasswordPageObj.destroy();
}
function destroyPswdPage() {
    console.log("CAME INSIDE DESTROY PASSWORDPAGE");
    var PswdPageRef =   getCompRef(mainitem,"PswdPageParam");
    console.log("Destroy spinner ref ",PswdPageRef);
    if (PswdPageRef !== null && typeof(PswdPageRef) !== "undefined")
    {
        console.log("destroying destroyMainDefaultRef");
        PswdPageRef.destroy();
        spinner=null;
    } else {
        console.log("PswdPageRef deletion : PswdPageRef === null");
    }
}
