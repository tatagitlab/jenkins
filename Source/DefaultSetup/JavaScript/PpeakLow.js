var pPeakLowRangeList;
var pPeakLowMin;
var pPeakLowMax;
var pPeakLowStepSize;
pPeakLowRangeList = physicalparam.getRangeData("Ppeak_Low", "cmH2O")
pPeakLowMin = pPeakLowRangeList[0]
pPeakLowMax = pPeakLowRangeList[1]
pPeakLowStepSize = pPeakLowRangeList[2]

/*Increment step Logic for Ppeak Low*/
function clkHandlePpeakLow(m_ppeakLow) {
    var pPeakHighValue = parseInt(settingReader.read("Ppeak_High"))
    var highLimit =  parseInt(pPeakLowMax)
    console.log("m_ppeakLow " + m_ppeakLow)
    if(pPeakHighValue <= highLimit){
        highLimit = pPeakHighValue - parseInt(pPeakLowStepSize)
    }
    if((m_ppeakLow >= parseInt(pPeakLowMin)) && (m_ppeakLow < highLimit)) {
        m_ppeakLow += parseInt(pPeakLowStepSize );
    }
    return m_ppeakLow;
}

/*Decrement step Logic for Ppeak Low*/
function antClkHandlePpeakLow(m_ppeakLow){
    m_ppeakLow -= parseInt(pPeakLowStepSize);
    return m_ppeakLow;
}

function onPpeakLowIncrementValue(objectName) {
    console.log("onPpeakLowIncrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if(objectName === "PpeakLowObj"){
        var pPeakHighValue = parseInt(settingReader.read("Ppeak_High"))
        var highLimit = parseInt(pPeakLowMax)
        if (pPeakHighValue <= highLimit) {
            highLimit = pPeakHighValue - parseInt(pPeakLowStepSize)
        }
        if(parseInt(currentAlarmSetupComp.strValue) < highLimit){
            currentAlarmSetupComp.strValue = parseInt(clkHandlePpeakLow(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onPpeakLowDecrementValue(objectName){
    console.log("onPpeakLowDecrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log(currentAlarmSetupComp+"................")
    if(objectName === "PpeakLowObj"){
        if(parseInt(currentAlarmSetupComp.strValue) > parseInt(pPeakLowMin)){
            currentAlarmSetupComp.strValue =parseInt( antClkHandlePpeakLow(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}


function onPpeakLowEndOfScale(objectName){
    console.log("checking end of scale PpeakLow")
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "PpeakLowObj" ))
    {
        var pPeakHighValue = parseInt(settingReader.read("Ppeak_High"))
        var highLimit = parseInt(pPeakLowMax)
        if (pPeakHighValue <= highLimit) {
            highLimit = pPeakHighValue - parseInt(pPeakLowStepSize)
        }  if(currentVentComp.strValue === pPeakLowMin) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon
        } else if(currentVentComp.strValue === highLimit.toString()) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(currentVentComp.strValue !== pPeakLowMin && currentVentComp.strValue !== highLimit.toString()) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetPpeakLowUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "PpeakLowObj" )){
        var pPeakHighValue = parseInt(settingReader.read("Ppeak_High"))
        var highLimitPpeakLow = parseInt(pPeakLowMax)
        if (pPeakHighValue <= highLimitPpeakLow) {
            highLimitPpeakLow = pPeakHighValue - parseInt(pPeakLowStepSize)
        }
        if(parseInt(currentVentComp.strValue) === highLimitPpeakLow){
            console.log("changing  select Color  of Up arrow...........")
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("changing touch Color  of Up arrow............")
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}


function onSetPpeakLowDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "PpeakLowObj" )){
        if(parseInt(currentVentComp.strValue) === parseInt(pPeakLowMin) ){
            console.log("inside changing select color for down arrow...........")
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("inside changing touch color for down arrow...........")
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }

}
