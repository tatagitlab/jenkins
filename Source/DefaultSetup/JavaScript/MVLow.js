var mVLowRangeList;
var mVLowMin;
var mVLowMax;
var mVLowStepSize;
mVLowRangeList = physicalparam.getRangeData("MV_Low", "l/min")
mVLowMin = mVLowRangeList[0]
mVLowMax = mVLowRangeList[1]
mVLowStepSize = mVLowRangeList[2]

/*Increment step Logic for MV Low*/
function clkHandleMVLow(m_mvLow) {
    var mVHighValue = parseFloat(settingReader.read("MV_High"))
    var highLimit = parseFloat(mVLowMax)
    if(mVHighValue <= highLimit){
        highLimit = mVHighValue - parseFloat(mVLowStepSize)
    }
    if((m_mvLow >= parseFloat(mVLowMin)) && (m_mvLow < highLimit)) {
        m_mvLow =  (parseFloat (m_mvLow + parseFloat(mVLowStepSize))).toFixed(1) ;
    }
    return m_mvLow;
}
/*Decrement step Logic for MV Low*/
function antClkHandleMVLow(m_mvLow){
    m_mvLow = (parseFloat (m_mvLow - parseFloat(mVLowStepSize))).toFixed(1) ;
    return m_mvLow;
}

function onMVLowIncrementValue(objectName) {
    console.log("onMVLowIncrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if(objectName === "MVLowObj"){
        var mVHighValue = parseFloat(settingReader.read("MV_High"))
        var highLimit = parseFloat(mVLowMax)
        if (mVHighValue <= highLimit) {
            highLimit = mVHighValue - parseFloat(mVLowStepSize)
        }
        if(parseFloat(currentAlarmSetupComp.strValue) < highLimit){
            currentAlarmSetupComp.strValue = clkHandleMVLow(parseFloat(currentAlarmSetupComp.strValue))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}
function onMVLowDecrementValue(objectName) {
    console.log("onMVLowDecrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log(currentAlarmSetupComp+"................")
    if(objectName === "MVLowObj"){
        if(parseFloat(currentAlarmSetupComp.strValue) > parseFloat(mVLowMin)){
            currentAlarmSetupComp.strValue = antClkHandleMVLow(parseFloat(currentAlarmSetupComp.strValue))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}
function onMVLowEndOfScale(objectName){
    console.log("checking end of scale MVLow")
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "MVLowObj" ))
    {
        var mVHighValue = parseFloat(settingReader.read("MV_High"))
        var highLimit = parseFloat(mVLowMax)
        if (mVHighValue <= highLimit) {
            highLimit = parseFloat(mVHighValue - parseFloat(mVLowStepSize))
        }
        if(currentVentComp.strValue === mVLowMin) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon
        } else if(parseFloat(currentVentComp.strValue) === parseFloat(highLimit)) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(currentVentComp.strValue !== mVLowMin && parseFloat(currentVentComp.strValue)!== parseFloat(highLimit)) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetMVLowUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log("setting up arrow bg MV"+currentVentComp)
    if((objectName === "MVLowObj" )){
        var mVHighValue = parseFloat(settingReader.read("MV_High"))
        var highLimitMVLow = parseFloat(mVLowMax)
        if (mVHighValue <= highLimitMVLow) {
            highLimitMVLow = parseFloat(mVHighValue - parseFloat(mVLowStepSize))
        }
        if(parseFloat(currentVentComp.strValue) === highLimitMVLow){
            console.log("changing Color  of Up arrow for Select state...........")
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("changing Color  of Up arrow for Touch state..........")
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

function onSetMVLowDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log("setting down arrow bg MV High"+currentVentComp.strValue)
    if((objectName === "MVLowObj" )){
        if(parseFloat(currentVentComp.strValue) === parseFloat(mVLowMin) ){
            console.log("changing select color for down arrow...........")

            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("changing touch color for down arrow...........")

            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
