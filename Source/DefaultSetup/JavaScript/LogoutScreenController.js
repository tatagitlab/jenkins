var logout;
var logoutScreen;

var logoutPageWidth= parseInt(component.getComponentDimension("LogoutPage","width"));
var logoutPageHeight= parseInt(component.getComponentDimension("LogoutPage","height"));

var popupHeigth = component.getComponentXmlDataByTag("Component","LogoutPage","PopupHeigth");
var popupWidth = component.getComponentXmlDataByTag("Component","LogoutPage","PopupWidth");
var popupTextFontSize = component.getComponentXmlDataByTag("Component","LogoutPage","PopupTextFontSize");
var popupX = component.getComponentXmlDataByTag("Component","LogoutPage","PopupX");
var popupY = component.getComponentXmlDataByTag("Component","LogoutPage","PopupY");

//var popupHeigth = component.getComponentXmlDataByTagOne("Component","LogoutPage","Popup","height");
//var popupWidth = component.getComponentXmlDataByTagOne("Component","LogoutPage","Popup","width");
//var popupTextFontSize = component.getComponentXmlDataByTagOne("Component","LogoutPage","Popup","textPixelSize");
//var popupX = component.getComponentXmlDataByTagOne("Component","LogoutPage","Popup","x");
//var popupY = component.getComponentXmlDataByTagOne("Component","LogoutPage","Popup","y");

console.log("popupHeigth :",popupHeigth);
console.log("popupWidth :",popupWidth);
console.log("popupTextFontSize :",popupTextFontSize);
console.log("popupX :",popupX);
console.log("popupY :",popupY);

var logoutRectangleHeight = component.getComponentXmlDataByTag("Component","LogoutPage","LogoutRectangleHeight");
var logoutRectangleWidth = component.getComponentXmlDataByTag("Component","LogoutPage","LogoutRectangleWidth");
var buttonHeigth = component.getComponentXmlDataByTag("Component","LogoutPage","ButtonHeigth");
var buttonWidth = component.getComponentXmlDataByTag("Component","LogoutPage","ButtonWidth");
var buttonBorderWidth = component.getComponentXmlDataByTag("Component","LogoutPage","ButtonBorderWidth");
var buttonBorderRadius = component.getComponentXmlDataByTag("Component","LogoutPage","ButtonBorderRadius");
var buttonTextFontSize = component.getComponentXmlDataByTag("Component","LogoutPage","ButtonTextFontSize");
var buttonTopMargin = component.getComponentXmlDataByTag("Component","LogoutPage","ButtonTopMargin");
var buttonBorderColor = component.getComponentXmlDataByTag("Component","LogoutPage","ButtonBorderColor");

function createLogoutObjects() {
    logout = Qt.createComponent("qrc:Source/DefaultSetup/Qml/LogoutScreen.qml");
    if (logout.status === Component.Ready)
        finishCreation();
    else
        logout.statusChanged.connect(finishCreation);
}

function finishCreation() {
    if (logout.status === Component.Ready) {
        logoutScreen = logout.createObject(rightrect, {"x": 0, "y": 0,
                                                  "iLogoutPageWidth":logoutPageWidth,
                                                  "iLogoutPageHeight":logoutPageHeight,
                                                  "iPopupHeigth":popupHeigth,
                                                  "iPopupWidth":popupWidth,
                                                  "iPopupTextFontSize":popupTextFontSize,
                                                  "iPopupX":popupX,
                                                  "iPopupY":popupY,
                                                  "iLogoutRectangleHeight":logoutRectangleHeight,
                                                  "iLogoutRectangleWidth":logoutRectangleWidth,
                                                  "iButtonHeigth":buttonHeigth,
                                                  "iButtonWidth":buttonWidth,
                                                  "iButtonBorderWidth":buttonBorderWidth,
                                                  "iButtonBorderRadius":buttonBorderRadius,
                                                  "iButtonTextFontSize":buttonTextFontSize,
                                                  "iButtonTopMargin":buttonTopMargin,
                                                  "strButtonBorderColor":buttonBorderColor

                                              });
        console.log("=============Logout Component created==============")
        if (logoutScreen === null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (logout.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", logout.errorString());
    }
}
function hideComponent(){
    logoutScreen.destroy();
}
