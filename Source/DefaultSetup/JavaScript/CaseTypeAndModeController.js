var caseTypeAndModeComponent;
var caseTypeAndModeObj;

var subTitleTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","SubTitleTopMargin")
var subTitleLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","SubTitleLeftMargin")
var subTitleText = component.getComponentXmlDataByTag("Component","DefaultSetup","SubTitleText")
var subTitleFontFamily = component.getComponentXmlDataByTag("Component","DefaultSetup","SubTitleFontFamily")
var subTitlePixelSize = component.getComponentXmlDataByTag("Component","DefaultSetup","SubTitlePixelSize")
var textFieldLabel = component.getComponentXmlDataByTag("Component","DefaultSetup","TextFieldLabel")
var firstDropdownLabel = component.getComponentXmlDataByTag("Component","DefaultSetup","FirstDropdownLabel")
var secondDropdownLabel = component.getComponentXmlDataByTag("Component","DefaultSetup","SecondDropdownLabel")
var leftmargin = component.getComponentXmlDataByTag("Component","DefaultSetup","LabelLeftmargin")
var textFieldLabelTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","TextFieldLabelTopMargin")
var textFieldBorderColor = component.getComponentXmlDataByTag("Component","DefaultSetup","TextFieldBorderColor")
var textFieldWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","TextFieldWidth")
var textFieldHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","TextFieldHeight")
var textFieldLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","TextFieldLeftMargin")
var dropdownWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownRectWidth")
var dropdownHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownRectHeight")
var dropdownRectTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownRectTopMargin")
var dropdownObjTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownObjTopMargin")
var patienttypeLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","PatienttypeLeftMargin")
var ventilatortypeLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","VentilatortypeLeftMargin")
var checkboxLabel = component.getComponentXmlDataByTag("Component","DefaultSetup","CaseCheckboxLabel")
var checkboxLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","CaseCheckboxLeftMargin")
var checkBoxTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","CaseCheckboxTopMargin")
var labelFontSize = component.getComponentXmlDataByTag("Component","DefaultSetup","LabelFontSize")
var patienttypeValue1 = component.getComponentResourceString("Component","DefaultSetup","Label","Patient_Type")
var patienttypeValue2 = component.getComponentResourceString("Component","DefaultSetup","Label","Patient_Type1")
var ventilationModeValue1 = component.getComponentResourceString("Component","DefaultSetup","Label","Ventilation_Mode")
var ventilationModeValue2 = component.getComponentResourceString("Component","DefaultSetup","Label","Ventilation_Mode1")
var ventilationModeValue3 = component.getComponentResourceString("Component","DefaultSetup","Label","Ventilation_Mode2")
var ventilationModeValue4 = component.getComponentResourceString("Component","DefaultSetup","Label","Ventilation_Mode3")
var ventilationModeValue5 = component.getComponentResourceString("Component","DefaultSetup","Label","Ventilation_Mode4")
var textLabelRectWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","TextLabelRectWidth")
var textLabelRectHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","TextLabelRectHeight")
var columnSpacing = component.getComponentXmlDataByTag("Component","DefaultSetup","ColumnSpacing")
var columnZvalue = component.getComponentXmlDataByTag("Component","DefaultSetup","ColumnZvalue")
var textEntryFieldWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","TextEntryFieldWidth")
var textEntryFieldHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","TextEntryFieldHeight")
var focusColor = component.getComponentXmlDataByTag("Component","DefaultSetup","ComboBoxBorderFocusColor")
var checckBoxTextFontSize = component.getComponentXmlDataByTag("Component","DefaultSetup","CheckBoxTextFontSize")
var checkBoxTextLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","CheckBoxTextLeftMargin")
var checkBoxZvalue = component.getComponentXmlDataByTag("Component","DefaultSetup","CheckBoxZvalue")

var patienttypeArray = [patienttypeValue1, patienttypeValue2]
var ventilationModeArray = [ventilationModeValue1, ventilationModeValue2,ventilationModeValue3,ventilationModeValue4,ventilationModeValue5]

//Function for initiating CaseTypeAndMode
//return value is used for Checking constraint on component creation
function createCaseTypeAndModeComponent() {
    caseTypeAndModeComponent = Qt.createComponent("qrc:/Source/DefaultSetup/Qml/CaseTypeAndMode.qml");
    if (caseTypeAndModeComponent.status === Component.Ready)
        finishCreation();
    else
        caseTypeAndModeComponent.statusChanged.connect(finishCreation);
    return 1;
}

// Function for creating CaseTypeAndMode
function finishCreation() {
    if (caseTypeAndModeComponent.status === Component.Ready) {
        caseTypeAndModeObj = caseTypeAndModeComponent.createObject(rightrect, {"iSubTitleTopMargin":subTitleTopMargin,
                                                                       "objectName": "CaseTypeAndModeObj",
                                                                                "iSubTitleLeftMargin":subTitleLeftMargin,
                                                                                "iSubTitleText":subTitleText,
                                                                                "iSubTitlePixelSize":subTitlePixelSize,
                                                                                "iColumnSpacing":columnSpacing,
                                                                                "iColumnZvalue":columnZvalue,
                                                                                "iTextEntryFieldWidth":textEntryFieldWidth,
                                                                                "iTextEntryFieldHeight":textEntryFieldHeight,
                                                                                "strComboBoxBorderFocusColor":focusColor,
                                                                                "iCheckBoxTextFontSize":checckBoxTextFontSize,
                                                                                "iCheckBoxTextLeftMargin":checkBoxTextLeftMargin,
                                                                                "iCheckBoxZvalue":checkBoxZvalue,
                                                                                "iTextFieldLabel":textFieldLabel,
                                                                                "strFirstDropdownLabel":firstDropdownLabel,
                                                                                "strSecondDropdownLabel":secondDropdownLabel,
                                                                                "iLeftmargin":leftmargin,
                                                                                "iTextFieldLabelTopMargin":textFieldLabelTopMargin,
                                                                                "strTextFieldBorderColor":textFieldBorderColor,
                                                                                "iTextFieldWidth":textFieldWidth,
                                                                                "iTextFieldHeight":textFieldHeight,
                                                                                "iTextFieldLeftMargin":textFieldLeftMargin,
                                                                                "iDropdownWidth":dropdownWidth,
                                                                                "iDropdownHeight":dropdownHeight,
                                                                                "iDropdownRectTopMargin":dropdownRectTopMargin,
                                                                                "iDropdownObjTopMargin":dropdownObjTopMargin,
                                                                                "ipatienttypeLeftMargin":patienttypeLeftMargin,
                                                                                "iventilatortypeLeftMargin":ventilatortypeLeftMargin,
                                                                                "strCheckboxLabel":checkboxLabel,
                                                                                "iCheckboxLeftMargin":checkboxLeftMargin,
                                                                                "iCheckBoxTopMargin":checkBoxTopMargin,
                                                                                "iLabelFontSize":labelFontSize,
                                                                                "arrPatientType": patienttypeArray,
                                                                                "arrVentilationType": ventilationModeArray,
                                                                                "iTextLabelRectWidth":textLabelRectWidth,
                                                                                "iTextLabelRectHeight":textLabelRectHeight
                                                                   });

        if (caseTypeAndModeObj === null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (caseTypeAndModeComponent.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", caseTypeAndModeComponent.errorString());
    }
}

// Function for destroying CaseTypeAndMode
function hideComponent(){
    caseTypeAndModeObj.destroy();
}

