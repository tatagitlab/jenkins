Qt.include("qrc:/Source/Generic/JavaScript/MasterController.js")

//*******************************************
// Purpose: Handles setting the focus to the current group's first element when
// right rotated from the previous group's last element
//***********************************************/
function groupChangeOnRight() {
    console.log("ButtonGroupController->>>Right")
    buttonBarGrpId.strIconPreviousState = "Scroll"
    buttongrpnam.children[1].focus = true
}

//*******************************************
// Purpose: Handles setting the focus to the current group's last element when
// left rotated from the previous group's first element
//***********************************************/
function groupChangeOnLeft() {
    buttonBarGrpId.strIconPreviousState = "Scroll"
    console.log("ButtonGroupController->>>Left")
    buttongrpnam.children[0].focus = true
}

//*******************************************
// Purpose: Handles key navigation (right) within the button group
//***********************************************/
function onNavigateRightInButtonGroup() {
    console.log("onNavigateRightInButtonGroup")
    buttonBarGrpId.strIconPreviousState = "Active"
    groupNavigatorOnRight("ButtonGroup","Right")
}

//*******************************************
// Purpose: Handles key navigation (left) within the button group
//***********************************************/
function onNavigateLeftInButtonGroup() {
    console.log("onNavigateLeftInButtonGroup")
    buttonBarGrpId.strIconPreviousState = "Active"
    groupNavigatorOnLeft("ButtonGroup","Left")
}
