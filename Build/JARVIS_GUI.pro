QT += quick xml xmlpatterns core
CONFIG(release, debug|release) {
    QT -= qmltest
    CONFIG -= warn_on qmltestcase
}
#CONFIG(debug) {
#QT += qmltest
#CONFIG += warn_on qmltestcase
#}
#CONFIG += c++11
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../Source/Generic/Cpp/main.cpp \
    ../Source/VentSettingArea/Cpp/settingfileupdate.cpp \
    ../Source/Utilities/Cpp/componentdomreader.cpp \
    ../Source/Utilities/Cpp/layoutdomreader.cpp \
    ../Source/CSB/Cpp/csbcommunication.cpp \
    ../Source/CSB/Cpp/csbreadwrite.c \
    ../Source/VentSettingArea/Cpp/paramdualstorage.cpp \
    ../Source/AlarmArea/Cpp/alarmreader.cpp \
    ../Source/Utilities/Cpp/physicalparamdomreader.cpp \
    ../Source/Utilities/Cpp/fontreader.cpp \
    ../Source/AlarmArea/Cpp/alarmdatamodel.cpp \
    ../Source/VentSettingArea/Cpp/ventconstraintchecker.cpp \
    ../Source/Utilities/Cpp/checkoutreader.cpp \
    ../Source/VentSettingArea/Cpp/ventsettingmismatch.cpp

RESOURCES += \
    qml.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ../Source/VentSettingArea/Include/settingfileupdate.h \
    ../Source/Utilities/Include/componentdomreader.h \
    ../Source/Utilities/Include/layoutdomreader.h \
    ../Source/CSB/Include/csbcommunication.h \
    ../Source/CSB/Include/csbreadwrite.h \
    ../Source/Utilities/Include/consolemessageoutput.h \
    ../Source/VentSettingArea/Include/paramdualstorage.h \
    ../Source/AlarmArea/Include/alarmreader.h \
    ../Source/Utilities/Include/physicalparamdomreader.h \
    ../Source/Utilities/Include/fontreader.h \
    ../Source/VentSettingArea/Include/ventconstraintchecker.h \
    ../Source/AlarmArea/Include/alarmdatamodel.h \
    ../Source/Utilities/Include/checkoutreader.h \
    ../Source/Generic/Include/ventsettings.h \
    ../Source/VentSettingArea/Include/ventsettingmismatch.h

DISTFILES += \
    setting.txt \
    Source/VentSettingArea/JavaScript/ventBtnSteps.js \
    Source/Generic/JavaScript/componentCreation.js \
    Source/Generic/JavaScript/alarmBtnStepLogic.js \
    Config/Assets/reject.wav \
    Config/Assets/modeoff.PNG \
    Config/Assets/modeon.PNG \
    Config/Assets/failurescreen.png \
    Config/Assets/icon_popuptriangle.png \
    Config/Assets/icon_close.png \
    Config/Assets/icon_linkarrow_enabled.png \
    Config/Assets/icon_alarmstep_gray.png \
    Config/Assets/icon_spinner_up_enabled.png \
    Config/Assets/icon_spinner_down_enabled.png \
    Source/Generic/Qml/Group.qml \
    Source/Generic/Qml/main.qml \
    Source/Generic/Qml/AlarmSetupMenu.qml \
    Source/Generic/Qml/AlarmSetupBtn.qml \
    Source/Generic/JavaScript/AlarmSetupController.js\
    Source/Generic/JavaScript/PpeakLow.js\
    Source/Generic/JavaScript/PpeakHigh.js\
    Source/Generic/JavaScript/MVLow.js\
    Source/Generic/JavaScript/MVHigh.js\
    Source/Generic/JavaScript/O2Low.js\
    Source/Generic/JavaScript/O2High.js\
    Source/Generic/JavaScript/TVexpLow.js\
    Source/Generic/JavaScript/TVexpHigh.js\
    Source/Generic/JavaScript/ApneaDelay.js\
    Source/Generic/JavaScript/AlarmVolume.js\ 
    Source/Generic/Qml/ShutDownBanner.qml \
    Source/VentSettingArea/Qml/VentBtn.qml \
    Source/VentSettingArea/Qml/VentModeBtn.qml \
    Source/VentSettingArea/Qml/VentMoreSettingBtn.qml \
    Source/VentSettingArea/Qml/VentMenu.qml \
    ../Source/AlarmArea/Qml/AlarmGroup.qml \
	../Source/AlarmArea/Qml/AlarmMessage.qml \
	../Source/AlarmArea/Qml/AudioPause.qml \
	../Source/AlarmArea/Qml/SystemIcons.qml \
        ../Source/AlarmArea/Qml/AlarmTroubleShoot.qml \
    ../Source/VentSettingArea/JavaScript/VentModeController.js \
    ../Source/VentSettingArea/JavaScript/MoreSettingsController.js \
    ../Source/VentSettingArea/JavaScript/RRMechController.js \
    ../Source/VentSettingArea/JavaScript/ExitBackupController.js \
    ../Source/VentSettingArea/JavaScript/BackupTimeController.js \
    ../Source/Generic/JavaScript/AlarmLogMenuController.js \
    ../UnitTestCases/VentSettingsArea/tst_VentConstraintChecker.qml \
    ../UnitTestCases/VentSettingsArea/tst_AlarmSetupStepsLogic.qml \
    ../UnitTestCases/VentSettingsArea/tst_CompStyleXmlParser.qml \
    ../UnitTestCases/VentSettingsArea/tst_dualStorage.qml \
    ../UnitTestCases/VentSettingsArea/tst_LayoutXmlParser.qml \
    ../UnitTestCases/VentSettingsArea/tst_MoreSettingsParam.qml \
    ../UnitTestCases/VentSettingsArea/tst_MoreSettingsStepLogic.qml \
    ../UnitTestCases/VentSettingsArea/tst_PhysicalParamXmlParser.qml \
    ../UnitTestCases/VentSettingsArea/tst_SettingFileParser.qml \
    ../UnitTestCases/VentSettingsArea/tst_ShortcutKeyPress.qml \
    ../UnitTestCases/VentSettingsArea/tst_ValidateGroupComponent.qml \
    ../UnitTestCases/VentSettingsArea/tst_ValidateMoreSettingsComponent.qml \
    ../UnitTestCases/VentSettingsArea/tst_ValidateMoreSettingsMenu.qml \
    ../UnitTestCases/VentSettingsArea/tst_ValidateVentBtnComponent.qml \
    ../UnitTestCases/VentSettingsArea/tst_VentBtnStepsLogic.qml \
    ../UnitTestCases/VentSettingsArea/tst_VentMismatch.qml \
    ../UnitTestCases/VentSettingsArea/tst_VentModeBtn.qml \
    ../UnitTestCases/VentSettingsArea/tst_VentModeConfirmPopup.qml \
    ../UnitTestCases/VentSettingsArea/tst_VentModeMenu.qml \
    Config/Assets/GEInspiraSans-Regular-v02_0.ttf \
    ../UnitTestCases/VentSettingsArea/tst_ValidateQuickKey.qml \
    ../Source/Generic/Qml/ErrorScreen.qml \
    ../Source/Checkout/JavaScript/CheckoutController.js \
    ../Source/Checkout/Qml/CheckoutResults.qml \
    ../Source/Checkout/JavaScript/CheckoutResController.js \
    ../Source/Checkout/Qml/PriorToTest.qml \
    ../Source/Checkout/Qml/OtherEquipTest.qml \
    ../Source/Checkout/Qml/ByPassCheckout.qml \
    ../Source/Standby/TherapyComponent.qml \
    ../Source/DefaultSetup/Qml/DefaultSetupAlarmsPage.qml \
    ../Source/DefaultSetup/JavaScript/DefaultAlarmSetupController.js \
    ../Source/DefaultSetup/Qml/DefaultAlarmSetupBtn.qml \
    ../Source/DefaultSetup/JavaScript/AlarmVolume.js \
    ../Source/DefaultSetup/JavaScript/LogoutScreenController.js \
    ../Source/DefaultSetup/JavaScript/PasswordPageController.js \
    ../Source/DefaultSetup/Qml/PasswordPage.qml \
    ../Source/DefaultSetup/Qml/LogoutScreen.qml \
    ../Source/DefaultSetup/Qml/content/TextField.qml \
    ../Source/Spirometry/Qml/SpirometryMainPage.qml \
    ../Source/Spirometry/JavaScript/SpiroController.js \
    ../Source/Spirometry/Qml/SpiroDropDown.qml \
    ../Source/Spirometry/JavaScript/SpiroDropdownController.js \
    ../Source/DefaultSetup/JavaScript/VentilatorSettingsController.js

